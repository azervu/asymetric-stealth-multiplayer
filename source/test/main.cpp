/**
 * @file    source/test/main.cpp
 *
 * @brief   Implements the main class for unit tests.
 */

#include <chrono>
#include <thread>
#include <stdexcept>

#include "SDLHeaders.h"
#include "Test.h"
#include "Log.h"

#include "ClientServiceLocator.h"
#include "ServerServiceLocator.h"
#include "ResourceManager.h"
#include "PathTranslator.h"
#include "Protocol.h"
#include "EventManager.h"
#include "Protocol.h"

#ifdef __APPLE__
    #include <signal.h>
#endif


int gArgc;
const char **gArgv;


int main(int argc, char **argv) {
    Log::setLevel(Log::Level::LOG_ALL);
    Log::setLogFile("testlogfile.log");

    if (SDL_Init(SDL_INIT_EVERYTHING) == -1) {
        Log::error("Failed to initialize SDL (%s)", SDL_GetError());
        return 1;
    }

    if (SDLNet_Init() == -1) {
        Log::error("Failed to initialize SDLNet (%s)", SDLNet_GetError());
        return 1;
    }
    
    // Add argument to enable color
    int fakec = argc + 1;
    char **fakev = new char*[fakec];
	for (int i = 0; i < argc; i++) {
		fakev[i] = argv[i];
	}

    fakev[argc] = "--gtest_color=yes";

    gArgc = argc;
    gArgv = (const char**) argv;

    ::testing::InitGoogleTest(&fakec, fakev);
    int ret = RUN_ALL_TESTS();

    delete[] fakev;
    SDL_ClearError();
    SDL_Quit();
    SDLNet_Quit();
    return ret;
}



/**
==================
ServerProcess
==================
*/
int ServerProcess::_instances = 0;

ServerProcess::ServerProcess() 
    :   _udp("localhost", 0, SERVER_INITIALIZED_DEST_UDP_PORT) {
#ifdef WIN32
    ZeroMemory(&_process, sizeof(_process));
#else
    _pid = 0;
#endif

    _instances++;

    if (_instances != 1) {
        Log::warning("Multiple ServerProcess instances exist.");
    }

    if (!_udp.initSocket()) {
        failCurrentTest("Failed to initialize UDP socket");
    }

    if (!_udp.isGood()) {
        failCurrentTest("Bad UDP socket");
    }
}

ServerProcess::~ServerProcess() {
    _instances--;
    killServer();
}


#ifdef WIN32

void ServerProcess::startServer() {
    if (_process.hProcess != NULL) {
        failCurrentTest("Server already running");
        return;
    }
    
    STARTUPINFO si;

    ZeroMemory(&si, sizeof(si));
    ZeroMemory(&_process, sizeof(_process));
    si.cb = sizeof(si);

    if (!CreateProcess(NULL,
                       "server.exe --notif_init --lobby_size=2",
                       NULL,
                       NULL,
                       FALSE,
                       0,
                       NULL,
                       NULL,
                       &si,
                       &_process)) {
        failCurrentTest("Unable to start server process");
        return;
    }

    waitForServerInitialized();
}

bool ServerProcess::killServer() {
    bool ok = true;

    if (_process.hProcess) {
        if (!TerminateProcess(_process.hProcess, 0)) {
            Log::error("Failed to kill server process (%d)", GetLastError());
            ok = false;
        } 

        WaitForSingleObject(_process.hProcess, INFINITE);
        CloseHandle(_process.hProcess);
        CloseHandle(_process.hThread);

        ZeroMemory(&_process, sizeof(_process));
    }

    return ok;
}

#else

void ServerProcess::startServer() {
    if (_pid) {
        failCurrentTest("Server already running");
        return;
    }

    _pid = fork();
    if (_pid == -1) {
        _pid = 0;
        failCurrentTest("Failed to start server process");
        return;
    }

    if (!_pid) {
        execl("./server", "./server", 
              "--notif_init", "--silent", "--lobby_size=2", nullptr);
    }

    waitForServerInitialized();
}

bool ServerProcess::killServer() {
    if (_pid) {
        int pid = _pid;
        _pid = 0;

        // Force kill the server
        if (kill(pid, 9) != 0) {
            Log::error("Failed to kill the server process (%u)", pid);
            return false;
        }

        waitpid(pid, NULL, 0);

        return true;
    }

    return false;
}

#endif

void ServerProcess::waitForServerInitialized() {
    PacketFactory *pf = ServiceLocator::singleton()->getPacketFactory();
    std::chrono::time_point<std::chrono::system_clock> start, now;
    const double timeout = 5;
    std::chrono::duration<double> elapsed;

    start = std::chrono::system_clock::now();
    now = start;

    while (!_udp.hasActivity() && elapsed.count() < timeout) {
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
        now = std::chrono::system_clock::now();
        elapsed = (now - start);
    }

    if (elapsed.count() >= timeout) {
        failCurrentTest("Timed out waiting for 'ServerInitialized'");
    }

    if (!_udp.hasActivity()) {
        failCurrentTest("No activity on UDP socket");
    }

    if (!_udp.parseTrafficData(pf)) {
        failCurrentTest("Unable to parse traffic data");
    }

    Packet *pkt = _udp.getPacket("ServerInitialized");
    if (!pkt) {
        failCurrentTest("Server did not send 'ServerInitialized'");
    }

    delete pkt;
}

void ServerProcess::failCurrentTest(std::string message) {
    killServer();
    throw std::runtime_error("[ServerProcess]: " + message);
}
