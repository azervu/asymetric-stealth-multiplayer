#include "Test.h"
#include "NetworkClient.h"
#include "ClientEvents.h"
#include "ClientServiceLocator.h"
#include "EventManager.h"

#include <chrono>
#include <thread>


class NetworkClientTests : public BaseTestCase {

};


/**
 * NetworkClient that stores events that would otherwise have been fired onto
 * the queue in a member vector.
 */
class NetworkClientEventSaver : public NetworkClient {
public:
    NetworkClientEventSaver(std::string userName)
        :   NetworkClient(userName) { }

    ~NetworkClientEventSaver() {
        for (EventData *ed : _events)
            delete ed;
    }

    std::vector<EventData*> getEvents() {
        return _events;
    }

    // Wait for the NetworkClientEventSaver to fire an event of type 'type'.
    EventData* waitForEvent(EventType type, unsigned timeout) {
        EventData *event = nullptr;
        std::time_t start = std::time(0);
        std::time_t now = std::time(0);

        while (now - start < timeout && !(event=getEvent(type))) {
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
            update();
            now = std::time(0);
        }

        return event;
    }

    EventData* getEvent(EventType type) {
        for (EventData *evt : _events) {
            if (evt->getEventType() == type) {
                return evt;return evt;
            }
        }

        return nullptr;
    }

protected:
    virtual void fireEvent(EventData *event) {
        _events.push_back(event);
    }

    std::vector<EventData*> _events;
};


/**
 * Connect a disconnected NetworkClient to a running server process on
 * localhost.
 */
void connectNetworkClient(NetworkClient &client) {
    ASSERT_EQ(ProtocolState::DISCONNECTED, client.getProtocolState());

    ASSERT_TRUE(client.initiateConnection("localhost"));
    while (client.getProtocolState() == ProtocolState::JOINING) {
        ASSERT_TRUE(client.update());
    }

    ASSERT_EQ(ProtocolState::IN_LOBBY, client.getProtocolState());
}


TEST_F(NetworkClientTests, TestConnection) {
    ServerProcess server;
    ASSERT_NO_THROW(server.startServer());

    NetworkClient client("ThatGuy");
    ASSERT_EQ(ProtocolState::DISCONNECTED, client.getProtocolState());
    ASSERT_TRUE(client.initiateConnection("localhost"));
    ASSERT_EQ(ProtocolState::JOINING, client.getProtocolState());

    while (client.getProtocolState() == ProtocolState::JOINING) {
        ASSERT_TRUE(client.update());
    }

    ASSERT_EQ(ProtocolState::IN_LOBBY, client.getProtocolState());
    ASSERT_TRUE(server.killServer());
}

TEST_F(NetworkClientTests, TestFiredEventsNoServer) {
    /**
     *  When the server turns out to be unreachable, it should fire a 
     *  'ConnectionFailedEvent'.
     */
    EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();
    NetworkClient client("ThisGuy");

    ASSERT_EQ(ProtocolState::DISCONNECTED, client.getProtocolState());
    ASSERT_FALSE(client.initiateConnection("localhost"));

    ConnectionFailedEvent *conFailEvt = nullptr;
    
    const EventQueue &eq = emgr->peekEvents();
    for (EventData *evt : eq) {
        if (evt->getEventType() == ConnectionFailedEvent::eventType) {
            conFailEvt = (ConnectionFailedEvent*)evt;
        }
    }

    ASSERT_NE(nullptr, conFailEvt);
}

TEST_F(NetworkClientTests, TestFiredEventsJoinLobby) {
    ServerProcess server;
    ASSERT_NO_THROW(server.startServer());

    EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();
    NetworkClient client("ThisGuy");

    ASSERT_TRUE(client.initiateConnection("localhost"));

    while (client.getProtocolState() == ProtocolState::JOINING) {
        ASSERT_TRUE(client.update());
    }

    ASSERT_EQ(ProtocolState::IN_LOBBY, client.getProtocolState());

    /**
     * The event queue should now contain ConnectionEstablishedEvent.
     */
    ConnectionEstablishedEvent *conEstEvt = nullptr;

    const EventQueue &eq = emgr->peekEvents();
    for (EventData *evt : eq) {
        if (evt->getEventType() == ConnectionEstablishedEvent::eventType) {
            conEstEvt = (ConnectionEstablishedEvent*)evt;
        }
    }

    ASSERT_NE(nullptr, conEstEvt);

    ASSERT_NE(0, conEstEvt->getPlayerId());
    ASSERT_NE(Team::NONE, conEstEvt->getTeam());

    ASSERT_TRUE(server.killServer());
}

TEST_F(NetworkClientTests, TestTeamAndPlayerIDAssignment) {
    ServerProcess server;
    ASSERT_NO_THROW(server.startServer());

    EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();

    NetworkClient clientA("ClientA");
    NetworkClient clientB("ClientB");

    // Connect the two network clients
    connectNetworkClient(clientA);
    connectNetworkClient(clientB);

    // When two players join a lobby, they should not be assigned on the same
    // team. Their teams must also be defined to either SPY or MERC.
    ASSERT_NE(Team::NONE, clientA.getTeam());
    ASSERT_NE(Team::NONE, clientB.getTeam());
    ASSERT_NE(clientA.getTeam(), clientB.getTeam());

    // The player IDs of the two clients must be unique
    ASSERT_NE(clientA.getPlayerId(), clientB.getPlayerId());
}

TEST_F(NetworkClientTests, TestNewClientPacketSent) {
    ServerProcess server;
    ASSERT_NO_THROW(server.startServer());
    EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();

    NetworkClientEventSaver clientA("ClientA");
    NetworkClientEventSaver clientB("ClientB");

    // Connect clientA and copy the event queue
    connectNetworkClient(clientA);

    connectNetworkClient(clientB);

    // Assert that clientB was notified of clientA's existance.
    ClientJoinedSessionEvent *cjEvt = nullptr;
    cjEvt = (ClientJoinedSessionEvent*)clientB.waitForEvent(
                                       ClientJoinedSessionEvent::eventType, 5);

    ASSERT_NE(nullptr, cjEvt);
    EXPECT_EQ("ClientA", cjEvt->getClientName());
    EXPECT_EQ(clientA.getPlayerId(), cjEvt->getPlayerId());
    EXPECT_EQ(clientA.getTeam(), cjEvt->getTeam());

    // Assert that clientA was notified of clientB's joining
    cjEvt = (ClientJoinedSessionEvent*)clientA.waitForEvent(
                                       ClientJoinedSessionEvent::eventType, 5);

    ASSERT_NE(nullptr, cjEvt);
    EXPECT_EQ("ClientB", cjEvt->getClientName());
    EXPECT_EQ(clientB.getPlayerId(), cjEvt->getPlayerId());
    EXPECT_EQ(clientB.getTeam(), cjEvt->getTeam());
}
