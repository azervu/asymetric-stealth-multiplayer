#include "Test.h"
#include "Socket.h"
#include "Protocol.h"
#include "PathTranslator.h"
#include "ConnectionListener.h"
#include <ctime>
#include <thread>

const Uint16 SERVER_PORT = 59133;
const Uint16 CLIENT_PORT = 59134;


/**
 * Wait for activity on a socket with a timeout period.
 * @param sock  The socket to check for activity on
 * @param time  Timeout limit in seconds 
 * @return      True if the socket had activiy within the defined period, 
 *              false otherwise.
 */
bool waitForActivity(SocketBase *sock, std::time_t time) {
    std::time_t start = std::time(0);

    while (!sock->hasActivity() && (std::time(0) - start) <= time) {
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }

    return sock->hasActivity();
}

bool waitForConnection(server::ConnectionListener *connList, std::time_t time) {
    std::time_t start = std::time(0);

    while (!connList->hasNewConnection() && (std::time(0) - start) <= time) {
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }

    return connList->hasNewConnection();
}


class SocketTests : public BaseTestCase {
protected:
    virtual void SetUp() {
        BaseTestCase::SetUp();

        _pathTranslator = new PathTranslator(this->_settings);
        ASSERT_TRUE(_pathTranslator->addAssetDirectory("testassets"));

        _packetFactory = new PacketFactory(_pathTranslator);
        ASSERT_TRUE(_packetFactory->addTemplatesFromFile("testpackets.json"));
    }

    virtual void TearDown() {
        BaseTestCase::TearDown();

        delete _packetFactory;
        delete _pathTranslator;
    }


    PathTranslator *_pathTranslator;
    PacketFactory *_packetFactory;
};


TEST_F(SocketTests, InvalidSocketInitialization) {
    // Connecting to a port noone is listening on should not work
    SocketTCP socket("localhost", SERVER_PORT);
    EXPECT_TRUE(socket.validIPAddress());
    ASSERT_FALSE(socket.initSocket());

    socket = SocketTCP("mumbo fkn jumbo", 0);
    EXPECT_FALSE(socket.validIPAddress());
    ASSERT_FALSE(socket.initSocket());

    // A server socket with a randomly assigned port is considered invalid
    socket = SocketTCP(0, 0);
    EXPECT_FALSE(socket.validIPAddress());
    ASSERT_FALSE(socket.initSocket());
}

TEST_F(SocketTests, ValidSocketInitialization) {
    SocketTCP server(0, SERVER_PORT);
    EXPECT_TRUE(server.validIPAddress());
    ASSERT_TRUE(server.initSocket());

    SocketTCP client("localhost", SERVER_PORT);
    EXPECT_TRUE(client.validIPAddress());
    ASSERT_TRUE(client.initSocket());
}

TEST_F(SocketTests, TestSimplePacketSending) {
    SocketUDP server("localhost", CLIENT_PORT, SERVER_PORT);
    ASSERT_TRUE(server.initSocket());

    SocketUDP client("localhost", SERVER_PORT, CLIENT_PORT);
    ASSERT_TRUE(client.initSocket());

    ASSERT_TRUE(client.parseTrafficData(_packetFactory));
    ASSERT_TRUE(client.isGood());
    ASSERT_TRUE(server.parseTrafficData(_packetFactory));
    ASSERT_TRUE(server.isGood());
    
    // Create a packet and set some values
    Packet *sent = _packetFactory->createPacketFromTemplate("TestPacket");
    ASSERT_TRUE(sent != NULL);

    const unsigned val1    = 15;
    const int val2         = -99;
    const byte val3        = 242;
    const float val4       = 3.14f;
    const std::string val5 = "This is my string. There are many like it, but "
                             "this one is mine.";

    sent->setUnsigned("val1", val1);
    sent->setInt("val2", val2);
    sent->setByte("val3", val3);
    sent->setFloat("val4", val4);
    sent->setString("val5", val5);

    ASSERT_TRUE(client.sendPacket(sent));

    // Wait for the packet to arrive
    ASSERT_TRUE(waitForActivity(&server, 5));
    ASSERT_TRUE(server.parseTrafficData(_packetFactory));
    ASSERT_TRUE(server.isGood());

    Packet *recv = server.popPacket();
    ASSERT_TRUE(recv != NULL);

    EXPECT_EQ(recv->getName(), sent->getName());
    EXPECT_EQ(recv->getInt("id"), sent->getInt("id"));
    EXPECT_EQ(recv->getUnsigned("val1"), val1);
    EXPECT_EQ(recv->getInt("val2"), val2);
    EXPECT_EQ(recv->getByte("val3"), val3);
    EXPECT_EQ(recv->getFloat("val4"), val4);
    EXPECT_EQ(recv->getString("val5"), val5);

    ASSERT_TRUE(client.isGood());
    ASSERT_TRUE(server.isGood());

    delete recv;
    delete sent;
}

TEST_F(SocketTests, ConnectionListener) {
    server::ConnectionListener connList;

    // Sleep for a full second. The ConnectionListener is using a background
    // thread to listen for new connections, and it must be ready before we
    // can attempt to connect to it.
    std::this_thread::sleep_for(std::chrono::seconds(1));
    
    SocketTCP client("localhost", TCP_SERVER_PORT);
    ASSERT_TRUE(client.initSocket());
    ASSERT_TRUE(waitForConnection(&connList, 5));

    // Pop the new socket
    SocketTCP *server = connList.popNewConnection();
    ASSERT_TRUE(server != NULL);

    ASSERT_TRUE(client.isGood());
    ASSERT_TRUE(server->isGood());

    // Attempt to send a packet from the client to the server
    Packet *sent = _packetFactory->createPacketFromTemplate("TestPacket");

    const unsigned val1    = 15;
    const int val2         = -99;
    const byte val3        = 242;
    const float val4       = 3.14f;
    const std::string val5 = "This is my string. There are many like it, but "
                             "this one is mine.";

    sent->setUnsigned("val1", val1);
    sent->setInt("val2", val2);
    sent->setByte("val3", val3);
    sent->setFloat("val4", val4);
    sent->setString("val5", val5);

    ASSERT_TRUE(client.sendPacket(sent));
    ASSERT_TRUE(waitForActivity(server, 5));
    ASSERT_TRUE(server->parseTrafficData(_packetFactory));
    ASSERT_TRUE(server->isGood());

    Packet *recv = server->popPacket();

    EXPECT_EQ(sent->getName(), recv->getName());
    EXPECT_EQ(sent->getInt("id"),recv->getInt("id"));
    EXPECT_EQ(val1, recv->getUnsigned("val1"));
    EXPECT_EQ(val2, recv->getInt("val2"));
    EXPECT_EQ(val3, recv->getByte("val3"));
    EXPECT_EQ(val4, recv->getFloat("val4"));
    EXPECT_EQ(val5, recv->getString("val5"));

    ASSERT_TRUE(client.isGood());
    ASSERT_TRUE(server->isGood());

    delete recv;
    delete sent;
    delete server;
}
