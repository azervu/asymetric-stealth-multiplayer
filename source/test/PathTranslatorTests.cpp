#include "Test.h"
#include "PathTranslator.h"
#include <stdio.h>


#define REAL_ASSETS_DIR     "assets"
#define TEST_F_ASSETS_DIR   "testassets"
#define BOGO_ASSETS_DIR     "thisismydirtherearemanylikeitbutthisoneismine"
#define TEST_F_FILE         "testfile"
#define TFILE_CONTENTS      "testfile is bestfile"

/**
 * The tests of the path translator is not complete. None of the tests 
 * verify that the translated file-names are correct, because doing so would
 * require a path translator... Instead, the translated paths are attempted 
 * opened, and it is assumed that if the files can be opened, the name was 
 * translated properly. 
 */

class PathTranslatorTests : public BaseTestCase {

};


TEST_F(PathTranslatorTests, testInvalidAssetDirectory) {
    PathTranslator pt = PathTranslator(this->_settings);
    
    // We can probably assume that this directory does not exist 
    // anywhere in the file system.
    ASSERT_FALSE(pt.addAssetDirectory(BOGO_ASSETS_DIR));
    ASSERT_TRUE(pt.translatePath(TEST_F_FILE) == "");
}

TEST_F(PathTranslatorTests, testTranslateFile) {
    PathTranslator pt = PathTranslator(this->_settings);

    ASSERT_TRUE(pt.addAssetDirectory(TEST_F_ASSETS_DIR));
    ASSERT_TRUE(pt.addAssetDirectory(REAL_ASSETS_DIR));
    ASSERT_FALSE(pt.addAssetDirectory(BOGO_ASSETS_DIR));

    std::string original = TEST_F_FILE;
    std::string translated = pt.translatePath(original);

    ASSERT_TRUE(translated.length() != 0);
    ASSERT_TRUE(translated.find(TEST_F_ASSETS_DIR) != std::string::npos);
    ASSERT_TRUE(original != translated);

    // Attempt to open the file. If it cannot be opened, the path was 
    // translated wrong.
    std::ifstream file(translated);
    ASSERT_TRUE(file.good());
    file.close();
}

TEST_F(PathTranslatorTests, testOpenHandle) {
    PathTranslator pt = PathTranslator(this->_settings);
    pt.addAssetDirectory(TEST_F_ASSETS_DIR);

    std::ifstream file;
    ASSERT_TRUE(pt.openHandle(TEST_F_FILE, &file));
    ASSERT_TRUE(file.good());

    // Attempt to read from it. The contents of the file shoud be
    // "testfile is bestfile".
    char buf[64];
    file.getline(buf, 64);
    std::string str = buf;

    ASSERT_TRUE(str == TFILE_CONTENTS);
    file.close();
}

TEST_F(PathTranslatorTests, testOpenCHandle) {
    PathTranslator pt = PathTranslator(this->_settings);
    pt.addAssetDirectory(TEST_F_ASSETS_DIR);

    FILE *file = pt.openCHandle(TEST_F_FILE);
    ASSERT_TRUE(file != NULL);

    char words[3][12];
    ASSERT_TRUE(fscanf(file, "%s %s %s", words[0], words[1], words[2]) != EOF);
    ASSERT_TRUE(!strcmp(words[0], "testfile"));
    ASSERT_TRUE(!strcmp(words[1], "is"));
    ASSERT_TRUE(!strcmp(words[2], "bestfile"));

    fclose(file);
}
