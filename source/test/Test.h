/**
 * @brief   Core header in the test suite. Declares common types and functions
 *          often used when testing. Functions and classes declared here are
 *          defined in "test/main.cpp".
 */

#include <gtest/gtest.h>

#ifdef WIN32
    #include <Windows.h>
#else 
    #include <unistd.h>
    #include <sys/types.h>
    #include <sys/wait.h>
#endif

#include "BaseTestCase.h"
#include "Socket.h"


/**
 * 'argc' and 'argv' are assigned to the following variables from
 * main(). This allows them to be accessed from test cases.
 */
extern int gArgc;
extern const char **gArgv;


class ServerProcess {
public:
    ServerProcess();
    ~ServerProcess();

    /**
     * Start a server process. The server is started with the '--notif-init' 
     * flag, which causes the server to send a ServerInitialized packet on
     * "localhost:SERVER_INITIALIZED_DEST_UDP_PORT". If this packet is not 
     * retrieved within 5 seconds, binding the UDP socket or starting the server
     * process fails, the currently running test will automatically fail.
     */ 
    void startServer();

    /**
     * If a server instance has been started with startServer(), a call to this 
     * method will terminate the process. Use this method for manual control of
     * the process, it is automatically called in the destructor.
     */
    bool killServer();

private:
    static int _instances;

    /**
     * Wait for the server process to send the ServerInitialized packet, fail
     * the current unittest if it takes too long or something else fails.
     */
    void waitForServerInitialized();

    /**
     * Kill the server and throw an exception. GoogleTest fail-macros are 
     * unavailable outside of test cases.
     */
    void failCurrentTest(std::string message);


    SocketUDP _udp;

#ifdef WIN32
    PROCESS_INFORMATION _process;
#else 
    int _pid;
#endif
};

