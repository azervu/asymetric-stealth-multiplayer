#include "BaseTestCase.h"
#include "Engine.h"
#include "Triangulator.h"

#include <algorithm>


class TriangulatorTests : public BaseTestCase {};


TEST_F(TriangulatorTests, TestSquare) {
    // Test the most simple of shapes.
    std::vector<Vec2> verts;

    verts.push_back(Vec2(0.f, 10.f));
    verts.push_back(Vec2(10.f, 10.f));
    verts.push_back(Vec2(10.f, 0.f));
    verts.push_back(Vec2(0.f, 0.f));

    Triangulator tri(verts);
    auto triangles = tri.triangulateShape();

    ASSERT_EQ(2, triangles.size());
}

TEST_F(TriangulatorTests, TestCrescentSquare) {
    // Test a slightly more complex convex shape
    std::vector<Vec2> verts;

    verts.push_back(Vec2(0.f, 10.f));
    verts.push_back(Vec2(10.f, 10.f));
    verts.push_back(Vec2(5.f, 5.f));
    verts.push_back(Vec2(10.f, 0.f));
    verts.push_back(Vec2(0.f, 0.f));

    Triangulator tri(verts);
    auto triangles = tri.triangulateShape();

    ASSERT_EQ(3, triangles.size());
}
