#include "Test.h"
#include "ResourceManager.h"
#include "PathTranslator.h"

#define IMAGE_FILE "testimg.png"


class ResourceManagerTests : public BaseTestCase {
public:
    void SetUp() {
        BaseTestCase::SetUp();

        ClientServiceLocator::singleton()->getPathTranslator()->addAssetDirectory("testassets");
    }

};


TEST_F(ResourceManagerTests, TestLoadTexture) {
    ResourceManager mgr;

    Resource *res = mgr.getLoadResource(ResourceType::TEXTURE, IMAGE_FILE);
    ASSERT_TRUE(res != nullptr);
    ASSERT_EQ(1, res->getRetainCount());
    ASSERT_TRUE(mgr.isResourceLoaded(IMAGE_FILE));

    delete res;
}

TEST_F(ResourceManagerTests, TestDestroyReleasedResources) {
    ResourceManager mgr;

    Resource *res = mgr.getLoadResource(ResourceType::TEXTURE, IMAGE_FILE);
    ASSERT_TRUE(res != NULL);
    ASSERT_EQ(1, res->getRetainCount());

    // Delete the resource - the texture should now be released, but it won't 
    // be deleted until we explicitly tell the resource manager to.
    delete res;

    ASSERT_TRUE(mgr.isResourceLoaded(IMAGE_FILE));
    mgr.destroyReleasedResources();
    ASSERT_FALSE(mgr.isResourceLoaded(IMAGE_FILE));
}

TEST_F(ResourceManagerTests, TestSharedResources) {
    ResourceManager mgr;

    Resource *res1 = mgr.getLoadResource(ResourceType::TEXTURE, IMAGE_FILE);
    ASSERT_TRUE(res1 != NULL);
    ASSERT_EQ(1, res1->getRetainCount());

    Resource *res2 = mgr.getLoadResource(ResourceType::TEXTURE, IMAGE_FILE);
    ASSERT_TRUE(res2 != NULL);
    ASSERT_EQ(2, res2->getRetainCount());
    ASSERT_EQ(2, res1->getRetainCount());
    ASSERT_NE(res1, res2);

    // Delete res2 - res1 should still be alive and kicking
    delete res2;
    ASSERT_EQ(1, res1->getRetainCount());
    ASSERT_TRUE(mgr.isResourceLoaded(IMAGE_FILE));

    // The existing texture should not be deleted as a result, as there still
    // exists one reference to it.
    mgr.destroyReleasedResources();
    ASSERT_EQ(1, res1->getRetainCount());
    ASSERT_TRUE(mgr.isResourceLoaded(IMAGE_FILE));

    delete res1;
    mgr.destroyReleasedResources();
    ASSERT_FALSE(mgr.isResourceLoaded(IMAGE_FILE));
}
