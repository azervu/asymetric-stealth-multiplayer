#include "Test.h"
#include "SDLHeaders.h"
#include "InputState.h"


class InputStateTests : public BaseTestCase {

};


TEST_F(InputStateTests, TestKeyboard) {
    SDL_Keycode key = SDLK_a;

    InputState inputState;

    // In the initial state, no keys should be pressed
    ASSERT_FALSE(inputState.isKeyDown(key));
    ASSERT_FALSE(inputState.isKeyFresh(key));

    ASSERT_TRUE(inputState.keyPressed(key));
    ASSERT_FALSE(inputState.keyPressed(key));

    // The key was recently pressed, so it should also be fresh
    ASSERT_TRUE(inputState.isKeyDown(key));
    ASSERT_TRUE(inputState.isKeyFresh(key));

    // Clearing the fresh flags should only affect the fresh-state
    inputState.clearFreshFlags();
    ASSERT_TRUE(inputState.isKeyDown(key));
    ASSERT_FALSE(inputState.isKeyFresh(key));

    // Release the key - it should no longer be pressed or fresh
    ASSERT_TRUE(inputState.keyReleased(key));
    ASSERT_FALSE(inputState.keyReleased(key));

    ASSERT_FALSE(inputState.isKeyDown(key));
    ASSERT_FALSE(inputState.isKeyFresh(key));
}

TEST_F(InputStateTests, TestButtons) {
    Uint8 button = SDL_BUTTON_LEFT;

    InputState inputState;

    ASSERT_FALSE(inputState.isButtonDown(button));
    ASSERT_FALSE(inputState.isButtonFresh(button));

    ASSERT_TRUE(inputState.buttonPressed(button));
    ASSERT_FALSE(inputState.buttonPressed(button));

    ASSERT_TRUE(inputState.isButtonDown(button));
    ASSERT_TRUE(inputState.isButtonFresh(button));

    inputState.clearFreshFlags();
    ASSERT_TRUE(inputState.isButtonDown(button));
    ASSERT_FALSE(inputState.isButtonFresh(button));

    ASSERT_TRUE(inputState.buttonReleased(button));
    ASSERT_FALSE(inputState.buttonReleased(button));

    ASSERT_FALSE(inputState.isButtonDown(button));
    ASSERT_FALSE(inputState.isButtonFresh(button));
}

TEST_F(InputStateTests, TestButtonAndKeyDisjunction) {
    InputState inputState;

    // Flag all imaginable keys as pressed (9 bit)
    for (SDL_Keycode k = 0; k < 512; k++) {
        inputState.keyPressed(k);       
        inputState.keyPressed(SDL_SCANCODE_TO_KEYCODE(k));
    }

    // Assert that no possible button is pressed (8 bit)
    for (int i=0; i<256; i++) {
        ASSERT_FALSE(inputState.isButtonDown(i));
    }
}
