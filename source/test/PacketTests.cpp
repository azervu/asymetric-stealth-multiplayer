#include "Test.h"
#include "PathTranslator.h"
#include "Protocol.h"


class PacketTests : public BaseTestCase {
protected:
    void SetUp() {
        BaseTestCase::SetUp();

        _pathTranslator = new PathTranslator(this->_settings);
        ASSERT_TRUE(_pathTranslator->addAssetDirectory("testassets"));

        _factory = new PacketFactory(_pathTranslator);
        ASSERT_TRUE(_factory->addTemplatesFromFile("testpackets.json"));
    }

    void TearDown() {
        BaseTestCase::TearDown();

        delete _factory;
        delete _pathTranslator;       
    }

    PathTranslator *_pathTranslator;
    PacketFactory *_factory;
};


template<typename T>
void testPacketFactoryDWordParsing(T val, T (*staticMethod)(const byte*)) {
    static_assert(sizeof(T) == 4, "DWords only, please");

    unsigned dw = PacketFactory::networkDword((void*)&val);
    byte *buf = (byte*)&dw;

    T result = staticMethod(buf);
    ASSERT_EQ(val, result);
}

template<typename T>
void testGeneralPacketFieldAssignment(Packet *pkt, std::string field, T val,
                                      bool (Packet::*set)(std::string,T),
                                      T (Packet::*get)(std::string)const) {
    ASSERT_TRUE((*pkt.*set)(field, val));
    ASSERT_EQ(val, (*pkt.*get)(field));
}


TEST_F(PacketTests, StaticReadMethods) {
    testPacketFactoryDWordParsing<float>(3.14f, &PacketFactory::readFloat);
    testPacketFactoryDWordParsing<int>(1337, &PacketFactory::readInt);
    testPacketFactoryDWordParsing<unsigned>(1349, &PacketFactory::readUInt);
}

TEST_F(PacketTests, CorrectPacketFieldTypes) {
    Packet *pkt = _factory->createPacketFromTemplate("TestPacket");
    ASSERT_TRUE(pkt != NULL);

    EXPECT_TRUE(pkt->hasField("val1"));
    EXPECT_TRUE(pkt->hasField("val2"));
    EXPECT_TRUE(pkt->hasField("val3"));
    EXPECT_TRUE(pkt->hasField("val4"));
    EXPECT_TRUE(pkt->hasField("val5"));
    EXPECT_TRUE(pkt->hasField("val6"));

    EXPECT_EQ(FieldType::UINT32, pkt->getFieldType("val1"));
    EXPECT_EQ(FieldType::INT32, pkt->getFieldType("val2"));
    EXPECT_EQ(FieldType::BYTE, pkt->getFieldType("val3"));
    EXPECT_EQ(FieldType::FLOAT32, pkt->getFieldType("val4"));
    EXPECT_EQ(FieldType::STRING, pkt->getFieldType("val5"));
    EXPECT_EQ(FieldType::ARRAY, pkt->getFieldType("val6"));

    delete pkt;
}

TEST_F(PacketTests, FieldAssignment) {
    Packet *pkt = _factory->createPacketFromTemplate("TestPacket");
    ASSERT_TRUE(pkt != NULL);

    testGeneralPacketFieldAssignment<unsigned>(pkt, "val1", 448, 
            &Packet::setUnsigned, &Packet::getUnsigned);
    testGeneralPacketFieldAssignment<int>(pkt, "val2", -984, 
            &Packet::setInt, &Packet::getInt);
    testGeneralPacketFieldAssignment<byte>(pkt, "val3", 253, 
            &Packet::setByte, &Packet::getByte);
    testGeneralPacketFieldAssignment<float>(pkt, "val4", 3.14f, 
            &Packet::setFloat, &Packet::getFloat);
    testGeneralPacketFieldAssignment<std::string>(pkt, "val5", "poopy", 
            &Packet::setString, &Packet::getString);
    
    // Test array assignment - slightly more work
    const unsigned int len = 17;
    byte arr[len];
    for (int i=0; i<len; i++) 
        arr[i] = i * 18;

    pkt->setArray("val6", arr, len);
    std::vector<byte> vec = pkt->getArray("val6");
    ASSERT_EQ(len, vec.size());

    for (int i=0; i<len; i++) {
        ASSERT_EQ(arr[i], vec[i]);
    }

    delete pkt;
}

TEST_F(PacketTests, PacketSerialization) {
    Packet *pktA = _factory->createPacketFromTemplate("TestPacket");
    ASSERT_TRUE(pktA != NULL);
    
    const unsigned val1    = 15;
    const int val2         = -99;
    const byte val3        = 242;
    const float val4       = 3.14f;
    const std::string val5 = "This is my string. There are many like it, but "
                             "this one is mine.";
    std::vector<byte> val6;
    for (int i=0; i<15; i++)
        val6.push_back(i * 9);

    ASSERT_TRUE(pktA->setUnsigned("val1", val1));
    ASSERT_TRUE(pktA->setInt("val2", val2));
    ASSERT_TRUE(pktA->setByte("val3", val3));
    ASSERT_TRUE(pktA->setFloat("val4", val4));
    ASSERT_TRUE(pktA->setString("val5", val5));
    ASSERT_TRUE(pktA->setArray("val6", &val6[0], val6.size()));

    // Serialize pktA to a byte-array
    int len = 0;
    byte *buffer = pktA->serialize(len);
    ASSERT_TRUE(len > 0);
    ASSERT_TRUE(buffer != NULL);

    // Attempt to deserialize the packet into pktB
    Packet *pktB = _factory->createPacketFromTemplate("TestPacket");
    ASSERT_TRUE(pktB != NULL);

    int read = 0;
    read = pktB->deserialize(buffer, len);
    ASSERT_EQ(len, read);

    delete[] buffer;

    ASSERT_EQ(val1, pktB->getUnsigned("val1"));
    ASSERT_EQ(val2, pktB->getInt("val2"));
    ASSERT_EQ(val3, pktB->getByte("val3"));
    ASSERT_EQ(val4, pktB->getFloat("val4"));
    ASSERT_EQ(val5, pktB->getString("val5"));

    std::vector<byte> array = pktB->getArray("val6");
    ASSERT_EQ(val6.size(), array.size());
    ASSERT_TRUE(std::equal(val6.begin(), val6.end(), array.begin()));

    delete pktA;
    delete pktB;
}

TEST_F(PacketTests, TestUninitializedPacket) {
    Packet *pkt = _factory->createPacketFromTemplate("TestPacket");
    ASSERT_NE(nullptr, pkt);

    ASSERT_EQ(0, pkt->getUnsigned("val1"));
    ASSERT_EQ(0, pkt->getInt("val2"));
    ASSERT_EQ(0, pkt->getByte("val3"));
    ASSERT_EQ(0.f, pkt->getFloat("val4"));
    ASSERT_EQ("", pkt->getString("val5"));
    ASSERT_EQ(0, pkt->getArray("val6").size());

    delete pkt;
}
