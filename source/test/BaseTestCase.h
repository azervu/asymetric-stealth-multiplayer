#pragma once

#include <gtest/gtest.h>

#include "Settings.h"

/**
 * This class must be used as the superclass of all test cases. It automatically
 * initializes both the ClientServiceLocator and the ServerServiceLocator 
 * services before each test is run. This adds some overhead, but that's the
 * price to pay for using global instances.
 *
 * Implement test cases like this:
 *      class MyTest : public BaseTestCase {
 *           // OPTIONAL
 *           void SetUp() {
 *              // IMPORTANT!!! If you override 'SetUp()' or 'TearDown()', the
 *              // BaseTestCase methods MUST be called!!
 *              BaseTestCase::SetUp();
 *           }
 *      };
 *
 *      TEST_F(MyTest, TestFoo) {
 *          // Test body
 *      }
 */
class BaseTestCase : public ::testing::Test {
public:
    BaseTestCase();
    virtual ~BaseTestCase();

    /**
     * Initialize ClientServiceLocator and ServerServiceLocator - the services
     * provided to the two targets are required by some of the test cases.
     *
     * The ownership of the services are transferred to the ServiceLocators, so
     * some services must be created & initialized twice.
     */
    void SetUp();

    /**
     * Destroy client and server services.
     */
    void TearDown();

protected:
    Settings* _settings = nullptr;

private:
    bool _setUp;

    /**
     * TearDown() seems not to get called when the test fails. This makes the
     * following tests flaky. TearDown() is called manually from the dtor if 
     * the test failed.
     */
    bool _tornDown;
};
