#include "Test.h"
#include "ServiceLocator.h"
#include "PathTranslator.h"
#include "MapParser.h"
#include "Map.h"

#include <map>


class MapParserTests : public BaseTestCase {

};

static int c2i(int x, int y, Map *map) {
    return y * map->getWidth() + x;
}


TEST_F(MapParserTests, TestMapLayerParse) {
    MapParser parser;
    Map *map = parser.parseMap("testmap.tmx");
    ASSERT_NE(nullptr, map);
    ASSERT_EQ(20, map->getWidth());
    ASSERT_EQ(20, map->getHeight());

    const MapLayer *bg = map->getBackgroundLayer();
    ASSERT_NE(nullptr, bg);
    ASSERT_EQ(20, bg->getWidth());
    ASSERT_EQ(20, bg->getHeight());

    const MapLayer *fg = map->getForegroundLayer();
    ASSERT_NE(nullptr, fg);
    ASSERT_EQ(20, fg->getWidth());
    ASSERT_EQ(20, fg->getHeight());

    delete map;
}

TEST_F(MapParserTests, TestAreaLayer) {
    MapParser parser;
    Map *map = parser.parseMap("testmap.tmx");
    ASSERT_NE(nullptr, map);

    std::map<int,TileFlag> expected;
    expected[c2i(1, 18, map)] = TileFlag::TILE_AREA_A;
    expected[c2i(1, 17, map)] = TileFlag::TILE_AREA_A;
    expected[c2i(2, 18, map)] = TileFlag::TILE_AREA_A;
    expected[c2i(18, 18, map)] = TileFlag::TILE_AREA_B;
    expected[c2i(17, 18, map)] = TileFlag::TILE_AREA_B;
    expected[c2i(18, 17, map)] = TileFlag::TILE_AREA_B;
    expected[c2i(18, 1, map)] = TileFlag::TILE_AREA_C;
    expected[c2i(18, 2, map)] = TileFlag::TILE_AREA_C;
    expected[c2i(17, 1, map)] = TileFlag::TILE_AREA_C;

    TileFlag areas[3] = {TILE_AREA_A, TILE_AREA_B, TILE_AREA_C};

    for (int x=0; x<5; x++) {
        for (int y=0; y<5; y++) {
            int idx = c2i(x, y, map);

            for (int i=0; i<3; i++) {
                if (expected.count(idx) && expected[idx] == areas[i]) {
                    ASSERT_TRUE(map->hasFlag(x, y, areas[i]));
                } else {
                    ASSERT_FALSE(map->hasFlag(x, y, areas[i]));

                }
            }
        }
    }

    delete map;
}

TEST_F(MapParserTests, TestObjectiveLayer) {
    MapParser parser;
    Map *map = parser.parseMap("testmap.tmx");
    ASSERT_NE(nullptr, map);

    std::map<int,TileFlag> expected;
    expected[c2i(1, 18, map)] = TileFlag::TILE_OBJ_A;
    expected[c2i(18, 18, map)] = TileFlag::TILE_OBJ_B;
    expected[c2i(18, 1, map)] = TileFlag::TILE_OBJ_C;

    TileFlag objs[3] = {TILE_OBJ_A, TILE_OBJ_B, TILE_OBJ_C};

    for (int x=0; x<5; x++) {
        for (int y=0; y<5; y++) {
            int idx = c2i(x, y, map);

            for (int i=0; i<3; i++) {
                if (expected.count(idx) && expected[idx] == objs[i]) {
                    ASSERT_TRUE(map->hasFlag(x, y, objs[i]));
                } else {
                    ASSERT_FALSE(map->hasFlag(x, y, objs[i]));

                }
            }
        }
    }

    delete map;
}

TEST_F(MapParserTests, TestLights) {
    MapParser parser;
    Map *map = parser.parseMap("testmap.tmx");
    ASSERT_NE(nullptr, map);

    const std::vector<MapLightProperties>& lights = map->getLights();
    ASSERT_EQ(2, lights.size());

    MapLightProperties l0 = lights[0];
    MapLightProperties l1 = lights[1];

    // The first light should be approximately red
    ASSERT_NEAR(l0.color.r, 1.0f, 0.0001f);
    ASSERT_NEAR(l0.color.g, 0.0f, 0.0001f);
    ASSERT_NEAR(l0.color.b, 0.0f, 0.0001f);
    ASSERT_NEAR(l0.color.a, 1.0f, 0.0001f);

    // The second light should be approximately green
    ASSERT_NEAR(l1.color.r, 0.0f, 0.0001f);
    ASSERT_NEAR(l1.color.g, 1.0f, 0.0001f);
    ASSERT_NEAR(l1.color.b, 0.0f, 0.0001f);
    ASSERT_NEAR(l1.color.a, 1.0f, 0.0001f);

    // The first light should be placed inside the top left tile
    ASSERT_NEAR(l0.pos.x, 0.5f, 0.5f);
    ASSERT_NEAR(l0.pos.y, 0.5f, 0.5f);

    // The second light should be placed inside the bottom left tile
    ASSERT_NEAR(l1.pos.x, 0.5f, 0.5f);
    ASSERT_NEAR(l1.pos.y, 19.5f, 0.5f);

    // The first light should be roughly 12.5 tiles in diameter (6.25r)
    ASSERT_NEAR(l0.radius, 6.25f, 0.1f);

    // The second light should be roughly 7.8 tiles in diameter (3.9r)
    ASSERT_NEAR(l1.radius, 3.9f, 0.1f);

    delete map;
}
