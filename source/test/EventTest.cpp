#include "Test.h"
#include "EventManager.h"
#include "ChatMessageEvent.h"
#include "ServerEvents.h"
#include "SharedEvents.h"
#include "ClientEvents.h"

#include <map>


class EventTests : public BaseTestCase {

};


template<class E>
void assertUniqueEvent() {
    static std::map<EventType,bool> typeMap;

    EventType type = E::eventType;
    
    ASSERT_TRUE(typeMap.count(type) == 0);
    
    typeMap[type] = true;
}


/**
 * All events must be added to this test. This test guarantees that the event
 * has declared the public static field "eventType", and that it is defined to
 * a unique value.
 */
TEST_F(EventTests, TestUniqueEventTypes) {
    // Program flow events
    assertUniqueEvent<ClientInitiationCompleteEvent>();
    
    // Network Events
    assertUniqueEvent<InitiateConnectionEvent>();
    assertUniqueEvent<ConnectionFailedEvent>();
    assertUniqueEvent<DisconnectEvent>();
    assertUniqueEvent<ConnectionEstablishedEvent>();
    assertUniqueEvent<ClientJoinedSessionEvent>();
    assertUniqueEvent<ClientLeftSessionEvent>();
    assertUniqueEvent<ServerShutdownEvent>();

    // Input Events
    assertUniqueEvent<KeyEvent>();
    assertUniqueEvent<MouseMotionEvent>();
    assertUniqueEvent<ButtonEvent>();

    // Window Events
    assertUniqueEvent<WindowResizedEvent>();
    assertUniqueEvent<CameraViewFrameChangedEvent>();

    // Chat Message Events
    assertUniqueEvent<MessageSendEvent>();
    assertUniqueEvent<MessageReceivedEvent>();

    // Lobby Events
    assertUniqueEvent<ClientReadyToPlayEvent>();
    assertUniqueEvent<StartLoadingLevelEvent>();
    assertUniqueEvent<LevelLoadingCompleteEvent>();
    assertUniqueEvent<GameStartingEvent>();

    // Server Events
    assertUniqueEvent<server::ClientLoadedLevelEvent>();
    assertUniqueEvent<server::ClientReadyToPlayEvent>();
    assertUniqueEvent<server::StartLoadingLevelEvent>();
    assertUniqueEvent<server::ServerLoadedLevelEvent>();
    assertUniqueEvent<server::GameStartingEvent>();
    assertUniqueEvent<server::CreatePlayerEvent>();
    assertUniqueEvent<server::PlayerDidSpawnEvent>();

    // Shared Events
    assertUniqueEvent<CreatePlayerEvent>();
    assertUniqueEvent<SpawnPlayerEvent>();
    assertUniqueEvent<PlayerTransformUpdateEvent>();
    assertUniqueEvent<PhysicsUpdatedEvent>();
    assertUniqueEvent<GameOverEvent>();
    assertUniqueEvent<ContactBegunEvent>();
    assertUniqueEvent<ContactEndedEvent>();
}



class TestEvent : public EventData {
public:
    static const EventType eventType = 0xb329fb1004c442fd;

    EventType getEventType() const {
        return eventType;
    }
};

class TestListener {
public:
    TestListener() {
        _numEvents = 0;
        _eventDispatched = false;
        _delegate.bind(this, &TestListener::onTestEvent);
    }

    void addAsListener(EventManager *mgr) {
        ASSERT_TRUE(mgr->addListener(_delegate, TestEvent::eventType));
    }

    void removeAsListener(EventManager *mgr) {
        ASSERT_TRUE(mgr->removeListener(_delegate));
    }

    void onTestEvent(const EventData *evnetData) {
        _eventDispatched = true;
        _numEvents++;
    }

    bool hasEventFired() {
        return _eventDispatched;
    }

    int getNumEventsFired() {
        return _numEvents;
    }

private:
    EventListenerDelegate _delegate;
    bool _eventDispatched;
    int _numEvents;
};


TEST_F(EventTests, TestEventTrigger) {
    EventManager mgr;
    TestListener listener;

    // No listener added
    ASSERT_FALSE(listener.hasEventFired());
    ASSERT_FALSE(mgr.triggerEvent(new TestEvent));
    ASSERT_FALSE(listener.hasEventFired());

    listener.addAsListener(&mgr);

    // Listener added
    ASSERT_TRUE(mgr.triggerEvent(new TestEvent));
    ASSERT_TRUE(listener.hasEventFired());
}

TEST_F(EventTests, TestEventQueue) {
    EventManager mgr;
    TestListener listener;

    ASSERT_FALSE(listener.hasEventFired());
    listener.addAsListener(&mgr);

    // Queueing an event when there is a listener should return true. The event
    // is not dispatched until "update()" is called on the manager.
    ASSERT_TRUE(mgr.queueEvent(new TestEvent));
    ASSERT_FALSE(listener.hasEventFired());   

    mgr.update();
    ASSERT_TRUE(listener.hasEventFired());
}

TEST_F(EventTests, TestListeningAfterQueueingEvent) {
    EventManager mgr;
    TestListener listener;

    ASSERT_FALSE(listener.hasEventFired());
    ASSERT_FALSE(mgr.queueEvent(new TestEvent));

    listener.addAsListener(&mgr);

    mgr.update();
    ASSERT_TRUE(listener.hasEventFired());
}

TEST_F(EventTests, TestMultipleEventsQueued) {
    EventManager mgr;
    TestListener listener;
    
    listener.addAsListener(&mgr);
    ASSERT_EQ(0, listener.getNumEventsFired());

    mgr.queueEvent(new TestEvent);
    mgr.queueEvent(new TestEvent);
    
    ASSERT_EQ(0, listener.getNumEventsFired());
    mgr.update();
    ASSERT_EQ(2, listener.getNumEventsFired());
}

TEST_F(EventTests, TestAbortLastEvent) {
    EventManager mgr;
    TestListener listener;

    listener.addAsListener(&mgr);
    ASSERT_EQ(0, listener.getNumEventsFired());

    mgr.queueEvent(new TestEvent);
    mgr.queueEvent(new TestEvent);
    mgr.abortEvent(TestEvent::eventType, false);

    mgr.update();
    ASSERT_EQ(1, listener.getNumEventsFired());
}

TEST_F(EventTests, TestAbortAllEvents) {
    EventManager mgr;
    TestListener listener;

    listener.addAsListener(&mgr);
    ASSERT_EQ(0, listener.getNumEventsFired());

    mgr.queueEvent(new TestEvent);
    mgr.queueEvent(new TestEvent);
    mgr.abortEvent(TestEvent::eventType, true);

    mgr.update();
    ASSERT_EQ(0, listener.getNumEventsFired());
}

TEST_F(EventTests, TestRemoveDelegate) {
    EventManager emgr;
    TestListener list;

    list.addAsListener(&emgr);

    emgr.triggerEvent(new TestEvent);
    ASSERT_EQ(1, list.getNumEventsFired());

    emgr.triggerEvent(new TestEvent);
    ASSERT_EQ(2, list.getNumEventsFired());
    
    list.removeAsListener(&emgr);

    emgr.triggerEvent(new TestEvent);
    ASSERT_EQ(2, list.getNumEventsFired());
}


/* Semi-extensive test where delegate A in it's delegate method removes the  
 * delegate B. Delegate B should therefore never be informed that event B fired.
 */
class ETEventA : public EventData {
public:
    static const EventType eventType = 0x869f702b7c614125;

    EventType getEventType() const {
        return eventType;
    }
};

class ETEventB : public EventData {
public:
    static const EventType eventType = 0xf28c6b44439a47d2;

    EventType getEventType() const {
        return eventType;
    }
};

class DelegateB {
public:
    DelegateB() {
        _mgr = nullptr;
        _notif = false;
    }
    
    void init(EventManager *mgr) {
        _mgr = mgr;
        _delB.bind(this, &DelegateB::onB);
        ASSERT_TRUE(_mgr->addListener(_delB, ETEventB::eventType));
    }

    void removeAsDelegate() {
        ASSERT_TRUE(_mgr->removeListener(_delB));
    }
    
    bool wasNotifiedOfB() {
        return _notif;
    }

private:
    EventManager *_mgr;
    EventListenerDelegate _delB;
    bool _notif;

    void onB(const EventData *e) {
        _notif = true;
    }
};

class DelegateA {
public:
    DelegateA() {
        _delB = nullptr;
        _notif = false;
    }

    void init(EventManager *mgr, DelegateB *delB) {
        _delB = delB;

        _adel.bind(this, &DelegateA::onA);
        ASSERT_TRUE(mgr->addListener(_adel, ETEventA::eventType));
    }

    bool wasNotifiedOfA() {
        return _notif;
    }

private:
    EventListenerDelegate _adel;
    DelegateB *_delB;
    bool _notif;

    void onA(const EventData *e) {
        _notif = true;
        _delB->removeAsDelegate();
    }
};

TEST_F(EventTests, TestDelegateRemovalFromDelegate) {
    EventManager emgr;
    DelegateB db;
    DelegateA da;

    db.init(&emgr);
    da.init(&emgr, &db);

    emgr.queueEvent(new ETEventA);
    emgr.update();
    
    ASSERT_TRUE(da.wasNotifiedOfA());

    // There should now not be any recipients for ETEventB, as DelegateB was
    // removed in the callback from ETEventA.
    ASSERT_FALSE(emgr.triggerEvent(new ETEventB));
    ASSERT_FALSE(db.wasNotifiedOfB());
}
