#include "Test.h"
#include "GameObject.h"


class ComponentTests : public BaseTestCase {

};


class TestComponent : public Component {
public:
    TestComponent(int value) 
        :   _value(value) { }

    int getValue() const {
        return _value;
    }

    void update(DeltaTime dt) {
        
    }

private:
    int _value;
};

class FooComponent : public TestComponent {
public:
    FooComponent()
        : TestComponent(17) { }
};

class BarComponent : public TestComponent {
public:
    BarComponent()
        : TestComponent(23) { }
};


/**
 * The entire component system is based around certain assumptions regarding the
 * typeid-system of C++. This test asserts that typeid behaves as expected on
 * the current system.
 *
 * (if this test ever fails, the Component system must be rewritten)
 */
TEST_F(ComponentTests, TestTypeidAssumptions) {
    TestComponent tcomp(11);
    FooComponent foo;
    BarComponent bar;

    ASSERT_EQ(&typeid(FooComponent), &typeid(FooComponent));
    ASSERT_EQ(&typeid(BarComponent), &typeid(BarComponent));
    ASSERT_EQ(&typeid(TestComponent), &typeid(TestComponent));

    ASSERT_NE(&typeid(FooComponent), &typeid(BarComponent));
    ASSERT_NE(&typeid(FooComponent), &typeid(TestComponent));
    ASSERT_NE(&typeid(BarComponent), &typeid(TestComponent));

    // Test getComponentId with pass-by-reference
    ASSERT_EQ(getComponentId(foo), &typeid(FooComponent));
    ASSERT_EQ(getComponentId(bar), &typeid(BarComponent));
    ASSERT_EQ(getComponentId(tcomp), &typeid(TestComponent));

    ASSERT_NE(getComponentId(foo), &typeid(BarComponent));
    ASSERT_NE(getComponentId(foo), &typeid(TestComponent));
    ASSERT_NE(getComponentId(bar), &typeid(TestComponent));

    // Test getComponentId with pointer
    ASSERT_EQ(getComponentId(&foo), &typeid(FooComponent));
    ASSERT_EQ(getComponentId(&bar), &typeid(BarComponent));
    ASSERT_EQ(getComponentId(&tcomp), &typeid(TestComponent));

    ASSERT_NE(getComponentId(&foo), &typeid(BarComponent));
    ASSERT_NE(getComponentId(&foo), &typeid(TestComponent));
    ASSERT_NE(getComponentId(&bar), &typeid(TestComponent));
}

TEST_F(ComponentTests, TestAddComponent) {
    GameObject go;

    // Only one addition per type per GameObject is allowed
    ASSERT_TRUE(addComponent<TestComponent>(&go, 3));
    ASSERT_FALSE(addComponent<TestComponent>(&go, 3));

    ASSERT_TRUE(addComponent<FooComponent>(&go));
    ASSERT_FALSE(addComponent<FooComponent>(&go));

    ASSERT_TRUE(addComponent<BarComponent>(&go));
    ASSERT_FALSE(addComponent<BarComponent>(&go));
}

TEST_F(ComponentTests, TestGetComponent) {
    GameObject go;

    ASSERT_EQ(nullptr, getComponent<TestComponent>(&go));
    ASSERT_TRUE(addComponent<TestComponent>(&go, 85));
    TestComponent *tcomp = getComponent<TestComponent>(&go);
    ASSERT_NE(nullptr, tcomp);
    ASSERT_EQ(85, tcomp->getValue());

    ASSERT_EQ(nullptr, getComponent<FooComponent>(&go));
    ASSERT_TRUE(addComponent<FooComponent>(&go));
    FooComponent *foo = getComponent<FooComponent>(&go);
    ASSERT_NE(nullptr, foo);
    ASSERT_EQ(17, foo->getValue());

    ASSERT_EQ(nullptr, getComponent<BarComponent>(&go));
    ASSERT_TRUE(addComponent<BarComponent>(&go));
    BarComponent *bar = getComponent<BarComponent>(&go);
    ASSERT_NE(nullptr, bar);
    ASSERT_EQ(23, bar->getValue());
}

TEST_F(ComponentTests, TestRemoveComponent) {
    GameObject go;

    // Removing non-existing components should not be possible 
    ASSERT_FALSE(removeComponent<TestComponent>(&go));
    ASSERT_FALSE(removeComponent<FooComponent>(&go));
    ASSERT_FALSE(removeComponent<BarComponent>(&go));

    ASSERT_TRUE(addComponent<TestComponent>(&go, 85));
    ASSERT_TRUE(addComponent<FooComponent>(&go));
    ASSERT_TRUE(addComponent<BarComponent>(&go));

    // Getting them should not return null
    ASSERT_NE(nullptr, getComponent<TestComponent>(&go));
    ASSERT_NE(nullptr, getComponent<FooComponent>(&go));
    ASSERT_NE(nullptr, getComponent<BarComponent>(&go));

    // Removing them should be allowed (once)
    ASSERT_TRUE(removeComponent<TestComponent>(&go));
    ASSERT_FALSE(removeComponent<TestComponent>(&go));

    ASSERT_TRUE(removeComponent<FooComponent>(&go));
    ASSERT_FALSE(removeComponent<FooComponent>(&go));

    ASSERT_TRUE(removeComponent<BarComponent>(&go));
    ASSERT_FALSE(removeComponent<BarComponent>(&go));

    // Getting them should now return null
    ASSERT_EQ(nullptr, getComponent<TestComponent>(&go));
    ASSERT_EQ(nullptr, getComponent<FooComponent>(&go));
    ASSERT_EQ(nullptr, getComponent<BarComponent>(&go));
}
