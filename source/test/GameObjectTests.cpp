#include "Test.h"
#include "GameObject.h"


class GameObjectTests : public BaseTestCase {

};


TEST_F(GameObjectTests, TestUniqueId) {
    GameObject a, b, c;

    ASSERT_NE(a.getId(), b.getId());
    ASSERT_NE(a.getId(), c.getId());
    ASSERT_NE(b.getId(), c.getId());
}

TEST_F(GameObjectTests, TestAddChild) {
    GameObject *parent = new GameObject;
    GameObject *childA = new GameObject;
    GameObject *childB = new GameObject;

    // Only able to add child once.
    ASSERT_TRUE(parent->addChild(childA));
    ASSERT_FALSE(parent->addChild(childA));
    ASSERT_EQ(parent, childA->getParent());

    ASSERT_TRUE(childA->addChild(childB));
    ASSERT_FALSE(childA->addChild(childB));
    ASSERT_EQ(childA, childB->getParent());

    // The parent kills its children before it dies
    delete parent;
}

TEST_F(GameObjectTests, TestRemoveChild) {
    GameObject *parent = new GameObject;
    GameObject *childA = new GameObject;
    GameObject *childB = new GameObject;

    parent->addChild(childA);
    childA->addChild(childB);

    // Not able to remove indirect children
    ASSERT_FALSE(parent->removeChild(childB, false));

    // Only able to remove a child once. Child's parent pointer must then be
    // assigned as NULL.
    ASSERT_TRUE(childA->removeChild(childB, false));
    ASSERT_FALSE(childA->removeChild(childB, false));
    ASSERT_EQ(nullptr, childB->getParent());

    ASSERT_TRUE(parent->removeChild(childA, false)); 
    ASSERT_FALSE(parent->removeChild(childA, false));
    ASSERT_EQ(nullptr, childA->getParent());

    delete parent;
    delete childA;
    delete childB;
}

TEST_F(GameObjectTests, TestFindChild) {
    GameObject *parent = new GameObject;
    GameObject *childA = new GameObject;
    GameObject *childB = new GameObject;

    parent->addChild(childA);
    childA->addChild(childB);

    // Not able to find self
    ASSERT_EQ(nullptr, parent->findChild(parent->getId()));

    // Able to find any child in the tree below self
    ASSERT_EQ(childA, parent->findChild(childA->getId()));
    ASSERT_EQ(childB, parent->findChild(childB->getId()));
    ASSERT_EQ(childB, childA->findChild(childB->getId()));

    childA->removeChild(childB, false);

    // Obviously not able to find removed children
    ASSERT_EQ(nullptr, parent->findChild(childB->getId()));

    delete parent;
    delete childB;
}

TEST_F(GameObjectTests, TestWorldPosition) {
    GameObject *parent = new GameObject;
    GameObject *child = new GameObject;

    parent->addChild(child);
    parent->setPosition(Vec2(10.f, 10.f));
    child->setPosition(Vec2(10.f, 10.f));

    // getPosition() returns the relative offset of the parent 
    ASSERT_NEAR(10.f, child->getPosition().x, 0.0001f);
    ASSERT_NEAR(10.f, child->getPosition().y, 0.0001f);
    ASSERT_NEAR(10.f, parent->getPosition().x, 0.0001f);
    ASSERT_NEAR(10.f, parent->getPosition().y, 0.0001f);

    // getWorldPosition() returns the world position of the node 
    ASSERT_NEAR(10.f, parent->getWorldPosition().x, 0.0001f);
    ASSERT_NEAR(10.f, parent->getWorldPosition().y, 0.0001f);
    ASSERT_NEAR(20.f, child->getWorldPosition().x, 0.0001f);
    ASSERT_NEAR(20.f, child->getWorldPosition().y, 0.0001f);

    delete parent;
}

TEST_F(GameObjectTests, TestWorldRotation) {
    GameObject *parent = new GameObject;
    GameObject *child = new GameObject;

    parent->addChild(child);
    parent->setRotation(1.57f);
    child->setRotation(1.57f);

    // getRotation() returns the relative rotation to the parent
    ASSERT_NEAR(1.57f, parent->getRotation(), 0.0001f);
    ASSERT_NEAR(1.57f, child->getRotation(), 0.0001f);

    // getWorldRotation() returns the rotation relative to the world
    ASSERT_NEAR(1.57f, parent->getWorldRotation(), 0.0001f);
    ASSERT_NEAR(3.14f, child->getWorldRotation(), 0.0001f);

    delete parent;
}

