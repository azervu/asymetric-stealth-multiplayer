/**
 * @file    source/test/BaseTestCase.cpp
 *
 * @brief   Implements the base test case class.
 */

#include "BaseTestCase.h"
#include "ClientServiceLocator.h"
#include "ServerServiceLocator.h"
#include "PathTranslator.h"
#include "Protocol.h"
#include "EventManager.h"
#include "Test.h"
#include "ResourceManager.h"

extern int gArgc;
extern const char **gArgv;


BaseTestCase::BaseTestCase() {
    _tornDown = false;
    _setUp = false;
}

BaseTestCase::~BaseTestCase() {
    if (!_setUp) {
        printf("WARNING: Test case did not call BaseTestCase::SetUp()!");
    }

    if (!_tornDown) {
        TearDown();
    }
}



void BaseTestCase::SetUp() {
    Log::info("\n");
    Log::info("TEST CASE RUNNING: %s", typeid(*this).name());

    _setUp = true;
    _settings = new Settings(gArgc, gArgv);

    ClientServiceLocator *client = nullptr;
    server::ServerServiceLocator *server = nullptr;
    ResourceManager *resMgr = nullptr;
    PathTranslator *pt = nullptr;
    PacketFactory *pf = nullptr;
    EventManager *emgr = nullptr;

    client = ClientServiceLocator::singleton();
    server = server::ServerServiceLocator::singleton();
    

    // Client initialization
    pt = new PathTranslator(this->_settings);
    pt->addAssetDirectory("assets");
    pt->addAssetDirectory("testassets");
    client->provide(pt);

    pf = new PacketFactory(pt);
    pf->addTemplatesFromFile("packets.json");
    client->provide(pf);

    emgr = new EventManager();
    client->provide(emgr);


    // Server intitialization
    pt = new PathTranslator(this->_settings);
    pt->addAssetDirectory("assets");
    pt->addAssetDirectory("testassets");
    server->provide(pt);

    pf = new PacketFactory(pt);
    pf->addTemplatesFromFile("packets.json");
    server->provide(pf);

    emgr = new EventManager();
    server->provide(emgr);
}

void BaseTestCase::TearDown() {
    ClientServiceLocator::singleton()->destroyServices();
    server::ServerServiceLocator::singleton()->destroyServices();
    delete _settings;

    _tornDown = true;
}
