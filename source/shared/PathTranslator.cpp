#include "PathTranslator.h"

#include <string.h>

#ifdef WIN32
    #include <entdir.h>
    #include <direct.h>
#else
    #include <dirent.h>
    #include <unistd.h>
#endif //WIN32

/**
 * Don't attempt to check for, substitute or add any slashes manually. For complete
 * cross platform support, use the constant "gSlash" instead.
 */
#ifdef WIN32
    static const std::string gSlash = "\\";
#else
    static const std::string gSlash = "/";
#endif //WIN32

static const std::string CURRENT_DIR = ".";
static const std::string PARENT_DIR = "..";

bool PathTranslator::isPathAbsolute(std::string path) {
#ifdef WIN32
    if (path.length() < 3)
        return false;

    // Ignore the drive letter
    return path.substr(1, 2) == ":\\";
#else
    if (path.length() == 0)
        return false;

    return path[0] == '/';
#endif //WIN32
}


PathTranslator::PathTranslator(Settings* settings) {
    std::string path = settings->getString("path");

    if (isPathAbsolute(path)) {
        _execDir = path;
        size_t pos = _execDir.find_last_of(gSlash);
        if (pos != std::string::npos) {
            _execDir = _execDir.substr(0, pos);
        }
    } else {
        char cwd[FILENAME_MAX];
        if (!getcwd(cwd, sizeof(cwd))) {
            Log::error("Unable to retrieve the current working directory");
        } else {
            Log::debug("The current working directory is: %s", cwd);
            Log::debug("The program was started with %s", path.c_str());

            // Find the relative path between the WD and the executable position
            std::string execRel = path;
            size_t pos = execRel.find_last_of(gSlash);
            if (pos == std::string::npos) {
                // The executable exists on the global path (UNIX) or was 
                // started from the current directory (Windows). It's safe to 
                // use the CWD as the executable directory. 
                //
                // @TODO
                // Consider checking for the executable location using "whereis"
                // on Unix for when the executable path is in $PATH.
                _execDir = cwd;
            } else {
                execRel = execRel.substr(0, pos);
                _execDir = (std::string)cwd + gSlash + execRel;

                // Remove all any self-referencing directories ("a/./b")
                while ((pos = _execDir.find(gSlash + CURRENT_DIR + gSlash)) != std::string::npos) {
                    _execDir.replace(pos, 3, gSlash);
                }
            }

            // Remove trailing self-referencing directory
            if (_execDir.back() == '.') {
                _execDir = _execDir.substr(0, _execDir.length() - 1);
            }

            // Remove any trailing slashes
            pos = _execDir.length() - gSlash.length();
            std::string trailing = _execDir.substr(pos, gSlash.length());
            if (trailing == gSlash) {
                _execDir = _execDir.substr(0, pos);
            }
        }
    }

    Log::debug("The executable lies in directory: '%s'", _execDir.c_str());
}

std::string PathTranslator::translatePath(const std::string &assetPath) const {
    for (const std::string rel : _assetDirs) {
        std::string path = _execDir + gSlash + rel + gSlash;
        path += assetPath;

        if (fileExists(path)) {
            return path;
        }
    }

    return "";
}

FILE* PathTranslator::openCHandle(const std::string &path) const {
    std::string fullPath = translatePath(path);
    if (fullPath.length() == 0) {
        Log::warning("PathTranslator was unable to translate and open the "
            "asset path '%s'", path.c_str());
        return NULL;
    }

    return fopen(fullPath.c_str(), "r");
}

bool PathTranslator::openHandle(const std::string &path, std::ifstream *fhandle) const {
    std::string fullPath = translatePath(path);
    if (fullPath.length() == 0) {
        Log::warning("PathTranslator was unable to translate and open the "
            "asset path '%s'", path.c_str());
        return false;
    }

    fhandle->open(fullPath.c_str());
    return fhandle->is_open();
}

bool PathTranslator::addAssetDirectory(const std::string &directory) {
    std::string relative = findDirInParentDir(directory);
    if (relative.length() == 0) {
        Log::warning("Unable to find directory '%s' in any parent directory!",
            directory.c_str());
        return false;
    }

    relative += gSlash + directory;
    _assetDirs.push_back(relative);

    Log::info("Added directory %s to the asset search path", relative.c_str());

    return true;
}

std::string PathTranslator::findDirInParentDir(const std::string &directory) const {
    std::string rel = CURRENT_DIR;
    std::string path = _execDir;
    size_t pos;

    while ((pos = path.find_last_of(gSlash)) != std::string::npos) {
        if (dirInDir(directory, path)) {
            return rel;
        }

        path = path.substr(0, pos);
        rel += gSlash + PARENT_DIR;
    }

    return "";
}

bool PathTranslator::dirInDir(const std::string &needle, const std::string &haystack) const {
    DIR *dir;
    struct dirent *ent;

    dir = opendir(haystack.c_str());
    if (dir != NULL) {
        while ((ent = readdir(dir)) != NULL) {
            if (needle.compare(ent->d_name) == 0) {
                if (isDirectory(ent)) {
                    closedir(dir);
                    return true;
                }
            }
        }

        closedir(dir);
    } else {
        Log::warning("Unable to open directory '%s' for reading", haystack.c_str());
    }

    return false;
}

#ifdef WIN32
bool PathTranslator::isDirectory(const struct dirent *ent) const {
    return ent->data.dwFileAttributes == FILE_ATTRIBUTE_DIRECTORY;
}
#else
bool PathTranslator::isDirectory(const struct dirent *ent) const {
    return (ent->d_type == DT_DIR);
}
#endif //WIN32

bool PathTranslator::fileExists(const std::string &path) const {
    // RAII in work
    return std::ifstream(path).good();
}
