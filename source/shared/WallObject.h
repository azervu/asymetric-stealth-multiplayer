/**
 * @file    source/shared/WallObject.h
 *
 * @brief   Declares the wall object class.
 */

#pragma once

#include "GameObject.h"

class WallObject : public GameObject {
public:

    WallObject(b2World* world, int x, int y, unsigned flags);
    virtual ~WallObject();

private:


};