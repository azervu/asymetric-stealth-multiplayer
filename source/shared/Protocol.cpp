/**
 * @file    source/shared/Protocol.cpp
 *
 * @brief   Implements all the protocols and packet systems.
 */

#include "Protocol.h"

#include "Log.h"
#include "PathTranslator.h"

#include <stdlib.h>
#include <assert.h>
#include <sstream>

int volatile_strlen(const char *str, int maxlen) {
    int i = 0;

    while (*str && i < maxlen) {
        i++;
        str++;
    }

    if (i == maxlen) {
        return -1;
    }

    return i;
}

const char* fieldTypeToStr(FieldType type) {
    switch (type) {
        case FieldType::UINT32:
            return "UINT32";
        case FieldType::INT32:
            return "INT32";
        case FieldType::FLOAT32:
            return "FLOAT32";
        case FieldType::BYTE:
            return "BYTE";
        case FieldType::STRING:
            return "STRING";
        case FieldType::ARRAY:
            return "ARRAY";
        case FieldType::UNDEFINED:
            return "UNDEFINED";
        default:
            return "UNDEFINED";
    }
}


/*
================
PacketFactory
================
*/
int PacketFactory::readInt(const byte *buf) {
    unsigned no = *((unsigned*) buf);
    unsigned ho = SDLNet_Read32(&no);

    int i = 0;
    memcpy(&i, &ho, sizeof(int));
    return i;
}

unsigned PacketFactory::readUInt(const byte *buf) {
    unsigned no = *((unsigned*) buf);
    unsigned ho = SDLNet_Read32(&no);

    return ho;
}

float PacketFactory::readFloat(const byte *buf) {
    unsigned no = *((unsigned*) buf);
    unsigned ho = SDLNet_Read32(&no);

    float f = 0.f;

    assert(sizeof f == sizeof ho);

    memcpy(&f, &ho, sizeof(f));
    return f;
}

unsigned PacketFactory::networkDword(void *ptr) {
    Uint32 uint = 0;
    unsigned out = 0;
    memcpy(&uint, ptr, sizeof(Uint32));
    SDLNet_Write32(uint, &out);

    return out;
}


PacketFactory::PacketFactory(PathTranslator *pathTranslator) {
    _templateParser = new PacketTemplateParser(pathTranslator);

#ifndef ASM_UNITTEST
    /**
     * In non-testing environments, add "packets.json". The test-suite may
     * use another file, or none at all. 
     */
    if (!addTemplatesFromFile("packets.json")) {
        Log::warning("PacketFactory contains no packet templates");
    }
#endif //ASM_UNITTEST
}

PacketFactory::~PacketFactory() {
    delete _templateParser;
}

Packet* PacketFactory::readPacket(byte *buffer, int bufferlen, int &packetlen) {
    unsigned packetId = readUInt(buffer);
    const PacketTemplate *pktTemplate = getPacketTemplate(packetId);

    if (pktTemplate == NULL) {
        Log::error("PacketFactory::readPacket: garbage data packet, invalid packet id: %u", packetId);
        for (int i = 0; i < bufferlen; i+= 4) {
            Log::error("Data: %02x%02x%02x%02x", buffer[i], buffer[i + 1], buffer[i + 2], buffer[i + 3]);
        }
        return NULL;
    }

    Packet *packet = createPacketFromTemplate(packetId);
    packetlen = packet->deserialize(buffer, bufferlen);
    
    if (packetlen <= 0) {
        delete packet;
        return NULL;
    }

    return packet;
}

Packet* PacketFactory::createPacketFromTemplate(unsigned id) {
    const PacketTemplate *temp = getPacketTemplate(id);
    if (temp == NULL) {
        return NULL;
    }

    Packet *packet = new Packet();
    packet->_name = temp->getName();
    packet->setType(temp->getFields());

    for (Packet::Field field : temp->getFields()) {
        packet->addField(field.getName(), field.getType());
    }

    if (!packet->setUnsigned("id", id)) {
        Log::error("Unable to define the ID of packet '%s'!",
            temp->getName().c_str());
        delete packet;
        return NULL;
    }

    return packet;
}

Packet* PacketFactory::createPacketFromTemplate(std::string packetName) {
    unsigned id = getPacketId(packetName);
    if (id == 0) {
        return NULL;
    }

    return createPacketFromTemplate(id);
}

unsigned PacketFactory::getPacketId(std::string packetName) {
    auto& templates = _templateParser->getTemplates();

    auto iter = templates.begin();
    int id = 1;

    for (; iter != templates.end(); iter++, id++) {
        if (iter->getName() == packetName) {
            return id;
        }
    }

    return 0;
}

bool PacketFactory::addTemplatesFromFile(std::string file) {
    return (_templateParser->addTemplatesFromFile(file) >= 0);
}

const PacketTemplate* PacketFactory::getPacketTemplate(unsigned id) {
    // Packet IDs are 1-indexed, but the array is 0-indexed.
    unsigned index = (id - 1);

    if (index >= _templateParser->getTemplates().size()) {
        return NULL;
    }

    return &(_templateParser->getTemplates()[index]);
}


/*
================
Packet::Field
================
*/
Packet::Field::Field(std::string name, FieldType type) {
    _name = name;
    _type = type;
    memset(&_value, 0, sizeof(_value));

    assert(type != FieldType::UNDEFINED);
}

Packet::Field::~Field() {
    if (_type == FieldType::STRING && _value.str != NULL) {
        free(_value.str);
    } else if (_type == FieldType::ARRAY && _value.array.ptr != NULL) {
        free(_value.array.ptr);
    }
}

bool Packet::Field::set(void *val) {
    if (!val) {
        Log::warning("Packet::Field::set called with a NULL-pointer");
        return false;
    }

    switch (_type) {
        case FieldType::UINT32:
            _value.u32 = *((unsigned*)val);
            break;
        case FieldType::INT32:
            _value.i32 = *((int*)val);
            break;
        case FieldType::FLOAT32:
            _value.f32 = *((float*)val);
            break;
        case FieldType::BYTE:
            _value.b = *((byte*)val);
            break;
        default:
            Log::error("Called Packet::Field::set(void*) on Field of "
                       "type STRING or ARRAY");
            return false;
    }

    return true;
}

bool Packet::Field::set(std::string strValue) {
    if (_type != FieldType::STRING) {
        Log::error("Called Packet::Field::set(std::string) on Field of "
            "non-string type");
        return false;
    }

    if (_value.str) {
        free(_value.str);
    }

    _value.str = (char*) malloc(strValue.length() + 1);
    memcpy(_value.str, strValue.c_str(), strValue.length() + 1);

    return true;
}

bool Packet::Field::set(const byte *buffer, unsigned len) {
    if (_type != FieldType::ARRAY) {
        Log::error("Called Packet::Field::set(byte*,int) on Filed of "
            "non-array type");
        return false;
    }

    if (len <= 0 || buffer == NULL) {
        Log::error("Called Packet::Field::set(byte*,int) with zero-length "
            "or NULL-buffer");
        return false;
    }

    if (_value.array.ptr) {
        free(_value.array.ptr);
    }

    _value.array.len = len;
    _value.array.ptr = (byte*) malloc(len);
    memcpy(_value.array.ptr, buffer, len);
    return true;
}

Packet::Field::FieldValue Packet::Field::getValue() const {
    return _value;
}

FieldType Packet::Field::getType() const {
    return _type;
}

std::string Packet::Field::getName() const {
    return _name;
}

std::string Packet::Field::getString() const {
    if (_type != FieldType::STRING) {
        Log::warning("Packet::Field::getString() called on non-string field");
        return "";
    }

    if (!_value.str) {
        return "";
    }

    return std::string(_value.str);
}

std::vector<byte> Packet::Field::getArray() const {
    std::vector<byte> vec;

    if (_type != FieldType::ARRAY) {
        Log::warning("Packet::Field::getArray() called on non-array field");
        return vec;
    }

    for (size_t i = 0; i < _value.array.len; i++) {
        vec.push_back(_value.array.ptr[i]);
    }

    return vec;
}



/*
================
Packet
================
*/
Packet::Packet() {

}

Packet::~Packet() {
    for (auto &field : _fields) {
        delete field;
    }
}

byte* Packet::serialize(int &pktlen) const {
    std::vector<byte> buffer;

    for (auto field : _fields) {

        switch (field->getType()) {
            case FieldType::UINT32:
            case FieldType::INT32:
            case FieldType::FLOAT32: {

                Packet::Field::FieldValue fval = field->getValue();

                // Assert that the union uses the same memory address for 
                // all 32 bit fields, otherwise using a single fallthrough for
                // all 32 bit fields is impossible
                assert((void*) &fval.u32 == (void*) &fval.i32
                    && (void*) &fval.u32 == (void*) &fval.f32);

                unsigned u32 = fval.u32;
                void *vptr = (void*) &u32;
                unsigned netdw = PacketFactory::networkDword(vptr);
                byte *bytes = (byte*) &netdw;

                for (int j = 0; j < 4; j++) {
                    buffer.push_back(bytes[j]);
                }
            } break;

            case FieldType::BYTE: {

                buffer.push_back(field->getValue().b);

            } break;

            case FieldType::STRING: {

                std::string str = field->getString();
                for (size_t i = 0; i < str.length(); i++) {
                    buffer.push_back(str[i]);
                }

                buffer.push_back(0);

            } break;

            case FieldType::ARRAY: {
                /* The first four bytes of a serialized binary array is the
                 * 32 bit unsigned representation of the length in network
                 * byte order.
                 */
                unsigned l = field->getValue().array.len;
                byte *p = field->getValue().array.ptr;

                unsigned netdw = PacketFactory::networkDword(&l);
                byte *netdwptr = (byte*) &netdw;
                for (int i = 0; i < 4; i++) {
                    buffer.push_back(netdwptr[i]);
                }

                for (size_t i = 0; i < l; i++) {
                    buffer.push_back(p[i]);
                }
            } break;

            case FieldType::UNDEFINED: {
                Log::error("Packet::serialize(): Unable to serialize into "
                    "field with undefined type");
            } break;
        }
    }

    pktlen = buffer.size();
    if (pktlen == 0)
        return NULL;

    byte *b = new byte[pktlen];
    for (size_t i = 0; i < buffer.size(); i++)
        b[i] = buffer[i];

    return b;
}

int Packet::deserialize(const byte *buf, int buflen) {
    int read = 0;

    for (auto field : _fields) {

        switch (field->getType()) {
            case FieldType::UINT32: {
                unsigned val = PacketFactory::readUInt(buf + read);
                field->set(&val);
                read += sizeof(val);
            } break;

            case FieldType::INT32: {
                int val = PacketFactory::readInt(buf + read);
                field->set(&val);
                read += sizeof(val);
            } break;

            case FieldType::FLOAT32: {
                float val = PacketFactory::readFloat(buf + read);
                field->set(&val);
                read += sizeof(val);
            } break;

            case FieldType::BYTE: {
                byte val = buf[read];
                field->set(&val);
                read++;
            } break;

            case FieldType::STRING: {
                char *str = (char*) (buf + read);
                int len = volatile_strlen(str, buflen - read);
                if (len == -1) {
                    Log::error("Error in Packet::deserialize(): "
                        "String field '%s' is not null-terminated!",
                        field->getName().c_str());
                } else {
                    std::string cppstr(str);
                    field->set(cppstr);
                    read += (len + 1);
                }
            } break;

            case FieldType::ARRAY: {
                /* The first four bytes is the 32 bit unsigned representation
                 * of the length of the following array.
                 */
                unsigned len = PacketFactory::readUInt(buf + read);
                field->set(buf + read + sizeof(len), len);

                read += sizeof(len);
                read += len;
            } break;

            case FieldType::UNDEFINED: {
                Log::error("Packet::deserialize(): Unable to deserialize into "
                    "field with undefined type");
            } break;
        }
    }

    return read;
}

bool Packet::hasField(std::string name) const {
    return (_reverseLookup.count(name) != 0);
}

FieldType Packet::getFieldType(std::string name) const {
    if (!hasField(name)) {
        return FieldType::UNDEFINED;
    }
    
    return _fields.at(_reverseLookup.at(name))->getType();
}


/**
 * Getter-macro for implementing getUnsigned, getInt, getFloat, etc.
 *
 * @param name      std::string variable name or compliant literal
 * @param valuemem  The name of the entry within Packet::Field::value to return
 * @param errval    The default value to return if the field "name" is undefined.
 * @param type      The expected FieldType 
 */
#define PACKET_GET_FIELD(name, valuemem, errval, type)                      \
    if (_reverseLookup.count(name) == 0) {                                  \
        Log::error("Unable to find field of type "#valuemem" with name %s " \
                   "in packet '%s'!", name.c_str(), getName().c_str());     \
        return errval;                                                      \
    }                                                                       \
    const Packet::Field *field = _fields.at(_reverseLookup.at(name));       \
    if (field->getType() != type) {                                         \
        Log::error("Attempted to get value of type '"#type"' from field of "\
                   "type '%s' in packet '%s'",                              \
                    fieldTypeToStr(field->getType()), getName().c_str());   \
    }                                                                       \
    return field->getValue().valuemem;                                      


unsigned Packet::getUnsigned(std::string name) const {
    PACKET_GET_FIELD(name, u32, 0, FieldType::UINT32);
}

int Packet::getInt(std::string name) const {
    PACKET_GET_FIELD(name, i32, 0, FieldType::INT32);
}

float Packet::getFloat(std::string name) const {
    PACKET_GET_FIELD(name, f32, 0.f, FieldType::FLOAT32);
}

byte Packet::getByte(std::string name) const {
    PACKET_GET_FIELD(name, b, 0, FieldType::BYTE);
}

std::string Packet::getString(std::string name) const {
    if (_reverseLookup.count(name) == 0) {
        Log::error("Unable to find field of type STRING with name %s "
                   "in packet '%s'!", name.c_str(), getName().c_str());
        return "";
    }
    const Packet::Field *field = _fields[_reverseLookup.find(name)->second];
    if (field->getType() != FieldType::STRING) {
        Log::error("Attempted to get value of type 'STRING' from field of "
                   "type '%s' in packet '%s'",
                    fieldTypeToStr(field->getType()), getName().c_str());
    }

    return field->getString();
}

std::vector<byte> Packet::getArray(std::string name) const {
    if (_reverseLookup.count(name) == 0) {
        Log::error("Unable to find field of type ARRAY with name %s", 
                   name.c_str());
        return std::vector<byte>();
    }

    const Packet::Field *field = _fields[_reverseLookup.find(name)->second];
    if (field->getType() != FieldType::ARRAY) {
        Log::error("Attempted to get value of type 'ARRAY' from field of "
                   "type '%s'", fieldTypeToStr(field->getType()));
        return std::vector<byte>();
    }

    return field->getArray();
}


/**
 * Setter macro for implementing the overloaded setField(std::string, ...)
 * methods. This macro only works on generic fields (u32, i32, f32, b).
 *
 * @param name      std::string variable name or compliant value. The name of
 *                  the field to be altered.
 * @param value     Generic variable name.
 * @param type      The expected type of the field
 */
#define PACKET_SET_GENERIC_FIELD(name, value, type)                         \
    if (!_reverseLookup.count(name)) {                                      \
        Log::error("Attempted to set undefined field '%s' in packet '%s'!", \
                   name.c_str(), getName().c_str());                        \
        return false;                                                       \
    }                                                                       \
    Packet::Field *field = _fields.at(_reverseLookup.at(name));             \
    if (field->getType() != type) {                                         \
        Log::error("Attempted to set value-type "#type" to field of type %s"\
                   "in packet '%s'!",                                       \
                    fieldTypeToStr(field->getType()), getName().c_str());   \
        return false;                                                       \
    }                                                                       \
    return field->set(&value);


bool Packet::setUnsigned(std::string name, unsigned value) {
    PACKET_SET_GENERIC_FIELD(name, value, FieldType::UINT32);
}

bool Packet::setInt(std::string name, int value) {
    PACKET_SET_GENERIC_FIELD(name, value, FieldType::INT32);
}

bool Packet::setFloat(std::string name, float value) {
    PACKET_SET_GENERIC_FIELD(name, value, FieldType::FLOAT32);
}

bool Packet::setByte(std::string name, byte value) {
    PACKET_SET_GENERIC_FIELD(name, value, FieldType::BYTE);
}

bool Packet::setString(std::string name, std::string strValue) {
    if (!_reverseLookup.count(name)) {
        Log::error("Attempted to set undefined field '%s' in packet '%s'!", 
                   name.c_str(), getName().c_str());
        return false;
    }

    Packet::Field *field = _fields.at(_reverseLookup.at(name));
    if (field->getType() != FieldType::STRING) {
        Log::error("Attempted to assign STRING to field of type %s in "
                   "packet '%s'", 
                   fieldTypeToStr(field->getType()), getName().c_str());
        return false;
    }

    return field->set(strValue);
}

bool Packet::setArray(std::string name, const byte *arr, unsigned len) {
    if (!_reverseLookup.count(name)) {
        Log::error("Attempted to set undefined field '%s' in packet '%s'", 
                   name.c_str(), getName().c_str());
        return false;
    }

    Packet::Field *field = _fields.at(_reverseLookup.at(name));
    if (field->getType() != FieldType::ARRAY) {
        Log::error("Attempted to assign ARRAY to field of type %s in packet %s",
                   fieldTypeToStr(field->getType()), getName().c_str());
        return false;
    }

    return field->set(arr, len);
}

bool Packet::addField(std::string name, FieldType ft) {
    auto res = _reverseLookup.find(name);
    if (res == _reverseLookup.end()) {
        Log::warning("Attempted to add field to packet that don't belong here");
        return false;
    } else if (_fields[res->second] != nullptr) {
        Log::warning("Attempted to overwrite field to packet");
        return false;
    }

    _fields[_reverseLookup[name]] = new Packet::Field(name, ft);
    return true;
}

void Packet::setType(const std::vector<Packet::Field> &fields) {
    for (int i = 0; i < fields.size(); i++) {
        _reverseLookup[fields[i].getName()] = i;
    }

    _fields.resize(fields.size());

    for (int i = 0; i < fields.size(); i++) {
        _fields[i] = nullptr;
    }
}

std::string Packet::getName() const {
    return _name;
}

void Packet::logContents() const {
    std::stringstream ss;
    ss << getName() << ": { ";

    bool first = true;

    for (auto field : _fields) {
        if (!first)
            ss << ", ";
        first = false;

        ss << "'" << field->getName() << "': '";

        switch (field->getType()) {
            case FieldType::INT32:
                ss << field->getValue().i32;
                break;
            case FieldType::UINT32:
                ss << field->getValue().u32;
                break;
            case FieldType::FLOAT32:
                ss << field->getValue().f32;
                break;
            case FieldType::BYTE:
                ss << field->getValue().b;
                break;
            case FieldType::STRING:
                ss << field->getString();
                break;
            case FieldType::ARRAY:
                ss << "[array, " << field->getValue().array.len << " bytes]";
                break;
            case FieldType::UNDEFINED:
                ss << "[undefined]";
                break;
        }

        ss << "'";
    }

    ss << " }";

    Log::debug(ss.str());
}

/*
==================
PacketTemplate
==================
*/
const std::vector<Packet::Field>& PacketTemplate::getFields() const {
    return _fields;
}

std::string PacketTemplate::getName() const {
    return _name;
}



/*
==================
PacketTemplateParser
==================
*/
PacketTemplateParser::PacketTemplateParser(PathTranslator *pathTranslator)
: _pathTranslator(pathTranslator) {

}

const std::vector<PacketTemplate>& PacketTemplateParser::getTemplates() const {
    return _templates;
}

int PacketTemplateParser::addTemplatesFromFile(std::string path) {
    if (hasFileBeenParsed(path)) {
        Log::info("PacketTemplateParser: File %s has already been parsed",
                  path.c_str());
        return 0;
    }

    FILE *file = _pathTranslator->openCHandle(path);
    if (!file) {
        Log::error("PacketTemplateParser was unable to open file %s",
            path.c_str());
        return -1;
    }

    rapidjson::Document document;
    rapidjson::FileStream filestream(file);
    document.ParseStream<0>(filestream);

    if (document.HasParseError()) {
        Log::error("PacketTemplateParser error in packet-defining "
            "JSON file %s: %i",
            path.c_str(), document.GetParseError());
        fclose(file);
        return -1;
    }

    if (!document.IsObject()) {
        Log::error("PacketTemplateParser expected object at root");
        fclose(file);
        return -1;
    }

    if (!document.HasMember("packets")) {
        Log::error("PacketTemplateParser expected root to contain object "
            "'packets', no such object found.");
        fclose(file);
        return -1;
    }

    rapidjson::Value *packets = &document["packets"];
    if (!packets->IsArray()) {
        Log::error("PacketTemplateParser expected node 'packets' to be array");
        fclose(file);
        return -1;
    }

    rapidjson::Value::ValueIterator iter;
    int added = 0;

    for (iter = packets->Begin(); iter != packets->End(); iter++) {
        if (!parsePacket(*iter)) {
            Log::error("PacketTemplateParser aborting parsing of file %s. "
                "Discarding %i parsed templates.",
                path.c_str(), added);

            // Remove the packets added by this call to addTemplatesFromFile()
            for (size_t i = _templates.size() - added; i < _templates.size(); i++) {
                _templates.erase(_templates.begin() + i--);
            }

            fclose(file);
            return -1;
        } else {
            added++;
        }
    }

    fclose(file);
    Log::info("Loaded %i network packet templates", added);
    _parsedFiles.push_back(path);
    return added;
}

bool PacketTemplateParser::parsePacket(const rapidjson::Value &packetNode) {
    if (!packetNode.HasMember("name") || !packetNode["name"].IsString()) {
        Log::error("PacketTemplateParser found packet template without a name");
        return false;
    }


    // All templates require the "id"-field.
    Packet::Field packetField("id", FieldType::UINT32);

    // Create the template, but do not add it until the parsing is done.
    // Errors may yet occur, and invalid templates should not be used.
    PacketTemplate packetTemplate;
    packetTemplate._fields.push_back(packetField);
    packetTemplate._name = packetNode["name"].GetString();

    // Fields are NOT required (but they sure do help)
    if (packetNode.HasMember("fields")) {
        const rapidjson::Value& fields = packetNode["fields"];
        if (!fields.IsArray()) {
            Log::error("PacketTemplateParser expected field 'fields' to be of "
                "type array");;
            return false;
        }

        for (rapidjson::SizeType i = 0; i < fields.Size(); i++) {
            const rapidjson::Value &field = fields[i];

            if (!field.IsObject()) {
                Log::error("PacketTemplateParser expected field to be "
                    "an 'Object'. It is not.");
                return false;
            }

            if (!field.HasMember("name") || !field.HasMember("type")) {
                Log::error("PacketTemplateParser: field %i in packet is missing"
                    " either 'type' or 'name'.", i);
                return false;
            }

            const rapidjson::Value& nameNode = field["name"];
            const rapidjson::Value& typeNode = field["type"];
            if (!nameNode.IsString() || !typeNode.IsString()) {
                Log::error("PacketTemplateParser: Invalid type of field 'name' "
                    "or field 'type'. Expected string!");
                return false;
            }

            std::string fieldName = nameNode.GetString();
            std::string typeString = typeNode.GetString();
            FieldType fieldType = getFieldType(typeString);

            if (fieldType == FieldType::UNDEFINED) {
                Log::error("PacketTemplateParser: unknown type '%s'!",
                    typeString.c_str());
                return false;
            }

            packetField = Packet::Field(fieldName, fieldType);
            packetTemplate._fields.push_back(packetField);
        }
    }

    _templates.push_back(packetTemplate);
    return true;
}

FieldType PacketTemplateParser::getFieldType(std::string type) const {
    if (type == "uint")
        return FieldType::UINT32;

    if (type == "int")
        return FieldType::INT32;

    if (type == "float")
        return FieldType::FLOAT32;

    if (type == "byte")
        return FieldType::BYTE;

    if (type == "string")
        return FieldType::STRING;

    if (type == "array")
        return FieldType::ARRAY;

    return FieldType::UNDEFINED;
}

bool PacketTemplateParser::hasFileBeenParsed(const std::string &file) const {
    for (const std::string parsed : this->_parsedFiles) {
        if (parsed == file) {
            return true;
        }
    }

    return false;
}
