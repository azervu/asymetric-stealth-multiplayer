#pragma once

#include <chrono>


/**
 * Class for measuring the time elapsed between two arbitrary points
 * in time.
 */
class Timer {
public:
    /**
     * The constructor starts the timer.
     */
    Timer();

    /**
     * Stop the timer (if it was already running) and start it.
     */
    void start();

    /**
     * Return the time elapsed since the last call to Timer::start() in 
     * microseconds.
     */
    long long getElapsedMicro() const;

    double getElapsedSeconds() const;

private:
    std::chrono::time_point<std::chrono::high_resolution_clock> _start;
};
