#include "Map.h"


static int tileIndex(int x, int y, int width, int height) {
    if (x >= width || y >= height || x < 0 || y < 0) {
        Log::error("Attempted to access tile at invalid coordinate [%i, %i]. "
                   "Map is of dimension [%i, %i].", x, y, width, height);
        return -1;
    }

    return (y * width + x);
}


/*
==================
MapLayer
==================
*/
MapLayer::MapLayer(int mapWidth, int mapHeight, const std::string& tileSet)
    :   _width(mapWidth),
        _height(mapHeight),
        _tileSet(tileSet) {
    _tileTexIndex.resize(_width * _height);
}

int MapLayer::getWidth() const {
    return _width;
}

int MapLayer::getHeight() const {
    return _height;
}

int MapLayer::getTileTexIndex(int x, int y) const {
    int idx = tileIndex(x, y, _width, _height);
    if (idx < 0) {
        return -1;
    }

    return _tileTexIndex[idx];
}

const std::string& MapLayer::getTileTexture() const {
    return this->_tileSet;
}

void MapLayer::setTileTexIndex(int x, int y, int tt) {
    int idx = tileIndex(x, y, _width, _height);
    if (idx < 0) {
        return;
    }

    _tileTexIndex[idx] = tt;
}


/*
==================
Map
==================
*/
Map::Map(int width, int height) 
    :   _bgLayer(nullptr),
        _fgLayer(nullptr),
        _width(width),
        _height(height) { 
    _tileFlags.resize(_width * _height);
}

Map::~Map() {
    if (_fgLayer) {
        delete _fgLayer;
    }

    if (_bgLayer) {
        delete _bgLayer;
    }
}

const MapLayer* Map::getBackgroundLayer() const {
    return _bgLayer;
}

const MapLayer* Map::getForegroundLayer() const {
    return _fgLayer;
}

unsigned Map::getTileFlags(int x, int y) const {
    int idx = tileIndex(x, y, _width, _height);
    if (idx < 0) {
        return 0;
    }

    return _tileFlags[idx];
}

bool Map::hasFlag(int x, int y, TileFlag flag) const {
    unsigned flags = getTileFlags(x, y);
    return (flags & flag) > 0;
}

bool Map::hasFlags(int x, int y, unsigned flag) const {
    unsigned flags = getTileFlags(x, y);
    return (flags & flag) == flag;
}

int Map::getWidth() const {
    return _width;
}

int Map::getHeight() const {
    return _height;
}

const std::vector<MapLightProperties>& Map::getLights() const {
    return _lights;
}

const std::vector<MapTriggerProperties>& Map::getTriggers() const {
    return _triggers;
}

const std::vector<Rect>& Map::getVisionBlockers() const {
    return _visionBlocks;
}

std::vector<Vec2> Map::getSpawnPositions(Team team) const {
    std::vector<Vec2> spawns;

    TileFlag flag = (team == Team::SPY) 
                    ? TileFlag::TILE_SPAWN_SPY
                    : TileFlag::TILE_SPAWN_MERC;

    for (int x = 0; x < _width; x++) {
        for (int y = 0; y < _height; y++) {
            if (hasFlags(x, y, flag)) {
                // Find the spawning position in tile-space coordinates
                Vec2 pos((float)x + 0.5f, (float)y + 0.5f);

                // Convert the tile-space position to metric positon
                pos.x /= ASM_TILES_PER_METER;
                pos.y /= ASM_TILES_PER_METER;

                spawns.push_back(pos);
            }
        }
    }

    return spawns;
}



void Map::setBackgroundLayer(MapLayer *layer) {
    _bgLayer = layer;
}

void Map::setForegroundLayer(MapLayer *layer) {
    _fgLayer = layer;
}

void Map::addTileFlags(int x, int y, unsigned flags) {
    int idx = tileIndex(x, y, _width, _height);
    if (idx < 0) {
        return;
    }

    if (flags & ~(TileFlag::TILE_EVERYTHING)) {
        Log::warning("Invalid flags attempted assigned to tile: %x", flags);
        return;
    }

    _tileFlags[idx] |= flags;
}

