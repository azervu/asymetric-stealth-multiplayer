/**
 * @file    source/shared/GameObject.h
 *
 * @brief   Declares the game object and component-related classes.
 */

#pragma once

#include "SDLHeaders.h"
#include "Engine.h"
#include "EventManager.h"

#include <Box2D/Box2D.h>

#include <map>
#include <typeinfo>
#include <type_traits>


class Component;
class ComponentManager;
class ComponentAggregator;
class GameObject;
class Scene;

typedef const std::type_info* ComponentId;


/**
 * Aggregator of components. Used as the superclass of GameObject to encapsulate
 * all Component related behaviour, and to avoid GameObject befriending
 * ComponentManager. However, Components are ONLY intended to be added to 
 * GameObjects.
 */
class ComponentAggregator {
    friend class ComponentManager;
    std::map<ComponentId,Component*> _components;

public:
    void update(DeltaTime deltaTime);

protected:
    virtual ~ComponentAggregator();
};


/**
 * @class   GameObject
 *
 * @brief   The class for all physical objects/actors in the game world. Game
 *          Objects must NEVER be deleted arbitrarily if they are or has been
 *          attached to another GameObject or a Scene. The owning Scene will
 *          handle deletion following a DeleteGameObjectEvent.
 */
class GameObject : public ComponentAggregator {
public:
    typedef unsigned Id;

    GameObject();
    virtual ~GameObject();

    /**
     * Retrieve the GameObject ID. 
     */
    GameObject::Id getId() const;

    virtual void setPosition(Vec2 vec);
    Vec2 getPosition() const;
    Vec2 getWorldPosition() const;

    virtual void setRotation(float rot);
    float getRotation() const;
    float getWorldRotation() const;

    GameObject* getParent();

    void setScene(Scene *scene);
    Scene* getScene() const;

    /**
     * Attempt to find a GameObject with a specific ID in any of the children
     * recursively. Implemented as a BFS as I'm expecting the height of the
     * GameObject tree to be shallow and "deep" nodes to be less interesting.
     */
    GameObject* findChild(GameObject::Id id);

    bool addChild(GameObject *go);

    bool isChild(GameObject *go);
    bool isChild(GameObject::Id id);

    /**
     * Removes a child object and nullifies the child's parent pointer.
     * @param del   Whether or not to delete the child if it exists 
     * @return      True if the child was found and removed, false otherwise.
     */
    bool removeChild(GameObject *go, bool del);
    bool removeChild(GameObject::Id id, bool del);

    const std::vector<GameObject*>& getChildren() const;
    
private:
    GameObject::Id _id;

    GameObject *_parent;
    std::vector<GameObject*> _children;
    Scene *_scene;

    /**
     * Coordinate relative to parent unless orphan. In world-space coordinate
     * system.
     */
    Vec2 _position;

    /**
     * Rotation in radians, relative to parent unless orphan.
     */
    float _rotation;
};



/*
================================================
# IMPORTANT COMPONENT INFORMATION
# 
# There can only exist one component of each type per GameObject instance. This
# is enforced by mapping the address of a class' vtable to an instance of said
# class. 
#
# Components are identified by the type_id-object of their class. The ONLY
# safe way to retrieve the type_info object is by calling typeinfo() on the
# class, not on the instance. In the following scenario, the two ComponentIds
# are UNEQUAL. 
#
#       MyComponent comp;
#       ComponentId id1 = &typeid(comp);
#       ComponentId id2 = &typeid(MyComponent);
#       if (id1 == id2) 
#           printf("NEVER REACHED");:
#
# If the ComponentId of an instance is required, the function getComponentId()
# does this. In the next example, the two ComponentIds are EQUAL:
#
#       MyComponent comp;
#       ComponentId id1 = getComponentId(comp);
#       ComponentId id2 = &typeid(MyComponent);
#       if (id1 == id2)
#           printf("ALWAYS REACHED");
#
# These conditions are thoroughly tested in the ComponentTests-suite, and ONLY
# apply to the templated Component-functions as ComponentIds are not retrieved
# elsewhere. 
#
================================================
*/



/**
 * Components are intended to encapsulate game logic behaviour, such as 
 * moving when the user presses the arrow keys, creating a physical entity
 * on the GameObject, or adding drawable components to the GameObject.
 */
class Component {
    friend class ComponentManager;
public:
    Component();
    virtual ~Component();

    virtual void update(DeltaTime dt) = 0;

    GameObject* getGameObject() const;

private:
    GameObject *_gameObject;
};


/**
 * The ComponentManager is responsible for performing the alteration to the 
 * GameObject's Component-set. An instance is attached to a single 
 * GameObject instance, and can in it's lifetime only act on that
 * instance.
 * 
 * Do note that this class CAN be used to manage Components, but the templated
 * functions 'addComponent()', 'getComponent()' and 'removeComponent()' should
 * be used, as the hack this system builds upon is potentially volatile.
 */
class ComponentManager {
public:
    ComponentManager(GameObject *gameObject);

    /**
     * Add a component to the attached GameObject.
     * @param compo     The component to attach to 'agg'.
     * @param id        The ComponentId of the component type.
     * @return          True if the GameObject does not already contain a 
     *                  Component of the same type as 'comp'.
     */
    bool addComponent(Component *comp, ComponentId id);

    /**
     * Attempt to remove a component from the attached GameObject. The
     * component is deleted in the process.
     * @param id        The ID of the Component-type.
     * @return          True if a component with the id existed ans was removed,
     *                  false if no such component is attached.
     */
    bool removeComponent(ComponentId id);

    /**
     * Check if the GameObject contains an instance with id 'id'.
     */
    bool hasComponent(ComponentId id);

    /**
     * Attempt to retrieve a component from the attached GameObject.
     * @param id        The ComponentId of the Component to retrieve.
     * @return          NULL if no such component exists, a pointer to the
     *                  component otherwise.
     */
    Component* getComponent(ComponentId id);

private:
    GameObject *_gameObject;
};


/**
 * Retrieve the ComponentId of the passed component.
 */
template<class CompT>
inline ComponentId getComponentId(CompT &comp) {
    static_assert(std::is_base_of<Component,CompT>::value, 
                  "CompT must be a subclass of Component");
    return &typeid(CompT);
}

template<class CompT>
inline ComponentId getComponentId(CompT *comp) {
    // Call non-pointer version
    return getComponentId(*comp);
}


/**
 * Add a component to a game object. There can only exist one Component of each
 * type per GameObject. A GameObject CAN have 'superclass A' and 'subclass B'
 * attached to it simultaneously, as 'A' and 'B' are treated as completely 
 * different classes.
 * @param CompT         A Component-subclass.
 * @param args          The arguments to pass to the constructor of CompT.
 * @param gameObject    The GameObject instance to add a Component of type CompT
 *                      to.
 * @return              True if the component was successfully created and added
 *                      to the GameObject, false if the GameObject already has
 *                      a component 'CompT' attached.
 */
template<class CompT, typename...Args>
bool addComponent(GameObject *gameObject, Args... args) {
    static_assert(std::is_base_of<Component,CompT>::value, 
                  "CompT must be a subclass of Component");
    ComponentId componentId = &typeid(CompT);
    ComponentManager mgr(gameObject);

    if (mgr.hasComponent(componentId)) {
        Log::info("Component '%s' already exists on GameObject %d",
                  componentId->name(), gameObject->getId());
        return false;
    }
    Component *component = new CompT(args...);
    if (!mgr.addComponent(component, componentId)) {
        Log::error("Failed to add component '%s' to GameObject %d",
                   componentId->name(), gameObject->getId());
        delete component;
        return false;
    }
    return true;
}


/**
 * Get an attached component from a GameObject.
 * @param gameObject    The instance from which to retrieve a Component.
 * return               A component of type CompT if one exists, NULL otherwise.
 */
template<class CompT>
CompT* getComponent(GameObject *gameObject) {
    static_assert(std::is_base_of<Component,CompT>::value, 
                  "CompT must be a subclass of Component");

    ComponentId componentId = &typeid(CompT);
    ComponentManager mgr(gameObject);
    return (CompT*)mgr.getComponent(componentId);
}


/**
 * Remove a component from a GameObject.
 */
template<class CompT>
bool removeComponent(GameObject *gameObject) {
    static_assert(std::is_base_of<Component,CompT>::value, 
                  "CompT must be a subclass of Component");

    ComponentId componentId = &typeid(CompT);
    ComponentManager mgr(gameObject);
    return mgr.removeComponent(componentId);
}
