/**
 * @file    source/shared/Preloader.h
 *
 * @brief   Declares the preloader class.
 */

#pragma once

#if defined(ASM_CLIENT) || defined(ASM_UNITTEST)
    #include "ResourceManager.h"
#endif

#include "SpriteManager.h"

#include <future>
#include <string>

/**
 * @class   Preloader
 *
 * @brief   A preloader.
 */

class Preloader {
public:
    Preloader();
    virtual ~Preloader();

    /**
     * @fn  std::future<bool> Preloader::startLoading(const std::string& filePath);
     *
     * @brief   Loads the assets as an async operation.
     *
     * @param   filePath    Pathname of the file.
     *
     * @return  A future: true if success, false otherwise.
     */

    std::future<bool> startLoading(const std::string& filePath);

private:

    bool loadAssets(const std::string& filePath);

#ifdef ASM_CLIENT
    ResourceManager* _resourceManager;
#endif //ASM_CLIENT
    SpriteManager::Manager* _spriteManager;
};
