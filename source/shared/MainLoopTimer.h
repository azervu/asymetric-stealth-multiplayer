
#pragma once
#include "SDLHeaders.h"
#include "Engine.h"

const double MIN_TIME_STEP = 1000.0f / 60; //Time step in milliseconds

class MainLoopTimer {
public:
    MainLoopTimer();
    DeltaTime getTimeStep();
private:
    DeltaTime lastTime;
    DeltaTime currentTime;

    DeltaTime logInterval;
};
