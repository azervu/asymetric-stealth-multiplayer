/**
 * @file    source/shared/PathTranslator.h
 *
 * @brief   Declares the path translator class.
 */

#pragma once

#include "Engine.h"
#include <string>
#include <vector>
#include <fstream>

#include "ServiceLocator.h"


/**
 * Translator between brief file name and their "physical" location in the
 * file system. This translator should be used whenever a file is required, as
 * it will be informed of the relative path between the current working
 * directory and the location of the asset directories.
 */
class PathTranslator : public Service {
public:
    static bool isPathAbsolute(std::string path);

    PathTranslator(Settings* settings);

    /**
     * @fn  std::string PathTranslator::translatePath(const std::string &assetPath) const;
     *
     * @brief   Translate a path to a file contained in one of the asset directories into the
     *          relative path-name. The file must exist for it to be translated.
     *
     * @param   assetPath   Path to an object contained in any of the asset directories. For instance
     *                      "img/player.png".
     *
     * @return  Path relative to the current working directory. For instance, with the previous
     *          example as parameter: "../assets/img/player.png". If the file does not exist in any
     *          of the asset directories, an empty string is returned.
     */

    std::string translatePath(const std::string &assetPath) const;

    /**
     * @fn  FILE* PathTranslator::openCHandle(const std::string &path) const;
     *
     * @brief   Open a C-file handle given an asset-path.
     *
     * @param   path    Path to an object contained in any of the asset directories.
     *
     * @return  A read-only C-file handle on success, NULL on error.
     */

    FILE* openCHandle(const std::string &path) const;

    /**
     * @fn  bool PathTranslator::openHandle(const std::string &path, std::ifstream *fhandle) const;
     *
     * @brief   Open an ifstream given an asset path.
     *
     * @param   path            Path to an object contained in any of the asset directories.
     * @param [in,out]  fhandle Pointer to an existing std::ifstream instance. The file will be
     *                          attempted opened in this handle.
     *
     * @return  True on success, false on error.
     */

    bool openHandle(const std::string &path, std::ifstream *fhandle) const;

    /**
     * @fn  bool PathTranslator::addAssetDirectory(const std::string &directory);
     *
     * @brief   Add an asset directory to the search path. The method searches recursively upwards
     *          from the directory of the executable in the FS until a directory with this name is
     *          found, or the root of the FS is encountered. If such a directory is found, it is
     *          added to the asset search path. param directory  Name (NOT PATH) of a directory to be
     *          included in the
     *                           asset search path.
     *
     * @param   directory   Pathname of the directory.
     *
     * @return  True on success (the directory was found), false if the directory could not be found.
     */

    bool addAssetDirectory(const std::string &directory);

private:
    /**
     * The directory containing the executable. Does not contain any trailing
     * slashes, and is on either of these forms, depending on the OS:
     *      "/home/user/games/asm"
     *      "C:/Users/user/games/asm"
     */
    std::string _execDir;

    /**
     * The search directories used when looking for assets. The paths does not
     * contain any trailing or leading slashes, and exists on the form:
     *      "./../some_asset_dir"
     */
    std::vector<std::string> _assetDirs;

    /**
     * @fn  std::string PathTranslator::findDirInParentDir(const std::string &directory) const;
     *
     * @brief   Search for a directory in the parent directories, searching all the way down to the
     *          root of the FS from the path of the current executable.
     *
     * @param   directory   Pathname of the directory.
     *
     * @return  The relative path to the directory on success, empty string if the dir could not be
     *          found.
     */

    std::string findDirInParentDir(const std::string &directory) const;

    /**
     * @fn  bool PathTranslator::dirInDir(const std::string &needle, const std::string &haystack) const;
     *
     * @brief   Check if a directory exists within another directory.
     *
     * @param   needle      The child directory.
     * @param   haystack    Full or relative path to the parent directory.
     *
     * @return  True if a directory exists within the haystack with the name 'needle', false if it
     *          does not exist or id not a directory.
     */

    bool dirInDir(const std::string &needle, const std::string &haystack) const;

    /**
     * @fn  bool PathTranslator::isDirectory(const struct dirent *ent) const;
     *
     * @brief   Check if a directory entry is a directory or not.
     *
     * @param   ent The dirent which may or may not be a directory.
     *
     * @return  True if it is a directory, false otherwise.
     */

    bool isDirectory(const struct dirent *ent) const;

    /**
     * @fn  bool PathTranslator::fileExists(const std::string &path) const;
     *
     * @brief   Check if a file exists at the given path.
     *
     * @param   path    Full or relative path to the file.
     *
     * @return  True if it exists, false if it doesn't.
     */

    bool fileExists(const std::string &path) const;
};
