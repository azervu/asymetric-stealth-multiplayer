/**
 * @file    source/shared/SpriteManager.cpp
 *
 * @brief   Implements the sprite manager class.
 */

#include "SpriteManager.h"

#if defined(ASM_CLIENT) || defined(ASM_UNITTEST)
    #include "ClientServiceLocator.h"
    #include "ResourceManager.h"
#endif

#include "Log.h"
#include "ServiceLocator.h"
#include "PathTranslator.h"

#include <rapidjson/document.h>
#include <rapidjson/filestream.h>

static const char* TEXTURE_KEY = "Texture";
static const char* SPRITES_KEY = "Spites";

static const char* SPRITE_NAME_KEY = "Name";
static const char* SPRITE_IMAGES_KEY = "Images";
static const char* X_KEY = "x";
static const char* Y_KEY = "y";
static const char* WIDTH_KEY = "Width";
static const char* HEIGHT_KEY = "Height";

std::hash<std::string> ID_HASHER;

SpriteManager::Manager::Manager() {
}

SpriteManager::Manager::~Manager() {
    for (Sheet* sheet : this->_sheets) {
        delete sheet;
    }

    for (Sprite* sprite : this->_sprites) {
        delete sprite;
    }
}

bool SpriteManager::Manager::parseSpriteSheet(const std::string &path) {
    FILE* file = ServiceLocator::singleton()->getPathTranslator()->openCHandle(path);
    if (!file) {
        Log::error("SpriteManager::parseSpriteSheet: Nonexistent file %s", path.c_str());
        return false;
    }

    rapidjson::FileStream fileStream(file);
    rapidjson::Document document;
    document.ParseStream<0>(fileStream);
    fclose(file);

    bool validDocument = true;

    validDocument &= document.HasMember(TEXTURE_KEY) && document[TEXTURE_KEY].IsString();
    validDocument &= document.HasMember(SPRITES_KEY) && document[SPRITES_KEY].IsArray();

    if (!validDocument) {
        Log::error("SpriteManager::parseSpriteSheet: Invalid JSON file %s", path.c_str());
        return false;
    }

    Sheet* sheet = new Sheet();
    sheet->_textureId = ID_HASHER(document[TEXTURE_KEY].GetString());//(Texture*) _resourceManager->getResource(ResourceType::TEXTURE, document[TEXTURE_KEY].GetString());
    sheet->_loaded = false;

#ifdef ASM_CLIENT
    ResourceManager* resourceManager = ClientServiceLocator::singleton()->getResourceManager();
    auto textureLoaded = resourceManager->queueResource(ResourceType::TEXTURE, document[TEXTURE_KEY].GetString());
#endif //ASM_CLIENT

    std::vector<Sprite*> loadedSprites;
    const auto& sprites = document[SPRITES_KEY];

    for (size_t i = 0; i < sprites.Size(); i++) {

        if (!sprites[i].IsObject()) {
            Log::error("SpriteManager::parseSpriteSheet: Malformed JSON file, %s[%i] is not an object. skipping", SPRITES_KEY, i);
            continue;
        }

        const auto& entry = sprites[i];

        bool validEntry = true;

        validEntry &= entry.HasMember(SPRITE_NAME_KEY) && entry[SPRITE_NAME_KEY].IsString();
        validEntry &= entry.HasMember(SPRITE_IMAGES_KEY) && entry[SPRITE_IMAGES_KEY].IsArray();
        validEntry &= entry.HasMember(WIDTH_KEY) && entry[WIDTH_KEY].IsInt();
        validEntry &= entry.HasMember(HEIGHT_KEY) && entry[HEIGHT_KEY].IsInt();

        if (!validEntry) {
            Log::error("SpriteManager::parseSpriteSheet: Invalid invalid sprite entry %i in file %s, skipping", i, path.c_str());
            continue;
        }

        Sprite* sprite = new Sprite();

        sprite->_sheet = sheet;
        sprite->_name = std::string(entry[SPRITE_NAME_KEY].GetString());
        sprite->_width = entry[WIDTH_KEY].GetInt();
        sprite->_height = entry[HEIGHT_KEY].GetInt();

        const auto& images = entry[SPRITE_IMAGES_KEY];

        for (size_t j = 0; j < images.Size(); j++) {

            const auto& imageEntry = images[j];

            validEntry &= imageEntry.HasMember(X_KEY) && imageEntry[X_KEY].IsInt();
            validEntry &= imageEntry.HasMember(Y_KEY) && imageEntry[Y_KEY].IsInt();

            if (!validEntry) {
                break;
            }

            sprite->_images.push_back({imageEntry[X_KEY].GetInt(), imageEntry[Y_KEY].GetInt()});
        }

        if (validEntry) {
            loadedSprites.push_back(sprite);
        } else {
            Log::error("SpriteManager::parseSpriteSheet: Invalid invalid sprite image entries %i in file %s, skipping", i, path.c_str());
        }
    }

    this->_sheets.push_back(sheet);

    for (Sprite* s : loadedSprites) {
        this->_sprites.push_back(s);
        this->_spriteLookup[ID_HASHER(s->_name)] = s;
    }

#ifdef ASM_CLIENT
    delete textureLoaded.get();
#endif //ASM_CLIENT

    return true;
}

const SpriteManager::Sprite* SpriteManager::Manager::getSprite(const std::string &name) {
    return getSprite(ID_HASHER(name));
}

const SpriteManager::Sprite* SpriteManager::Manager::getSprite(size_t spriteId) {
    try {
        return _spriteLookup.at(spriteId);
    } catch (std::out_of_range e) {
        Log::error("SpriteManager::getSprite: SpriteId %u is not loaded", spriteId);
        return nullptr;
    }
}
