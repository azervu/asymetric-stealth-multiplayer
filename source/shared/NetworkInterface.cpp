#include "NetworkInterface.h"
#include "ServiceLocator.h"
#include "Protocol.h"
#include "SharedEvents.h"


NetworkInterface::NetworkInterface() 
    :   _udp(nullptr),
        _tcp(nullptr) {

    mapCallback("PlayerTransformUpdate", 
                BIND_PACKET_CALLBACK(NetworkInterface::onPlayerTransformIn));
    mapCallback("GrenadeThrow",
                BIND_PACKET_CALLBACK(NetworkInterface::onGrenadeThrow));
}

void NetworkInterface::setSocketTCP(SocketTCP *socket) {
    _tcp = socket;
}

void NetworkInterface::setSocketUDP(SocketUDP *socket) {
    _udp = socket;
}

void NetworkInterface::mapCallback(std::string pktName, PacketCallback cb) {
    PacketFactory *pf = ServiceLocator::singleton()->getPacketFactory();   

    unsigned id = pf->getPacketId(pktName);
    if (id == 0) {
        Log::warning("Unable to map callback for packet '%s': does not exist",
                     pktName.c_str());
        return;
    }

    if (_pktMap.count(id) != 0) {
        Log::warning("Attempted to add multiple callbacks for '%s'!",
                     pktName.c_str());
        return;
    }

    _pktMap[id] = cb;
}

void NetworkInterface::dispatchPacketQueue() {
    std::vector<Packet*> pktBuffer;
    
    if (_tcp != nullptr) {
        for (int i=0; i<_tcp->getIncomingPacketQueueSize(); i++) {
            pktBuffer.push_back(_tcp->popPacket());
        }
    }

    if (_udp != nullptr) {
        for (int i=0; i<_udp->getIncomingPacketQueueSize(); i++) {
            pktBuffer.push_back(_udp->popPacket());
        }
    }

    for (Packet *packet : pktBuffer) {
        unsigned id = packet->getUnsigned("id");
        
        if (_pktMap.count(id) != 0) {
            PacketCallback callback = _pktMap[id];
            callback(packet);
        } else {
            Log::warning("NetworkInterface: Unhandled packet: '%s'", 
                         packet->getName().c_str());
        }

        delete packet;
    }
}


void NetworkInterface::onPlayerTransformIn(const Packet *packet) {
    PlayerId id = packet->getUnsigned("playerId");
    Vec2 pos(packet->getFloat("x"), packet->getFloat("y"));
    Vec2 vel(packet->getFloat("velX"), packet->getFloat("velY"));
    float angVel = packet->getFloat("angVel");
    float rot = packet->getFloat("rotation");
    unsigned step = packet->getUnsigned("step");
    NetworkDirection dir = NetworkDirection::NET_IN;

    PlayerTransformUpdateEvent *evt;
    evt = new PlayerTransformUpdateEvent(id, pos, vel, angVel, rot, dir, step);

    EventManager *emgr = ServiceLocator::singleton()->getEventManager();
    emgr->triggerEvent(evt);
}

void NetworkInterface::onGrenadeThrow(const Packet *pkt) {
    Vec2 pos(pkt->getFloat("posX"), pkt->getFloat("posY"));
    float rot = pkt->getFloat("rot");
    PlayerId pid = pkt->getUnsigned("playerId");

    auto evt = new GrenadeThrowEvent(NetworkDirection::NET_IN, pid, pos, rot);
    auto mgr = ServiceLocator::singleton()->getEventManager();
    mgr->queueEvent(evt);
}
