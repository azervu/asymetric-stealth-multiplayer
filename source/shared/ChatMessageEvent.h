/**
 * @file    source/client/ChatMessageEvent.h
 *
 * @brief   Declares the chat message event class.
 */

#pragma once

#include <string>

#include "EventManager.h"

class MessageSendEvent : public EventData {
public:
    static const EventType eventType;

    MessageSendEvent(std::string message);

    EventType getEventType() const;
    std::string getMessage() const;

private:
    std::string _message;
};

class MessageReceivedEvent : public EventData {
public:
    static const EventType eventType;

    MessageReceivedEvent(std::string username, std::string message);

    EventType getEventType() const;
    std::string getUsername() const;
    std::string getMessage() const;

private:
    std::string _username;
    std::string _message;
};
