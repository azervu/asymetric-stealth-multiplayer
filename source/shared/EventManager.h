/**
 * @file    source/shared/EventManager.h
 *
 * @brief   Declares the event manager class that is responsible for facilitating communication between systems
 */

#pragma once

#include <list>
#include <map>
#include <string>

#include "FastDelegate.h"
#include "FastDelegateBind.h"
#include "ServiceLocator.h"


class EventData;

typedef long long EventType;
typedef fastdelegate::FastDelegate1<const EventData*> EventListenerDelegate;

typedef std::list<EventListenerDelegate> DelegateList;
typedef std::map<EventType, DelegateList> DelegateMap;
typedef std::list<EventData*> EventQueue;


const unsigned int EVENTMANAGER_NUM_QUEUES = 2;

/**
 * @class   EventManager
 *
 * @brief   The EventManager code is largely taken from the book Game Coding 
 *          4th ed., written by Mike McShaffry and David Graham. The event 
 *          manager operates on two buffers. The back buffer is where all events
 *          are stored. When it's time to dispatch the events (update()-method),
 *          the buffers are switched and the contents of the front buffer is 
 *          issued to the listeners.
 */

class EventManager : public Service {
public:

    /**
     * @fn  EventManager::EventManager();
     *
     * @brief   Default constructor.
     */

    EventManager();

    /**
     * @fn  EventManager::~EventManager();
     *
     * @brief   Destructor.
     */

    ~EventManager();

    /**
     * @fn  bool EventManager::addListener(EventListenerDelegate delegate, EventType eventType);
     *
     * @brief   Add a delegate method as a listener to an event type. The delegate will be notified
     *          of all events with the specified type. All delegates added via this method must
     *          subsequently be removed by the 'removeListener()' method.
     *
     * @param   delegate    The delegate object to be notified.
     * @param   eventType   Type of the event.
     *
     * @return  True if the delegate was added, false if the delegate already was listening to this
     *          event type.
     */

    bool addListener(EventListenerDelegate delegate, EventType eventType);

    /**
     * @fn  bool EventManager::removeListener(EventListenerDelegate delegate, EventType eventType);
     *
     * @brief   Remove a delegate listening for ALL event types.
     *
     * @param   delegate    The delegate.
     *
     * @return  True if the delegate was successfully removed, false otherwise.
     */

    bool removeListener(EventListenerDelegate delegate);

    /**
     * @fn  bool EventManager::queueEvent(EventData *event);
     *
     * @brief   Add an event to the dispatch queue. The event will be sent to all registered
     *          listeners on the next update tick. The EventManager takes ownership of the EventData
     *          object and deletes it after dispatching it. The event is added to the queue and
     *          attempted dispatched regardless of whether anyone is listening for it's type at the
     *          point of adding.
     *
     * @param [in,out]  event   If non-null, the event.
     *
     * @return  True if anyone is listening to this type of event, false otherwise. The event is
     *          queued regardless.
     */

    bool queueEvent(EventData *event);

    /**
     * @fn  bool EventManager::triggerEvent(EventData *event);
     *
     * @brief   Instantly trigger an event for all registered listeners. The EventManager takes
     *          ownership of the EventData object and deletes it after dispatching it.
     *
     * @param [in,out]  event   If non-null, the event.
     *
     * @return  True if the event were delivered to at least one delegate, false otherwise.
     */

    bool triggerEvent(EventData *event);

    /**
     * @fn  bool EventManager::abortEvent(EventType eventType, bool removeAll = false);
     *
     * @brief   Abort one or all events of a given type. If removeAll is true,
     *          both buffers are cleared of the event. If removeAll is false, 
     *          only the last added event on the back buffer is removed (if one
     *          exists at all - the front buffer is not looked at).
     *
     * @param   eventType   The event type to remove.
     * @param   removeAll   (Optional) If true, removes all events of the type. If false, only the
     *                      event last queued is removed.
     *
     * @return  true if any events were removed, false otherwise.
     */

    bool abortEvent(EventType eventType, bool removeAll = false);

    /**
     * @fn  void EventManager::update();
     *
     * @brief   Dispatch all events in the event queue.
     */

    void update();

    /**
     * Returns the full list of un-dispatched, queued events.
     */
    const EventQueue& peekEvents() const;

private:

    /**
     * @fn  bool EventManager::isListener(EventListenerDelegate delegate, EventType type);
     *
     * @brief   Check if a delegate is listening for certain events.
     *
     * @param   delegate    The delegate.
     * @param   type        The type.
     *
     * @return  True if the delegate is listening for this event, false otherwise.
     */

    bool isListener(EventListenerDelegate delegate, EventType type);

    /**
     * @brief   There are two vectors used for the event queue. This allows the delegate methods
     *          notified by events in the queue being drained to raise new events which will be
     *          dispatched in the next cycle.
     */

    EventQueue _eventQueue[EVENTMANAGER_NUM_QUEUES];

    /**
     * The index contained in _activeQueue is the event back buffer.
     */
    int _activeQueue;


    DelegateMap _listeners;
};

/**
 * @class   EventData
 *
 * @brief   An event data.
 */

class EventData {
public:
    virtual ~EventData() {}

    /**
     * The ID of the event must be defined by the subclasses. ALL SUBCLASSES
     * OF EVENTS ARE REQUIRED TO HAVE A PUBLIC, STATIC CONST EventType NAMED
     * "eventType", AND RETURN THIS FROM THEIR IMPLEMENTATION OF THIS METHOD.
     *
     * The ID itself must be unique. To ensure this, use Visual Studio's
     * "Create GUID", or Python's uid.uuid4(). The number should be 64 bits in
     * length.
     */
    virtual EventType getEventType() const = 0;
    
    /**
     * The name is retrieved by using the typeid-operator.
     */
    std::string getEventName() const;
};


