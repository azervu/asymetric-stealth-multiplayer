#include "Player.h"
#include "StatsComponent.h"
#include "PhysicsComponent.h"
#include "DrawableComponent.h"
#include "GrenadeThrowComponent.h"
#include "CollisionFlags.h"
#include <Box2D/Box2D.h>

#ifdef ASM_CLIENT
#include "GuiObject.h"
#include "ClientEvents.h"
#endif //ASm_CLIENT

/*
==================
Player
==================
*/
Player::Player(Team team, PlayerId id, PlayerType ptype, 
               std::string name, b2World *world) 
    :   _team(team),
        _playerId(id),
        _playerName(name),
        _playerType(ptype),
        _b2world(world),
        _active(false),
        _health(100) {
    createStatsComponent(team);
    createPhysicsComponent();
    createDrawableComponent();
}

Team Player::getTeam() const {
    return _team;
}

PlayerId Player::getPlayerId() const {
    return _playerId;
}

std::string Player::getPlayerName() const {
    return _playerName;
}

PlayerType Player::getPlayerType() const {
    return _playerType;
}

bool Player::isActive() const {
    return _active;
}

int Player::getHealth() const {
    return _health;
}

void Player::setHealth(int health) {
    getComponent<StatsComponent>(this)->limitHp(health);
    _health = health;

#ifdef ASM_CLIENT
    if (_playerType == PlayerType::LOCAL) {
        EventManager *emgr = ServiceLocator::singleton()->getEventManager();
        emgr->queueEvent(new UpdateGuiEvent(_health, 1));
    }
#endif //ASm_CLIENT
}

b2Vec2 Player::getVelocity() {
    PhysicsComponent *pc = getComponent<PhysicsComponent>(this);
    if (pc) {
        return pc->getVelocity();
    }

    return b2Vec2_zero;
}

void Player::setVelocity(b2Vec2 velocity) {
    PhysicsComponent *pc = getComponent<PhysicsComponent>(this);
    if (pc) {
        pc->setLinearVelocity(velocity);
    }
}

float Player::getAngularVelocity() {
    PhysicsComponent *pc = getComponent<PhysicsComponent>(this);
    if (pc) {
        return pc->getAngularVelocity();
    }

    return 0.f;
}

void Player::setAngularVelocity(float angVel) {
    PhysicsComponent *pc = getComponent<PhysicsComponent>(this);
    if (pc) {
        pc->setAngularVelocity(angVel);
    }
}

void Player::setPosition(Vec2 position) {
    GameObject::setPosition(position);

    PhysicsComponent *pc = getComponent<PhysicsComponent>(this);
    if (pc) {
        pc->setPosition(position);
    }
}

void Player::setRotation(float rotation) {
    GameObject::setRotation(rotation);

    PhysicsComponent *pc = getComponent<PhysicsComponent>(this);
    if (pc) {
        pc->setRotation(rotation);
    }
}

void Player::spawn(Vec2 pos) {
    setPosition(pos);
    setHealth(1000);    //guaranteed to be more than the actual limit

    _active = true;

    createPhysicsComponent();
}

void Player::despawn() {
    _active = false;

    removeComponent<PhysicsComponent>(this);
}

void Player::createStatsComponent(Team team) {
	addComponent<StatsComponent>(this, team);
}

void Player::createPhysicsComponent() {
    // Don't add the physics component if one already exists
    if (getComponent<PhysicsComponent>(this) != nullptr) {
        return;
    }

    addComponent<PhysicsComponent>(this);
    getComponent<PhysicsComponent>(this)->createBody( 
                                   _b2world, 
                                   PhysicsComponent::Shape::ROUND, 
                                   PhysicsComponent::Type::DYNAMIC, 
                                   0.25);
    getComponent<PhysicsComponent>(this)->setPosition(getPosition());

    // Set up the collision filter
    b2Filter filter;
    filter.maskBits = CollisionFlag::WALL | 
                      CollisionFlag::MERC | 
                      CollisionFlag::SPY  |
                      CollisionFlag::TRIG_AUTO;
    filter.categoryBits = CollisionFlag::TRIG_AUTO;

    if (_playerType == PlayerType::LOCAL) {
        filter.maskBits |= CollisionFlag::TRIG_MANUAL;
        filter.categoryBits |= CollisionFlag::TRIG_MANUAL;
    }

    if (getTeam() == Team::SPY) {
        filter.categoryBits |= CollisionFlag::SPY;
    } else if (getTeam() == Team::MERC) {
        filter.categoryBits |= CollisionFlag::MERC;
        filter.maskBits |= CollisionFlag::VENT;
    }

    getComponent<PhysicsComponent>(this)->setCollisionFilter(filter);
}

void Player::createDrawableComponent() {
    std::string spriteName;

    if (this->getTeam() == Team::MERC) {
        spriteName  = "PlayerMerch";
    } else if (this->getTeam() == Team::SPY) {
        spriteName =  "PlayerSpy";
    } else {
        spriteName = "DebugPlayer";
    }

    spriteProvider = new SpriteProvider(spriteName, this);
    spriteProvider->setSize({0.5f, 0.5f});
    addComponent<DrawableComponent>(this, spriteProvider, true);
}


/*
==================
PlayerComponent
==================
*/
Player* PlayerComponent::getPlayer() const {
    return (Player*)getGameObject();
}

Team PlayerComponent::getTeam() const {
    return getPlayer()->getTeam();
}

PlayerId PlayerComponent::getPlayerId() const {
    return getPlayer()->getPlayerId();
}
