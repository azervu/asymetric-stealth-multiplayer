#pragma once

#include "Socket.h"

#include <functional>


/**
 * NetworkInterface is a class intended for subclassing in classes that are 
 * logically placed on the frontier of the network. It provides interfaces for
 * mapping packet names to methods and an interface for dispatching all packets
 * on the queue to said methods. 
 *
 * Use BIND_PACKET_CALLBACK(method) to bind subclass packet callback methods to
 * an std::function object.
 *
 * NetworkInterface handles the following packets by default, as their
 * implementations would otherwise be identical on both the client and server:
 *  -   PlayerTransformUpdate
 *      Triggers a PlayerTransformEvent-instance immediately upon arrival.
 */
class NetworkInterface {
protected:
    typedef std::function<void(const Packet*)> PacketCallback;

    NetworkInterface();

    /**
     * Set the TCP-connection to be used. NetworkInterface does NOT own the
     * object after calling this method!
     */
    void setSocketTCP(SocketTCP *socket);

    /**
     * Set the UDP-connection to be used. NetworkInterface does NOT own the
     * object after calling this method!
     */
    void setSocketUDP(SocketUDP *socket);

    void mapCallback(std::string packetName, PacketCallback callback);

    /**
     * Pop EVERY packet on both the UDP and TCP connection (if exists) and 
     * dispatches them to the mapped callback methods, given that they are
     * defined.
     *
     * Remember this when using this interface:
     *  1. Call 'SocketBase::parseTrafficData()' manually on both sockets.
     *  2. Update your conversation-instances BEFORE calling this method.
     *  3. parseTrafficData() is _NOT_ called on either of the sockets. This is
     *     to avoid accidentally attempting to dispatch packets required by
     *     conversations that arrived AFTER updating convos, but BEFORE this
     *     method was called.
     */
    void dispatchPacketQueue();

private:
    std::map<unsigned,PacketCallback> _pktMap;
    SocketTCP *_tcp;
    SocketUDP *_udp;

    void onPlayerTransformIn(const Packet *pkt);
    void onGrenadeThrow(const Packet *pkt);
};

/**
 * Bind NetworkInterfaceSubclass::method to a PacketCallback object.
 */
#define BIND_PACKET_CALLBACK(method)                                    \
                std::bind(&method, this, std::placeholders::_1)
