#include "GrenadeThrowComponent.h"
#include "ServiceLocator.h"
#include "SharedEvents.h"
#include "BodyCreator.h"
#include "Scene.h"
#include "Player.h"

#ifdef ASM_CLIENT
    #include "ClientEvents.h"
    #include "ResourceManager.h"
#endif


/*
==================
GrenadeThrowComponent
==================
*/
GrenadeThrowComponent::GrenadeThrowComponent(b2World *world, GrenadeType type) 
    :   _world(world),
        _type(type),
        _player(nullptr) {
    auto emgr = ServiceLocator::singleton()->getEventManager();   

#ifdef ASM_CLIENT
    _inputDelegate.bind(this, &GrenadeThrowComponent::onButtonClicked);
    emgr->addListener(_inputDelegate, ButtonEvent::eventType);
#endif // ASM_CLIENT

    _throwDelegate.bind(this, &GrenadeThrowComponent::onGrenadeThrow);
    emgr->addListener(_throwDelegate, GrenadeThrowEvent::eventType);

}

GrenadeThrowComponent::~GrenadeThrowComponent() {
    auto emgr = ServiceLocator::singleton()->getEventManager();   
    emgr->removeListener(_throwDelegate);

#ifdef ASM_CLIENT
    emgr->removeListener(_inputDelegate);
#endif // ASM_CLIENT
}

void GrenadeThrowComponent::update(DeltaTime dt) {
    
}


#ifdef ASM_CLIENT
void GrenadeThrowComponent::onButtonClicked(const EventData *e) {
    if (getPlayer()->getPlayerType() != PlayerType::LOCAL) 
        return;

    ButtonEvent *evt = (ButtonEvent*)e;

    if (evt->getButton() == SDL_BUTTON_RIGHT && evt->isKeyDown()) {
        if (_timer.getElapsedSeconds() < GRENADE_COOLDOWN) {
            return;
        }

        _timer.start();
        throwGrenade();

        auto emgr = ServiceLocator::singleton()->getEventManager();
        auto evt = new GrenadeThrowEvent(NetworkDirection::NET_OUT,
                                         getPlayer()->getPlayerId(),
                                         getPlayer()->getWorldPosition(),
                                         getPlayer()->getWorldRotation());
        emgr->queueEvent(evt);
    }
}
#endif // ASM_CLIENT

void GrenadeThrowComponent::onGrenadeThrow(const EventData *e) {
    // Don't react on local players
    if (getPlayer()->getPlayerType() != PlayerType::REMOTE) 
        return;

    GrenadeThrowEvent *evt = (GrenadeThrowEvent*)e;

    // Only react if the remote counterpart was the thrower and the event
    // was fired remotely.
    if (evt->getPlayerId() != getPlayer()->getPlayerId() ||
        evt->getNetworkDirection() != NetworkDirection::NET_IN) {
        return;
    }

    throwGrenade(evt->getPosition(), evt->getRotation());
}

void GrenadeThrowComponent::throwGrenade() {
    GameObject *gob = getGameObject();
    Vec2 pos = gob->getWorldPosition();
    float rot = gob->getWorldRotation();
    
    throwGrenade(pos, rot);
}

void GrenadeThrowComponent::throwGrenade(Vec2 pos, float rot) {
    Log::debug("Throwing grenade!!");

    GameObject *gob = getGameObject();
    GameObject *g = nullptr; 
    PlayerId pid = getPlayer()->getPlayerId();

    switch (_type) {
        case GrenadeType::FLASHBANG:
            g = new Grenade<FlashbangComponent>(_world, pid, pos, rot);
            break;

        default:
            return;
    }

    gob->getScene()->addGameObject(g);
}

Player* GrenadeThrowComponent::getPlayer() {
    if (_player) 
        return _player;

    GameObject *gob = getGameObject();
    Player *player = nullptr;

#ifdef ASM_DEBUG
    player = dynamic_cast<Player*>(gob);
    assert(player);
#else
    player = (Player*)gob;
#endif

    _player = player;

    return player;
}



/*
==================
FlashbangComponent
==================
*/
void FlashbangComponent::onExplode() {
#ifdef ASM_CLIENT
    ResourceId rid = hashResource("audio/flashexp.ogg");

    auto emgr = ServiceLocator::singleton()->getEventManager();
    emgr->queueEvent(new CreateSoundEvent(rid, getGameObject()->getWorldPosition()));
#endif // ASM_CLIENT
}


GrenadeType FlashbangComponent::getGrenadeType() {
    return GrenadeType::FLASHBANG;
}

std::string FlashbangComponent::getSpriteName() {
    return "Flashbang";
}

float FlashbangComponent::getFuseTime() {
    return 1.f;
}
