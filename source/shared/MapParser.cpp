#include "MapParser.h"
#include "PathTranslator.h"
#include "ServiceLocator.h"

#include <sstream>
#include <list>

// Some Windows header defines the symbol "GetObject" to "GetObjectA". This
// collides with the TmxParser interface.
#ifdef GetObject
    #undef GetObject
#endif //GetObject

// textures
static const std::string TERRAIN_TILESET = "Terrain";
static const std::string OBJECT_TILESET = "Objects";

// things
static const std::string LIGHT_GROUP = "lights";
static const std::string TRIGGER_GROUP = "triggers";

// look and feel
static const std::string BACKGROUND_LAYER = "background";
static const std::string FOREGROUND_LAYER = "foreground";

// flags and such
static const std::string WALL_LAYER = "walls";
static const std::string AREA_LAYER = "areas";
static const std::string OBJECTIVE_LAYER = "objectives";
static const std::string SPAWN_LAYER = "spawns";
static const std::string VISION_GROUP = "vision";

// Trigger attributes
static const std::string TRIG_ACTION = "action";
static const std::string TRIG_ACTION_PRINT = "print";
static const std::string TRIG_ACTION_PLAYAMB = "playAmbient";
static const std::string TRIG_ACTION_OBJECTIVE = "objective";
static const std::string TRIG_PARAM_MESSAGE = "message";
static const std::string TRIG_PARAM_TRACK = "track";
static const std::string TRIG_PARAM_OBJ_ID = "id";

static const float TRIG_OBJECTIVE_SIZE = 1.7f;

static const std::string TRIG_RUDE_MSG = "Someone needs to stop taking "
                                         "stupid pills and start taking "
                                         "remember-how-to-create-maps-"
                                         "pills.";
 



/*
================
MapParser Public
================
*/
MapParser::MapParser()
	:	_mapFile(""),
		_mapset(nullptr),
		_objset(nullptr)
{
}

MapParser::~MapParser() {
	
}


Map* MapParser::parseMap(std::string mapFile) {
    _parsedLayers.clear();
    
	// Locate mapset and objectset
    PathTranslator *pt = ServiceLocator::singleton()->getPathTranslator();
	_tmxMap.ParseFile(pt->translatePath(mapFile));
	
	if (_tmxMap.HasError()) {
		Log::error("Failed to parse map: %s", _tmxMap.GetErrorText().c_str());
        return nullptr;
	}
	
    std::vector<Tmx::Tileset*> tsets = _tmxMap.GetTilesets();
    for (Tmx::Tileset *tset : tsets) {
        if (tset->GetName() == TERRAIN_TILESET) {
			_mapset = tset;
        } else if (tset->GetName() == OBJECT_TILESET) {
			_objset = tset;
		}
	}
	
	if (!_mapset || !_objset) {
		Log::error("Failed to find the required tilesets");
        _mapset = nullptr;
        _objset = nullptr;
        return nullptr;
	}

    Map *map = new Map(_tmxMap.GetWidth(), _tmxMap.GetHeight());

	const std::vector<Tmx::Layer*> layers = _tmxMap.GetLayers();
    for (Tmx::Layer *layer : layers) {
        // Assume parsed, remove later if we cannot parse it
        _parsedLayers.insert(layer->GetName());

        if (layer->GetName() == BACKGROUND_LAYER) {
			MapLayer *ml = parseDrawableLayer(layer);
			map->setBackgroundLayer(ml);
        } else if (layer->GetName() == FOREGROUND_LAYER) {
			MapLayer *ml = parseDrawableLayer(layer);
			map->setForegroundLayer(ml);
        } else if (layer->GetName() == WALL_LAYER) {
			parseWallLayer(layer, map);
        } else if (layer->GetName() == AREA_LAYER) {
            parseAreaLayer(layer, map);
        } else if (layer->GetName() == OBJECTIVE_LAYER) {
            parseObjectiveLayer(layer, map);
        } else if (layer->GetName() == SPAWN_LAYER) {
            parseSpawnLayer(layer, map);
        } else {
            // Remove from parsed-list
            _parsedLayers.erase(layer->GetName());
			Log::warning("Unable to parse Layer: %s", layer->GetName().c_str());
		}
	}

	for (int i = 0; i < _tmxMap.GetNumObjectGroups(); i++) {
		const Tmx::ObjectGroup *objGroup = _tmxMap.GetObjectGroup(i);

        if (objGroup->GetName() == LIGHT_GROUP) {
            _parsedLayers.insert(objGroup->GetName());
			parseLightLayer(objGroup, map);
		} else if (objGroup->GetName() == TRIGGER_GROUP) {
            _parsedLayers.insert(objGroup->GetName());

            #ifdef ASM_CLIENT
            // Only include the trigger layer on the client application
            parseTriggerLayer(objGroup, map);
            #endif
        } else if (objGroup->GetName() == VISION_GROUP) {
            _parsedLayers.insert(objGroup->GetName());
            parseVisionLayer(objGroup, map);
        } else {
			Log::debug("Unable to parse ObjectGroup: %s", objGroup->GetName().c_str());
		}
	}

    _objset = nullptr;
    _mapset = nullptr;

    if (!validateMap(map)) {
        delete map;
        return nullptr;
    }
	
	return map;
}


MapLayer* MapParser::parseDrawableLayer(Tmx::Layer *layer) {
    std::string imgFile = _mapset->GetImage()->GetSource();

    MapLayer *mapLayer = new MapLayer(_tmxMap.GetWidth(), _tmxMap.GetHeight(), imgFile);
	
	for (int x = 0; x < _tmxMap.GetWidth(); x++) {
		for (int y = 0; y < _tmxMap.GetHeight(); y++) {
			int gid = layer->GetTileGid(x, y);
			
			Rect texClip = getTextureClip(_mapset, gid);
			Rect worldClip = getWorldClip(_mapset, x, y);

            mapLayer->setTileTexIndex(x, y, gid);
		}
	}
	
	return mapLayer;
}

Rect MapParser::getTextureClip(Tmx::Tileset* set, int gid) {
	int imgw = _mapset->GetImage()->GetWidth();
	int imgh = _mapset->GetImage()->GetHeight();
	
	int xcount = imgw / set->GetTileWidth();
	int ycount = imgh / set->GetTileHeight();
	int id = gid - set->GetFirstGid();
	
	// Tiles go from left to right, top to bottom.
	//  1  2  3
	//  4  5  6
	int tilex = id % xcount;
	int tiley = id / ycount;
	
	Rect texClip;
	texClip.x = tilex * set->GetTileWidth();
	texClip.y = tiley * set->GetTileHeight();
	texClip.w = set->GetTileWidth();
	texClip.h = set->GetTileHeight();
	
	// Divide all fields by the texture dimensions so the
	// coordinates are all in the range [0-1]
	texClip.x /= (float)imgw;
	texClip.y /= (float)imgh;
	texClip.w /= (float)imgw;
	texClip.h /= (float)imgh;
	
	return texClip;
}

Rect MapParser::getWorldClip(Tmx::Tileset* set, int x, int y) {
	float xpos = x * set->GetTileWidth();
	float ypos = y * set->GetTileHeight();
	
	Rect wClip;
	wClip.x = xpos;
	wClip.y = ypos;
	wClip.w = set->GetTileWidth();
	wClip.h = set->GetTileHeight();
	
	return wClip;
}


void MapParser::parseWallLayer(Tmx::Layer *layer, Map* map) {
	for (int x = 0; x < _tmxMap.GetWidth(); x++) {
		for (int y = 0; y < _tmxMap.GetHeight(); y++) {
			Tmx::MapTile tile = layer->GetTile(x, y);
            if (tile.gid == 0) {
                continue;
            }

            switch (tile.id) {
                case ID_WALL_NW_NE_SE_SW:
                    map->addTileFlags(x, y, TileFlag::TILE_WALL);
                    map->addTileFlags(x, y, TileFlag::TILE_SHADOW);
                    map->addTileFlags(x, y, TileFlag::TILE_WALL_NW);
                    map->addTileFlags(x, y, TileFlag::TILE_WALL_NE);
                    map->addTileFlags(x, y, TileFlag::TILE_WALL_SE);
                    map->addTileFlags(x, y, TileFlag::TILE_WALL_SW);
                    break;

                case ID_WALL_NW_SW:
                    map->addTileFlags(x, y, TileFlag::TILE_WALL);
                    map->addTileFlags(x, y, TileFlag::TILE_SHADOW);
                    map->addTileFlags(x, y, TileFlag::TILE_WALL_NW);
                    map->addTileFlags(x, y, TileFlag::TILE_WALL_SW);
                    break;

                case ID_WALL_SE_SW:
                    map->addTileFlags(x, y, TileFlag::TILE_WALL);
                    map->addTileFlags(x, y, TileFlag::TILE_SHADOW);
                    map->addTileFlags(x, y, TileFlag::TILE_WALL_SE);
                    map->addTileFlags(x, y, TileFlag::TILE_WALL_SW);
                    break;

                case ID_WALL_NE_SE:
                    map->addTileFlags(x, y, TileFlag::TILE_WALL);
                    map->addTileFlags(x, y, TileFlag::TILE_SHADOW);
                    map->addTileFlags(x, y, TileFlag::TILE_WALL_NE);
                    map->addTileFlags(x, y, TileFlag::TILE_WALL_SE);
                    break;

                case ID_WALL_NW_NE:
                    map->addTileFlags(x, y, TileFlag::TILE_WALL);
                    map->addTileFlags(x, y, TileFlag::TILE_SHADOW);
                    map->addTileFlags(x, y, TileFlag::TILE_WALL_NW);
                    map->addTileFlags(x, y, TileFlag::TILE_WALL_NE);
                    break;

                case ID_WALL_SW:
                    map->addTileFlags(x, y, TileFlag::TILE_WALL);
                    map->addTileFlags(x, y, TileFlag::TILE_SHADOW);
                    map->addTileFlags(x, y, TileFlag::TILE_WALL_SW);
                    break;

                case ID_WALL_SE:
                    map->addTileFlags(x, y, TileFlag::TILE_WALL);
                    map->addTileFlags(x, y, TileFlag::TILE_SHADOW);
                    map->addTileFlags(x, y, TileFlag::TILE_WALL_SE);
                    break;

                case ID_WALL_NE:
                    map->addTileFlags(x, y, TileFlag::TILE_WALL);
                    map->addTileFlags(x, y, TileFlag::TILE_SHADOW);
                    map->addTileFlags(x, y, TileFlag::TILE_WALL_NE);
                    break;

                case ID_WALL_NW:
                    map->addTileFlags(x, y, TileFlag::TILE_WALL);
                    map->addTileFlags(x, y, TileFlag::TILE_SHADOW);
                    map->addTileFlags(x, y, TileFlag::TILE_WALL_NW);
                    break;

                case ID_WALL_NE_SW:
                    map->addTileFlags(x, y, TileFlag::TILE_WALL);
                    map->addTileFlags(x, y, TileFlag::TILE_SHADOW);
                    map->addTileFlags(x, y, TileFlag::TILE_WALL_NE);
                    map->addTileFlags(x, y, TileFlag::TILE_WALL_SW);
                    break;

                case ID_WALL_NW_SE:
                    map->addTileFlags(x, y, TileFlag::TILE_WALL);
                    map->addTileFlags(x, y, TileFlag::TILE_SHADOW);
                    map->addTileFlags(x, y, TileFlag::TILE_WALL_NW);
                    map->addTileFlags(x, y, TileFlag::TILE_WALL_SE);
                    break;

                case ID_WALL_NW_SE_SW:
                    map->addTileFlags(x, y, TileFlag::TILE_WALL);
                    map->addTileFlags(x, y, TileFlag::TILE_SHADOW);
                    map->addTileFlags(x, y, TileFlag::TILE_WALL_NW);
                    map->addTileFlags(x, y, TileFlag::TILE_WALL_SE);
                    map->addTileFlags(x, y, TileFlag::TILE_WALL_SW);
                    break;

                case ID_WALL_NE_SE_SW:
                    map->addTileFlags(x, y, TileFlag::TILE_WALL);
                    map->addTileFlags(x, y, TileFlag::TILE_SHADOW);
                    map->addTileFlags(x, y, TileFlag::TILE_WALL_NE);
                    map->addTileFlags(x, y, TileFlag::TILE_WALL_SE);
                    map->addTileFlags(x, y, TileFlag::TILE_WALL_SW);
                    break;

                case ID_WALL_NW_NE_SE:
                    map->addTileFlags(x, y, TileFlag::TILE_WALL);
                    map->addTileFlags(x, y, TileFlag::TILE_SHADOW);
                    map->addTileFlags(x, y, TileFlag::TILE_WALL_NW);
                    map->addTileFlags(x, y, TileFlag::TILE_WALL_NE);
                    map->addTileFlags(x, y, TileFlag::TILE_WALL_SE);
                    break;

                case ID_WALL_NW_NE_SW:
                    map->addTileFlags(x, y, TileFlag::TILE_WALL);
                    map->addTileFlags(x, y, TileFlag::TILE_SHADOW);
                    map->addTileFlags(x, y, TileFlag::TILE_WALL_NW);
                    map->addTileFlags(x, y, TileFlag::TILE_WALL_NE);
                    map->addTileFlags(x, y, TileFlag::TILE_WALL_SW);
                    break;
                
                case ID_VENT:
                    map->addTileFlags(x, y, TileFlag::TILE_VENT);
                    break;
            }
		}
	}
}

void MapParser::parseAreaLayer(const Tmx::Layer *layer, Map *map) {
    for (int x = 0; x < _tmxMap.GetWidth(); x++) {
        for (int y = 0; y < _tmxMap.GetHeight(); y++) {
            Tmx::MapTile tile = layer->GetTile(x, y);
            if (tile.gid == 0) {
                continue;
            }
            
            if (tile.id == ObjectsetTileID::ID_TERM_A) {
                map->addTileFlags(x, y, TileFlag::TILE_AREA_A);
            } else if (tile.id == ObjectsetTileID::ID_TERM_B) {
                map->addTileFlags(x, y, TileFlag::TILE_AREA_B);
            } else if (tile.id == ObjectsetTileID::ID_TERM_C) {
                map->addTileFlags(x, y, TileFlag::TILE_AREA_C);
            }
        }
    }
}

void MapParser::parseObjectiveLayer(const Tmx::Layer *layer, Map *map) {
    for (int x = 0; x < _tmxMap.GetWidth(); x++) {
        for (int y = 0; y < _tmxMap.GetHeight(); y++) {
            Tmx::MapTile tile = layer->GetTile(x, y);
            if (tile.gid == 0) {
                continue;
            }

            std::string objectiveId;

            if (tile.id == ObjectsetTileID::ID_TERM_A) {
                map->addTileFlags(x, y, TileFlag::TILE_OBJ_A);
                objectiveId = "a";
            } else if (tile.id == ObjectsetTileID::ID_TERM_B) {
                map->addTileFlags(x, y, TileFlag::TILE_OBJ_B);
                objectiveId = "b";
            } else if (tile.id == ObjectsetTileID::ID_TERM_C) {
                map->addTileFlags(x, y, TileFlag::TILE_OBJ_C);
                objectiveId = "c";
            } else {
                continue;
            }

            // Create a MapTriggerProperties-object. We need to "fake" having
            // parsed a trigger in order to create it.
            Vec2 mpos(x / ASM_TILES_PER_METER, y / ASM_TILES_PER_METER);

            MapTriggerProperties props;
            props.name = "__autoObjective " + objectiveId;
            props.position = mpos;
            props.triggerType = TriggerType::OBJECTIVE;
            props.attributes["action"] = "objective";
            props.attributes["id"] = objectiveId;
            
            // Fake some vertices - 
            const float halfw = TRIG_OBJECTIVE_SIZE / 2.f;
            props.vertices.push_back(Vec2(-halfw, -halfw));
            props.vertices.push_back(Vec2( halfw, -halfw));
            props.vertices.push_back(Vec2( halfw,  halfw));
            props.vertices.push_back(Vec2(-halfw,  halfw));

            map->_triggers.push_back(props);
        }
    }
}

void MapParser::parseSpawnLayer(const Tmx::Layer *layer, Map *map) {
    for (int x = 0; x < _tmxMap.GetWidth(); x++) {
        for (int y = 0; y < _tmxMap.GetHeight(); y++) {
            Tmx::MapTile tile = layer->GetTile(x, y);
            if (tile.gid == 0) {
                continue;
            }

            if (tile.id == ObjectsetTileID::ID_SPY_SPAWN) {
                map->addTileFlags(x, y, TileFlag::TILE_SPAWN_SPY);
            } else if (tile.id == ObjectsetTileID::ID_MERC_SPAWN) {
                map->addTileFlags(x, y, TileFlag::TILE_SPAWN_MERC);
            }
        }
    }
}


void MapParser::parseLightLayer(const Tmx::ObjectGroup *group, Map *map) {
	for (int i = 0; i < group->GetNumObjects(); i++) {
		const Tmx::Object *obj = group->GetObject(i);
        std::string type = obj->GetType();

		/* Handle all lights the same way - read from the Ellipse */
		const Tmx::Ellipse *ellipse = obj->GetEllipse();
		if (ellipse == NULL) {
			Log::warning("None-elliptic object in Layer 'lights' (name = %s, "
                         "type = %s)", obj->GetName().c_str(), type.c_str());
			continue;
		}

		// The light shape should be a perfect circle. The radius values returned
        // are in pixel values and must be converted to 'tile-space' lengths.
		float rx = ellipse->GetRadiusX() / (float)ASM_PIXELS_PER_METER;
		float ry = ellipse->GetRadiusY() / (float)ASM_PIXELS_PER_METER;
		if (fabs(rx - ry) > 0.01) {
			Log::warning("Non-perfect circle light in layer 'lights' (name = %s"
                         ", type = %s)", obj->GetName().c_str(), type.c_str());
            rx = std::max(rx, ry);
            ry = rx;
		}

		// Read the color
		Color color;
		const Tmx::PropertySet &set = obj->GetProperties();
		
		if (set.HasProperty("color")) {
            std::stringstream ss(set.GetLiteralProperty("color"));
            ss >> color.r >> color.g >> color.b >> color.a;
		} else {
            color.r = 1.f;
            color.g = 1.f;
            color.b = 1.f;
            color.a = 1.f;
		}

		// GetCenterX() and GetCenterY() returns the position in pixel values.
        // As the coordinate system in the game is in 'tiles', divide the values
        // by the size of a tile.
		Vec2 pos(ellipse->GetCenterX(), ellipse->GetCenterY());
        pos.x /= (float)ASM_PIXELS_PER_METER;
        pos.y /= (float)ASM_PIXELS_PER_METER;

        MapLightProperties props;
        props.pos = pos;
		props.radius = rx;
		props.color = color;
		
		map->_lights.push_back(props);
	}

    Log::debug("Parsed %i lights", map->_lights.size());
}

void MapParser::parseTriggerLayer(const Tmx::ObjectGroup *group, Map *map) {
    static std::map<std::string,TriggerType> actionMap;
    if (!actionMap.size()) {
        actionMap[TRIG_ACTION_PRINT] = TriggerType::PRINT;
        actionMap[TRIG_ACTION_PLAYAMB] = TriggerType::PLAY_AMBIENT;
    }

    std::vector<Tmx::Object*> objs = group->GetObjects();

    for (Tmx::Object *obj : objs) {
        const Tmx::Polygon *poly = obj->GetPolygon();
        if (!poly) {
            Log::warning("Object %s in Trigger-group has no polygon",
                         obj->GetName().c_str());
            continue;
        }

        if (!poly->GetNumPoints()) {
            Log::error("Object %s in Trigger-group has no vertices",
                       obj->GetName().c_str());
            continue;
        }

        /* The vertices of the Polygon are in pixel-space, and must be
         * converted to metric coordinates. 
         */
        MapTriggerProperties trigger;
        trigger.name = obj->GetName();

        Vec2 pos(obj->GetX(), obj->GetY());
        pos.x /= (float)ASM_PIXELS_PER_METER;
        pos.y /= (float)ASM_PIXELS_PER_METER;
        trigger.position = pos;
        
        for (int i=0; i<poly->GetNumPoints(); i++) {
            Tmx::Point pt = poly->GetPoint(i);
            
            Vec2 vertex = Vec2(pt.x, pt.y);
            vertex.x /= (float)ASM_PIXELS_PER_METER;
            vertex.y /= (float)ASM_PIXELS_PER_METER;

            trigger.vertices.push_back(vertex);
        }

        const Tmx::PropertySet &ps = obj->GetProperties();
        trigger.attributes = ps.GetList();

        // If not action attribute is defined, insert a rude print action
        if (!trigger.attributes.count(TRIG_ACTION)) {
            Log::warning("MapParser: Trigger %s has no action", 
                         obj->GetName().c_str());
            
            trigger.attributes[TRIG_ACTION] = TRIG_ACTION_PRINT;
            trigger.attributes[TRIG_PARAM_MESSAGE] = TRIG_RUDE_MSG;
        }

        // Set the TriggerType 
        std::string action = trigger.attributes[TRIG_ACTION];
        if (actionMap.count(action)) {
            trigger.triggerType = actionMap[trigger.attributes[TRIG_ACTION]];   
        } else {
            trigger.triggerType = TriggerType::UNDEFINED;
        }

        // Add the trigger to the Map's list.
        map->_triggers.push_back(trigger);
    }
}

void MapParser::parseVisionLayer(const Tmx::ObjectGroup *group, Map *map) {
    for (int i=0; i<group->GetNumObjects(); i++) {
        const Tmx::Object *obj = group->GetObject(i);
        
        Rect rect;
        rect.x = (float)obj->GetX() / (float)ASM_PIXELS_PER_METER;
        rect.y = (float)obj->GetY() / (float)ASM_PIXELS_PER_METER;
        rect.w = (float)obj->GetWidth() / (float)ASM_PIXELS_PER_METER;
        rect.h = (float)obj->GetHeight() / (float)ASM_PIXELS_PER_METER;

        map->_visionBlocks.push_back(rect);
    }

    Log::debug("Parsed %i vision blockers", map->_visionBlocks.size());
}


bool MapParser::validateMap(Map *map) {
    bool ok = true;
    Log::debug("Validating map..");

    ok &= validateParsedLayers(map);
    ok &= validateObjectives(map);
    ok &= validateAreas(map);
    ok &= validateSpawns(map);
    ok &= validateTriggers(map);

    if (!ok) {
        Log::error("Map validation failed");
    } else {
        Log::debug("Map validated successfully");
    }

    return ok;
}

bool MapParser::validateParsedLayers(Map *map) {
    bool ok = true;

    std::vector<std::string> required = std::vector<std::string>({
        BACKGROUND_LAYER,
        FOREGROUND_LAYER,
        SPAWN_LAYER,
        OBJECTIVE_LAYER,
        AREA_LAYER
    });

    std::vector<std::string> optional = std::vector<std::string>({
        WALL_LAYER,
        LIGHT_GROUP,
        TRIGGER_GROUP,
        VISION_GROUP
    });

    for (std::string l : required) {
        if (_parsedLayers.find(l) == _parsedLayers.end()) {
            Log::error("Missing required layer: %s", l.c_str());
            ok = false;
        }
    }

    for (std::string l : optional) {
        if (_parsedLayers.find(l) == _parsedLayers.end()) {
            Log::warning("Missing optional layer: %s", l.c_str());
        }
    }

    return ok;
}

bool MapParser::validateObjectives(Map *map) {
    /* Objective tiles must be adjacent to at least one AREA-tile. The objective
     * tiles themselves must also be flagged as AREA.
     */
    int count[3]    = {0,0,0};
    TileFlag obj[3] = {TILE_OBJ_A, TILE_OBJ_B, TILE_OBJ_C};
    TileFlag exp[3] = {TILE_AREA_A, TILE_AREA_B, TILE_AREA_C}; 
    bool ok = true; 
    
    for (int x = 0; x < map->getWidth(); x++) {
        for (int y = 0; y < map->getHeight(); y++) {
            for (int i = 0; i < 3; i++) {
                if (map->hasFlag(x, y, obj[i])) {
                    if (map->hasFlag(x, y, exp[i])) {
                        count[i]++;
                    } else {
                        Log::error("Objective %c at [%i,%i] is not an area "
                                   "tile.", ('A'+i), x, y);
                        ok = false;
                    }

                    if (!neighbouring(map, x, y, exp[i])) {
                        Log::error("Objective %c at [%i,%i] has no adjacent "
                                   "area tile", ('A'+i), x, y);
                        ok = false;
                    }
                }
            }
        }
    }

    for (int i=0; i<3; i++) {
        if (count[i] != 1) {
            Log::error("Objective %c is defined %i times (1 is the only "
                       "allowed number)", ('A'+i), count[i]);
            ok = false;
        }
    }

    return ok;
}

bool MapParser::validateAreas(Map *map) {
    /* 
     * There must be at least two area-tiles per objective, all area tiles per
     * objective must be adjacent to eachother.
     */
    bool ok             = true;
    int count[3]        = {0, 0, 0};
    TileFlag areas[3]   = {TILE_AREA_A, TILE_AREA_B, TILE_AREA_C};

    for (int x = 0; x < map->getWidth(); x++) {
        for (int y = 0; y < map->getHeight(); y++) {
            for (int i = 0; i < 3; i++) {
                if (map->hasFlag(x, y, areas[i])) {
                    count[i]++;
                    if (!neighbouring(map, x, y, areas[i])) {
                        Log::error("Tile %i %i (area %c) is an area-island",
                                   x, y, ('A' + i));
                        ok = false;
                    }
                }
            }
        }
    }

    for (int i=0; i<3; i++) {
        if (count[i] <= 1) {
            Log::error("There are only %i area tiles defined for objective %c,"
                       " at least 2 is required.", count[i], 'A'+i);
            ok = false;
        }
    }

    return ok;
}

bool MapParser::validateSpawns(Map *map) {
    /**
     * There must be at least two spawns defined per team. A tile can not be
     * defined as both SPAWN_SPY and SPAWN_MERC.
     */
    bool ok = true;
    int count[2] = {0, 0};
    TileFlag spawn[2] = {TILE_SPAWN_SPY, TILE_SPAWN_MERC};

    for (int x = 0; x < map->getWidth(); x++) {
        for (int y = 0; y < map->getHeight(); y++) {
            for (int i = 0; i < 2; i++) {
                if (map->hasFlag(x, y, spawn[i])) 
                    count[i]++;

                if (map->hasFlag(x, y, spawn[0]) && map->hasFlag(x, y, spawn[1])) {
                    Log::error("Tile [%i,%i] is both a spy and merc spawn",x,y);
                    ok = false;
                }
            }
        }
    }

    for (int i = 0; i < 2; i++) {
        if (count[i] < 2) {
            Log::error("There are not 2 spawns defined for team %s "
                       "(there are %i)", (i?"MERC":"SPAWN"), count[i]);
            ok = false;
        }
    }

    return ok;
}

bool MapParser::validateTriggers(Map *map) {
    std::set<std::string> names;

    // Map between allowed keys and allowed values for said keys. The values
    // of the entries are NOT checked for validity.
    std::map<std::string, std::vector<std::string>> expAttr;
    expAttr[TRIG_ACTION].push_back(TRIG_ACTION_PRINT);
    expAttr[TRIG_ACTION].push_back(TRIG_ACTION_PLAYAMB);
    expAttr[TRIG_ACTION].push_back(TRIG_ACTION_OBJECTIVE);

    expAttr[TRIG_ACTION_PRINT].push_back(TRIG_PARAM_MESSAGE);
    expAttr[TRIG_ACTION_PLAYAMB].push_back(TRIG_PARAM_TRACK);
    expAttr[TRIG_ACTION_OBJECTIVE].push_back(TRIG_PARAM_OBJ_ID);

    bool ok = true;

    for (MapTriggerProperties mtp : map->_triggers) {
        // Trigger names must be unique
        if (names.count(mtp.name) != 0) {
            Log::error("Trigger name is not unique: %s", mtp.name.c_str());
            ok = false;
        }

        names.insert(mtp.name);

        // The polygon of the trigger must contain at least 3 points (duh). 
        // Checking for holes or in general a valid polygon is beyond the 
        // scope of this check.
        if (mtp.vertices.size() < 3) {
            Log::error("Trigger %s has fewer than 3 vertiecs", mtp.name.c_str());
            ok = false;
        }

        // Ensure that the TriggerType is properly defined
        if (mtp.triggerType == TriggerType::UNDEFINED) {
            Log::error("Trigger %s has undefined type", mtp.name.c_str());
            return false;
        }

        // Ensure that the action is defined.
        if (!mtp.attributes.count(TRIG_ACTION)) {
            Log::error("Trigger %s has no action", mtp.name.c_str());
            ok = false;
        }

        // Ensure that the action is valid.
        std::string action = mtp.attributes[TRIG_ACTION];
        auto allowed = expAttr[TRIG_ACTION];

        if (std::find(allowed.begin(), allowed.end(), action) == allowed.end()) {
            Log::error("Trigger %s uses unknown action: %s", 
                       mtp.name.c_str(), action.c_str());
            ok = false;
        }

        // Ensure that ALL expected arguments are defined
        auto expParams = expAttr[action];
        auto attrs = mtp.attributes;
        for (std::string s : expParams) {
            if (!attrs.count(s)) {
                Log::error("Trigger %s does not define required param %s.",
                           mtp.name.c_str(), s.c_str());
                ok = false;
            }
        }
    }

    return ok;
}


bool MapParser::neighbouring(Map *map, int x, int y, TileFlag flag) {
    int hits = 0;
    std::vector<std::pair<int, int>> coords = std::vector<std::pair<int, int>>({
        {x - 1  , y     },
        {x + 1  , y     },
        {x      , y - 1 },
        {x      , y + 1 }
    });

    for (auto c : coords) {
        int mx = c.first; 
        int my = c.second;

        if (mx >= 0 && my >= 0 && mx < map->getWidth() && my < map->getHeight()) {
            hits += (int) map->hasFlag(x, y, flag);
        }
    }

    return (hits != 0);
}
