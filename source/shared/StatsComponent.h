#pragma once
#include "GameObject.h"
#include <vector>



class StatsComponent : public Component {
public:
    StatsComponent(Team team);
	virtual ~StatsComponent();
    void update(DeltaTime dt);

    float getSpeed();
    float getPower();
    float getTurn();
    float getTorque();
    void limitHp(int& hp);

private:
	float _agility;
	float _strength;
};

