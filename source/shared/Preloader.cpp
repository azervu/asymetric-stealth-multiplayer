/**
 * @file    source/shared/Preloader.cpp
 *
 * @brief   Implements the preloader class.
 */

#include "Preloader.h"

#if defined(ASM_CLIENT) || defined(ASM_UNITTEST)
    #include "ClientServiceLocator.h"
    #include "ClientEvents.h"
#endif



#include "Log.h"
#include "Settings.h"
#include "ServiceLocator.h"
#include "PathTranslator.h"

#include <rapidjson/document.h>
#include <rapidjson/filestream.h>

// JSON parser key names
static const char* SPRITESHEET_KEY = "Spritesheet";
static const char* TEXTURE_KEY = "Texture";
static const char* MAP_KEY = "Map";
static const char* AUDIO_KEY = "Audio";
static const char* FONT_KEY = "Font";

Preloader::Preloader() {
#ifdef ASM_CLIENT
    _resourceManager = ClientServiceLocator::singleton()->getResourceManager();
#endif //ASM_CLIENT

    _spriteManager = ServiceLocator::singleton()->getSpriteManager();
}

Preloader::~Preloader() {
}

std::future<bool> Preloader::startLoading(const std::string& filePath) {
    return std::async(std::launch::async, &Preloader::loadAssets, this, filePath);
}

bool Preloader::loadAssets(const std::string& filePath) {
    FILE* file = ServiceLocator::singleton()->getPathTranslator()->openCHandle(filePath);
    if (!file) {
        Log::error("Preloader::loadAssets: Nonexistent file %s", filePath.c_str());
        return false;
    }

    rapidjson::FileStream fileStream(file);
    rapidjson::Document document;
    document.ParseStream<0>(fileStream);
    fclose(file);

    bool validDocument = true;

    validDocument &= document.HasMember(SPRITESHEET_KEY) && document[SPRITESHEET_KEY].IsArray();
    validDocument &= document.HasMember(TEXTURE_KEY) && document[TEXTURE_KEY].IsArray();
    validDocument &= document.HasMember(MAP_KEY) && document[MAP_KEY].IsArray();
    validDocument &= document.HasMember(AUDIO_KEY) && document[AUDIO_KEY].IsArray();
    validDocument &= document.HasMember(FONT_KEY) && document[FONT_KEY].IsArray();

    const auto& spritesheets = document[SPRITESHEET_KEY];

    for (int i = 0; i < spritesheets.Size(); i++) {
        const auto& entry = spritesheets[i];

        if (entry.IsString()) {
            std::string value {entry.GetString()};

            Log::debug("Loading sprite sheet %s", value.c_str());
            this->_spriteManager->parseSpriteSheet(value);
        } else {
            Log::error("Map at index %i is not a string", i);
        }
    }

#ifdef ASM_CLIENT
    const auto& textures = document[TEXTURE_KEY];
    std::vector<std::future<Resource*>> futures;

    for (int i = 0; i < textures.Size(); i++) {
        const auto& entry = textures[i];

        if (entry.IsString()) {
            std::string value {entry.GetString()};

            Log::debug("Loading textures %s", value.c_str());
            futures.push_back(this->_resourceManager->queueResource(ResourceType::TEXTURE, value));
        } else {
            Log::error("Map at index %i is not a string", i);
        }
    }

    const auto& audio = document[AUDIO_KEY];
    for (int i = 0; i < audio.Size(); i++) {
        const auto& entry = audio[i];

        if (entry.IsString()) {
            std::string path{ entry.GetString() };
            Log::debug("Loading audio %s", path.c_str());
            EventManager *emgr = ServiceLocator::singleton()->getEventManager();
            emgr->triggerEvent(new LoadSoundEvent(path));
            //triggerEvent
            //this->_resourceManager->queueResource(ResourceType::AUDIO, value);
        } else {
            Log::error("Audio at index %i is not a string", i);
        }
    }

    const auto& font = document[FONT_KEY];

    for (int i = 0; i < font.Size(); i++) {
        const auto& entry = font[i];

        if (entry.IsString()) {
            std::string value {entry.GetString()};

            Log::debug("Loading font %s", value.c_str());
            futures.push_back(this->_resourceManager->queueResource(ResourceType::FONT, value));
        } else {
            Log::error("Font at index %i is not a string", i);
        }
    }

    // cleanup
    for (auto& f : futures) {
        delete f.get();
    }

#endif //ASM_CLIENT

    const auto& maps = document[MAP_KEY];

    for (int i = 0; i < maps.Size(); i++) {
        const auto& entry = maps[i];

        if (entry.IsString()) {
            Log::debug("Loading map %s", entry.GetString());
            // @TODO
        } else {
            Log::error("Map at index %i is not a string", i);
        }
    }

    return true;
}
