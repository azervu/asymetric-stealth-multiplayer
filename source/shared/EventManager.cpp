/**
 * @file    source/shared/EventManager.cpp
 *
 * @brief   Implements the event manager class.
 */

#include "EventManager.h"
#include "Log.h"
#include <typeinfo>


EventManager::EventManager()
: _activeQueue(0) {

}

EventManager::~EventManager() {
    for (int i=0; i<EVENTMANAGER_NUM_QUEUES; i++) {
        for (EventData *event : _eventQueue[i]) {
            delete event;
        }
    }
}

bool EventManager::addListener(EventListenerDelegate delegate, EventType eventType) {
    if (isListener(delegate, eventType)) {
        return false;
    }

    if (!_listeners.count(eventType)) {
        DelegateList list;
        _listeners[eventType] = list;
    }

    _listeners[eventType].push_back(delegate);
    return true;
}

bool EventManager::removeListener(EventListenerDelegate delegate) {
    bool removed = false;

    auto li = _listeners.begin();
    for (; li != _listeners.end(); li++) {
        DelegateList *list = &li->second;

        DelegateList::iterator di = list->begin();
        while (di != list->end()) {
            if (delegate == (*di)) {
                di = list->erase(di);
                removed = true;
            } else {
                di++;
            }
        }
    }

    return removed;
}

bool EventManager::queueEvent(EventData *event) {
    _eventQueue[_activeQueue].push_back(event);
    return (_listeners.count(event->getEventType()) != 0);
}

bool EventManager::triggerEvent(EventData *event) {
    bool sent = false;
    DelegateMap::iterator search = _listeners.find(event->getEventType());

    if (search != _listeners.end()) {
        DelegateList list = search->second;
        DelegateList::iterator iter = list.begin();

        for (; iter != list.end(); iter++) {
            (*iter)(event);
            sent = true;
        }
    }

    delete event;
    return sent;
}

bool EventManager::abortEvent(EventType eventType, bool removeAll) {
    bool success = false;

    const int max = (removeAll ? 2 : 1);

    for (int i=0; i<max; i++) {
        int idx = (_activeQueue + i) % EVENTMANAGER_NUM_QUEUES;
        EventQueue &queue = _eventQueue[idx];

        // If we are not removing all events, the order of the queue matter. If
        // we are removing all, the order doesn't matter.
        if (!removeAll) {
            queue.reverse();
        }

        EventQueue::iterator iter = queue.begin();
        while (iter != queue.end()) {
            if ((*iter)->getEventType() == eventType) {
                delete *iter;
                iter = queue.erase(iter);
                success = true;

                if (!removeAll) {
                    break;
                }
            } else {
                iter++;
            }
        }

        if (!removeAll) {
            // Put the queue back in it's original order
            queue.reverse();
        }
    }

    return success;
}

void EventManager::update() {
    EventQueue &queue = _eventQueue[_activeQueue];
    _activeQueue = (_activeQueue + 1) % EVENTMANAGER_NUM_QUEUES;
    _eventQueue[_activeQueue].clear();

    // Do NOT use iterators or other state-dependent method of iterating, events
    // can be removed by delegates!
    while (!queue.empty()) {
        EventData *event = queue.front();
        queue.pop_front();

        // Find the listening delegates (if any)
        EventType type = event->getEventType();
        DelegateMap::iterator search = _listeners.find(type);
        if (search != _listeners.end()) {
            DelegateList list = search->second;
            DelegateList::iterator iter = list.begin();

            // Notify all delegates 
            for (; iter != list.end(); iter++) {
                EventListenerDelegate delegate = *iter;
                delegate(event);
            }
        }

        delete event;
    }
}

const EventQueue& EventManager::peekEvents() const {
    return _eventQueue[_activeQueue]; 
}



bool EventManager::isListener(EventListenerDelegate delegate, EventType type) {
    if (_listeners.count(type) != 0) {
        DelegateList list = _listeners[type];

        DelegateList::const_iterator iter = list.cbegin();

        for (; iter != list.end(); iter++) {
            if (delegate == (*iter)) {
                return true;
            }
        }
    }

    return false;
}


std::string EventData::getEventName() const {
    return typeid(*this).name();
}
