/**
 * @file    source/shared/GamePlayScene.cpp
 *
 * @brief   Implements the game play scene class.
 */

#include "GamePlayScene.h"
#include "EventManager.h"
#include "ControlComponent.h"
#include "DrawableComponent.h"
#include "PhysicsComponent.h"
#include "WallObject.h"
#include "WallComponent.h"
#include "Map.h"
#include "MapParser.h"
#include "Player.h"
#include "SharedEvents.h"

#include <algorithm>
#include <functional>

GamePlayScene::GamePlayScene(std::string mapFile) 
    :   _map(nullptr),
        _world(nullptr),
        _state(Scene::State::RUNNING),
        _mapFile(mapFile),
        _nextScene(nullptr) {
}

GamePlayScene::~GamePlayScene() {
    destroyGameObjects();
    delete _world;
}

bool GamePlayScene::loadScene() {
    b2Vec2 gravity(0.0f, 0.0f);
    _world = new b2World(gravity);
    _world->SetContactListener(&_conList);

    Log::info("Loading map %s...", _mapFile.c_str());
    MapParser parser;
    _map = parser.parseMap(_mapFile);
    if (!_map) {
        Log::error("Failed to load map '%s'", _mapFile.c_str());
        return false;
    }

    createMapObjects();

    return true;
}

void GamePlayScene::onResume() {
	_state = Scene::State::RUNNING;
}

Scene::State GamePlayScene::getState() {
	return _state;
}

Scene* GamePlayScene::getNextScene() {
	return _nextScene;
}

void GamePlayScene::setSceneState(Scene::State state) {
    _state = state;
}

void GamePlayScene::setNextScene(Scene *scene) {
    _nextScene = scene;
}

const Map* GamePlayScene::getMap() const {
    return _map;
}

Player* GamePlayScene::createCommonPlayer(PlayerId playerId, Team team, 
                                          PlayerType ptype, std::string name) {
    Player *player = new Player(team, playerId, ptype, name, _world);
    addGameObject(player);

    return player;
}


void GamePlayScene::createMapObjects() {
    // Add all wall-component game objects to a shared parent. This avoid
    // creating a huge forest at the root of the scene hierarchy, which greatly
    // increases the speed of future removals / additions of game objects.
    GameObject *wallRoot = new GameObject();
    addGameObject(wallRoot);

    for (int x = 0; x < _map->getWidth(); x++) {
        for (int y = 0; y < _map->getHeight(); y++) {
            if (
                _map->hasFlag(x, y, TileFlag::TILE_WALL) ||
                _map->hasFlag(x, y, TileFlag::TILE_VENT)) {

                unsigned flags = _map->getTileFlags(x, y);

                WallObject* wobj = new WallObject(_world, x, y, flags);
                wallRoot->addChild(wobj);
            }
        }
    }
}

b2World* GamePlayScene::getWorld() {
    return _world;
}

void GamePlayScene::updatePhysics(DeltaTime timeStep) {
    _world->Step(timeStep, _velocityIterations, _positionIterations);
}

void GamePlayScene::sendPhysicsUpdatedEvent() {
    EventManager *emgr = ServiceLocator::singleton()->getEventManager();
    emgr->triggerEvent(new PhysicsUpdatedEvent());
}
