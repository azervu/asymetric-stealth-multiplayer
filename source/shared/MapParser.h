#pragma once 

#ifdef WIN32
    #include "Tmx.h"
#else
    #include <TmxParser/Tmx.h>
#endif //WIN32

#include "Engine.h"
#include "Map.h"

#include <set>

/**
 * @class   MapParser
 *
 * @brief   Produces a Map instance given a TMX file. The TMX files must conform to a few
 *          standards - see the tech doc for further details.
 */

class MapParser {
public:
	MapParser();
	~MapParser();
	
	Map* parseMap(std::string mapFile);

private:

    /**
     * @enum    ObjectsetTileID
     *
     * @brief   This enum maps to the tiles of the tileset "objtiles.png".
     */

	enum ObjectsetTileID {
		ID_TERM_A 		    = 0,
		ID_TERM_B 		    = 1,
		ID_TERM_C 		    = 2,
        ID_SPY_SPAWN        = 3,
        ID_MERC_SPAWN       = 4,
		ID_WALL_NW_NE_SE_SW = 5,
		ID_VENT 		    = 6,
        ID_WALL_NW_SW       = 7,
        ID_WALL_SE_SW       = 8,
        ID_WALL_NE_SE       = 9,
        ID_WALL_NW_NE       = 10,
        ID_WALL_SW          = 11,
        ID_WALL_SE          = 12,
        ID_WALL_NE          = 13,
        ID_WALL_NW          = 14,
        ID_WALL_NE_SW       = 15,
        ID_WALL_NW_SE       = 16,
        ID_WALL_NW_SE_SW    = 17,
        ID_WALL_NE_SE_SW    = 18,
        ID_WALL_NW_NE_SE    = 19,
        ID_WALL_NW_NE_SW    = 20,
	};

    std::string _mapFile;
	Tmx::Map _tmxMap;
	Tmx::Tileset *_mapset;
	Tmx::Tileset *_objset;

    std::set<std::string> _parsedLayers;
	
	// Parse the Background or Foreground layer
	MapLayer* parseDrawableLayer(Tmx::Layer *layer);
	Rect getTextureClip(Tmx::Tileset *set, int gid);
	Rect getWorldClip(Tmx::Tileset *set, int x, int y);

	void parseWallLayer(Tmx::Layer *layer, Map *map);
    void parseAreaLayer(const Tmx::Layer *layer, Map *map);
    void parseObjectiveLayer(const Tmx::Layer *layer, Map *map);
    void parseSpawnLayer(const Tmx::Layer *layer, Map *map);

	void parseLightLayer(const Tmx::ObjectGroup *objGroup, Map *map);
    void parseTriggerLayer(const Tmx::ObjectGroup *objGroup, Map *map);
    void parseVisionLayer(const Tmx::ObjectGroup *objGroup, Map *map);

    /**
     * Ensure that the map defines all required properties.
     */
    bool validateMap(Map *map);
    bool validateParsedLayers(Map *map);
    bool validateObjectives(Map *map);
    bool validateAreas(Map *map);
    bool validateSpawns(Map *map);
    bool validateTriggers(Map *map);

    bool neighbouring(Map *map, int x, int y, TileFlag flag);
};
