#pragma once

#include "GameObject.h"
#include "EventManager.h"

#ifdef ASM_CLIENT
#include "GuiObject.h"
#endif //ASm_CLIENT

#include "Engine.h"
#include "DrawableComponent.h"


enum class PlayerType {
    LOCAL,
    REMOTE,
};


class b2World;


/**
 * A Player-instance in itself is NOT a valid player-instance, but merely a
 * blank sleet that must aggregate a set of Components to be fully operational.
 *
 * The Player-class handles generic player functionality and data-management.
 */
class Player : public GameObject {
public:
    /**
     * The default constructor attaches all common player components during
     * construction. The common Components are: 
     *  - DrawableComponent
     *  - PhysicsComponent
     */
    Player(Team team, PlayerId id, PlayerType ptype, 
           std::string playerName, b2World *world);
    
    Team getTeam() const;
    PlayerId getPlayerId() const;
    std::string getPlayerName() const;
    PlayerType getPlayerType() const;
    bool isActive() const;


    int getHealth() const;
    void setHealth(int health);

    /**
     * If a PhysicsComponent is attached to the Player, it's velocity is
     * returned. Oterwise, b2Vec2_zero is returned.
     */
    b2Vec2 getVelocity();
    void setVelocity(b2Vec2 velocity);

    /**
     * If a PhysicsComponent is attached to the Player, it's angular velocity
     * is returned. Otherwise, 0.f is returned.
     */
    float getAngularVelocity();
    void setAngularVelocity(float angvel);

    /**
     * Set the position of the GameObject and the attached PhysicsComponent if
     * one exists.
     */
    virtual void setPosition(Vec2 position);

    /**
     * Set the rotation of the GameObject and the attached PhysicsComponent if
     * one exists.
     */
    virtual void setRotation(float rotation);
    
    /**
     * Spawn the player at a position. A new PhysicsComponent
     * is attached to the parent GameObject if one doesn't already exist.
     * Makes the PlayerComponent active.
     */
    void spawn(Vec2 pos);

    /**
     * Despawn the player. This method should be called as a reacion to the
     * player actually getting killed or for other reasons being removed from
     * the world temporarily. Makes the PlayerComponent inactive.
     *
     * If a PhysicsComponent is attached to the parent GameObject, it is 
     * removed.
     */
    void despawn();

private:
    Team _team;
    PlayerId _playerId;
    std::string _playerName;
    PlayerType _playerType;
    b2World *_b2world;
    bool _active;
    int _health;
    SpriteProvider* spriteProvider;

    void createStatsComponent(Team team);
    void createPhysicsComponent();
    void createDrawableComponent();
};


/**
 * PlayerComponent is the base-class for all "player-only" Components. The 
 * component provides getter-functionality not otherwise found in GameObject.
 *
 * All classes inheriting from PlayerComponent should only be attached to
 * GameObject-instances inheriting from Player.
 */
class PlayerComponent : public Component {
public:
    Player* getPlayer() const;

    Team getTeam() const;
    PlayerId getPlayerId() const;
};
