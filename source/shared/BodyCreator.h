#pragma once


#include "Engine.h"


/**
 * BodyCreator is a creator of b2Bodies. BodyCreator supports both convex and
 * concave hulls. 
 *
 * Concave shapes are created by tearing apart a concave hull and reassembling
 * multiple convex pieces. Box2D does not support concavity, so this is the 
 * only real solution to create concave shapes. As all the convex shapes are
 * attached to the same b2Body, the body operates exactly as it would if Box2D
 * supported concave shapes.
 */
class BodyCreator {
public:
    BodyCreator(b2World *world);
    ~BodyCreator();

    void setCircularShape(float radius);
    void setRectangularShape(float width, float height);

    /**
     * Set a (potentially) concave shape. The shape will be broken down into
     * triangles and all the triangle fixtures will be added to the b2Body.
     * This process is RIDICULOUSLY slow and show ONLY be used when loading,
     * and sparcely even then.
     *
     * The shape must be a simple polygon - no holes or shared vertices.
     */
    void setPolygonalShape(std::vector<b2Vec2> vertices);

    void setDensity(float density);
    void setFriction(float friction);
    void setUserData(void *userData);
    void setCanSleep(bool canSleep);
    void setCategoryBits(unsigned category);
    void setMaskBits(unsigned maskBits);
    void setFilter(b2Filter filter);
    void setStatic();
    void setDynamic();
    void setSensor(bool sensor);
    void setPosition(Vec2 position);

    /**
     * Create the body. A new body is created with each call.
     */
    b2Body* createBody();

private:
    b2World *_world;
    void *_userData;
    float _density;
    float _friction;
    bool _sensor;
    b2Filter _filter;

    b2BodyDef _bodyDef;
    std::vector<b2Shape*> _shapes;
    std::vector<b2FixtureDef> _fixDefs;


    void updateFixDefs();

    /**
     * Clear _shapes and _fixDefs. All b2Shapes will be deleted. A fresh
     * b2FixtureDef instance is added to _fixDefs.
     */
    void clean();
};
