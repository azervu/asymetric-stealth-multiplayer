/**
 * @file    source/shared/Protocol.h
 *
 * @brief   Declares the protocol systems and all that entails with it.
 */

#pragma once

#include "SDLHeaders.h"

#include <map>
#include <vector>
#include <string>
#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>
#include <rapidjson/filestream.h>
#include "ServiceLocator.h"
#include "Engine.h"

#define TCP_SERVER_PORT                     ((Uint16) 43720)
#define SERVER_INITIALIZED_DEST_UDP_PORT    ((Uint16) 43975)


/**
 * Safe function for testing the string-length of a buffer that cannot be
 * trusted to contain a terminating null-character. The function checks
 * "maxlen" bytes for a null-character.
 *
 * @param str           The untrusted string-buffer
 * @param maxlen        The maximum allowed length of the string
 * @return              If a null-character is found, the number of preceding
 *                      bytes is returned. -1 is returned if the buffer does
 *                      not contain a terminating null character.
 */
int volatile_strlen(const char *str, int maxlen);

/**
 * @enum    
 *
 * @brief   Transport Layer Protocol global IDs.
 */

enum class TLProtocol {
    UDP,
    TCP,
};

/**
 * If there ever is any ambiguity of which direction an event, a message or a
 * packet is going, use this enum to explicitly state the direction.
 */
enum class NetworkDirection {
    // The destination is across the network
    NET_OUT,

    // The destination is local
    NET_IN,
};

/**
 * 
 * @enum    
 *
 * @brief   Values that represent state of connection.
 */

enum class ProtocolState {
    /**
     * The protocol is uninitialized
     */
    DISCONNECTED,

    /**
     * The client and the server are negotiating the terms of the connection.
     */
    JOINING,

    /**
     * The client(s) are properly connected to the server. This is the "lowest"
     * state the server can be in.
     */
    IN_LOBBY,

    /**
     * The game is running.
     */
    IN_GAME,
};

enum class FieldType {
    UNDEFINED,              // If ever encountered, an error has occurred!
    UINT32,                 // Unsigned 32 bit integer
    INT32,                  // Signed 32 bit integer
    FLOAT32,                // 32 bit float
    BYTE,                   // 8 bit unsigned
    STRING,                 // C string
    ARRAY,                  // Binary array
};

const char* fieldTypeToStr(FieldType type);

class Packet;
class PacketFieldComparator;
class PacketTemplate;
class PacketTemplateParser;
class PathTranslator;


/**
 * Converts raw bytes into de-serialized Packet-instances. The factory is
 * dependent on the PacketTemplateParser to define the available packets.
 */
class PacketFactory : public Service {
public:
    static int readInt(const byte *buf);
    static unsigned readUInt(const byte *buf);
    static float readFloat(const byte *buf);

    // The returned unsigned can be cast directly to byte[4] and be
    // sent over the network.
    static unsigned networkDword(void *ptr);


    /**
     * The PathTranslator is required to be passed to the PacketTemplateParser.
     */
    PacketFactory(PathTranslator *pathTranslator);
    ~PacketFactory();

    /**
     * @fn  Packet* PacketFactory::readPacket(byte *buffer, int bufferlen, int &packetlen);
     *
     * @brief   Read a packet from a byte-array. The number of bytes read into the packet is stored
     *          in "packetlen".
     *
     * @param   buffer      Byte array consisting of a serialized Packet.
     * @param   bufferlen           The total length of the buffer.
     * @param [in,out]  packetlen   The length of the returned packet is assigned to it.
     *
     * @return  A deserialized Packet-instance on success, NULL on error. The packetlen is set to
     *          contain the number of bytes deserialized.
     */

    Packet* readPacket(byte *buffer, int bufferlen, int &packetlen);

    /**
     * @fn  Packet* PacketFactory::createPacketFromTemplate(unsigned id);
     *
     * @brief   Create a packet based on the ID. The packet is initialized from the template if it
     *          exists.
     *
     * @param   id  The ID of a potentially existing packet template.
     *
     * @return  An initialized Packet instance on success, NULL on error. The returned packet has
     *          it's field declared, but only the field "id" contains an actual value.
     */

    Packet* createPacketFromTemplate(unsigned id);

    /**
     * @fn  Packet* PacketFactory::createPacketFromTemplate(std::string packetName);
     *
     * @brief   Wrapper around createPacketFromTemplate(unsigned) and getPacketId(std::string).
     *
     * @param   packetName  Name of the packet.
     *
     * @return  null if it fails, else the new packet from template.
     */

    Packet* createPacketFromTemplate(std::string packetName);

    /**
     * @fn  unsigned PacketFactory::getPacketId(std::string packetName);
     *
     * @brief   Get the ID of a packet, given it's name.
     *
     * @param   packetName  Name of the packet.
     *
     * @return  The packet ID (>= 1) on success, 0 on error.
     */

    unsigned getPacketId(std::string packetName);

    /**
     * @fn  bool PacketFactory::addTemplatesFromFile(std::string file);
     *
     * @brief   Add packet templates from a packet-file. See
     *          PacketTemplateParser::addTemplatesFromFile() for details.
     *
     * @param   file    The file.
     *
     * @return  True if the templates were parsed successfully, false on error.
     */

    bool addTemplatesFromFile(std::string file);

private:
    PacketTemplateParser *_templateParser;

    /**
     * @fn  const PacketTemplate* PacketFactory::getPacketTemplate(unsigned id);
     *
     * @brief   Get the packet template given a valid packet-ID.
     *
     * @param   id  A valid packet ID.
     *
     * @return  A pointer to the specified template on success, NULL if the packet ID does not exist.
     */

    const PacketTemplate* getPacketTemplate(unsigned id);
};

/**
 * @class   Packet
 *
 * @brief   Encapsulation of a message to be transmitted over the network. Packets are defined
 *          dynamically by adding the required fields to an instance.
 */

class Packet {
private:
    friend class PacketFactory;

public:

    /**
     * @class   Field
     *
     * @brief   Defines a field in the packet. A packet is defined by it's set of fields, and a field
     *          is a named value within the packet.
     */

    class Field {
    public:
        typedef union {
            unsigned u32;   // UINT32
            int i32;        // INT32
            float f32;      // FLOAT32
            byte b;         // BYTE
            char *str;      // STRING
            struct {
                unsigned len;
                byte *ptr;
            } array;        // ARRAY
        } FieldValue;

        Field(std::string name, FieldType type);
        ~Field();

        /**
         * @fn  bool Field::set(void *val);
         *
         * @brief   Setter for simple types: i32, u32, f32, b. The pointer is dereferenced to the type
         *          correlating to the active field type.
         *
         * @param   val If non-null, the value to set.
         *
         * @return  True on success, false on failure.
         */

        bool set(void *val);

        /**
         * @fn  bool Field::set(std::string strValue);
         *
         * @brief   Setter for Field-instances of type FieldType::STRING.
         *
         * @param   strValue    The value to set.
         *
         * @return  True if the type of the Field is STRING, false otherwise.
         */

        bool set(std::string strValue);

        /**
         * @fn  bool Field::set(const byte *buffer, unsigned len);
         *
         * @brief   Setter for Field-instances of type FieldType::ARRAY.
         *
         * @param   buffer  The buffer.
         * @param   len     The length.
         *
         * @return  True if the type of field is ARRAY and the assignment went OK, false otherwise.
         */

        bool set(const byte *buffer, unsigned len);

        FieldValue getValue() const;
        FieldType getType() const;
        std::string getName() const;

        /**
         * @fn  std::string Field::getString() const;
         *
         * @brief   Get the string value of a Field of type STRING.
         *
         * @return  The std::string value of "value.str" if the type of the field is STRING, an empty
         *          string otherwise.
         */

        std::string getString() const;

        /**
         * @fn  std::vector<byte> Field::getArray() const;
         *
         * @brief   Get the byte-array of a Field of type ARRAY. The array is pushed into a std::vector
         *          for easier usage.
         *
         * @return  A std::vector with the contents of FieldType::array if the type of the Field is
         *          FieldType::ARRAY, an empty std::vector otherwise.
         */

        std::vector<byte> getArray() const;

    private:
        std::string _name;
        FieldType _type;
        FieldValue _value;
    };

public:
    Packet();
    ~Packet();

    /**
     * @fn  byte* Packet::serialize(int &pktlen) const;
     *
     * @brief   Serialize the Packet's fields into a binary array. The returned array must be
     *          explicitly deleted by the caller.
     * 
     * @param [in,out]  pktlen  The number of bytes returned is assigned to this variable.
     *
     * @return  Byte-array if any fields could be serialized, NULL if the Packet does not contain any
     *          serializable fields.
     */
    // @TODO: Modify this method so it returns an std::vector<byte>.
    byte* serialize(int &pktlen) const;

    /**
     * @fn  int Packet::deserialize(const byte *buf, int buflen);
     *
     * @brief   Deserialize a byte-buffer into the proper fields. The Packet-instance must have been
     *          initialized with it's correct fields beforehand.
     *
     * @param   buf     The byte-buffer containing packed field data.
     * @param   buflen  The total length of the byte buffer.
     *
     * @return  The number of bytes read from the buffer.
     */

    int deserialize(const byte *buf, int buflen);

    bool hasField(std::string name) const;
    FieldType getFieldType(std::string fieldName) const;

    unsigned getUnsigned(std::string name) const;
    int getInt(std::string name) const;
    float getFloat(std::string name) const;
    byte getByte(std::string name) const;
    std::string getString(std::string name) const;
    std::vector<byte> getArray(std::string name) const;

    bool setUnsigned(std::string name, unsigned value);
    bool setInt(std::string name, int value);
    bool setFloat(std::string name, float value);
    bool setByte(std::string name, byte value);
    bool setString(std::string name, std::string value);
    bool setArray(std::string name, const byte *arr, unsigned len);

    std::string getName() const;

    /**
     * @fn  void Packet::logContents() const;
     *
     * @brief   Write the contents of this packet to Log::LOG_DEBUG.
     */

    void logContents() const;

protected:

    /**
     * @fn  bool Packet::addField(std::string name, FieldType ft);
     *
     * @brief   Add a new field to the Packet. This should only be done by PacketFactory or another
     *          explicitly stated friend of Packet.
     *
     * @param   name    The name of the new field. The field must be unique within each packet.
     * @param   ft      The type of the field.
     *
     * @return  true if it succeeds, false if it fails.
     */

    bool addField(std::string name, FieldType ft);
    void setType(const std::vector<Packet::Field> &fields);

private:
    std::vector<Packet::Field*> _fields;
    std::map<std::string, int> _reverseLookup;
    std::string _name;
};

/**
 * @class   PacketTemplate
 *
 * @brief   Contains the name and the fields of a packet. Used as a template when constructing
 *          new Packet-instances conforming to the network protocol.
 */

class PacketTemplate {
public:

    /**
     * @fn  const std::vector<Packet::Field>& PacketTemplate::getFields() const;
     *
     * @brief   Get a vector of all the fields of the packet.
     *
     * @return  The fields.
     */

    const std::vector<Packet::Field>& getFields() const;

    /**
     * @fn  std::string PacketTemplate::getName() const;
     *
     * @brief   Get the name of the packet.
     *
     * @return  The name.
     */

    std::string getName() const;

private:

    friend class PacketTemplateParser;

    std::string _name;
    std::vector<Packet::Field> _fields;
    std::map<const std::string, int> _reverseLookup;
};

/**
 * @class   PacketTemplateParser
 *
 * @brief   Parser of packet-defining JSON files.
 */

class PacketTemplateParser {
public:

    /**
     * @fn  PacketTemplateParser::PacketTemplateParser(PathTranslator *pathTranslator);
     *
     * @brief   The PacketTemplateParser requires a PathTranslator to open the packets file. As it is
     *          target-independent, it cannot use ClientServiceLocator or ServerServiceLocator to
     *          locate the PT-Service, so it must be injected via the constructor.
     *
     * @param [in,out]  pathTranslator  If non-null, the path translator.
     */

    PacketTemplateParser(PathTranslator *pathTranslator);

    /**
     * @fn  const std::vector<PacketTemplate>& PacketTemplateParser::getTemplates() const;
     *
     * @brief   Gets the templates.
     *
     * @return  The templates.
     */

    const std::vector<PacketTemplate>& getTemplates() const;

    /**
     * @fn  int PacketTemplateParser::addTemplatesFromFile(std::string path);
     *
     * @brief   Add templates from a file complying with the format specified in the wiki-page
     *          "Networking". If the file in any way deviates from the defined format, the entire
     *          contents of the file will be discarded.
     *
     * @param   path    The path to a file conforming to the packet-defining JSON format.
     *
     * @return  The number of packets parsed, -1 on error. If the file has already been parsed, 0 is
     *          returned.
     */

    int addTemplatesFromFile(std::string path);

private:

    /**
     * @fn  bool PacketTemplateParser::parsePacket(const rapidjson::Value &packetNode);
     *
     * @brief   Parse a single packet-node. The parsed template is added to _templates.
     *
     * @param   packetNode  An object contained in the root-object "packets".
     *
     * @return  True on success, false on error.
     */

    bool parsePacket(const rapidjson::Value &packetNode);

    /**
     * @fn  FieldType PacketTemplateParser::getFieldType(std::string type) const;
     *
     * @brief   Return the FieldType based on the typename contained within the JSON definition file.
     *
     * @param   type    The value of the field "name" within a field.
     *
     * @return  A valid FieldType value on success, FieldType::UNDEFINED on error.
     */

    FieldType getFieldType(std::string type) const;

    /**
     * @fn  bool PacketTemplateParser::hasFileBeenParsed(const std::string &file) const;
     *
     * @brief   Check if a file has been parsed previously. Path tomfoolery is NOT taken into account,
     *          i.e., adding "a/b.json" and later checking if "a/../a/b.json" has been added WILL
     *          return false.
     *
     * @param   file    The file.
     *
     * @return  true if file been parsed, false if not.
     */

    bool hasFileBeenParsed(const std::string &file) const;

    PathTranslator *_pathTranslator;
    std::vector<PacketTemplate> _templates;
    std::vector<std::string> _parsedFiles;
};
