#pragma once

#include <string>
#include <random>

namespace utility {

    /**
     * @fn  std::string getString(const std::string &message, unsigned int min, unsigned int max, bool showLimit = true);
     *
     * @brief   Gets a string.
     *
     * @param   message     The message.
     * @param   min         The minimum.
     * @param   max         The maximum.
     * @param   showLimit   (Optional) true to show, false to hide the limit.
     *
     * @return  The string.
     */

	std::string getString(const std::string &message, unsigned int min, unsigned int max, bool showLimit = true);

    /**
     * @fn  std::string getString(const std::string &message, bool emptyStringAllowed = false);
     *
     * @brief   Gets a string.
     *
     * @param   message             The message.
     * @param   emptyStringAllowed  (Optional) true to allow, false to deny empty string.
     *
     * @return  The string.
     */

	std::string getString(const std::string &message, bool emptyStringAllowed = false);

    /**
     * @fn  std::string loadFileToString (const std::string &path);
     *
     * @brief   Loads file to string.
     *
     * @param   path    Filename of the file.
     *
     * @return  The string containing the file as text.
     */

	std::string loadFileToString (const std::string &path);

    extern std::default_random_engine defaultRandom;
}
