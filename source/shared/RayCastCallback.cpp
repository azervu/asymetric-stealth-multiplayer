#include "RayCastCallback.h"
#include "CollisionFlags.h"


const unsigned RayCastCallback::gunFireCategoryFilter = CollisionFlag::SPY |
                                                        CollisionFlag::WALL;

RayCastCallback::RayCastCallback(unsigned categoryFilter) 
    :   _catFilter(categoryFilter),
        _hit(false),
        _gameObject(nullptr),
        _body(nullptr),
        _point(b2Vec2_zero) {
    
}

float32 RayCastCallback::ReportFixture(b2Fixture *fixture, const b2Vec2 &point,
                                       const b2Vec2 &normal, float32 fraction) {
    b2Filter filter = fixture->GetFilterData();
    
    if ((filter.categoryBits & _catFilter) == 0) {
        // Ignore this fixture
        return 1.f;
    }

    _body = fixture->GetBody();
    _gameObject = (GameObject*)_body->GetUserData();
    _point = point;
    _hit = true;

    // Stop the ray at this intersection
    return fraction;
}

bool RayCastCallback::didHitAnything() {
    return _hit;
}

GameObject* RayCastCallback::getHitGameObject() {
    return _gameObject;
}

b2Body* RayCastCallback::getHitBody() {
    return _body;
}

b2Vec2 RayCastCallback::getHitPoint() {
    return _point;
}
