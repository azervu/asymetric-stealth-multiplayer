#include "ContactListener.h"
#include "EventManager.h"
#include "ServiceLocator.h"
#include "SharedEvents.h"


void ContactListener::BeginContact(b2Contact *contact) {
    if (contact->IsTouching()) {
        ContactBegunEvent *evt = new ContactBegunEvent(
            contact->GetFixtureA(),
            contact->GetFixtureB()
        );

        ServiceLocator::singleton()->getEventManager()->queueEvent(evt);
    }
}

void ContactListener::EndContact(b2Contact *contact) {
    if (!contact->IsTouching()) {
        ContactEndedEvent *evt = new ContactEndedEvent(
            contact->GetFixtureA(),
            contact->GetFixtureB()
        );

        ServiceLocator::singleton()->getEventManager()->queueEvent(evt);
    }
}
