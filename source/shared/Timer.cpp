#include "Timer.h"


Timer::Timer() {
    start();   
}

void Timer::start() {
    _start = std::chrono::high_resolution_clock::now();
}

long long Timer::getElapsedMicro() const {
    std::chrono::microseconds us;

    us = std::chrono::duration_cast<std::chrono::microseconds>(
             std::chrono::high_resolution_clock::now() - _start
    );

    return us.count();
}

double Timer::getElapsedSeconds() const {
    unsigned us = getElapsedMicro();
    return double(us) / 1000000.0;
}
