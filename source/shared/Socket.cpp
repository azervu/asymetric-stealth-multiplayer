#include "Socket.h"
#include "Log.h"
#include <cstdlib>
#include <stdlib.h>
#include <sstream>
#include <algorithm>

/* 
================
SocketBase
================
*/
bool SocketBase::resolveHost(std::string hostname, Uint16 port, 
                             IPaddress &ipaddr) {
    if (SDLNet_ResolveHost(&ipaddr, hostname.c_str(), port) == -1) {
        Log::error("Failed to resolve host: %s:%hu", hostname.c_str(), port);
        ipaddr.host = 0;
        ipaddr.port = 0;
        return false;
    }

    return true;
}


SocketBase::SocketBase(IPaddress ipaddr) 
    :   _ipaddr(ipaddr),
        _initialized(false), 
        _dc(false),
        _sockset(0) {

}

SocketBase::SocketBase(Uint32 host, Uint16 port) 
    :   _initialized(false),
        _dc(false),
        _sockset(0) {

    SDLNet_Write32(host, &_ipaddr.host);
    SDLNet_Write16(port, &_ipaddr.port);
}

SocketBase::SocketBase(std::string hostname, Uint16 port) 
    :   _initialized(false),
        _dc(false),
        _sockset(0) {

    if (!resolveHost(hostname, port, _ipaddr)) {
        Log::error("Unable to resolve host %s:%u! '%s'",
                   hostname.c_str(), port, SDLNet_GetError());
    }
}

SocketBase::~SocketBase() {
    for (auto iter = _inQueue.begin(); iter != _inQueue.end(); iter++) {
        delete (*iter);
    }

    for (auto iter = _outQueue.begin(); iter != _outQueue.end(); iter++) {
        delete (*iter);
    }

    reset();
}

bool SocketBase::isServerSocket() const {
    return (_ipaddr.host == 0);
}

bool SocketBase::isInitialized() const {
    return _initialized;
}

bool SocketBase::isGood() const {
    return isInitialized() && !_dc;
}

bool SocketBase::validIPAddress() const {
    return !(!_ipaddr.host && !_ipaddr.port);
}

bool SocketBase::hasActivity() const {
    if (!isGood()) {
        return false;
    }

    int numready = SDLNet_CheckSockets(_sockset, 0);
    if (numready) {
        SDLNet_GenericSocket socket = getGenericSocket();
        return (SDLNet_SocketReady(socket) != 0);
    }

    return false;
}

bool SocketBase::parseTrafficData(PacketFactory *pFactory) {
    if (!isGood()) {
        return false;
    }

    if (readPackets(pFactory) < 0) {
        return false;
    }

    return true;
}

bool SocketBase::sendPacket(const Packet *packet) const {
    if (!isGood()) {
        Log::debug("Attempted to send packet '%s' on bad socket",
                   packet->getName().c_str());
        return false;
    }

    int len = 0;
    byte *buf = packet->serialize(len);

    if (!buf || !len) {
        if (buf) {
            delete [] buf;
        }

        return false;
    }

    bool status = sendPacket(buf, len);
    delete [] buf;

    return status;
}

bool SocketBase::sendPacket(const byte *buffer, int len) const {
    if (!isGood()) {
        Log::debug("Attempted to send unnamed buffer on bad socket");
        return false;
    }

    return sendBuffer(buffer, len);
}

int SocketBase::getIncomingPacketQueueSize() const {
    return _inQueue.size();
}

void SocketBase::queuePacket(Packet *packet) const {
    _outQueue.push_back(packet);
}

int SocketBase::getOutgoingPacketQueueSize() const {
    return _outQueue.size();
}

bool SocketBase::flushOutgoingPacketQueue() const {
    std::vector<byte> buffer;

    for (Packet *pkt: _outQueue) {
        int len = 0;
        byte *buf = pkt->serialize(len);
        if (len <= 0 || buf == nullptr) {
            Log::warning("SocketBase: Unable to serialize packet of type '%s'", 
                         pkt->getName().c_str());
        } else {
            buffer.insert(buffer.end(), buf, buf+len);
            delete[] buf;
        }

        delete pkt;
    }

    _outQueue.clear();
    bool success = true;

    if (buffer.size()) {
        if (!sendPacket(&buffer[0], buffer.size())) {
            Log::error("SocketBase: Unable to flush outgoing packet queue");
            success = false;
        }
    }

    return success;
}

Packet* SocketBase::popPacket() {
    if (_inQueue.size()) {
        Packet *p = _inQueue.front();
        _inQueue.erase(_inQueue.begin());
        return p;
    }

    return NULL;
}

Packet* SocketBase::getPacket(std::string packetName, unsigned index) {
    Packet *packet = NULL;

    auto iter = _inQueue.begin();
    for (; iter != _inQueue.end(); iter++) {
        if ((*iter)->getName () == packetName) {
            if (index == 0) {
                packet = *iter;
                _inQueue.erase(iter);
                break;
            } else {
                index--;
            }
        }
    }

    return packet;
}

Packet* SocketBase::getPacket(unsigned packetId, unsigned index) {
    Packet *packet = NULL;

    auto iter = _inQueue.begin();
    for (; iter != _inQueue.end(); iter++) {
        if ((*iter)->getUnsigned("id") == packetId) {
            if (index == 0) {
                packet = *iter;
                _inQueue.erase(iter);
                break;
            } else {
                index--;
            }
        }
    }

    return packet;
}

const Packet* SocketBase::peekPacket(unsigned index) const {
    if (_inQueue.size() <= index) {
        return NULL;
    }

    auto iter = _inQueue.cbegin();

    for (; iter != _inQueue.cend(); iter++, index--) {
        if (index == 0) {
            return *iter;
        }
    }

    return NULL;
}

const Packet* SocketBase::peekPacket(std::string packetName, 
                                     unsigned index) const {
    auto iter = _inQueue.cbegin();

    for (; iter != _inQueue.cend(); iter++, index--) {
        if ((*iter)->getName() == packetName) {
            if (index == 0) {
                return *iter;
            }
        }
    }

    return NULL;
}

const Packet* SocketBase::peekPacket(unsigned packetId, unsigned index) const {
    auto iter = _inQueue.cbegin();

    for (; iter != _inQueue.cend(); iter++, index--) {
        if ((*iter)->getUnsigned("id") == packetId) {
            if (index == 0) {
                return *iter;
            }
        }
    }

    return NULL;
}

IPaddress SocketBase::getIPAddress() const {
    return _ipaddr;
}

std::string SocketBase::getOctalIP() const {
    Uint32 ip = this->getIP();

    std::stringstream ss;
    ss << ((ip >> 24) & 0xFF) << ".";
    ss << ((ip >> 16) & 0xFF) << ".";
    ss << ((ip >> 8 ) & 0xFF) << ".";
    ss << ((ip      ) & 0xFF);
    return ss.str();
}

Uint32 SocketBase::getIP() const {
    return SDLNet_Read32(&_ipaddr.host);
}

Uint16 SocketBase::getPort() const {
    return SDLNet_Read16(&_ipaddr.port);
}

int SocketBase::readPackets(PacketFactory *pFactory) {
    if (!hasActivity())
        return 0;
    
    int len = 0;
    byte *buf = NULL;

    buf = getSocketData(&len);
    if (!buf || len <= 0) {
        // It is VERY likely that the remote host has 
        // disconnected. 
        Log::warning("SocketBase::readPackets(): Host disconnected");
        _dc = true;
        return -1;
    }

    int numRecv = 0;
    int read = 0;
    byte *bptr = buf;

    while (len > 0) {
        int pktLen = 0;
        Packet *packet = pFactory->readPacket(bptr, len, pktLen);

        if (packet) {
            len -= pktLen;
            read += pktLen;
            _inQueue.push_back(packet);
            numRecv++;
        } else {
            Log::error("Unable to decode received data. None of the "
                "remaining data (%i bytes) can be trusted!", len);
            break;
        }

        bptr += pktLen;
    }

    delete[] buf;
    return numRecv;
}

bool SocketBase::onInitSuccess() {
    if (!createSocketSet()) {
        reset();
        return false;
    }

    SDLNet_GenericSocket socket = getGenericSocket();
    if (socket == NULL) {
        Log::error("SocketBase::onInitSuccess(): "
            "getGenericSocket() returned NULL");
        reset();
        return false;
    }

    if (!addToSocketSet(socket)) {
        reset();
        return false;
    }

    _initialized = true;
    return true;
}

bool SocketBase::createSocketSet() {
    if (!_sockset) {
        _sockset = SDLNet_AllocSocketSet(10);
        if (!_sockset) {
            Log::error("Failed to allocate socket set");
            return false;
        }
    }

    return true;
}

bool SocketBase::addToSocketSet(SDLNet_GenericSocket socket) {
    if (!_sockset || !socket) {
        return false;
    }

    if (SDLNet_AddSocket(_sockset, socket) == -1) {
        Log::error("Unable to add socket to socket set (%s:%hu)", 
                   getOctalIP().c_str(), getPort());
        return false;
    }

    return true;
}

void SocketBase::reset() {
    if (_sockset) {
        SDLNet_FreeSocketSet(_sockset);
        _sockset = NULL;
    }

    _dc = false;
    _initialized = false;
}


/*
================
SocketTCP
================
*/
SocketTCP::SocketTCP(Uint32 remHost, Uint16 remPort) 
    :   SocketBase(remHost, remPort),
        _socket(NULL) {

}

SocketTCP::SocketTCP(std::string hostname, Uint16 remPort) 
    :   SocketBase(hostname, remPort),
        _socket(NULL) {
    
}

SocketTCP::SocketTCP(TCPsocket sock)
: SocketBase(*SDLNet_TCP_GetPeerAddress(sock)),
_socket(sock) {

}

SocketTCP::SocketTCP(IPaddress ipaddr) 
    :   SocketBase(ipaddr),
        _socket(NULL) {

}

SocketTCP::~SocketTCP() {
    cleanup();
}

bool SocketTCP::initSocket() {
    if (isInitialized()) {
        return true;
    }

    if (!validIPAddress()) {
        return false;
    }

    if (!_socket) {
        // The connection has NOT been previously established, 
        // connect to the remote host (or bind port in case of server)
        IPaddress ipaddr = getIPAddress();
        _socket = SDLNet_TCP_Open(&ipaddr);
        if (!_socket) {
            Log::debug("Unable to initialize TCP socket (%s:%hu): \"%s\"",
                       getOctalIP().c_str(), 
                       getPort(),
                       SDLNet_GetError());
            cleanup();
            return false;
        }
    }

    if (!onInitSuccess()) {
        cleanup();
        return false;
    }

    return true;
}

TLProtocol SocketTCP::getTransportLayerProtocol() const {
    return TLProtocol::TCP;
}

SDLNet_GenericSocket SocketTCP::getGenericSocket() const {
    return (SDLNet_GenericSocket) _socket;
}

byte* SocketTCP::getSocketData(int *bufferLen) {
    *bufferLen = 0;
    byte *buf = NULL;
    int read = 0;

    if (isServerSocket()) {
        Log::warning("Attempted to get socket data from server-TCP socket");
        return NULL;
    }

    if (SDLNet_SocketReady(_socket) <= 0) {
        return NULL;
    }

    unsigned maxlen = 128;
    unsigned totRead = 0;
    buf = new byte[maxlen];

    read = SDLNet_TCP_Recv(_socket, buf, maxlen);
    if (read <= 0) {
        delete [] buf;
        return NULL;
    }

    totRead = read;

    while (read == maxlen) {
        maxlen *= 2;
        buf = (byte*) realloc(buf, maxlen);
        read = SDLNet_TCP_Recv(_socket, buf + totRead, maxlen);
        totRead += read;
    }

    *bufferLen = totRead;
    return buf;
}

bool SocketTCP::sendBuffer(const byte *buffer, int bufferLen) const {
    if (!buffer || !bufferLen) {
        return false;
    }

    if (SDLNet_TCP_Send(_socket, buffer, bufferLen) <= 0) {
        Log::error("Failed to send TCP packet: %s", SDLNet_GetError());
        return false;
    }

    return true;
}

void SocketTCP::cleanup() {
    if (_socket) {
        SDLNet_TCP_Close(_socket);
        _socket = NULL;
    }
}


/*
================
SocketUDP
================
*/
SocketUDP::SocketUDP(Uint32 remHost, Uint16 remPort, Uint16 listPort) 
    :   SocketBase(remHost, remPort),
        _listenPort(listPort),
        _socket(NULL) {
    _bufferPacket = SDLNet_AllocPacket(UDP_MAX_PKT_LEN);   
}

SocketUDP::SocketUDP(std::string hostname, Uint16 port, Uint16 listPort) 
    :   SocketBase(hostname, port),
        _listenPort(listPort),
        _socket(NULL) {
    _bufferPacket = SDLNet_AllocPacket(UDP_MAX_PKT_LEN);
}

SocketUDP::SocketUDP(IPaddress ipaddr, Uint16 listPort) 
    :   SocketBase(ipaddr),
        _listenPort(listPort),
        _socket(NULL) {
    _bufferPacket = SDLNet_AllocPacket(UDP_MAX_PKT_LEN);
}

SocketUDP::~SocketUDP() {
    cleanup();
    SDLNet_FreePacket(_bufferPacket);
}

bool SocketUDP::initSocket() {
    IPaddress *sockip = NULL;

    if (isInitialized()) {
        return true;
    }

    _socket = SDLNet_UDP_Open(_listenPort);
    if (!_socket) {
        Log::error("SocketUDP: Failed to bind UDP port %hu", _listenPort);
        cleanup();
        return false;
    }

    // Bind the remote host on channel 0 if this is not a server socket 
    if (!isServerSocket()) {
        IPaddress ipaddr = getIPAddress();
        int chan = SDLNet_UDP_Bind(_socket, 0, &ipaddr);
        if (chan == -1) {
            Log::error("SocketUDP: Unable to bind UDP socket on channel 0: %s",
                SDLNet_GetError());
            cleanup();
            return false;
        }
    }
    
    // Get the port bound on the local machine 
    sockip = SDLNet_UDP_GetPeerAddress(_socket, -1);
    Uint16 boundPort = SDLNet_Read16(&sockip->port);
    if (_listenPort && boundPort != _listenPort) {
        Log::warning("SocketUDP: Explicitly requested port %hu, bound on %hu",
            _listenPort, boundPort);
    }

    _listenPort = boundPort;

    Log::debug("Created UDP socket with remote host %s:%hu, listen on %hu",
               getOctalIP().c_str(), getPort(), _listenPort);

    if (!onInitSuccess()) {
        cleanup();
        return false;
    }

    return true;
}

TLProtocol SocketUDP::getTransportLayerProtocol() const {
    return TLProtocol::UDP;
}

Uint16 SocketUDP::getListenPort() const {
    return _listenPort;
}

SDLNet_GenericSocket SocketUDP::getGenericSocket() const {
    return (SDLNet_GenericSocket) _socket;
}

byte* SocketUDP::getSocketData(int *bufferLen) {
    *bufferLen = 0;
    byte *buf = NULL;
    int status = 0;

    status = SDLNet_UDP_Recv(_socket, _bufferPacket);
    if (status == 1) {
        if (_bufferPacket->len == _bufferPacket->maxlen) {
            Log::error("Received packet exceeding UDP_MAX_PKT_LEN (%u). "
                "Errors are very likely to occur!", UDP_MAX_PKT_LEN);
        }

        buf = new byte[_bufferPacket->len];
        memcpy(buf, _bufferPacket->data, _bufferPacket->len);
        *bufferLen = _bufferPacket->len;
    } else if (status == 0) {
        // No data received 
        return NULL;
    } else if (status == -1) {
        // Errors occurred when reading 
        Log::error("Error in SDLNet_UDP_Recv: %s", SDLNet_GetError());
        return NULL;
    }

    return buf;
}

bool SocketUDP::sendBuffer(const byte *buffer, int bufferLen) const {
    while (bufferLen > 0) {
        /* Do NOT send the entire packet in one batch if the packet
         * exceeds UDP_MAX_PKT_LEN - the receiving host WILL be unable
         * to receive it properly!!!! See SocketUDP documentation for
         * more information.
         */
        int toSend = (std::min)(UDP_MAX_PKT_LEN - 1, bufferLen);

        byte *tmp = new byte[toSend];
        memcpy(tmp, buffer, toSend);

        UDPpacket pkt;
        pkt.data = tmp;
        pkt.len = toSend;
        pkt.address = getIPAddress();
        pkt.channel = 0;

        if (SDLNet_UDP_Send(_socket, pkt.channel, &pkt) == 0) {
            Log::error("Failed to send UDP packet: %s", SDLNet_GetError());
            delete [] tmp;
            return false;
        }

        bufferLen -= toSend;

        delete [] tmp;
    }

    return true;
}

void SocketUDP::cleanup() {
    if (_socket) {
        SDLNet_UDP_Close(_socket);
        _socket = NULL;
    }
}
