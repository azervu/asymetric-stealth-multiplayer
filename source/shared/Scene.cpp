#include "Scene.h"
#include "DrawableComponent.h"
#include "SharedEvents.h"

#include <queue>


Scene::Scene() {
    _delGobjDelegate.bind(this, &Scene::onDeleteGameObject);

    EventManager *emgr = ServiceLocator::singleton()->getEventManager();
    emgr->addListener(_delGobjDelegate, DeleteGameObjectEvent::eventType);
}

Scene::~Scene() {
    destroyGameObjects();

    EventManager *emgr = ServiceLocator::singleton()->getEventManager();
    emgr->removeListener(_delGobjDelegate);
}

void Scene::updateGameObjects(DeltaTime dt) {
    int i = 0;

    while (i < _gameObjects.size()) {
        _gameObjects[i]->update(dt);
        i++;
    }
}

void Scene::addGameObject(GameObject *go) { 
#ifdef DEBUG
    // This check is very CPU-intensive, so only run it in debug builds.
    if (findGameObject(go->getId()) != nullptr) {
        Log::error("GameObject with ID %i added to Scene twice", go->getId());
        return;
    }
#endif
    _gameObjects.push_back(go);
    go->setScene(this);
}

GameObject* Scene::findGameObject(GameObject::Id id) {
    // Inspect the top level nodes first
    for (GameObject *go : _gameObjects) {
        if (go->getId() == id) {
            return go;
        }
    }

    // Inspect the entire forest
    GameObject *needle;
    for (GameObject *go : _gameObjects) {
        needle = go->findChild(id);
        if (needle)
            return needle;
    }

    return nullptr;
}

bool Scene::removeGameObject(GameObject *go) {
    if (go == nullptr)
        return false;

    bool success = false;

    if (go->getScene() != this) {
        return false;
    }

    if (go->getParent() == nullptr) {
        auto it = _gameObjects.begin();
        for (; it != _gameObjects.end(); it++) {
            if ((*it) == go) {
                EventManager *emgr = ServiceLocator::singleton()->getEventManager();
                emgr->queueEvent(new DeleteGameObjectEvent(*it));
                success = true;
                break;
            }
        }
    } else {
        go->getParent()->removeChild(go, true);
        success = true;
    }

    return success;
}

bool Scene::removeGameObject(GameObject::Id id) {
    return removeGameObject(findGameObject(id));
}

#ifdef ASM_CLIENT
void Scene::fillGraphicsBuffer() {
    _graphicsBuffer.clear();

    std::queue<GameObject*> queue;

    for (auto it = _gameObjects.begin(); it != _gameObjects.end(); it++) {
        queue.push(*it);

        while (queue.size()) {
            GameObject *go = queue.front();
            queue.pop();

            DrawableComponent *dc = getComponent<DrawableComponent>(go);
            if (dc) {
                _graphicsBuffer.push_back(dc->getDrawable());
            }

            for (GameObject *child : go->getChildren()) {
                queue.push(child);
            }
        }
    }

}

std::vector<Drawable::BaseDrawable*>& Scene::getRenderVector() {
    return _graphicsBuffer;
}
#endif //ASM_CLIENT

void Scene::destroyGameObjects() {
    for (GameObject *go : _gameObjects) {
        delete go;
    }

    _gameObjects.clear();
}

void Scene::onDeleteGameObject(const EventData *e) {
    DeleteGameObjectEvent *evt = (DeleteGameObjectEvent*)e;

    GameObject *gob = evt->getGameObject();
    assert(gob != nullptr);
    assert(gob != NULL);

    // It is not our responsibility to clean up after other Scenes.
    if (gob->getScene() != this) {
        return;
    }

    std::lock_guard<std::recursive_mutex> lock(_listGuard);
    if (gob->getParent()) {
        //  Don't delete in the parent - that will cause an infinite loop
        gob->getParent()->removeChild(gob, false);
    } else {
        auto it = _gameObjects.begin();
        auto end = _gameObjects.end();

        while(it != end) {
            if(*it == gob) {
                delete *it;
                _gameObjects.erase(it);
                return;
            } else {
                it++;
            }
        }
    }

    delete gob;
}
