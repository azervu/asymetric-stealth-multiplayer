#include "Utility.h"

#include <iostream>
#include <fstream>
#include <string>

#include "Log.h"
#include "ServiceLocator.h"
#include "PathTranslator.h"

// Queries the user for a string with spesific length
std::string utility::getString(const std::string &message, unsigned int min, unsigned int max, bool showLimit) {
	if (min > max) {
		std::cerr << "Illegal operation, minimum length for getString cant be larger than maximum" << std::endl;
		return "";
	}

	std::string inn;
	std::string limitMessage = " | max char [" + std::to_string(min) + " - " + std::to_string(max) + " ]: ";

	do {

		std::cout << message << ((showLimit) ? (limitMessage) : "");
		std::getline(std::cin, inn);

	} while (inn.length() < min || inn.length() > max);

	return inn;
}

std::string utility::getString(const std::string &message, bool emptyStringAllowed /*= false*/) {

	std::string inn;

	do {

		std::cout << message;
		std::getline(std::cin, inn);

	} while (inn == "" && !emptyStringAllowed);

	return inn;
}

std::string utility::loadFileToString(const std::string &path) {
    std::ifstream stream;
    std::string content;
    std::string line;

	if(!ServiceLocator::singleton()->getPathTranslator()->openHandle(path, &stream)) {
        Log::error("utility::loadFileToString: Couldn't load file: %s", path.c_str());
	}

    while (std::getline(stream, line).good()) {
        content.append(line + "\n");
    }

    return content;
}

namespace utility {
    std::random_device rd;
    std::default_random_engine defaultRandom = std::default_random_engine(rd());
}