/**
 * @file    source/shared/SpriteManager.h
 *
 * @brief   Declares the sprite manager class.
 */

#pragma once

#include "ServiceLocator.h"

#include <vector>
#include <map>
#include <string>

namespace SpriteManager {

    /**
     * @class  SpriteSheet
     *
     * @brief   A sprite sheet. Contains data on how sprites are stored on it.
     */

    class Sheet {
    public:
        size_t _textureId;
        bool _loaded;
    };

    /**
     * @class  Sprite
     *
     * @brief   A sprite on a sprite sheet
     */

    class Sprite {
    public:
        std::string _name;
        int _width;
        int _height;

        std::vector<std::pair<int, int>> _images;

        const Sheet * _sheet;
    };

    /**
     * @class   SpriteManager
     *
     * @brief   Manager for sprites. Only organizes sprite relation on texture sheets.
     */

    class Manager : public Service {
    public:

        Manager();
        ~Manager();

        bool parseSpriteSheet(const std::string &path);

        const Sprite* getSprite(const std::string &name);
        const Sprite* getSprite(size_t spriteId);

    private:

        std::map<size_t, Sprite*> _spriteLookup;

        std::vector<Sheet*> _sheets;
        std::vector<Sprite*> _sprites;
    };
}