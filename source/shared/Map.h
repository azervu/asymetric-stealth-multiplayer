/**
 * @file    source/shared/Map.h
 *
 * @brief   Declares the map class.
 */

#pragma once

#include "Engine.h"

#include <vector>

class MapParser;

/**
 * Bitmap enum. Tiles can have zero or mulitple flags assigned. 
 */
enum TileFlag {
    // Tile is only blocking mercenaries
    TILE_VENT				= 0x0001,

    // Tile cast shadow
    TILE_SHADOW 			= 0x0002,

    // Area tiles
    TILE_AREA_A             = 0x0004,
    TILE_AREA_B             = 0x0008,
    TILE_AREA_C             = 0x0010,

    // Objective tiles
    TILE_OBJ_A              = 0x0020,
    TILE_OBJ_B              = 0x0040,
    TILE_OBJ_C              = 0x0080,

    // Spawns
    TILE_SPAWN_SPY          = 0x0100,
    TILE_SPAWN_MERC         = 0x0200,

    // TILE_WALL indicates that the tile is blocking in one of the four
    // quadrants, and does not mean anything by itself.
    TILE_WALL 				= 0x0400,
    TILE_WALL_NW            = 0x0800 | TILE_WALL,
    TILE_WALL_NE            = 0x1000 | TILE_WALL,
    TILE_WALL_SE            = 0x2000 | TILE_WALL,
    TILE_WALL_SW            = 0x4000 | TILE_WALL,


    /* When adding new flags, update this value to be all other flags
     * OR-ed together. It is used to assert the validity of assigned flags.
     */ 
    TILE_EVERYTHING         = 0x7FFF,
};


enum class TriggerType {
    UNDEFINED,
    PRINT,
    PLAY_AMBIENT,
    OBJECTIVE,
};


/**
 * MapLightProperties defines a light. It is parsed from the map file's 
 * "lights"-object group.
 */
struct MapLightProperties {
    Vec2 pos;
    float radius;
    Color color;
};

/**
 * MapTriggerProperties is a temporary container for trigger objects. The data
 * in MTP is sufficient to effectively create a valid TriggerComponent to be
 * placed in the game-level. 
 *
 * The correct TriggerType is assigned to "triggerType", but the action (which
 * defines the trigger type) can still be found inthe "attributes" map.
 */
struct MapTriggerProperties {
    // The unique name of this trigger.
    std::string name;

    // The position of this TriggerComponent. Typically at the top-left corner
    // of the shape's AABB.
    Vec2 position;

    // The vertices of the shape.
    std::vector<Vec2> vertices;

    // The attributes of this Trigger - the action and it's arguments.
    std::map<std::string, std::string> attributes;

    // The type of this Trigger, derived from the action-string.
    TriggerType triggerType;
};


/**
 * @class   MapLayer
 *
 * @brief   Map layers contain data of which tiles are used at which locations on the map. Layers
 *          do not contain auxilliary data such as spawning positions or wall flags.
 */

class MapLayer {
    friend class MapParser;
public:
    MapLayer(int mapWidth, int mapHeight, const std::string& tileSet);

    /**
     * Retrieve the width of the layer in number of tiles.
     */
    int getWidth() const;

    /**
     * Retrieve the height of the layer in number of tiles.
     */
    int getHeight() const;

    /**
     * Retrieve the tile texture index of a specified tile. The returned value
     * "points" to a tile on the terrain texture, which can be calculated by:
     *      x = l->getTileTexIndex(tileX, tileY) % l->getWidth();
     *      y = l->getTileTexIndex(tileX, tileY) / l->getHeight();
     */
    int getTileTexIndex(int x, int y) const;
    const std::string& getTileTexture() const;

private:
    void setTileTexIndex(int x, int y, int tt);

    std::vector<int> _tileTexIndex;
    std::string _tileSet;
    int _width;
    int _height;
};


/**
 * Map aggregates MapLayers, which contains rendering data, and flags for all
 * the tiles on the map.
 */
class Map {
public:
    friend class MapParser;

    Map(int mapWidth, int mapHeight);
    ~Map();

    const MapLayer* getBackgroundLayer() const;
    const MapLayer* getForegroundLayer() const;

    /**
     * Retrieve all the flags on a specified tile.
     */
    unsigned getTileFlags(int x, int y) const;

    /**
     * Check if a given tile has a certain flag.
     * @param x     The x-tile-coordinate of the tile.
     * @param y     The y-tile-cooridnate of the tile.
     * @param flag  The flag to check of exists on the tile.
     * @return      True if the flag exist on the tile, false otherwise.
     */
    bool hasFlag(int x, int y, TileFlag flag) const;

    /**
     * Check if a given tile has ALL of the specified flags.
     * @param x     The x-tile-coordinate of the tile.
     * @param y     The y-tile-cooridnate of the tile.
     * @param flag  One or more TileFlag-values OR-ed together
     * @return      True if all of the flags exist on the tile, false otherwise.
     */
    bool hasFlags(int x, int y, unsigned flag) const;

    /**
     * Get the width of the map in number of tiles.
     */
    int getWidth() const;

    /**
     * Get the height of the map in number of tiles.
     */
    int getHeight() const;

    const std::vector<MapLightProperties>& getLights() const;
    const std::vector<MapTriggerProperties>& getTriggers() const;
    const std::vector<Rect>& getVisionBlockers() const;

    /**
     * Retrieve the spawning positions for a given team in metric coordinates.
     */
    std::vector<Vec2> getSpawnPositions(Team team) const;

private:
    void setBackgroundLayer(MapLayer *layer);
    void setForegroundLayer(MapLayer *layer);
    void addTileFlags(int x, int y, unsigned flags);

    int _width;
    int _height;
    std::vector<unsigned> _tileFlags;
    std::vector<MapLightProperties> _lights;
    std::vector<MapTriggerProperties> _triggers;
    std::vector<Rect> _visionBlocks;

    MapLayer *_bgLayer;
    MapLayer *_fgLayer;
};
