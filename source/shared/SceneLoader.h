/**
 * @brief       Declares and defines the templated class SceneLoader.
 */

#pragma once

#include "Scene.h"

#include <thread>
#include <atomic>
#include <functional>



/**
 * SceneLoader is a template class to load a Scene-subclass asynchronously. It
 * is possible to load is synchronously as well, but then what's the point of
 * using a loader..?
 *
 * A SceneLoader instance can only be used to load a single Scene instance. If
 * a SceneLoader fails, the instance must be destroyed and a new one must be 
 * created in order to try again.
 *
 * Example usage:
 *
 *      MyScene *ms = new MyScene(arg0, arg1);
 *      sceneLoader = new SceneLoader(ms);
 *      sceneLoader->loadAsynchronously();
 *      
 *      // ... way layer:
 *      if (sceneLoader->isDone()) { 
 *          if (sceneLoader->success())
 *              handleScene(sceneLoader->getScene());
 *          else
 *              delete sceneLoader->getScene();
 *      }
 */
template<class Sc>
class SceneLoader {
    static_assert(std::is_base_of<Scene,Sc>::value, "Sc must be a subclass of Scene");

public:            
    /**
     * @param scene     The scene to be loaded. It must be instantiated outside
     *                  the scope of this class.
     */
    SceneLoader(Sc *scene) {
        _done.store(false);
        _success.store(false);
        _started.store(false);
        _sfront.store(nullptr);
        _sback = scene;
        _thread = nullptr;
    }

    ~SceneLoader() {

    }

    void loadAsynchronously() {
        if (!_started.load()) {
            _thread = new std::thread(&SceneLoader::loadSynchronously, this);
        } else {
            Log::warning("Attempted to load multiple Scenes from "
                         "same SceneLoader instance. Time to RTFM.");
        }
    }

    void loadSynchronously() {
        if (_started.load()) {
            Log::warning("Attempted to load multiple Scenes from "
                         "same SceneLoader instance. Time to RTFM.");
            return;
        }

        _started.store(true);
        _success.store(_sback->loadScene());
        _sfront.store(_sback);
        _done.store(true);
    }

    Sc *getScene() {
        return _sfront.load();
    }

    bool doneLoading() {
        return _done.load();
    }

    bool success() {
        return _success.load();
    }

private:
    std::atomic_bool _started;
    std::atomic_bool _done;
    std::atomic_bool _success;
    std::thread *_thread;

    std::atomic<Sc*> _sfront;
    Sc *_sback;
};

