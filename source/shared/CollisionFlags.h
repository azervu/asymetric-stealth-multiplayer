#pragma once


/**
 * Collsion flags are passed to Box2D to allow / disallow collisions between
 * certain types of fixtures. 
 *
 * Set "what I am" as the category flag, and "what I can collide with" as the
 * mask flag. For instance will a spy have "SPY" as it's category, and 
 * "ALL" as it's mask.
 */
enum CollisionFlag {
    SPY         = 0x0001,
    MERC        = 0x0002,
    WALL        = 0x0004,
    VENT        = 0x0008,

    /* Flag for triggers requiring no user interaction (automatic doors) */
    TRIG_AUTO   = 0x0010,

    /* Flag for triggers requiring user interaction (objectives, etc) */
    TRIG_MANUAL = 0x0020,

    GRENADE     = 0x0040,

    ALL         = 0xFFFF,
};
