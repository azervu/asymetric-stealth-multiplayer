#include <Box2D/Box2D.h>


/* Dispatcher of b2ContactListener events onto the EventManager's queue. The
 * ContactListener must be added to the b2World to properly propagate the 
 * contact events.
 */
class ContactListener : public b2ContactListener {
public:
    virtual void BeginContact(b2Contact *contact);
    virtual void EndContact(b2Contact *contact);
};
