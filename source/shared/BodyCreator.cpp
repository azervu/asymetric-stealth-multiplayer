#include "BodyCreator.h"
#include "Triangulator.h"


BodyCreator::BodyCreator(b2World *world)
    :   _world(world),
        _userData(nullptr),
        _density(1.f),
        _friction(0.5f),
        _sensor(false) { 
    setDynamic();
}

BodyCreator::~BodyCreator() {
    clean();
}


void BodyCreator::setCircularShape(float radius) {
    clean();

    b2CircleShape *shape = new b2CircleShape();
    shape->m_radius = radius;
    _shapes.push_back(shape);

    updateFixDefs();
}

void BodyCreator::setRectangularShape(float width, float height) {
    clean();

    b2PolygonShape *shape = new b2PolygonShape();
    shape->SetAsBox(width / 2.f, height / 2.f);
    _shapes.push_back(shape);
    
    updateFixDefs();
}

void BodyCreator::setPolygonalShape(std::vector<b2Vec2> vertices) {
    Triangulator triangulator(vertices);
    auto tris = triangulator.triangulateShape();

    if (tris.size() == 0) {
        Log::error("BodyCreator: Unable to triangulate shape");
        return;
    }

    // Don't clean until we're sure the shape was valid.
    clean();

    for (Triangulator::Triangle tri : tris) {
        b2PolygonShape *shape = new b2PolygonShape();
        shape->Set(&tri.getPoints().front(), 3);
        _shapes.push_back(shape);
    }

    updateFixDefs();
}


void BodyCreator::setDensity(float density) {
    _density = density;
}

void BodyCreator::setFriction(float friction) {
    _friction = friction;
}

void BodyCreator::setUserData(void *userData) {
    _userData = userData;
    _bodyDef.userData = _userData;
}

void BodyCreator::setCanSleep(bool canSleep) {
    _bodyDef.allowSleep = canSleep;   
}

void BodyCreator::setCategoryBits(unsigned catBits) {
    _filter.categoryBits = catBits;
}

void BodyCreator::setMaskBits(unsigned maskBits) {
    _filter.maskBits = maskBits;
}

void BodyCreator::setFilter(b2Filter filter) {
    _filter = filter;
}

void BodyCreator::setStatic() {
    _bodyDef.type = b2_staticBody;
}

void BodyCreator::setDynamic() {
    _bodyDef.type = b2_dynamicBody;
}

void BodyCreator::setSensor(bool sensor) {
    _sensor = sensor;
}

void BodyCreator::setPosition(Vec2 position) {
    _bodyDef.position = position;
}


b2Body* BodyCreator::createBody() {
    if (!_shapes.size()) {
        Log::warning("BodyCreator: no shapes defined");
        return nullptr;
    }

    updateFixDefs();

    b2Body *body = _world->CreateBody(&_bodyDef);
    if (!body) {
        Log::error("BodyCreator: unable to create b2Body");
        return nullptr;
    }

    for (b2FixtureDef &fd : _fixDefs) {
        body->CreateFixture(&fd);
    }

    return body;
}


void BodyCreator::updateFixDefs() {
    // This is a safety-measure, and never expected to actually be a problem.
    // Better safe than sorry, etc.
    if (_fixDefs.size() > _shapes.size()) {
        _fixDefs.resize(_shapes.size());
    }

    for (int i=0; i<_shapes.size(); i++) {
        if (i >= _fixDefs.size()) {
            _fixDefs.push_back(b2FixtureDef());
        }

        b2FixtureDef &fd = _fixDefs[i];

        fd.userData = _userData;
        fd.friction = _friction;
        fd.density = _density;
        fd.filter = _filter;
        fd.shape = _shapes[i];
        fd.isSensor = _sensor;
    }
}

void BodyCreator::clean() {
    for (b2Shape *shape : _shapes) {
        delete shape;
    }

    _shapes.clear();
    _fixDefs.clear();
}
