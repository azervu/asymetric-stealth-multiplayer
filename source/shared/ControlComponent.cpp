#include "ControlComponent.h"
#include "ServiceLocator.h"

#include <math.h>

Control::Control() {}

Control::~Control() {}

ControlComponent::ControlComponent(Control* control) {
    _control = control;
}

ControlComponent::~ControlComponent() {
    delete _control;
}

void ControlComponent::update(DeltaTime dt) {
    PhysicsComponent* pComp = getComponent<PhysicsComponent>(getGameObject());
    StatsComponent* sComp = getComponent<StatsComponent>(getGameObject());
    std::vector<Command> commands = _control->getCommands();
    if (pComp != NULL && sComp != NULL) {

        while (!commands.empty()) {
            applyCommand(dt, commands.back(), pComp, sComp);
            commands.pop_back();
        }
    } else {

    }
}

void ControlComponent::applyCommand(DeltaTime dt, Command command, PhysicsComponent* pComp, StatsComponent* sComp) {
    switch (command._type) {
    case Command::Type::MOVE:
        pComp->move(dt, command._vector, sComp->getSpeed(), sComp->getPower());

        break;
    case Command::Type::STOP:
        pComp->move(dt, command._vector, 0.0f, sComp->getPower());
        break;
    case Command::Type::AIM:
        command._vector.Normalize();
        float mouseAngle = acos(command._vector.x);
        if (command._vector.y < 0) {
            mouseAngle = 2 * M_PI - mouseAngle;
        }
        mouseAngle -= M_PI / 2;
        if (mouseAngle < 0) {
            mouseAngle += 2 * M_PI;
        }
        pComp->aim(dt, mouseAngle, sComp->getTurn(), sComp->getTorque());
        break;
    }
}
