/**
 * @file    source/shared/DrawableComponent.h
 *
 * @brief   Declares the drawable component class.
 */

#pragma once

#include "GameObject.h"
#include "Engine.h"
#ifdef ASM_CLIENT
    #include "Renderable.h"
#endif //ASM_CLIENT


/**
 * DrawableProvider is an abstract interface that DrawableComponent relies on
 * to retrieve the content to be drawn.
 */
class DrawableProvider {
public:
#ifdef ASM_CLIENT
    virtual Drawable::BaseDrawable* getDrawable() = 0;
#endif //ASM_CLIENT

    virtual void update(DeltaTime delta) {
        // default nothing
    }
};

class SpriteProvider : public DrawableProvider {
public:

    SpriteProvider(const std::string& spriteName);
    SpriteProvider(const std::string& spriteName, GameObject *parent);

#ifdef ASM_CLIENT
    virtual Drawable::BaseDrawable* getDrawable();
#endif //ASM_CLIENT

    void setPosition(Vec2 pos);
    void setX(float x);
    void setY(float y);

    void setSize(Vec2 size);
    void setWidth(float width);
    void setHeight(float height);
    void setAnchor(Vec2 anchor);
    void setRotation(float angle);
    void setDepth(float z);
    void setFrame(int frameNumber);
    void setAlwaysVisible(bool visible);

private:
    Vec2 _pos = {0.f, 0.f};
    Vec2 _size = {0.f, 0.f};
    Vec2 _anchor = {0.5f, 0.5f};
    float _angle = 0.f;
    float _depth = 0.5f;
    bool _alwaysVisible = false;
    ResourceId _spriteId;
    GameObject *_parent;

    size_t _frames = 0;
    size_t _frameNumber = 0;
};

/**
 * A DrawableComponent gives GameObjects the functionality to be drawn on the
 * screen. The DrawableProvider is the one mking it possible.
 */
class DrawableComponent : public Component {
public:
    DrawableComponent(DrawableProvider* provider, bool owner);
    
    virtual ~DrawableComponent();

    virtual void update(DeltaTime delta);

#ifdef ASM_CLIENT
    virtual Drawable::BaseDrawable* getDrawable() final;
#endif //ASM_CLIENT

private:
    DrawableProvider* _drawProvider = nullptr;
    bool _owner;
};

