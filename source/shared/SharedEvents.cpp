#include "SharedEvents.h"


/*
================== 
CreatePlayerEvent
================== 
*/
const EventType CreatePlayerEvent::eventType = 0x2bcefb74a5d342ed;

CreatePlayerEvent::CreatePlayerEvent(PlayerId id, std::string name, 
                                     PlayerType type, Team team) 
    :   _playerId(id),
        _name(name),
        _type(type),
        _team(team) {
}

EventType CreatePlayerEvent::getEventType() const {
    return eventType;
}

PlayerId CreatePlayerEvent::getPlayerId() const {
    return _playerId;
}

std::string CreatePlayerEvent::getPlayerName() const {
    return _name;
}

PlayerType CreatePlayerEvent::getPlayerType() const {
    return _type;
}

Team CreatePlayerEvent::getTeam() const {
    return _team;
}



/*
================== 
SpawnPlayerEvent
================== 
*/
const EventType SpawnPlayerEvent::eventType = 0x8fe24cd5aef747be;

SpawnPlayerEvent::SpawnPlayerEvent(PlayerId id, Vec2 pos) 
    :   _playerId(id),
        _position(pos) {
    
}

EventType SpawnPlayerEvent::getEventType() const {
    return eventType;
}

PlayerId SpawnPlayerEvent::getPlayerId() const {
    return _playerId;
}

Vec2 SpawnPlayerEvent::getPosition() const {
    return _position;
}


/*
================== 
PlayerTransformUpdateEvent
================== 
*/
const EventType PlayerTransformUpdateEvent::eventType = 0x27458c4788044b11;

PlayerTransformUpdateEvent::PlayerTransformUpdateEvent(
            PlayerId id, 
            Vec2 pos, 
            Vec2 vel,
            float angVel,
            float rot,
            NetworkDirection dir,
            unsigned step) 
    :   _direction(dir),
        _playerId(id),
        _position(pos),
        _velocity(vel),
        _angVel(angVel),
        _rotation(rot),
        _step(step) { }

EventType PlayerTransformUpdateEvent::getEventType() const {
    return eventType;
}

NetworkDirection PlayerTransformUpdateEvent::getDirection() const {
    return _direction;
}

PlayerId PlayerTransformUpdateEvent::getPlayerId() const {
    return _playerId;
}

Vec2 PlayerTransformUpdateEvent::getPosition() const {
    return _position;
}

Vec2 PlayerTransformUpdateEvent::getVelocity() const {
    return _velocity;
}

float PlayerTransformUpdateEvent::getAngularVelocity() const {
    return _angVel;
}

float PlayerTransformUpdateEvent::getRotation() const {
    return _rotation;
}

unsigned PlayerTransformUpdateEvent::getStep() const {
    return _step;
}


/*
================== 
PhysicsUpdatedEvent
================== 
*/
const EventType PhysicsUpdatedEvent::eventType = 0x26135e5058154fb6;

EventType PhysicsUpdatedEvent::getEventType() const {
    return eventType;
}


/*
==================
GameOverEvent
==================
*/
const EventType GameOverEvent::eventType = 0x4786c83b3321423e;

GameOverEvent::GameOverEvent(Team winner, std::string reason) 
    :   _winner(winner),
        _reason(reason) {}

EventType GameOverEvent::getEventType() const {
    return eventType;
}

Team GameOverEvent::getWinningTeam() const {
    return _winner;
}

std::string GameOverEvent::getReason() const {
    return _reason;
}




/*
==================
ContactListenerEvent
==================
*/
ContactListenerEvent::ContactListenerEvent(b2Fixture *a, b2Fixture *b) 
    :   _fixA(a),
        _fixB(b) {
    
}

b2Fixture* ContactListenerEvent::getFixtureA() const {
    return _fixA;
}

b2Fixture* ContactListenerEvent::getFixtureB() const {
    return _fixB;
}

bool ContactListenerEvent::isRelevant(b2Body *body) const {
    if (!body) {
        return false;
    }
    
    b2Fixture *fix = body->GetFixtureList();
    while (fix) {
        if (fix == _fixA || fix == _fixB)
            return true;
        fix = fix->GetNext();
    }

    return false;
}


/*
==================
ContactBegunEvent
==================
*/
const EventType ContactBegunEvent::eventType = 0x05af79ae2a434753;

ContactBegunEvent::ContactBegunEvent(b2Fixture *fixA, b2Fixture *fixB) 
    :   ContactListenerEvent(fixA, fixB) {

}

EventType ContactBegunEvent::getEventType() const {
    return eventType;
}


/*
==================
ContactEndedEvent
==================
*/
const EventType ContactEndedEvent::eventType = 0xa07cb0591fc14f58;

ContactEndedEvent::ContactEndedEvent(b2Fixture *fixA, b2Fixture *fixB) 
    :   ContactListenerEvent(fixA, fixB) {

}

EventType ContactEndedEvent::getEventType() const {
    return eventType;
}


/*
==================
ObjectiveInitiatedEvent
==================
*/
const EventType ObjectiveInitiatedEvent::eventType = 0x1055fa62aa764cd5;

ObjectiveInitiatedEvent::ObjectiveInitiatedEvent(PlayerId playerId, Team team,
                                                 ObjectiveId id) 
    :   _playerId(playerId),
        _team(team),
        _objId(id) { }

PlayerId ObjectiveInitiatedEvent::getPlayerId() const {
    return _playerId;
}

Team ObjectiveInitiatedEvent::getTeam() const {
    return _team;
}

ObjectiveId ObjectiveInitiatedEvent::getObjectiveId() const {
    return _objId;
}

EventType ObjectiveInitiatedEvent::getEventType() const {
    return eventType;
}


/*
==================
ObjectiveStateChangedEvent
==================
*/
const EventType ObjectiveStateChangedEvent::eventType = 0xb880971e8cc44077;

ObjectiveStateChangedEvent::ObjectiveStateChangedEvent(ObjectiveId id, 
                                                       ObjectiveState state,
                                                       PlayerId playerId,
                                                       NetworkDirection dir) 
    :   _objState(state),
        _objId(id),
        _playerId(playerId),
        _direction(dir) { }

ObjectiveState ObjectiveStateChangedEvent::getObjectiveState() const {
    return _objState;
}

ObjectiveId ObjectiveStateChangedEvent::getObjectiveId() const {
    return _objId;
}

PlayerId ObjectiveStateChangedEvent::getPlayerId() const {
    return _playerId;
}

NetworkDirection ObjectiveStateChangedEvent::getNetworkDirection() const {
    return _direction;
}

EventType ObjectiveStateChangedEvent::getEventType() const {
    return eventType;
}


/*
==================
GunFiredEvent
==================
*/
const EventType GunFiredEvent::eventType = 0x098bea6b645444db;

GunFiredEvent::GunFiredEvent(PlayerId id, b2Vec2 gunPt, b2Vec2 endPt) 
    :   _playerId(id),
        _gunPt(gunPt),
        _endPt(endPt) { }

PlayerId GunFiredEvent::getPlayerId() const {
    return _playerId;
}

b2Vec2 GunFiredEvent::getGunPoint() const {
    return _gunPt;
}

b2Vec2 GunFiredEvent::getEndPoint() const {
    return _endPt;
}

EventType GunFiredEvent::getEventType() const {
    return eventType;
}


/*
==================
BulletHitEvent
==================
*/
const EventType BulletHitEvent::eventType = 0x630fdcd5131c439b;

BulletHitEvent::BulletHitEvent(PlayerId firingId, PlayerId hitId, 
                               int health, bool kill) 
    :   _firePlayerId(firingId),
        _hitPlayerId(hitId),
        _health(health),
        _kill(kill) { }

PlayerId BulletHitEvent::getFiringPlayerId() const {
    return _firePlayerId;
}

PlayerId BulletHitEvent::getHitPlayerId() const {
    return _hitPlayerId;
}

int BulletHitEvent::getPlayerHealth() const {
    return _health;
}

bool BulletHitEvent::didPlayerDie() const {
    return _kill;
}

EventType BulletHitEvent::getEventType() const {
    return eventType;
}


/*
==================
GrenadeThrowEvent
==================
*/
const EventType GrenadeThrowEvent::eventType = 0x7e8ce15335b6418e;

GrenadeThrowEvent::GrenadeThrowEvent(NetworkDirection dir, PlayerId pid,
                                     Vec2 pos, float rot) 
    :   _dir(dir),
        _playerId(pid),
        _pos(pos),
        _rot(rot) {

}

NetworkDirection GrenadeThrowEvent::getNetworkDirection() const {
    return _dir;
}

PlayerId GrenadeThrowEvent::getPlayerId() const {
    return _playerId;
}

Vec2 GrenadeThrowEvent::getPosition() const {
    return _pos;
}

float GrenadeThrowEvent::getRotation() const {
    return _rot;
}

EventType GrenadeThrowEvent::getEventType() const {
    return eventType;
}



/*
==================
GrenadeExplodeEvent
==================
*/
const EventType GrenadeExplodeEvent::eventType = 0x24357735e0024e2b;

GrenadeExplodeEvent::GrenadeExplodeEvent(Vec2 pos, GrenadeType type) 
    :   _pos(pos),
        _type(type) {
    
}

Vec2 GrenadeExplodeEvent::getPosition() const {
    return _pos;
}

GrenadeType GrenadeExplodeEvent::getGrenadeType() const {
    return _type;
}

EventType GrenadeExplodeEvent::getEventType() const {
    return eventType;
}



/*
==================
GrenadeHitEvent
==================
*/
const EventType GrenadeHitEvent::eventType = 0x6e3e02f44bdf4799;

GrenadeHitEvent::GrenadeHitEvent(PlayerId pid, GrenadeType type, float hitFactor) 
    :   _playerId(pid),
        _type(type),
        _hitFactor(hitFactor) {

}

PlayerId GrenadeHitEvent::getPlayerId() const {
    return _playerId;
}

GrenadeType GrenadeHitEvent::getGrenadeType() const {
    return _type;
}

float GrenadeHitEvent::getHitFactor() const {
    return _hitFactor;
}

EventType GrenadeHitEvent::getEventType() const {
    return eventType;
}





/*
==================
DeleteGameObjectEvent
==================
*/
const EventType DeleteGameObjectEvent::eventType = 0x6d3784da76814aa1;

DeleteGameObjectEvent::DeleteGameObjectEvent(GameObject *gob) 
    :   _gameObject(gob) {

}

GameObject* DeleteGameObjectEvent::getGameObject() const {
    return _gameObject;
}

EventType DeleteGameObjectEvent::getEventType() const {
    return eventType;
}
