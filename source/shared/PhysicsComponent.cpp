#include "PhysicsComponent.h"
#include "ControlComponent.h"
#include "BodyCreator.h"
#include "SharedEvents.h"

#include "MainLoopTimer.h"


static float positiveFloatModulo(float numerator, float denominator) {
    if (numerator < 0) {
        numerator += denominator*ceil(fabs(numerator / denominator));
    }
    return fmod(numerator, denominator);
}

enum TurnDirection {
    LEFT = -1,
    RIGHT = 1
};

PhysicsComponent::PhysicsComponent() 
    :   _world(nullptr),
        _body(nullptr) {
    _physicsDelegate.bind(this, &PhysicsComponent::onPhysicsUpdated);

    EventManager *mgr = ServiceLocator::singleton()->getEventManager();
    mgr->addListener(_physicsDelegate, PhysicsUpdatedEvent::eventType);
}

PhysicsComponent::~PhysicsComponent() {
    if (_world && _body) {
        _world->DestroyBody(_body);
    }

    EventManager *mgr = ServiceLocator::singleton()->getEventManager();
    mgr->removeListener(_physicsDelegate);
}

void PhysicsComponent::update(DeltaTime dt) {

}

void PhysicsComponent::createBody(b2World *world, Shape shape, 
                                  Type type, float radius) {
    BodyCreator bc(world);

    if (shape == Shape::ROUND) {
        bc.setCircularShape(radius);
    } else if (shape == Shape::SQUARE) {
        bc.setRectangularShape(radius, radius);
    } 

    if (type == Type::DYNAMIC) {
        bc.setDynamic();
    } else if (type == Type::STATIC) {
        bc.setStatic();
    }

    bc.setDensity(4.f);
    bc.setFriction(0.f);

    createBody(bc);
}

void PhysicsComponent::createBody(BodyCreator &bc) {
    if (_body) {
        Log::debug("PhysicsComponent: already has body, creating new one");
        _world->DestroyBody(_body);
    }

    if (getGameObject() == nullptr) {
        Log::warning("PhysicsComponent: Premature body creation, the created "
                     "body will contain no user-data pointer.");
    }

    bc.setUserData(getGameObject());

    _body = bc.createBody();
}


void PhysicsComponent::move(DeltaTime dt, b2Vec2 direction, float32 maxSpeed, float32 maxForce) {
    
    b2Vec2 currentDirection = _body->GetLinearVelocity();
    b2Vec2 forceDirection = maxSpeed*direction-currentDirection;
    float32 speedDifference = forceDirection.Length();
    currentDirection.Normalize();
    forceDirection.Normalize();

    //float32 dotProduct = currentDirection.x*forceDirection.x+currentDirection.y*forceDirection.y;
   // float32 stoppingBonus = (dotProduct < 0) ? -dotProduct : 0;

    float32 maxSpeedForce = _body->GetMass()*speedDifference / dt;

    if (maxSpeedForce < maxForce) {
        maxForce = maxSpeedForce;
    }
    _body->ApplyLinearImpulse(maxForce*dt*forceDirection, _body->GetWorldCenter(), true);
}

void PhysicsComponent::aim(DeltaTime dt, float angle, float32 maxTurnSpeed, float32 maxTorque) {
    //check for shortest direction
    float objectAngle = positiveFloatModulo(getGameObject()->getRotation(), 2.0f*M_PI);
    float leftDist = positiveFloatModulo((objectAngle-angle), 2.0f*M_PI);
    float rightDist = positiveFloatModulo((angle-objectAngle), 2.0f*M_PI);
    float angleDist;
    float angleVel = _body->GetAngularVelocity();
    int turn;
    if(rightDist < leftDist) {
        turn = TurnDirection::RIGHT;
        angleDist = rightDist;
    } else {
        turn = TurnDirection::LEFT;
        angleDist = leftDist;
    }   


    float32 stoppingImpulse = -angleVel*_body->GetInertia();

    if (fabs(stoppingImpulse) < maxTorque*dt && angleDist < 0.05f) {
        _body->ApplyAngularImpulse(stoppingImpulse, true);
        return;
    }

    //break rotation before rotating over target
    float angleAccel = maxTorque/_body->GetInertia();
    if(angleDist < angleVel*angleVel/(2*angleAccel) && (turn*angleVel) > 0) {
        turn *= -1; //Reverse direction
    }
    this->turn(dt, turn, maxTurnSpeed, maxTorque);
}

void PhysicsComponent::turn(DeltaTime dt, int dir, float32 maxTurn, float32 maxTorque) {
    float32 currentRotation = _body->GetAngularVelocity();
    float32 turnDifference = maxTurn - dir*currentRotation;

    //check if rotating faster than allowed
    if (turnDifference < 0) {
        dir *= -1; //Reverse direction
    }

    //check if impulse is too great for the time steep
    float32 maxTurnTorque = fabs(turnDifference)*_body->GetInertia() / dt;
    if (maxTurnTorque < maxTorque) {
        maxTorque = maxTurnTorque;
    }

    //_body->ApplyTorque(dir*maxTorque, true);
    _body->ApplyAngularImpulse(dt*dir*maxTorque, true);
}

void PhysicsComponent::setCollisionFilter(const b2Filter &filter) {
    if (!_body) {
        Log::warning("PhysicsComponent::setCollisionFilter(): No body defined");
        return;
    }

    b2Fixture *fix = _body->GetFixtureList();
    while (fix) {
        fix->SetFilterData(filter);
        fix = fix->GetNext();
    }
}

void PhysicsComponent::setPosition(b2Vec2 pos) {
    _body->SetTransform(pos, _body->GetAngle());
}

void PhysicsComponent::setAngularVelocity(float angVel) {
    _body->SetAngularVelocity(angVel);
}

void PhysicsComponent::setRotation(float rotation) {
    _body->SetTransform(_body->GetPosition(), rotation);
}

b2Vec2 PhysicsComponent::getVelocity() const {
    return _body->GetLinearVelocity();
}

float PhysicsComponent::getAngularVelocity() const {
    return _body->GetAngularVelocity();
}

void PhysicsComponent::setLinearVelocity(b2Vec2 velocity) {
    _body->SetLinearVelocity(velocity);
}

void PhysicsComponent::moveToGameObject() {
    GameObject *go = getGameObject();
    _body->SetTransform(go->getPosition(), go->getRotation());
}

b2Body* PhysicsComponent::getBody() const {
    return _body;
}


void PhysicsComponent::onPhysicsUpdated(const EventData *e) {
    if (_body) {
        GameObject *gob = getGameObject();

        gob->setPosition(_body->GetWorldCenter());
        gob->setRotation(_body->GetAngle());
    }
}
