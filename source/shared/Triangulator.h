#pragma once

#include "Engine.h"
#include <array>

/**
 * Triangulator uses ear-clippping to convert a concave shape into a set of 
 * convex shapes.
 *
 * This class is a C++ rewrite of the ActionScript implementation posted on 
 * TouchMyPixel.com. The AS source code can be downloaded at: 
 *      goo.gl/VjMHWN
 */
class Triangulator {
public:
    class Triangle;
    class Polygon;

    Triangulator(std::vector<Vec2> verts);

    /**
     * Triangulate the flat vertex array into a set of triangles. If a shape
     * initially cannot be found, the order of the vertices is attempted
     * reversed (is allowReversal == true). If the shape still cannot be 
     * triangulated, something has gone very wrong.
     */
    std::vector<Triangle> triangulateShape(bool allowReversal = true);

private:
    bool isEar(int index);

    std::vector<Vec2> _verts;

public:
    /**
     * Representation of a triangle. The vertices are always wound CCW.
     */
    class Triangle {
    public:
        Triangle(Vec2 p1, Vec2 p2, Vec2 p3);
        bool isInside(Vec2 pt);
        std::array<Vec2, 3> getPoints();

    private:
        std::array<Vec2, 3> _points;

        float sign(Vec2 &p1, Vec2 &p2, Vec2 &p3);
    };

    /**
     * Representation of a Polygon.
     */
    class Polygon {
    public:
        std::vector<Vec2> getPoints() const;

    private:
        std::vector<Vec2> _points;

        Polygon(std::vector<Vec2> points);

        bool isConvex();

        // Attempt to add a triangle to the polygon. Two of the vertices from
        // the polygon must overlap with two of the vertices from the triangle.
        // False is returned if the resulting polygon is concave or we couldn't
        // find two matching vertices. 
        bool add(Triangle triangle);
    };

};
