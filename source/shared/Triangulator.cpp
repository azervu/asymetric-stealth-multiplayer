#include "Triangulator.h"

/*
==================
Triangle
==================
*/
Triangulator::Triangle::Triangle(Vec2 p1, Vec2 p2, Vec2 p3) {
    float cross = b2Cross(p2-p1, p3-p1);
    bool clockwise = (cross > 0.f);

    if (clockwise) {
        _points[0] = p1;
        _points[1] = p3;
        _points[2] = p2;
    } else {
        _points[0] = p1;
        _points[1] = p2;
        _points[2] = p3;
    }
}

bool Triangulator::Triangle::isInside(Vec2 pt) {
    bool b1 = sign(pt, _points[0], _points[1]) < 0.f;
    bool b2 = sign(pt, _points[1], _points[2]) < 0.f;
    bool b3 = sign(pt, _points[2], _points[0]) < 0.f;

    return (b1 == b2) && (b2 == b3);
}

std::array<Vec2,3> Triangulator::Triangle::getPoints() {
    return _points;
}

float Triangulator::Triangle::sign(Vec2 &p1, Vec2 &p2, Vec2 &p3) {
    return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
}


/*
==================
Polygon
==================
*/
Triangulator::Polygon::Polygon(std::vector<Vec2> points) 
    :   _points(points) {
    
}

bool Triangulator::Polygon::isConvex() {
    const int num = _points.size();
    bool isPositive = false;

    for (int i=0; i<num; i++) {
        // Wrap around nicely.
        int lower = (!i) ? (num-1) : (i-1);
        int middle = i;
        int upper = (i == num-1) ? (0) : (i+1);

        float cross = b2Cross(_points[middle] - _points[lower], 
                              _points[upper] - _points[middle]);
        if (!i) {
            isPositive = (cross > 0);
        } else if (isPositive != (cross > 0)) {
            return false;
        }
    }

    return true;
}

bool Triangulator::Polygon::add(Triangle tri) {
    /**
     * This method has been shortened and optimized slightly. See the original
     * implementation for a more verbose version in ActionScript. The link
     * can be found in the header-comment for Triangulator.
     */
    
    // Consider two floats equal if the absolute difference is silly tiny
    const float maxDiff = 0.000001f;
    auto feq = [maxDiff](float a, float b) -> bool { 
        return fabs(a) - fabs(b) < maxDiff; 
    };

    auto v2eq = [feq](Vec2 a, Vec2 b) -> bool {
        return feq(a.x, b.x) && feq(a.y, b.y);
    };
    
    // Xmatch[0] is the index of the first vertex from both Polygon and Triangle
    // that matches - Xmatch[1] is the second.
    int pmatch[2] = {-1, -1};
    int tmatch[2] = {-1, -1};

    std::array<Vec2,3> tripts = tri.getPoints();

    // If we find a second match, pmatch[1] will be positive. Break out of the
    // loop if this is ever the case.
    for (int i=0; i<_points.size() && pmatch[1] == -1; i++) {
        for (int j=0; j<3; j++) {
            if (v2eq(tripts[j], _points[i])) {
                if (pmatch[0] == -1) {
                    // We found the first match
                    pmatch[0] = i;
                    tmatch[0] = j;
                } else {
                    // We found the second match - no need to continue.
                    pmatch[1] = i;
                    tmatch[1] = j;

                    // Break out of the inner loop; the outer loop will abort
                    // as pmatch[1] is now !(-1).
                    break;
                }
            }
        }
    }

    // As we go from 0 to N, the second match in the Polygon could be the last
    // element of the vertices. Swap them so they are in "correct" order.
    if (pmatch[0] == 0 && pmatch[1] == _points.size() - 1) {
        pmatch[0] = _points.size() - 1;
        pmatch[1] = 0;
    }

    // We may have found ONE match, in which case match[x][0] will be a positive
    // value. If we didn't find two matches, match[x][1] will be negative.
    if (pmatch[1] == -1) {
        return false;
    }

    // Find the "tip"-index of the triangle; the new vertex to insert into the
    // polygon.
    int tip = 0;
    if (tmatch[0] == tip || tmatch[1] == tip)
        tip = 1;
    if (tmatch[0] == tip || tmatch[1] == tip)
        tip = 2;

    // Insert the tip between _points[pmatch[0]] and _points[pmatch[1]]. Just
    // the tip. Just to see how it feels. 
    _points.insert(_points.begin() + pmatch[1], tripts[tip]);

    // Take it out if it feels wrong.
    if (!isConvex()) {
        _points.erase(_points.begin() + pmatch[1]);
        return false;
    }

    // Everything is OK.
    return true;
}


/*
==================
Triangulator
==================
*/
Triangulator::Triangulator(std::vector<Vec2> verts) 
    :   _verts(verts) {}

std::vector<Triangulator::Triangle> Triangulator::triangulateShape(bool reversal) {
    std::vector<Triangle> tribuf;
    std::vector<Vec2> vertCopy(_verts);

    if (_verts.size() < 3) {
        Log::error("Triangulator: Unable to triangulate < 3 vertices");
        return tribuf;
    }

    while (_verts.size() > 3) {
        int earIndex = -1;

        for (int i=0; i<_verts.size(); i++) {
            if (isEar(i)) {
                earIndex = i;
                break;
            }
        }

        // If we've found no ear, the concave polygon is somehow malformed. The
        // shape may contain holes or other unsupported tomfoollery.
        if (earIndex < 0) {
            if (reversal) {
                Log::warning("No ears.. Attempting to reverse winding");
                // Reverse the vertices and try again
                std::reverse(vertCopy.begin(), vertCopy.end());
                _verts = vertCopy;
                return triangulateShape(false);
            }

            Log::error("Triangulator: Unable to find any ears on the polygon");
            tribuf.clear();
            return tribuf;
        }

        // Create a triangle with the ear and cut it off
        int under = (!earIndex) ? (_verts.size()-1) : (earIndex-1);
        int over = (earIndex == _verts.size()-1) ? (0) : (earIndex+1);
        tribuf.push_back(Triangle(_verts[under], _verts[earIndex], _verts[over]));

        _verts.erase(_verts.begin() + earIndex);
    }

    // We should now have exactly 3 vertices remaining.
    assert(_verts.size() == 3);

    tribuf.push_back(Triangle(_verts[0], _verts[1], _verts[2]));

    return tribuf;
}

bool Triangulator::isEar(int index) {
    const int num = _verts.size();

    int lower = (!index) ? (num-1) : (index-1);
    int middle = index;
    int upper = (index == num-1) ? (0) : (index+1);

    Vec2 v0 = _verts[middle] - _verts[lower];
    Vec2 v1 = _verts[upper] - _verts[middle];
    float cross = (v0.x * v1.y) - (v1.x * v0.y);

    if (cross > 0.f) {
        return false;
    }

    Triangle tri(_verts[middle], _verts[upper], _verts[lower]);
    for (int i=0; i<_verts.size(); i++) {
        if (i != lower && i != middle && i != upper) {
            if (tri.isInside(_verts[i])) {
                return false;
            }
        }
    }

    return true;
}
