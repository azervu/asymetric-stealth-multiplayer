/**
 * @file    source/shared/ServiceLocator.h
 *
 * @brief   Declares the service and service locator class.
 */

#pragma once

#include <map>
#include "Settings.h"

class PacketFactory;
class PathTranslator;
class EventManager;
class ChatMessageService;
namespace SpriteManager {
    class Manager;
}

typedef unsigned ServiceId;
const ServiceId GENERAL_SERVICE_FIRST_ID = 1;
const ServiceId TARGET_SPECIFIC_SERVICE_FIRST_ID = 10000;


/**
 * Abstract superclass for services. A service is an instance that provides
 * general and often required functionality.
 */
class Service {
public:
    virtual ~Service() {}

    /**
     * Services may initialize additionaly by overriding this method. The 
     * Settings-instance contains settings from the config file and the
     * command line.
     */
    virtual void init(Settings *settings) {};

    /**
     * Some services depend on other services. When the services are destroyed,
     * there are no guarantee in which order they are deleted. Hence, this 
     * method is called on all services before they get destroyed so that 
     * services depending on other services can make peace before they die.
     */
    virtual void onExit() {};
};


/**
 * The service locator is resopnsible for providing access to services
 * initialized elsewhere in the application. This is an alternative to the
 * singleton pattern.
 *
 * Services are provided to the service locator after they have been
 * instantiated. At this point, they are owned by the service locator and will
 * be deleted by it.
 *
 * The ServiceLocator is intended to be used as a general superclass used as
 * a base for target-specific subclasses for the server and the game client.
 * Services required by BOTH targets can be added to this class, and the IDs are
 * required to start at GENERAL_SERVICE_FIRST_ID. Subclasses are required to
 * start their target-specific services at TARGET_SPECIFIC_SERVICE_FIRST_ID.
 */
class ServiceLocator {
public:
    /**
     * Multi-target services are required to start at GENERAL_SERVICE_FIRST_ID.
     */
    enum class GeneralServiceId : ServiceId {
        PATH_TRANSLATOR = GENERAL_SERVICE_FIRST_ID,
        PACKET_FACTORY,
        EVENT_MANAGER,
        CHAT_SERVICE,
        SPRITE_MANAGER
    };

    /**
     * Add a service to the locator with the specified ID. If a Service with the
     * ID exists, it will be deleted and replaced.
     * @param service       The service to add.
     * @param id            The ID of the service, used for later access
     */
    void provide(Service* service, ServiceId id);

    /**
     * Get a service with the specified ID, given that it has been provided.
     * @param id            The ID of the service to retrieve
     * @return              A provided service if the ID is valid, otherwise
     *                      NULL.
     */
    Service* getService(ServiceId id);

    void provide(PathTranslator* pathTranslator);
    PathTranslator* getPathTranslator();

    void provide(PacketFactory* pktFactory);
    PacketFactory* getPacketFactory();

    void provide(EventManager* evtMgr);
    EventManager* getEventManager();

    void provide(ChatMessageService* chatServ);
    ChatMessageService* getChatService();

    void provide(SpriteManager::Manager* spriteMgr);
    SpriteManager::Manager* getSpriteManager();

    /**
     * Cleanup of registered services. This method deletes and nullfies all
     * registered services. Should only be called once, and with caution.
     */
    void destroyServices();

    static ServiceLocator* singleton();

protected:

    static ServiceLocator* _singleton;

private:
    std::map<ServiceId, Service*> _services;
};
