/**
 * @file    source/shared/GamePlayScene.h
 *
 * @brief   Declares the game play scene class.
 */

#pragma once

#include "Scene.h"
#include "GameObject.h"
#include "EventManager.h"
#include "Player.h"
#include "ContactListener.h"

#include <vector>
#include <atomic>

class Map;


/**
 * The Scene managing the game flow and execution. 
 *
 * The server and client differ slightly in the implementations. See
 * ClientGameScene and server::ServerGameScene.
 */
class GamePlayScene : public Scene {
public:
    GamePlayScene(std::string mapFile);
    virtual ~GamePlayScene();

    virtual bool loadScene();

    /**
     * Implementers of logicUpdate are responsible for calling updatePhysics()
     * and sendPhysicsUpdatedEvent().
     */
    virtual void logicUpdate(DeltaTime deltaTime) = 0;

    virtual void onResume();
    virtual Scene::State getState();
    virtual Scene* getNextScene();

    const Map* getMap() const;

protected:
    /**
     * Creates a new Player and adds it to the scene. Components used in 
     * all situations (REMOTE and LOCAL) are added to it.
     */
    Player* createCommonPlayer(PlayerId playerId, Team team, PlayerType ptype, std::string name);
    virtual Player* createRemotePlayer(PlayerId playerId, Team team, std::string name) = 0;

    void setSceneState(Scene::State state);
    void setNextScene(Scene *scene);

    virtual void createMapObjects();

    b2World* getWorld();

    void updatePhysics(DeltaTime timeStep);
    void sendPhysicsUpdatedEvent();

private:
    std::string _mapFile;
    int32 _velocityIterations = 6;
    int32 _positionIterations = 2;
    b2World* _world;
    ContactListener _conList;
    std::vector<GameObject*> _gameObjects;
    Map* _map;

    Scene::State _state;
    Scene* _nextScene;
};

