#pragma once

#include "GameObject.h"
#include "DrawableComponent.h"
#include "BodyCreator.h"
#include "CollisionFlags.h"
#include "PhysicsComponent.h"
#include "Timer.h"
#include "SharedEvents.h"

const float GRENADE_COOLDOWN = 2.f;
const float GRENADE_RADIUS = 2.f;


class GrenadeControlComponent : public Component {
public:
    virtual void onExplode() = 0;

    virtual GrenadeType getGrenadeType() = 0;
    virtual std::string getSpriteName() = 0;
    virtual float getFuseTime() = 0;

    virtual void update(DeltaTime dt) {
        // Override if required
    }
};


/**
 * Grenade is a fully fleshed out implementation of a Grenade, including all
 * Event launchers and listeners. The only thing missing is a handler for what
 * happens when the grenade explodes, which is managed by an instance of a
 * GrenadeControlComponent. Due to the ipmlementation of Component creation,
 * the addition of the GrenadeControlComponent must be templated.
 *
 * The client side version only explodes as a reaction to GrenadeExplodeEvent,
 * and the server side version times the explosion and fires a 
 * GrenadeExplodeEvent when appropriate.
 */
template<class CtrlComp>
class Grenade : public GameObject {
    static_assert(std::is_base_of<GrenadeControlComponent,CtrlComp>::value,
                  "Grenade requires a GrenadeControlComponent");

    // The server attaches a TimerComponent to itself. Grenade::explode is
    // called when the timer expires.
#ifdef ASM_SERVER
    class TimerComponent : public Component {
    public:
        TimerComponent(Grenade<CtrlComp> *grenade) {
            _grenade = grenade;
        }

        void update(DeltaTime dt) {
            auto ctrlComp = getComponent<CtrlComp>(_grenade);
            if (_timer.getElapsedSeconds() >= ctrlComp->getFuseTime()) {
                _grenade->explode();
            }
        }

    private:
        Grenade<CtrlComp> *_grenade;
        Timer _timer;
    };

    friend class TimerComponent;
#endif

public:
    Grenade(b2World *world, PlayerId parentId, Vec2 pos, float angle) 
        :   _playerId(parentId) {

        // The player angle is 90 degrees off. Adjust it.
        angle += M_PI / 2.f;

        setPosition(pos);
        setRotation(angle);

#ifdef ASM_CLIENT
        // Listen for events
        _explodeDelegate.bind(this, &Grenade::onGrenadeExplode);   
        auto mgr = ServiceLocator::singleton()->getEventManager();
        mgr->addListener(_explodeDelegate, GrenadeExplodeEvent::eventType);
#endif
#ifdef ASM_SERVER
        addComponent<TimerComponent>(this, this);
#endif

        // Create the grenade-control component
        addComponent<CtrlComp>(this);
        _ctrlComp = getComponent<CtrlComp>(this);

        // Create the DrawableComponent
        SpriteProvider *prov = new SpriteProvider(_ctrlComp->getSpriteName(), this);
        prov->setSize({0.5f, 0.5f});
        addComponent<DrawableComponent>(this, prov, true);

        // Create the PhysicsComponent
        BodyCreator bc(world);
        bc.setRectangularShape(0.1f, 0.2f);
        bc.setCategoryBits(CollisionFlag::GRENADE);
        bc.setMaskBits(CollisionFlag::WALL);
        bc.setDynamic();

        addComponent<PhysicsComponent>(this);
        getComponent<PhysicsComponent>(this)->createBody(bc);

        // "Throw" the grenade
        b2Body *body = getComponent<PhysicsComponent>(this)->getBody();
        body->SetTransform(pos, 0.f);
        body->SetLinearDamping(1.f);
        body->SetLinearVelocity(b2Vec2(cos(angle) * 3.f, sin(angle) * 3.f));
    }


private:
    PlayerId _playerId;
    CtrlComp *_ctrlComp;

    /* The client lisens for GrenadeExplodeEvent and reacts to it by calling 
     * explode().
     */
#ifdef ASM_CLIENT
    EventListenerDelegate _explodeDelegate;

    void onGrenadeExplode(const EventData *e) {
        auto mgr = ServiceLocator::singleton()->getEventManager();
        mgr->removeListener(_explodeDelegate);

        explode();
    }
#endif

    /**
     * Remove self from parent GO or Scene. 
     */
    void explode() {
        // The server broadcasts a GrenadeExplodeEvent when exploding.
#ifdef ASM_SERVER
        auto evt = new GrenadeExplodeEvent(getWorldPosition(), 
                                           _ctrlComp->getGrenadeType());
        auto mgr = ServiceLocator::singleton()->getEventManager();
        mgr->queueEvent(evt);
#endif

        getComponent<CtrlComp>(this)->onExplode();

        if (getParent() != nullptr) {
            getParent()->removeChild(this, true);
        } else {
            getScene()->removeGameObject(this);
        }
    }

};

