/**
 * @file    source/shared/Settings.h
 *
 * @brief   Declares the settings class.
 */

#pragma once

#include <map>
#include <vector>
#include <string>

class Settings {
public:
    
    Settings();
    Settings(const std::string &configFile);
    Settings(int argc, const char **argv);
    ~Settings();

    /**
     * @fn  void Settings::loadSettingsFile(const std::string &configFile);
     *
     * @brief   Loads settings file.
     *
     * @param   configFile  The configuration file.
     */

    void loadSettingsFile(const std::string &configFile);

    /**
     * @fn  bool Settings::keyExists(const std::string &key);
     *
     * @brief   Queries if a given key exists.
     *
     * @param   key The key.
     *
     * @return  true if it exists, false otherwise.
     */

    bool keyExists(const std::string &key);

    /**
     * @fn  const std::string Settings::getString(const std::string &key, const std::string &defaultValue = "");
     *
     * @brief   Gets the value that key refers to as a string.
     *
     * @param   key             The key.
     * @param   defaultValue    (Optional) the default value if no key exists.
     *
     * @return  The value as a std::string.
     */

    const std::string getString(const std::string &key, const std::string &defaultValue = "");

    /**
     * @fn  int Settings::getInt(const std::string &key, int defaultValue = 0, int base = 10);
     *
     * @brief   Gets the value that key refers to as an int.
     *
     * @param   key             The key.
     * @param   defaultValue    (Optional) the default value if no key exists or value is invalid as
     *                          an integer.
     * @param   base            (Optional) the base of the number to load.
     *
     * @return  The value as an int.
     */

    int getInt(const std::string &key, int defaultValue = 0, int base = 10);

    /**
     * @fn  long Settings::getLong(const std::string &key, long defaultValue = 0, int base = 10);
     *
     * @brief   Gets the value that key refers to as an int.
     *
     * @param   key             The key.
     * @param   defaultValue    (Optional) the default value if no key exists or value is invalid as
     *                          an long integer.
     * @param   base            (Optional) the base of the number to load.
     *
     * @return  The value as an int.
     */

    long getLong(const std::string &key, long defaultValue = 0, int base = 10);


    /**
     * @fn  double Settings::getDouble(const std::string &key, double defaultValue = 0.0);
     *
     * @brief   Gets the value that key refers to as a double precision float.
     *
     * @param   key             The key.
     * @param   defaultValue    (Optional) the default value if no key exists or value is invalid as an double.
     *
     * @return  The double.
     */

    double getDouble(const std::string &key, double defaultValue = 0.0);

    /**
     * @fn  bool Settings::getBool(const std::string &key, bool defaultValue = false);
     *
     * @brief   Gets the value that key refers to as a boolean.
     *
     * @param   key             The key.
     * @param   defaultValue    (Optional) true to default value if no key exists or value is invalid as an boolean.
     *
     * @return  the value as a bool.
     */

    bool getBool(const std::string &key, bool defaultValue = false);

    /**
     * @fn  static void Settings::setDefaultConfig(const std::string &configFile);
     *
     * @brief   Sets the default configuration used for Settings constructor.
     *
     * @param   configFile  The configuration file path.
     */

    static void setDefaultConfig(const std::string &configFile);

private:

    void parseLine(const std::string &line);

    std::map<std::string, std::string> _parameters;

    std::vector<std::string> _argList;
};