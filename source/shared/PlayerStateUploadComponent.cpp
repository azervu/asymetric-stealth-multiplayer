#include "PlayerStateUploadComponent.h"
#include "ServiceLocator.h"
#include "EventManager.h"
#include "SharedEvents.h"

PlayerStateUploadComponent::PlayerStateUploadComponent() 
    :   _lastStep(0) {
    _physicsEvent.bind(this, &PlayerStateUploadComponent::onPhysicsUpdated);

    EventManager *emgr = ServiceLocator::singleton()->getEventManager();
    emgr->addListener(_physicsEvent, PhysicsUpdatedEvent::eventType);
}

PlayerStateUploadComponent::~PlayerStateUploadComponent() {
    EventManager *emgr = ServiceLocator::singleton()->getEventManager();
    emgr->removeListener(_physicsEvent);
}

void PlayerStateUploadComponent::update(DeltaTime dt) {

}

void PlayerStateUploadComponent::onPhysicsUpdated(const EventData *e) {
    const double minTime = 1.0 / (double)PLAYER_STATE_UPDATE_FREQ;
    const double elapsed = _timer.getElapsedSeconds();

    if (elapsed > minTime) {
        _timer.start();

        PlayerId playerId = getPlayerId();
        Vec2 vel = getPlayer()->getVelocity();
        Vec2 pos = getPlayer()->getPosition();
        float angVel = getPlayer()->getAngularVelocity();
        float rot = getPlayer()->getRotation();

        unsigned step = ++_lastStep;

        // Trigger an update-event
        PlayerTransformUpdateEvent *evt = nullptr;
        NetworkDirection dir = NetworkDirection::NET_OUT;
        evt = new PlayerTransformUpdateEvent(playerId, pos, vel, 
                                             angVel, rot, dir, step);
        
        EventManager *emgr = ServiceLocator::singleton()->getEventManager();
        emgr->triggerEvent(evt);
    }
}
