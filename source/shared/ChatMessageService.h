/**
 * @file    source/shared/ChatMessageService.h
 *
 * @brief   Declares the chat message service class.
 */

#pragma once

#include "ServiceLocator.h"
#include "EventManager.h"
#include <atomic>

class ChatMessageService : public Service {
public:
    ChatMessageService();
    ~ChatMessageService();

    virtual void onExit();
    
    void sendChatMessage(const std::string &message) const;
    void MessageReceived(const EventData *e) const;

private:

    void run();

    EventListenerDelegate _receiveMessage;

    std::atomic_bool _quit;
};
