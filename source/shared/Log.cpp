#include "Log.h"

#ifdef WIN32
    #include <Windows.h>
#endif

#include <map>
#include <chrono>
#include <iomanip>
#include <mutex>

/***** Static initialization *****/
Log::Level Log::_logLevel = Log::Level::LOG_INFO;
bool Log::_timeStamp = true;
auto START_TIME = std::chrono::steady_clock::now();

std::mutex logMutex;

std::ofstream Log::_file;
const std::string LOG_TIME_PARAM = "log-time";
const std::string LOG_LEVEL_PARAM = "log-level";
const std::string LOG_FILE_PARAM = "log-file";
std::map<std::string, Log::Level> levelLookup = {
    {"ALL"      ,Log::Level::LOG_ALL     },
    {"INFO"     ,Log::Level::LOG_INFO    },
    {"DEBUG"    ,Log::Level::LOG_DEBUG   },
    {"WARNING"  ,Log::Level::LOG_WARNING },
    {"ERROR"    ,Log::Level::LOG_ERROR   },
    {"FATAL"    ,Log::Level::LOG_FATAL   },
    {"NONE"     ,Log::Level::LOG_NONE    }
};

/***** Static Public Methods *****/
std::string Log::levelToString(Log::Level level) {
    switch (level) {
        case Log::Level::LOG_ALL:
            return "ALL";
        case Log::Level::LOG_INFO:
            return "INFO";
        case Log::Level::LOG_DEBUG:
            return "DEBUG";
        case Log::Level::LOG_WARNING:
            return "WARNING";
        case Log::Level::LOG_ERROR:
            return "ERROR";
        case Log::Level::LOG_FATAL:
            return "FATAL";
        case Log::Level::LOG_NONE:
            return "NONE";
        default:
            return "";
    }
}

void Log::setSettings(Settings* settings) {
    Log::setTimestamp(settings->getBool(LOG_TIME_PARAM, _timeStamp));

    std::string logLevel = settings->getString(LOG_LEVEL_PARAM, Log::levelToString(Log::Level::LOG_INFO));
    
    if (levelLookup.find(logLevel) == levelLookup.end()) {

        Log::error("Log::setSettings: malformed \"%s\" parameter: %s", 
            LOG_LEVEL_PARAM.c_str(), logLevel.c_str());
        
        logLevel = Log::levelToString(Log::Level::LOG_INFO);
    }

    Log::setLevel(levelLookup[logLevel]);


    std::string logFile = settings->getString(LOG_FILE_PARAM);

    if (logFile.size() != 0) {
        _file.open(logFile);
        if (!_file.good()) {
            _file.close();

            Log::error("Log::setSettings: invalid log file name: %s", logFile.c_str());
        }
    }
}

void Log::setLevel(Log::Level level) {
    _logLevel = level;
}

void Log::setTimestamp(bool timeStamp) {
    _timeStamp = timeStamp;
}

bool Log::setLogFile(std::string file) {
    useStdout();
    _file.open(file.c_str());

    return _file.is_open();
}

bool Log::useStdout() {
    if (_file.is_open()) {
        _file.close();
        return true;
    }

    return false;
}



/**
* Macro for including the log-level in the log-message.
*
* @param LEVEL             A member of the Log::Level enum.
* @param COLOR             An ANSI color code (see definitions above).
*/
#define WRITE_LOG(LEVEL)                                            \
    if ((int)_logLevel <= (int)LEVEL) {                             \
        va_list args;                                               \
        va_start(args, format);                                     \
        write(LEVEL, format, args);                                 \
        va_end(args);                                               \
    }

void Log::info(std::string format, ...) {
    WRITE_LOG(Log::Level::LOG_INFO);
}

void Log::debug(std::string format, ...) {
    WRITE_LOG(Log::Level::LOG_DEBUG);
}

void Log::warning(std::string format, ...) {
    WRITE_LOG(Log::Level::LOG_WARNING);
}

void Log::error(std::string format, ...) {
    WRITE_LOG(Log::Level::LOG_ERROR);
}

void Log::fatal(std::string format, ...) {
    WRITE_LOG(Log::Level::LOG_FATAL);
}

/***** Static Private Methods *****/

/**
* Terminal color (TC) codes.
*/
#ifndef WIN32
#define TC_NORMAL      "\033[0m"
#define TC_GREEN       "\033[0;32m"
#define TC_YELLOW      "\033[0;33m"
#define TC_BLUE        "\033[0;34m"
#define TC_RED         "\033[0;31m"
#define TC_BOLDRED     "\033[1;31m"

void Log::colorOutput(Log::Level level) {
    switch (level) {
        case Level::LOG_NONE:
        case Level::LOG_ALL:
            printf(TC_NORMAL);
        break;
        
        case Level::LOG_INFO:
            printf(TC_GREEN);
        break;
    
        case Level::LOG_DEBUG:
            printf(TC_BLUE);
        break;
    
        case Level::LOG_WARNING:
            printf(TC_YELLOW);
        break;
    
        case Level::LOG_ERROR:
            printf(TC_RED);
        break;
    
        case Level::LOG_FATAL:
            printf(TC_BOLDRED);
        break;
    }
}

#else
#define TERMC_NORMAL       0x07
#define TERMC_INFO         0x0A
#define TERMC_DEBUG        0x0D
#define TERMC_WARNING      0x06
#define TERMC_ERROR        0x0C
#define TERMC_FATAL        0x04

void Log::colorOutput(Log::Level level) {
    WORD color = 0x00;

    switch (level) {
        case Level::LOG_INFO:
            color = TERMC_INFO;
        break;

        case Level::LOG_DEBUG:
            color = TERMC_DEBUG;
        break;

        case Level::LOG_WARNING:
            color = TERMC_WARNING;
        break;

        case Level::LOG_ERROR:
            color = TERMC_ERROR;
        break;

        case Level::LOG_FATAL:
            color = TERMC_FATAL;
        break;

        default:
            color = TERMC_NORMAL;
        break;
    }

    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), color);
}
#endif

void Log::write(Log::Level lvl, std::string format, va_list args) {
    static char chbuf[1024];
    auto diff = std::chrono::steady_clock::now() - START_TIME;

    int mill = std::chrono::duration_cast<std::chrono::milliseconds>(diff).count() % 1000;
    int sec = std::chrono::duration_cast<std::chrono::seconds>(diff).count() % 60;
    int min = std::chrono::duration_cast<std::chrono::minutes>(diff).count();

    std::lock_guard<std::mutex> lock(logMutex);

    vsprintf(chbuf, format.c_str(), args);

    if (!_file.is_open()) {
        colorOutput(Level::LOG_NONE);

        if (_timeStamp) {
            printf("%04i:%02i:%03i", min, sec, mill);
        }

        printf("[");
        colorOutput(lvl);
        printf("%s", levelToString(lvl).c_str());
        colorOutput(Level::LOG_NONE);
        printf("]:%*c%s\n", (int)(8-levelToString(lvl).size()), ' ', chbuf);
    } else {
        _file
            << std::setfill('0') << std::setw(4) << min << ':'
            << std::setfill('0') << std::setw(2) << sec << ':'
            << std::setfill('0') << std::setw(3) << mill
            << "[" << levelToString(lvl).c_str() << "]: " 
            << chbuf << "\n";
    }
}
