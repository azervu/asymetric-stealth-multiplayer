/**
 * @file    source/shared/Settings.cpp
 *
 * @brief   Implements the settings class.
 */

#include "Settings.h"

#include <algorithm>
#include <fstream>

#include "Log.h"

static std::string DEFAULT_FILE = "settings_default.cfg";
static const std::string COMMAND_BEGIN = "--";

void Settings::setDefaultConfig(const std::string &configFile) {
    DEFAULT_FILE = configFile;
}

Settings::Settings() : Settings(DEFAULT_FILE) {
}

Settings::Settings(const std::string &configFile) : _parameters(), _argList() {
    this->loadSettingsFile(configFile);
}

Settings::Settings(int argc, const char **argv) : Settings() {

    this->_parameters["path"] = std::string(argv[0]);

    for (int i = 1; i < argc; i++) {
        std::string line = std::string(argv[i]);
        _argList.push_back(line);

        if (line.find_first_of(COMMAND_BEGIN) != 0) {
            continue;
        }

        this->parseLine(line.substr(COMMAND_BEGIN.size()));
    }
}

Settings::~Settings() {
    // TODO anything?
}

void Settings::loadSettingsFile(const std::string &configFile) {
    std::ifstream stream(configFile);
    std::string line;

    if (stream.is_open() == true && !stream.bad()) {
        while (std::getline(stream, line)) {
            this->parseLine(line);
        }

        stream.close();
    } else {
        Log::error("Settings: %s is missing", configFile.c_str());
    }
}

void Settings::parseLine(const std::string &line) {
    // # = comment
    if (line.size() == 0 || line.front() == '#') {
        //Log::info("Settings line skipped: %s", line.c_str());
        return;
    }

    std::string key;
    std::string value;

    if (line.find('=') == std::string::npos) {
        key = line;
        value = "true";
    } else {
        key = line.substr(0, line.find_first_of('='));
        value = line.substr(line.find_first_of('=') + 1);
    }

    if (value.size() == 0) {
        Log::error("Settings::parseLine: Value can't be empty string \"%s\"", line.c_str());
        return;
    }

    if (value.at(0) == '\"' && value.at(value.size() - 1) == '\"') {
        value = value.substr(1, value.size() - 2);
    }

    this->_parameters[key] = value;
}

bool Settings::keyExists(const std::string &key) {
    return (this->_parameters.find(key) != this->_parameters.end());
}

const std::string Settings::getString(const std::string &key, const std::string &defaultValue) {
    try {
        return this->_parameters.at(key);
    } catch (std::out_of_range e) {
        Log::debug("Settings: %s has no value, using default instead", key.c_str());
        return defaultValue;
    }
}

int Settings::getInt(const std::string &key, int defaultValue, int base) {
    try {
        return std::stoi(this->getString(key), 0, base);
    } catch (std::invalid_argument e) {
        Log::debug("Settings: [%s]->[%s] not an int, using default instead", key.c_str(), this->getString(key).c_str());
        return defaultValue;
    } catch (std::out_of_range e) {
        Log::debug("Settings: [%s]->[%s] invalid range for int, using default instead", key.c_str(), this->getString(key).c_str());
        return defaultValue;
    }
}

long Settings::getLong(const std::string &key, long defaultValue, int base) {
    try {
        return std::stol(this->getString(key), 0, base);
    } catch (std::invalid_argument e) {
        Log::debug("Settings: [%s]->[%s] not an long, using default instead", key.c_str(), this->getString(key).c_str());
        return defaultValue;
    } catch (std::out_of_range e) {
        Log::debug("Settings: [%s]->[%s] invalid range for long, using default instead", key.c_str(), this->getString(key).c_str());
        return defaultValue;
    }
}

double Settings::getDouble(const std::string &key, double defaultValue) {
    try {
        return std::stod(this->getString(key));
    } catch (std::invalid_argument e) {
        Log::debug("Settings: [%s]->[%s] not an double, using default instead", key.c_str(), this->getString(key).c_str());
        return defaultValue;
    }
}

bool Settings::getBool(const std::string &key, bool defaultValue) {
    std::string value = this->getString(key);

    if (value == "true") {
        return true;
    } else if (value == "false") {
        return false;
    } else {
        Log::debug("Settings: [%s]->[%s] not an bool, using default instead", key.c_str(), this->getString(key).c_str());
        return defaultValue;
    }
}