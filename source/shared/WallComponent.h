#pragma once

#include "GameObject.h"
#include "PhysicsComponent.h"

#include <vector>


class WallComponent : public Component {
public:
    /**
     * Creates the physics for a wall-tile based
     * on a tile-coordinate and the flags of the tile at the specified index.
     * The tile flags must contain the flag TILE_FLAG and any permutation of the 
     * four directional wall-flags (NE, NW, SE, SW).
     * @param world     The b2World to create the wall in.
     * @param x         The X-tile coordinate of the tile.
     * @param y         The Y-tile coordinate of the tile.
     * @param flags     All the TileFlags of the specified tile.
     */
    void createPhysics(b2World *world, int x, int y, unsigned tileFlags);

    const std::vector<b2Body*> getBodies();

    virtual void update(DeltaTime dt);

private:
    std::vector<b2Body*> _bodies;

    void createQuadrant(b2World *world, b2Vec2 relOffset, unsigned tileFlags);
};

