#include "ChatMessageService.h"

#include <string>

#include "Log.h"
#include "Utility.h"
#include "ChatMessageEvent.h"

ChatMessageService::ChatMessageService() {
    this->_quit.store(false);
    this->_receiveMessage.bind(this, &ChatMessageService::MessageReceived);

    EventManager * eventManager = ServiceLocator::singleton()->getEventManager();
    eventManager->addListener(this->_receiveMessage, MessageReceivedEvent::eventType);
};

ChatMessageService::~ChatMessageService() {
    this->_quit.store(true);
};

void ChatMessageService::run() {
    std::string message("");

    while (message.compare("QUIT") == false && _quit.load() == false) {
        message = utility::getString("Chat: ", false);
        
        this->sendChatMessage(message);
    }
};

void ChatMessageService::onExit() {
    EventManager * eventManager = ServiceLocator::singleton()->getEventManager();
    eventManager->removeListener(_receiveMessage);
}

void ChatMessageService::sendChatMessage(const std::string &message) const {
}

void ChatMessageService::MessageReceived(const EventData *e) const {
    const MessageReceivedEvent* messageEvent = (MessageReceivedEvent*) e;

    Log::info("%s: %s", messageEvent->getUsername().c_str(), messageEvent->getMessage().c_str());
}
