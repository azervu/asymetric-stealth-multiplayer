/**
 * @file    source/shared/Subsystem.h
 *
 * @brief   Declares the subsystem class.
 */

#pragma once

#include "Engine.h"

/**
 * @class Subsystem
 * Defines encapsulation of major functionality. Subsystems are
 * independent, high level components. Communication between
 * subsystems is done via the Event System.
 */
class Subsystem {
public:
    virtual ~Subsystem() {}
    
    /**
     * Initialize the subsystem. This method is called during startup of the 
     * game. All services are initialized and ready for use by the time init
     * is called.
     *
     * @param settings  The settings to use
     * @return          True if the initialization was successful, false if 
     *                  the subsystem is in such an unusable state that the
     *                  program should terminate immediately.
     */
    virtual bool init(Settings* settings) = 0;

    /**
     * The update method of a Subsystem is where the Subsystem is
     * expected to do  it's work. The update() method should be called every
     * game-tick.
     * @param dt        The delta time, in whatever DeltaTime is defined to be.
     */
    virtual void update(DeltaTime dt) = 0;

protected:

};
