
#include "StatsComponent.h"
#include "ServiceLocator.h"

#include <math.h>

StatsComponent::StatsComponent(Team team) {
    _agility = 100.f;
    _strength = 100.f;
    if (team == Team::SPY) {
        _agility *= 1.2f;
    }
    if (team == Team::MERC) {
        _strength *= 1.2f;
    }


}


void StatsComponent::limitHp(int& hp){
    if (hp < 0)
        hp = 0;
    if (hp > _strength)
        hp = _strength;
}

StatsComponent::~StatsComponent() {
}

void StatsComponent::update(DeltaTime dt) {
}

float StatsComponent::getSpeed() {

    return _agility / 80.f + 2.5f;
}

float StatsComponent::getPower() {
    return _strength / 20.f;
}

float StatsComponent::getTurn() {
    return _agility / 30.f;
}

float StatsComponent::getTorque() {
    return _strength / 5.f;
}