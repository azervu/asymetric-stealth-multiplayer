#pragma once

#include "EventManager.h"
#include "Player.h"


/**
 * Player State Download Component does not directly download player state
 * updates, but it does APPLY the remote state locally. The incoming state 
 * change is dispathced from the network-interface via a 
 * "PlayerTransformUpdateEvent"-event.
 */
class PlayerStateDownloadComponent : public PlayerComponent {
public:
    PlayerStateDownloadComponent();
    ~PlayerStateDownloadComponent();

    virtual void update(DeltaTime dt);

private:
    PlayerId _playerId;
    unsigned _lastStep;

    /* PlayerTransformUpdateEvent callback method
     */
    EventListenerDelegate _ptransDelegate;
    void onPlayerTransformUpdate(const EventData *e);
};
