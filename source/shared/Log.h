/**
 * @file    source/shared/Log.h
 *
 * @brief   Declares the log class and that is responsible for program wide logging functionality
 */

#pragma once

#include <fstream>
#include <cstdarg>

#include "Settings.h"

class Log {
public:
	enum class Level : int {
        LOG_ALL     = 0,
		LOG_INFO    = 1,
        LOG_DEBUG   = 2,
        LOG_WARNING = 3,
        LOG_ERROR   = 4,
        LOG_FATAL   = 5,
        LOG_NONE    = 6,
	};

    static std::string levelToString(Log::Level level);

    static void setSettings(Settings* settings);

    /**
     * Set the desired log-level. Any output from a LOWER log level 
     * will be ignored.
     * 
     * @param level             The lowest allowed level of importance
     */
	static void setLevel(Level level);

    /**
     * Outputs the log to a file rather than stdout. If the file exists, it
     * will be overwritten. 
     *
     * @param file              The name of the file to output the log to.
     * @return                  True if opening the file went ok, else false.
     */
	static bool setLogFile(std::string file);

    /**
     * Enable or disable the output of the current timestamp (in seconds since
     * program start) in all log-messages.
     */
    static void setTimestamp(bool timeStamp);

    /**
     * If it exists, close the log-file handle and output any future logging 
     * to stdout.
     *
     * @return                  True if the file was closed, false otherwise.
     */
	static bool useStdout();

	static void info(std::string format, ...);
	static void debug(std::string format, ...);
	static void warning(std::string format, ...);
	static void error(std::string format, ...);
    static void fatal(std::string format, ...);

private:
    /**
     * Change the color outputted to the terminal, based on the log-level.
     * Level::LOG_NONE will reset the terminal to the default colors.
     */
    static void colorOutput(Log::Level level);

    /**
     * Write a log entry to STDOUT or _file (if it is open). 
     * @param lvl               The log level of this message
     * @param format            String containing format data
     * @param args              An assembled va_list.
     */
	static void write(Log::Level lvl, std::string format, va_list args);

	static Level _logLevel;
    static bool _timeStamp;
	static std::ofstream _file;
};


