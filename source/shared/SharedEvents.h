/**
 * @brief   Events that are used on both the server and the client side. These
 *          events largely relate to the gameplay itself, and the instantiation
 *          of said gameplay.
 */

#pragma once

#include "Engine.h"
#include "EventManager.h"
#include "Player.h"
#include "Protocol.h"


class TriggerComponent;


/**
 * Issued after the map has loaded and the game should create the player 
 * game objects. These events are fired locally from the objects managing the
 * clients in the lobby (server::LobbyController and NetworkSystem).
 *
 * The response to this event should be instantiating a GameObject with the
 * correct player-behaviour component attached. 
 */
class CreatePlayerEvent : public EventData {
public:
    static const EventType eventType;

    CreatePlayerEvent(PlayerId id, std::string name, PlayerType type, Team t);

    EventType getEventType() const;
    PlayerId getPlayerId() const;
    std::string getPlayerName() const;
    PlayerType getPlayerType() const;
    Team getTeam() const;

private:
    PlayerId _playerId;
    std::string _name;
    PlayerType _type;
    Team _team;
};


/**
 * Fired when a player should respawn. The life-cycle of this event is ALWAYS
 * triggered by server::GameController, and is propagated via LobbyController
 * to the clients.
 *
 * Respond to this event by moving the related player to the defined position.
 */
class SpawnPlayerEvent : public EventData {
public:
    static const EventType eventType;

    SpawnPlayerEvent(PlayerId id, Vec2 pos);

    EventType getEventType() const;
    PlayerId getPlayerId() const;
    Vec2 getPosition() const;

private:
    PlayerId _playerId;
    Vec2 _position;
};


/**
 * Fired when a player object has updated it's position and wishes to notify a
 * NetworkInterface, or when the NetworkInterface has received a transform
 * update packet and wishes to notify the out-of-date player object.
 *
 * The direction of the event can be read from 'getDirection()'. A value of
 * OUT means that the event should be propagated to some remote peer. A value 
 * of IN means that the event is directed at a player object.
 *
 * When firing the event, increment the "step"-variable from the last event.
 * When handling the event, ignore the contents if the "step" is lower than
 * something you have previously handled.
 */
class PlayerTransformUpdateEvent : public EventData {
public:
    static const EventType eventType;

    PlayerTransformUpdateEvent(PlayerId id, Vec2 pos, Vec2 vel, 
                               float rot, float angVel, 
                               NetworkDirection dir, unsigned step);

    EventType getEventType() const;
    NetworkDirection getDirection() const;
    PlayerId getPlayerId() const;
    Vec2 getPosition() const;
    Vec2 getVelocity() const;
    float getAngularVelocity() const;
    float getRotation() const;
    unsigned getStep() const;

private:
    PlayerId _playerId;
    Vec2 _position;
    Vec2 _velocity;
    float _angVel;
    float _rotation;
    NetworkDirection _direction;
    unsigned _step;
};


/**
 * TRIGGERED by GamePlayeScene after the physics has been updated. 
 */
class PhysicsUpdatedEvent : public EventData {
public:
    static const EventType eventType;

    EventType getEventType() const;
};


/**
 * Fired when the game ends.
 *
 * Server side:
 *  - Fired by LobbyController when all players have disconnected
 *
 * Client side:
 *  - Fired by NetworkClient when packet "GameOver" is received.
 */
class GameOverEvent : public EventData {
public:
    static const EventType eventType;

    GameOverEvent(Team winningTeam, std::string reason);

    EventType getEventType() const;
    Team getWinningTeam() const;
    std::string getReason() const;

private:
    Team _winner;
    std::string _reason;
};



/*
==================
Contact Listener Events
==================
*/
class ContactListenerEvent : public EventData {
public:
    ContactListenerEvent(b2Fixture *fixA, b2Fixture *fixB);

    b2Fixture* getFixtureA() const;
    b2Fixture* getFixtureB() const;
    bool isRelevant(b2Body *body) const;

private:
    b2Fixture *_fixA;
    b2Fixture *_fixB;
};

/* Fired when two b2Bodies start touching */
class ContactBegunEvent : public ContactListenerEvent {
public:
    static const EventType eventType;

    ContactBegunEvent(b2Fixture *fixA, b2Fixture *fixB);
    EventType getEventType() const;
};

/* Fired when two b2Bodies stop touching */
class ContactEndedEvent : public ContactListenerEvent {
public:
    static const EventType eventType;

    ContactEndedEvent(b2Fixture *fixA, b2Fixture *fixB);
    EventType getEventType() const;
};


/*
==================
Objective Events
==================
*/

/* Fired by the TriggerComponent when a spy initiates an objective or a 
 * merc initiates the shutdown of a hack.
 */
class ObjectiveInitiatedEvent : public EventData {
public:
    static const EventType eventType;

    ObjectiveInitiatedEvent(PlayerId id, Team team, ObjectiveId objId);

    PlayerId getPlayerId() const;
    Team getTeam() const;
    ObjectiveId getObjectiveId() const;
    EventType getEventType() const;

private:
    PlayerId _playerId;
    Team _team;
    ObjectiveId _objId;
};

/* Fired by the server when the objective was successfully activated, 
 * deactivated or successfully hacked, or by the client when the objective
 * hack was aborted.
 */
class ObjectiveStateChangedEvent : public EventData {
public:
    static const EventType eventType;

    ObjectiveStateChangedEvent(ObjectiveId id, ObjectiveState state, 
                               PlayerId playerId, NetworkDirection dir);

    ObjectiveId getObjectiveId() const;
    ObjectiveState getObjectiveState() const;
    PlayerId getPlayerId() const;
    NetworkDirection getNetworkDirection() const;
    EventType getEventType() const;

private:
    ObjectiveId _objId;
    ObjectiveState _objState;
    PlayerId _playerId;
    NetworkDirection _direction;
};

/**
 * Fired when a gun is fired.
 */
class GunFiredEvent : public EventData {
public:
    static const EventType eventType;

    GunFiredEvent(PlayerId playerId, b2Vec2 gunPoint, b2Vec2 endPoint);

    PlayerId getPlayerId() const;
    b2Vec2 getGunPoint() const;
    b2Vec2 getEndPoint() const;
    EventType getEventType() const;

private:
    PlayerId _playerId;
    b2Vec2 _gunPt;
    b2Vec2 _endPt;
};

/**
 * Fired when a bullet hits a player.
 */
class BulletHitEvent : public EventData {
public:
    static const EventType eventType;

    BulletHitEvent(PlayerId firingId, PlayerId hitId, int health, bool didKill);

    PlayerId getFiringPlayerId() const;
    PlayerId getHitPlayerId() const;
    int getPlayerHealth() const;
    bool didPlayerDie() const;
    EventType getEventType() const;

private:
    PlayerId _firePlayerId;
    PlayerId _hitPlayerId;
    int _health;
    bool _kill;
};

/**
 * Fired when a grenade is thrown.
 */
class GrenadeThrowEvent : public EventData {
public:
    static const EventType eventType;

    GrenadeThrowEvent(NetworkDirection dir, PlayerId id, Vec2 pos, float rot);

    NetworkDirection getNetworkDirection() const;
    PlayerId getPlayerId() const;
    Vec2 getPosition() const;
    float getRotation() const;
    EventType getEventType() const;

private:
    NetworkDirection _dir;
    PlayerId _playerId;
    Vec2 _pos;
    float _rot;
};

/**
 * Fired from the server when a grenade explodes at a certain location.
 */
class GrenadeExplodeEvent : public EventData {
public:
    static const EventType eventType;

    GrenadeExplodeEvent(Vec2 pos, GrenadeType type);

    Vec2 getPosition() const;
    GrenadeType getGrenadeType() const;
    EventType getEventType() const;

private:
    Vec2 _pos;
    GrenadeType _type;
};

/* Fired by the server when a player is hit by a grenade. The hit factor is
 * a value between 0.0 and 1.0 indicating how close to the blast the player
 * was, where 0.0 is closest and 1.0 is the furthest away.
 */
class GrenadeHitEvent : public EventData {
public:
    static const EventType eventType;

    GrenadeHitEvent(PlayerId pid, GrenadeType type, float hitFactor);  

    PlayerId getPlayerId() const;
    GrenadeType getGrenadeType() const;
    float getHitFactor() const;
    EventType getEventType() const;

private:
    PlayerId _playerId;
    GrenadeType _type;
    float _hitFactor;
};

/**
 * Fired when a GameObject is to be deleted. Deletion must occur at a safe point
 * in time to prevent altering a list being iterated over. The GameObject 
 * should NOT be (a) removed from it's parent GameObject, or (b) removed from
 * the parent Scene until this event is caught by it's parent.
 */
class DeleteGameObjectEvent : public EventData {
public:
    static const EventType eventType;

    DeleteGameObjectEvent(GameObject *gameObject);

    GameObject *getGameObject() const;
    EventType getEventType() const;

private:
    GameObject *_gameObject;
};
