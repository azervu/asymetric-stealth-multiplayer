#pragma once

#include "GameObject.h"
#include "EventManager.h"
#include <functional>

class BodyCreator;


/**
 * The PhysicsComponent creates a physical body on a GameObject. The physics 
 * component is authorative over the transform of the GameObject.
 */
class PhysicsComponent : public Component {
public:
    enum class Shape {
        ROUND,
        SQUARE
    };

    enum class Type {
        DYNAMIC,
        STATIC
    };

    /**
     * Create a physics component that initializes a b2Body and adds it to the 
     * world. 
     * @param world     The b2World instance to add the body to.
     * @param shape     One of the supported shapes (square & circle currently)
     * @param type      Static or dynamic body.
     * @param radius    Defines the radius of the circle shape or HALF the width
     *                  of the square. 
     */
    PhysicsComponent();
    virtual ~PhysicsComponent();

    void createBody(b2World *world, Shape shape, Type type, float radius);
    void createBody(BodyCreator &bc);

    virtual void update(DeltaTime dt);

    void move(DeltaTime dt, b2Vec2 direction, float maxSpeed, float maxForce);
    void turn(DeltaTime dt, int dir, float32 maxTurn, float32 maxTorque);
    void aim(DeltaTime dt, float angle, float32 maxTurnSpeed, float32 maxTorque);

    void setPosition(b2Vec2 pos);
    void setAngularVelocity(float angVel);
    void setRotation(float rotation);
    void setCollisionFilter(const b2Filter &filter);

    b2Vec2 getVelocity() const;
    float getAngularVelocity() const;
    void setLinearVelocity(b2Vec2 velocity);

    /**
     * Changes the transform of the b2Body to be that of the GameObject this
     * instance is attached to.
     */
    void moveToGameObject();

    b2Body* getBody() const;

protected:
    struct MoveData {
        bool ordercommandGiven;
        b2Vec2 direction;
        float maxSpeed;
        float maxForce;
    };
    struct AimData {
        bool ordercommandGiven;
        float angle;
        float32 maxTurnSpeed;
        float32 maxTorque;
    };

    /* PhysicsUpdatedEvent delegate method. The GameObject owning this component
     * must be moved to the position of the body, and this must occur after the
     * b2World has been updated.
     */
    EventListenerDelegate _physicsDelegate;
    void onPhysicsUpdated(const EventData *e);

private:
    std::vector<void(*)(DeltaTime)> _commands;

    b2World *_world;
    mutable b2Body *_body;
};
