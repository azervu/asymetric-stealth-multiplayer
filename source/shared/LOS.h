#pragma once

#include "Engine.h"


/**
 * LOS provides functionality to check if there is a clear line of sight
 * between two b2Bodies.
 */
class LOS {
public:
    /* Check if there is a clear LOS between two bodies. 
     * @param world     The world. Duh.
     * @param b1        One of the bodies.
     * @param b2        Another body.
     * @param catFilter Category bits of fixtures that will break the LOS.
     */
    static bool isLOS(b2World *world, b2Body *b1, b2Body *b2, unsigned catFilter);
};
