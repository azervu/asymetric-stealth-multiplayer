/**
 * @file    source/shared/WallObject.cpp
 *
 * @brief   Implements the wall object class.
 */

#include "WallObject.h"

#include "WallComponent.h"
#include "DrawableComponent.h"

class WallDebugDrawer : public DrawableProvider {
public:
    WallDebugDrawer(WallComponent* wcRef)
        : _wcRef(wcRef)  {
    };

#ifdef ASM_CLIENT
    virtual Drawable::BaseDrawable* getDrawable() {

        Drawable::GeometryCloud* cloud = new Drawable::GeometryCloud(Drawable::GeometryCloud::TYPE::LINE);

        std::vector<b2Body*> list = _wcRef->getBodies();

        for (const b2Body* body : list) {
            const b2Fixture* f = body->GetFixtureList();

            while (f) {
                f = f->GetNext();
            }
        }

        cloud->addVertex({
            
        });


        return cloud;
    }
#endif

private:
    WallComponent* _wcRef;
};

WallObject::WallObject(b2World* world, int x, int y, unsigned flags)
    :   GameObject()    {

    addComponent<WallComponent>(this);
    

    WallComponent* wc = getComponent<WallComponent>(this);
    wc->createPhysics(world, x, y, flags);

    //addComponent<DrawableComponent>(this, new WallDebugDrawer(wc));
    //DrawableComponent* dc = getComponent<DrawableComponent>(this);
}

WallObject::~WallObject() {
}