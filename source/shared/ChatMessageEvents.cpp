/**
 * @file    source/client/ChatMessageEvents.cpp
 *
 * @brief   Implements the chat message events class.
 */

#include "ChatMessageEvent.h"
/*
==================
MessageSendEvent
==================
*/
const EventType MessageSendEvent::eventType = 0x8B453ACE2B8D4CED;

MessageSendEvent::MessageSendEvent(std::string message)
    :   _message(message) {
}

EventType MessageSendEvent::getEventType() const {
    return eventType;
}

std::string MessageSendEvent::getMessage() const {
    return _message;
}

/*
==================
MessageReceivedEvent
==================
*/
const EventType MessageReceivedEvent::eventType = 0xE033A94399704618;

MessageReceivedEvent::MessageReceivedEvent(std::string username, std::string message)
    :   _username(username),
        _message(message) {
}

EventType MessageReceivedEvent::getEventType() const {
    return eventType;
}

std::string MessageReceivedEvent::getUsername() const {
    return _username;
}

std::string MessageReceivedEvent::getMessage() const {
    return _message;
}
