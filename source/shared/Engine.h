/**
 * @file    source/shared/Engine.h
 *
 * @brief   Container file for common type definitions, global functions and
 *          other commonly required features. Do not include ANY header files
 *          which MAY in turn include Engine.h. This file is included in all
 *          project targets, so only functionality required by all targets
 *          which rarely change should be contained in here.
 */

#pragma once 

/* Include-loop safe header files
 */
#include "Log.h"
#include "SDLHeaders.h"

#include <Box2D/Box2D.h>


/**
 * One meter in metric game coordinates corresponds to 64 pixels on the screen
 * when drawn in 100% scale.
 */
const int ASM_PIXELS_PER_METER = 64;

/**
 * Tiles are always ASM_TILE_SIZE * ASM_TILE_SIZE pixels in dimension.
 */
const int ASM_TILE_SIZE = 16;

/**
 * There are 4 tiles in each dimension per meter. 16 tiles fit into one m^2.
 */
const float ASM_TILES_PER_METER = (float)(ASM_PIXELS_PER_METER / ASM_TILE_SIZE);;

/**
 * @typedef long double DeltaTime
 *
 * @brief   Common type containing the delta time. Used as the parameter to most update()
 *          methods.
 */

typedef long double DeltaTime;

typedef unsigned char byte;

typedef size_t ResourceId;

/**
 * @brief   Unique identified of a client on the network.
 */
typedef unsigned int PlayerId;

enum class Team : byte {
    NONE,
    SPY,
    MERC,
};

enum class ObjectiveId : byte {
    NONE, A, B, C
};

enum class ObjectiveState : byte {
    // Spies are able to hack this objective
    AVAILABLE,

    // A spy has hacked this objective, and mercs can stop it
    TRIGGERED,

    // The objective was successfully hacked
    HACKED,

    // Another objective is being hacked, this objective is rendered
    // unusable for now. 
    DISABLED,
};

enum class GrenadeType : unsigned {
    FLASHBANG,
};


/**
 * @brief   General purpose typedef around an existing 2-column vector 
 *          implementation.
 */
typedef b2Vec2 Vec2;

struct Rect {
    Rect() {
        x = y = w = h = 0.f;
    }

    Rect(float px, float py, float pw, float ph) {
        x = px;
        y = py;
        w = pw;
        h = ph;
    }

    Vec2 topLeft() const {
        return Vec2(x, y);
    }

    Vec2 topRight() const {
        return Vec2(x + w, y);
    }
    
    Vec2 botLeft() const {
        return Vec2(x, y + h);
    }

    Vec2 botRight() const {
        return Vec2(x + w, y + h);
    }

    bool contains(Vec2 pt) const {
        return  pt.x >= x && pt.y >= y &&
                pt.x <= x+w &&
                pt.y <= y+h;
    }

    float x;
    float y;
    float w;
    float h;
};

struct Color {
    float r;
    float g;
    float b;
    float a;
};

// Settings
static const std::string PRELOADING_DEFAULT_FILE_NAME = "preload.json";
static const std::string PRELOADING_FILE_NAME_KEY = "preloading-file";

#define RAD_TO_DEG(_X)      (_X * (180.f / M_PI))
#define DEG_TO_RAD(_X)      (_X * (M_PI / 180.f))
