#include "PlayerStateDownloadComponent.h"
#include "SharedEvents.h"


PlayerStateDownloadComponent::PlayerStateDownloadComponent() 
    :   _lastStep(0) {
    _ptransDelegate.bind(this, &PlayerStateDownloadComponent::onPlayerTransformUpdate);
    
    EventManager *emgr = ServiceLocator::singleton()->getEventManager();
    emgr->addListener(_ptransDelegate, PlayerTransformUpdateEvent::eventType);
}

PlayerStateDownloadComponent::~PlayerStateDownloadComponent() {
    EventManager *emgr = ServiceLocator::singleton()->getEventManager();
    emgr->removeListener(_ptransDelegate);
}

void PlayerStateDownloadComponent::update(DeltaTime dt) { }


void PlayerStateDownloadComponent::onPlayerTransformUpdate(const EventData *e) {
    PlayerTransformUpdateEvent *evt = (PlayerTransformUpdateEvent*)e;

    if (evt->getDirection() == NetworkDirection::NET_IN
        && evt->getPlayerId() == getPlayerId()) {

        unsigned step = evt->getStep();
        if (step > _lastStep) {
            _lastStep = step;
            getPlayer()->setRotation(evt->getRotation());
            getPlayer()->setAngularVelocity(evt->getAngularVelocity());

            // Don't update the position OR velocity of the local player.
            if (getPlayer()->getPlayerType() != PlayerType::LOCAL) {
                getPlayer()->setVelocity(evt->getVelocity());
                getPlayer()->setPosition(evt->getPosition());
            }
        }
    }
}
