/**
 * @file    source/client/GameState.h
 *
 * @brief   Declares the Scene class.
 */

#pragma once

#include "Engine.h"
#include "GameObject.h"

#ifdef ASM_CLIENT
#include "Renderable.h"
#endif //ASM_CLIENT

#include <vector>
#include <mutex>

/**
 * This class contains the current state of the game.
 * Every instance except the first are launched from another Scene. Scenes are responsible for constructing and destructing their children.
 * Only one Scene is active at anyone time, so initialising a state transition means that the current Scene will not be able to run until it's child is finished.
 * At that the child can be queried about what happened.
 */
class Scene {
public:
    enum class State {
        RUNNING,
        SET_CHILD_ACTIVE,
        RETURN_TO_PARENT,
        EXIT_APPLICATION,
    };

    Scene();
	virtual ~Scene();

    void updateGameObjects(DeltaTime dt);

    /**
     * Default initializer method for Scene instances.
     * @return          True if the initialization was successful, false 
     *                  if the Scene is in an unusable state and should be 
     *                  discarded immediately.
     */
    virtual bool loadScene() = 0;

    virtual void logicUpdate(DeltaTime) = 0;
    virtual void onResume() = 0;
	virtual Scene::State getState() = 0;
	virtual Scene* getNextScene() = 0;

    void addGameObject(GameObject *gameObject);

    /**
     * Find a GameObject anywhere in the game object hierarchy
     */
    GameObject* findGameObject(GameObject::Id id);

    /**
     * Removes and deletes the passed game object if it is found anywhere in
     * the game object hierarchy.
     * @return          True if it was found and was properly discarded.
     */
    bool removeGameObject(GameObject *gameObject);
    bool removeGameObject(GameObject::Id id);

#ifdef ASM_CLIENT
    virtual std::vector<Drawable::BaseDrawable*>& getRenderVector();
    void fillGraphicsBuffer();
#endif //ASM_CLIENT

protected:
    void destroyGameObjects();

private:

#ifdef ASM_CLIENT
    std::vector<Drawable::BaseDrawable*> _graphicsBuffer;
#endif //ASM_CLIENT

    std::vector<GameObject*> _gameObjects;
    std::recursive_mutex _listGuard;
    EventListenerDelegate _delGobjDelegate;
    void onDeleteGameObject(const EventData *e);
};
