#include "Conversation.h"
#include "Log.h"


bool Conversation::start(SocketBase *socket) {
    Log::warning("Conversation::start() is not overriden! Was it called by "
                 "a mistake?");
    return false;
}
