#pragma once

#include "GameObject.h"
#include "EventManager.h"
#include "Timer.h"
#include "Grenade.h"
#include "Engine.h"

class Player;


/**
 * GrenadeThrowComponent allows a Player object to throw a grenade. This 
 * component can be used on both local and remote players. Components attached
 * to local players will throw a grenade upon a "right click", while components
 * attached to remote players will throw a grenade upon an incoming 
 * GrenadeThrowEvent.
 */
class GrenadeThrowComponent : public Component {
public:
    GrenadeThrowComponent(b2World *world, GrenadeType type);
    ~GrenadeThrowComponent();

    void update(DeltaTime dt);

private:
    b2World *_world;
    Timer _timer;
    GrenadeType _type;

    // Do NOT access this directly. Use getPlayer() instead.
    Player *_player;

#ifdef ASM_CLIENT
    EventListenerDelegate _inputDelegate;
    void onButtonClicked(const EventData *e);
#endif // ASM_CLIENT

    EventListenerDelegate _throwDelegate;
    void onGrenadeThrow(const EventData *e);

    void throwGrenade();
    void throwGrenade(Vec2 position, float rotation);

    Player* getPlayer();
};



class FlashbangComponent : public GrenadeControlComponent {
public:
    virtual void onExplode();

    virtual GrenadeType getGrenadeType();
    virtual std::string getSpriteName();
    virtual float getFuseTime();
};

