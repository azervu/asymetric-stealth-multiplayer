#pragma once

#include <vector>

#include "Player.h"
#include "Timer.h"
#include "EventManager.h"

const int PLAYER_STATE_UPDATE_FREQ = 20;

/**
 * A Player State Upload Component (PSUC) updates locally held generic 
 * player data (position, rotation) on a remote peer. The component
 * fires PLAYER_STATE_UPDATE_FREQ events per second which are expected to be 
 * picked up by the object(s) responsible for sending the data across the 
 * network.
 *
 * The physics-state is only broadcasted for updating as the response to a
 * PhsyicsUpdatedEvent. 
 */
class PlayerStateUploadComponent : public PlayerComponent {
public:
    PlayerStateUploadComponent();
    ~PlayerStateUploadComponent();

    virtual void update(DeltaTime dt);

private:
    Timer _timer;
    unsigned _lastStep;

    /* PhysicsUpdatedEvent callback delegate
     */
    EventListenerDelegate _physicsEvent;
    void onPhysicsUpdated(const EventData *e);
};
