/**
 * @brief   File including all SDL headers required by the game server. This
 *          file is necessary to avoid cluttering the code-base with conditional
 *          includes whenever something from SDL is included.
 */

#pragma once

#ifdef ASM_UNITTEST
    #define ASM_CLIENT
    #define ASM_SERVER
#endif

#ifdef WIN32
    #include <SDL.h>
    #include <SDL_net.h>
#else
    #include <SDL2/SDL.h>
    #include <SDL2/SDL_net.h>
#endif

// Client only headers
#ifdef ASM_CLIENT
    #ifdef WIN32
        #include <gl/glew.h>    //necessary or glew WILL complain as it MUST be loaded before gl.h
        #include <SDL_opengl.h>
        #include <SDL_ttf.h>
    #else
        #include <GL/glew.h>
        #include <SDL2/SDL_opengl.h>
        #include <SDL2/SDL_ttf.h>
    #endif
#endif

// Server only headers
#ifdef ASM_SERVER
    
#endif

