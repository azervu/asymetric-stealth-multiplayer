#include "LOS.h"
#include "RayCastCallback.h"


bool LOS::isLOS(b2World *world, b2Body *b1, b2Body *b2, unsigned catFilter) {
    RayCastCallback cb(catFilter);
    
    world->RayCast(&cb, b1->GetPosition(), b2->GetPosition());

    // Assume clear line of sight. No hit fixtures is an indicator that no
    // fixtures matching the filter were found. The ray DID hit the other body.
    if (!cb.didHitAnything()) {
        return true;
    }

    return cb.getHitBody() == b2;
}
