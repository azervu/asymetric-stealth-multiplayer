#include "GameObject.h"
#include "SharedEvents.h"

#include <cmath>


/*
==================
ComponentAggregator
==================
*/
ComponentAggregator::~ComponentAggregator() {
    for (auto iter = _components.begin(); iter != _components.end(); iter++) {
        delete iter->second;
    }
}

void ComponentAggregator::update(DeltaTime deltaTime) {
    for (auto iter = _components.begin(); iter != _components.end(); iter++) {
        iter->second->update(deltaTime);
    }
}

/*
==================
GameObject
==================
*/
GameObject::GameObject() 
    :   _parent(nullptr),
        _scene(nullptr) {
    static GameObject::Id idCounter;
    _id = ++idCounter;
}

GameObject::~GameObject() {
    // Remove self from parent
    if (_parent) {
        // No need to delete, we are obviously already being deleted
        _parent->removeChild(this, false);
    }

    // Delete children
    while (_children.size()) {
        removeChild(_children.back(), true);
    }
}

GameObject::Id GameObject::getId() const {
    return _id;
}

void GameObject::setPosition(Vec2 vec) {
    _position = vec;
}

Vec2 GameObject::getPosition() const {
    return _position;
}

Vec2 GameObject::getWorldPosition() const {
    if (_parent) {
        return _parent->getWorldPosition() + _position;
    }

    return _position;
}

void GameObject::setRotation(float rotation) {
    _rotation = rotation;
}

float GameObject::getRotation() const {
    return _rotation;
}

float GameObject::getWorldRotation() const {
    if (_parent) {
        return _parent->getWorldRotation() + _rotation;
    }
    
    return _rotation;
}

GameObject* GameObject::getParent() {
    return _parent;
}

void GameObject::setScene(Scene *scene) {
    _scene = scene;
}

Scene* GameObject::getScene() const {
    return _scene;
}

GameObject* GameObject::findChild(GameObject::Id id) {
    for (GameObject *c : _children) {
        if (c->_id == id)
            return c;
    }

    GameObject *needle;
    for (GameObject *c : _children) {
        needle = c->findChild(id);
        if (needle)
            return needle;
    }

    return nullptr;
}

bool GameObject::addChild(GameObject *go) {
    if (go->_parent) {
        Log::error("Unable to add GameObject %i to GameObject %i: child "
                   "object already has a parent.", go->getId(), getId());
        return false;
    }

    _children.push_back(go);
    go->_parent = this;
    go->setScene(getScene());
    return true;
}

bool GameObject::isChild(GameObject *go) {
    return (go->_parent == this);
}

bool GameObject::isChild(GameObject::Id id) {
    for (GameObject *c : _children) {
        if (c->_id == id) {
            return true;
        }
    }
    return false;
}

bool GameObject::removeChild(GameObject *go, bool del) {
    return removeChild(go->_id, del);
}

bool GameObject::removeChild(GameObject::Id id, bool del) {
    for (auto it = _children.begin(); it != _children.end(); it++) {
        if ((*it)->_id == id) {
            (*it)->_parent = nullptr;

            if (del) {
                EventManager *emgr = ServiceLocator::singleton()->getEventManager();
                emgr->queueEvent(new DeleteGameObjectEvent(*it));
            }  else {
                (*it)->setScene(nullptr);
            }

            _children.erase(it);
            return true;
        }
    }

    return false;
}

const std::vector<GameObject*>& GameObject::getChildren() const {
    return _children;
}



/*
==================
ComponentManager
==================
*/
ComponentManager::ComponentManager(GameObject *gameObject) 
    :   _gameObject(gameObject) {

}

bool ComponentManager::addComponent(Component *component, ComponentId id) {
    if (hasComponent(id)) {
        return false;
    }

    component->_gameObject = _gameObject;
    _gameObject->_components[id] = component;
    return true;
}

bool ComponentManager::removeComponent(ComponentId id) {
    if (!hasComponent(id)) {
        return false;
    }

    _gameObject->_components.erase(id);
    return true;
}

bool ComponentManager::hasComponent(ComponentId id) {
    return (_gameObject->_components.count(id) != 0);
}

Component* ComponentManager::getComponent(ComponentId id) {
    if (!hasComponent(id)) {
        return nullptr;
    }

    return _gameObject->_components[id];
}



/*
==================
Component
==================
*/
Component::Component() {

}

Component::~Component() {

}

GameObject* Component::getGameObject() const {
#ifdef DEBUG
    if (!_gameObject) {
        Log::warning("Component::getGameObject() called on Component before "
                     "a GameObject has been associated with it. Are you "
                     "attempting to access it from the constructor?");
    }
#endif

    return _gameObject;
}
