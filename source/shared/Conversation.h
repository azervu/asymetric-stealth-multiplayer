#pragma once

class SocketBase;

/**
 * A conversation is a tiny subset of the networking protocol, in which a 
 * host makes a request to it's peer, and filters out all other
 * retrieved packets to be handled by someone else. For instance, imagine a 
 * client attempting to change his name, change his team, send a chat message 
 * and flag himself as ready at the same time.
 *
 * Imagine the following scenario. ClientA is connected to the server as the
 * sole client, and wishes to change his name. The packet diagram is as follows:
 *
 *  CLIENT_A                    SERVER
 *     |--->nameChangeRequest---->| 
 *     |<-- nameChangeResponse<---|
 *
 * In this scenario, there is no need to filter out anything, because the server
 * only has one thing to respond to. Imagine two clients changing their names at
 * the same time:
 *
 *  CLIENT_A                    SERVER                    CLIENT_B
 *     |--->nameChangeRequest---->|<---nameChangeRequest<----| 
 *     |<-- clientBChangedName<---|--->nameChangeResponse--->|
 *     |<---nameChangeResponse<---|--->clientAChangedName--->|
 *
 * In the latter scenario, if clientA does no filtering of any incoming
 * packets, he will be thoroughly confused and disconnect.
 *
 * Conversation simplifies this process by only keeping track of whatever is
 * relevant. A Conversation-implementation "ChangeNameConversation" would first
 * send a "nameChangeRequest", and ignore any packets which are not 
 * "nameChangeResponse". This allows multiple conversations to occur at the 
 * same time, which drastically simplifies things during the lobby-phase of the
 * network protocol.
 *
 * The Conversation-interface is *NOT* designed to support mulitple instances
 * waiting for the same type of packets simultaneously. I.e., don't attempt to
 * change your name while changing your name.
 */
class Conversation {
public:
    /**
     * Starts the conversation. This method should not be used if the
     * conversation is not initiated by this host. For instance, the 
     * conversation to change a client's name is not initiated by the server. 
     *
     * @return          True on success, false on error. Error indicates that
     *                  the conversation failed and that this instance should
     *                  be discarded.
     */
    virtual bool start(SocketBase *socket);

    /**
     * Where the Conversations do their work.
     *
     * @return          True on success, false on error. Error indicates that
     *                  the conversation failed and that this instance should
     *                  be discarded.
     */
    virtual bool update(SocketBase *socket) = 0;

    /**
     * Check to see if the Conversation is over. 
     */
    virtual bool isDone() = 0;
};

