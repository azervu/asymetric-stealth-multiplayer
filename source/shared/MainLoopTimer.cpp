#include "MainLoopTimer.h"

#include "Log.h"

MainLoopTimer::MainLoopTimer() {
    currentTime = SDL_GetTicks();
    lastTime = SDL_GetTicks();

    logInterval = 0;
}

DeltaTime MainLoopTimer::getTimeStep() {
    currentTime = SDL_GetTicks();
    DeltaTime timeStep = currentTime - lastTime;
    if (timeStep < MIN_TIME_STEP) {
        SDL_Delay(MIN_TIME_STEP - timeStep);
        timeStep = MIN_TIME_STEP;
    }
    lastTime = currentTime;
    return timeStep/1000.0f;
}


/*




*/