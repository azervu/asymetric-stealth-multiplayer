/**
 * @file    source/shared/DrawableComponent.cpp
 *
 * @brief   Implements the drawable component class.
 */

#include "DrawableComponent.h"
#include "ServiceLocator.h"
#include "SpriteManager.h"
#include "GameObject.h"

std::hash<std::string> HASHER;

SpriteProvider::SpriteProvider(const std::string& spriteName) 
    :   _parent(nullptr) {

    this->_frameNumber = 0;
    this->_spriteId = HASHER(spriteName);
    this->_frames = ServiceLocator::singleton()->getSpriteManager()->getSprite(_spriteId)->_images.size();
}

SpriteProvider::SpriteProvider(const std::string& spriteName, GameObject *parent) 
    :   SpriteProvider(spriteName) {
    _parent = parent;
}

#ifdef ASM_CLIENT
Drawable::BaseDrawable* SpriteProvider::getDrawable() {
    assert(_spriteId != 0);

    Drawable::Sprite* drawable = new Drawable::Sprite(_spriteId, _frameNumber, _depth);

    Vec2 position = _pos;
    float angle = _angle;

    if (_parent) {
        position += _parent->getWorldPosition();
        angle += _parent->getWorldRotation();
    }

    drawable->setPosition(position);
    drawable->setSize(_size);
    drawable->setAngle(angle);
    drawable->setAnchor({_anchor.x * _size.x, _anchor.y * _size.y});

    if (this->_alwaysVisible) {
        drawable->tagOverride(Drawable::DrawableData::DataType::OVERLAY);
    }

    return drawable;
}
#endif //ASM_CLIENT

void SpriteProvider::setPosition(Vec2 pos) {
    this->_pos = pos;
}

void SpriteProvider::setX(float x) {
    this->_pos.x = x;
}

void SpriteProvider::setY(float y) {
    this->_pos.y = y;
}

void SpriteProvider::setSize(Vec2 size) {
    this->_size = size;
}

void SpriteProvider::setWidth(float width) {
    this->_size.x = width;
}

void SpriteProvider::setHeight(float height) {
    this->_size.y = height;
}

void SpriteProvider::setAnchor(Vec2 anchor) {
    this->_anchor = anchor;
}

void SpriteProvider::setRotation(float angle) {
    this->_angle = angle;
}

void SpriteProvider::setDepth(float depth) {
    this->_depth = depth;
}

void SpriteProvider::setFrame(int frameNumber) {
    this->_frameNumber = frameNumber % this->_frames;
}

void SpriteProvider::setAlwaysVisible(bool visible) {
    this->_alwaysVisible = visible;
}

DrawableComponent::DrawableComponent(DrawableProvider* provider, bool owner)
    :   _drawProvider(provider),
        _owner(owner) {
    assert(_drawProvider != nullptr);
}

DrawableComponent::~DrawableComponent() {
    if (this->_owner) {
        delete _drawProvider;
    }
}

void DrawableComponent::update(DeltaTime delta) {
    assert(_drawProvider != nullptr);
    this->_drawProvider->update(delta);
}

#ifdef ASM_CLIENT
Drawable::BaseDrawable* DrawableComponent::getDrawable() {
    assert(_drawProvider != nullptr);
    return _drawProvider->getDrawable();
}
#endif //ASM_CLIENT
