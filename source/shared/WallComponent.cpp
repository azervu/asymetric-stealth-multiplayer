#include "WallComponent.h"
#include "Map.h"
#include "CollisionFlags.h"
#include "BodyCreator.h"


void WallComponent::createPhysics(b2World *world, int x, int y, 
                                  unsigned tileFlags) {
    Vec2 pos(x, y);
    pos.x /= ASM_TILES_PER_METER;
    pos.y /= ASM_TILES_PER_METER;
    getGameObject()->setPosition(pos);

    const float halfTile = (1.f / ASM_TILES_PER_METER) / 2.f;
    const float qTile = halfTile / 2.f;

    if (tileFlags & TileFlag::TILE_WALL) {
        if (tileFlags & TileFlag::TILE_WALL_NW)
            createQuadrant(world, b2Vec2(qTile, qTile), tileFlags);
        if (tileFlags & TileFlag::TILE_WALL_NE) 
            createQuadrant(world, b2Vec2(qTile + halfTile, qTile), tileFlags);
        if (tileFlags & TileFlag::TILE_WALL_SE)
            createQuadrant(world, b2Vec2(qTile + halfTile, qTile + halfTile), tileFlags);
        if (tileFlags & TileFlag::TILE_WALL_SW)
            createQuadrant(world, b2Vec2(qTile, qTile + halfTile), tileFlags);
    } else if (tileFlags & TileFlag::TILE_VENT) {
        createQuadrant(world, b2Vec2(-halfTile, -halfTile), tileFlags);
        createQuadrant(world, b2Vec2( halfTile, -halfTile), tileFlags);
        createQuadrant(world, b2Vec2( halfTile,  halfTile), tileFlags);
        createQuadrant(world, b2Vec2(-halfTile,  halfTile), tileFlags);
    }

    if (!_bodies.size()) {
        Log::warning("WallComponent created for tile (%i, %i), but no wall "
                     "flags are defined for this tile (%x).", x, y, tileFlags);
    }
}

void WallComponent::update(DeltaTime dt) {

}

void WallComponent::createQuadrant(b2World *world, b2Vec2 relOffset, 
                                   unsigned tileFlags) {
    const float width = (1.f / ASM_TILES_PER_METER) / 3.f;

    BodyCreator bc(world);
    bc.setStatic();
    bc.setRectangularShape(width, width);
    b2Body *body = bc.createBody();

    if (body) {
        _bodies.push_back(body);

        body->SetTransform(getGameObject()->getPosition() + relOffset, 0.f);

        b2Filter filter;

        if (tileFlags & TileFlag::TILE_VENT) {
            filter.categoryBits = CollisionFlag::VENT;
            filter.maskBits = CollisionFlag::MERC;
        } else if (tileFlags & TileFlag::TILE_WALL) {
            filter.categoryBits = CollisionFlag::WALL;
            filter.maskBits = CollisionFlag::ALL;
        } else {
            Log::warning("Neither TILE_VENT or TILE_WALL is defined in the "
                         "specified physics-quadrant (%x).", tileFlags);
        }

        b2Fixture *fix = body->GetFixtureList();
        while (fix) {
           fix->SetFilterData(filter);
           fix = fix->GetNext();
        }
    }
}

const std::vector<b2Body*> WallComponent::getBodies() {
    return this->_bodies;
}
