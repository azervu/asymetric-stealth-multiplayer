#pragma once
#include "GameObject.h"
#include "EventManager.h"
#include "PhysicsComponent.h"
#include "StatsComponent.h"
#include <vector>

class Command {
public:
    enum Type {
        NONE,
        STOP,
        MOVE,
        AIM
    };
    Type _type = STOP;
    b2Vec2 _vector = b2Vec2(0.0f, 0.0f);
};

class Control {
public:
    Control();
    virtual ~Control();
    virtual std::vector<Command> getCommands() = 0;

};

class ControlComponent: public Component {
public:
    ControlComponent();
    ControlComponent(Control* control);
    virtual ~ControlComponent();
    void update(DeltaTime dt);
private:
    void applyCommand(DeltaTime dt, Command command, PhysicsComponent* pComp, StatsComponent* sComp);
    Control *_control = NULL;
};

