#include <assert.h>
#include <stdlib.h>
#include "ServiceLocator.h"
#include "PathTranslator.h"
#include "Protocol.h"
#include "EventManager.h"
#include "ChatMessageService.h"
#include "SpriteManager.h"

ServiceLocator* ServiceLocator::_singleton = nullptr;


void ServiceLocator::provide(Service *service, ServiceId id) {
    Service *existing = getService(id);
    if (existing != NULL) {
        Log::warning("ServiceLocator: existing service provided. Deleting "
                     "old Service instance with id %u", id);
        delete existing;
    }

    _services[id] = service;
}


Service* ServiceLocator::getService(ServiceId id) {
    if (_services.count(id) == 0) {
        return NULL;
    }

    return _services[id];
}


void ServiceLocator::provide(PathTranslator* pathTranslator) {
    provide(pathTranslator, (ServiceId) GeneralServiceId::PATH_TRANSLATOR);
}

PathTranslator* ServiceLocator::getPathTranslator() {
    GeneralServiceId id = GeneralServiceId::PATH_TRANSLATOR;
    Service* service = ServiceLocator::getService((ServiceId) id);
    return (PathTranslator*) service;
}


void ServiceLocator::provide(PacketFactory* pktFactory) {
    provide(pktFactory, (ServiceId) GeneralServiceId::PACKET_FACTORY);
}

PacketFactory* ServiceLocator::getPacketFactory() {
    GeneralServiceId id = GeneralServiceId::PACKET_FACTORY;
    Service* service = ServiceLocator::getService((ServiceId) id);
    return (PacketFactory*) service;
}


void ServiceLocator::provide(EventManager* evtMgr) {
    provide(evtMgr, (ServiceId) GeneralServiceId::EVENT_MANAGER);
}

EventManager* ServiceLocator::getEventManager() {
    GeneralServiceId id = GeneralServiceId::EVENT_MANAGER;
    Service* service = ServiceLocator::getService((ServiceId) id);
    return (EventManager*) service;
}


void ServiceLocator::provide(ChatMessageService* chatServ) {
    provide(chatServ, (ServiceId) GeneralServiceId::CHAT_SERVICE);
}

ChatMessageService* ServiceLocator::getChatService() {
    GeneralServiceId id = GeneralServiceId::CHAT_SERVICE;
    Service* service = ServiceLocator::getService((ServiceId) id);
    return (ChatMessageService*) service;
}


void ServiceLocator::provide(SpriteManager::Manager* spriteMgr) {
    provide(spriteMgr, (ServiceId) GeneralServiceId::SPRITE_MANAGER);
}

SpriteManager::Manager* ServiceLocator::getSpriteManager() {
    GeneralServiceId id = GeneralServiceId::SPRITE_MANAGER;
    Service* service = ServiceLocator::getService((ServiceId) id);
    return (SpriteManager::Manager*) service;
}


void ServiceLocator::destroyServices() {
    std::map<ServiceId, Service*>::iterator iter;

    for (iter = _services.begin(); iter != _services.end(); iter++) {
        iter->second->onExit();
    }
    
    for (iter = _services.begin(); iter != _services.end(); iter++) {
        delete iter->second;
    }

    _services.clear();
}


ServiceLocator* ServiceLocator::singleton() {
    return _singleton;
}
