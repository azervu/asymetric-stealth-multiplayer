#pragma once

#include "Engine.h"
#include "GameObject.h"


/**
 * RayCastCallback finds the first fixture matching one or more of the flags
 * in the defined filter.
 */
class RayCastCallback : public b2RayCastCallback {
public:
    static const unsigned gunFireCategoryFilter;


    RayCastCallback(unsigned categoryFilter);

    float32 ReportFixture(b2Fixture* fixture, const b2Vec2& point, 
                          const b2Vec2& normal, float32 fraction);

    bool didHitAnything();
    GameObject* getHitGameObject();
    b2Body* getHitBody();
    b2Vec2 getHitPoint();

private:
    unsigned _catFilter;
    GameObject *_gameObject;
    b2Body *_body;
    b2Vec2 _point;
    bool _hit;
};
