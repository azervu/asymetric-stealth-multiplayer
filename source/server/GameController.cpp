#include "GameController.h"
#include "Settings.h"
#include "LobbyController.h"
#include "ServerGameScene.h"
#include "ServerEvents.h"
#include "SharedEvents.h"
#include "ServerServiceLocator.h"


namespace server {


GameController::GameController()
    :   _lobbyController(nullptr),
        _gameScene(nullptr) {
    _startLoadDelegate.bind(this, &GameController::onStartLoadingLevel);
    _gameOverDelegate.bind(this, &GameController::onGameOver);
    _objCompDelegate.bind(this, &GameController::onAllObjectivesCompleted);
    _timeExhaustDelegate.bind(this, &GameController::onTimeExhausted);
}

GameController::~GameController() {
    if (_gameScene) {
        delete _gameScene;
    }

    if (_lobbyController) {
        delete _lobbyController;
    }

    EventManager *emgr = ServerServiceLocator::singleton()->getEventManager();
    emgr->removeListener(_startLoadDelegate);
    emgr->removeListener(_gameOverDelegate);
    emgr->removeListener(_objCompDelegate);
    emgr->removeListener(_timeExhaustDelegate);
}

bool GameController::initialize(Settings *settings) {
    _lobbyController = new LobbyController();
    if (!_lobbyController->isConnectionListenerGood()) {
        delete _lobbyController;
        _lobbyController = nullptr;
        return false;
    }

    std::string lobbySizeString = settings->getString("lobby_size");
    int lobbySize = (lobbySizeString.size() != 0) ? std::stoi(lobbySizeString) : 4;
    _lobbyController->setLobbySize(lobbySize);

    EventManager *emgr = ServerServiceLocator::singleton()->getEventManager();
    emgr->addListener(_startLoadDelegate, StartLoadingLevelEvent::eventType);
    emgr->addListener(_gameOverDelegate, GameOverEvent::eventType);
    emgr->addListener(_objCompDelegate, AllObjectivesCompletedEvent::eventType);
    emgr->addListener(_timeExhaustDelegate, TimeExhaustedEvent::eventType);

    return true;
}

void GameController::onServerShutdown() {
    if (_lobbyController) {
        _lobbyController->onServerShutdown("Server shutting down");
    }
}

void GameController::update() {
    if (_lobbyController) {
        _lobbyController->update();
    }

    if (_gameScene) {
        // TODO
        // Proper dt
        _gameScene->updateGameObjects(1.f / 60.f);
        _gameScene->logicUpdate(1.f / 60.f);
    }
}


void GameController::onStartLoadingLevel(const EventData *e) {
    Log::debug("Loading level!");

    if (_gameScene != nullptr) {
        Log::fatal("Logical error: GameController already has a GameScene!");
        exit(1);
        return;
    }

    StartLoadingLevelEvent *evt = (StartLoadingLevelEvent*)e;
    _gameScene = new ServerGameScene(evt->getLevelName());
    
    if (!_gameScene->loadScene()) {
        _lobbyController->onServerShutdown("Server failed to load map");
        Log::fatal("Failed to load the map. Incoming exit(-1). ");
        Log::fatal("TODO: Gracefully handle this problem if it turns out to be "
                   "even remotely frequent");
        exit(-1);
    }

    // Coolio, let's start the game
    EventManager *emgr = ServerServiceLocator::singleton()->getEventManager();
    emgr->queueEvent(new GameStartingEvent);
}

void GameController::onGameOver(const EventData *e) {
    if (_gameScene == nullptr) {
        Log::warning("GameController::onGameOver: No GameScene active");
        return;
    }

    Log::debug("GameController::onGameOver: deleting GameScene");

    delete _gameScene;
    _gameScene = nullptr;
}

void GameController::onAllObjectivesCompleted(const EventData *e) {
    _lobbyController->endGame(Team::SPY, "All terminals successfully hacked!");
}

void GameController::onTimeExhausted(const EventData *e) {
    _lobbyController->endGame(Team::MERC, "Time exhausted. Mercs win!!");
}

}
