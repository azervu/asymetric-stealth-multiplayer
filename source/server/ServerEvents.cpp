#include "ServerEvents.h"
#include "Client.h"

namespace server {


/*
==================
ClientReadyToPlayEvent
==================
*/
const EventType ClientReadyToPlayEvent::eventType = 0xb9639a4b952746c4;

ClientReadyToPlayEvent::ClientReadyToPlayEvent(const Client *client) 
    :   _client(client) {

}

EventType ClientReadyToPlayEvent::getEventType() const {
    return eventType;
}

const Client* ClientReadyToPlayEvent::getClient() const {
    return _client;
}


/*
==================
ClientLoadedLevelEvent
==================
*/
const EventType ClientLoadedLevelEvent::eventType = 0x6cde5730d84649bd;

ClientLoadedLevelEvent::ClientLoadedLevelEvent(const Client *client) 
    :   _client(client) {

}

EventType ClientLoadedLevelEvent::getEventType() const {
    return eventType;
}

const Client* ClientLoadedLevelEvent::getClient() const {
    return _client;
}


/*
==================
StartLoadingLevelEvent
==================
*/
const EventType StartLoadingLevelEvent::eventType = 0x185f15e6d2e8428e;

StartLoadingLevelEvent::StartLoadingLevelEvent(std::string level)
    :   _level(level) {
}

EventType StartLoadingLevelEvent::getEventType() const {
    return eventType;
}

std::string StartLoadingLevelEvent::getLevelName() const {
    return _level;
}


/*
==================
ServerLoadedLevelEvent
==================
*/
const EventType ServerLoadedLevelEvent::eventType = 0xf89a2be742f44272;

EventType ServerLoadedLevelEvent::getEventType() const {
    return eventType;
}


/*
==================
GameStartingEvent
==================
*/
const EventType GameStartingEvent::eventType = 0x6de93b13ced94513;

EventType GameStartingEvent::getEventType() const {
    return eventType;
}


/*
==================
ClientDisconnectedEvent
==================
*/
const EventType ClientDisconnectedEvent::eventType = 0x535f226dbc5c4a36;

ClientDisconnectedEvent::ClientDisconnectedEvent(PlayerId playerId) 
    :   _playerId(playerId) {
}

EventType ClientDisconnectedEvent::getEventType() const {
    return eventType;
}

PlayerId ClientDisconnectedEvent::getPlayerId() const {
    return _playerId;
}


/*
==================
CreatePlayerEvent
==================
*/
const EventType CreatePlayerEvent::eventType = 0x44485d83457b41d4;

CreatePlayerEvent::CreatePlayerEvent(Client *client) 
    :   _client(client) {
    
}

EventType CreatePlayerEvent::getEventType() const {
    return eventType;
}

Client* CreatePlayerEvent::getClient() const {
    return _client;
}


/*
==================
PlayerDidSpawnEvent
==================
*/
const EventType PlayerDidSpawnEvent::eventType = 0xe3264ae8b1814ba5;

PlayerDidSpawnEvent::PlayerDidSpawnEvent(Vec2 position, float rotation)
    :   _position(position),
        _rotation(rotation) {

}

EventType PlayerDidSpawnEvent::getEventType() const {
    return eventType;
}

Vec2 PlayerDidSpawnEvent::getPosition() const {
    return _position;
}

float PlayerDidSpawnEvent::getRotation() const {
    return _rotation;
}


/*
==================
AllObjectivesCompletedEvent
==================
*/
const EventType AllObjectivesCompletedEvent::eventType = 0x23f38db73219448d;

EventType AllObjectivesCompletedEvent::getEventType() const {
    return eventType;
}


/*
==================
TimeExhaustedEvent
==================
*/
const EventType TimeExhaustedEvent::eventType = 0x7b0f842ac72e4cca;

EventType TimeExhaustedEvent::getEventType() const {
    return eventType;
}


}
