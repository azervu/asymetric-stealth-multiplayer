#include "ServerGameScene.h"
#include "ServerServiceLocator.h"
#include "ServerEvents.h"
#include "SharedEvents.h"
#include "Client.h"
#include "Map.h"
#include "PlayerStateDownloadComponent.h"
#include "PlayerStateUploadComponent.h"
#include "Utility.h"
#include "RayCastCallback.h"
#include "GrenadeThrowComponent.h"

namespace server {

static const float ROUND_TIME_LIMIT = 300.f;
static const float TERMINAL_HACK_TIME = 45.f;


ServerGameScene::ServerGameScene(std::string mapFile)
    :   GamePlayScene(mapFile) {
    _createPlayerDelegate.bind(this, &ServerGameScene::onCreatePlayer);
    _clientDcDelegate.bind(this, &ServerGameScene::onClientDisconnect);
    _objInitDelegate.bind(this, &ServerGameScene::onObjectiveInitiated);
    _gunFireDelegate.bind(this, &ServerGameScene::onGunFired);
    _objStateDelegate.bind(this, &ServerGameScene::onObjectiveStateChanged);
    _explodeDelegate.bind(this, &ServerGameScene::onGrenadeExplode);

    EventManager *emgr = ServerServiceLocator::singleton()->getEventManager();
    emgr->addListener(_createPlayerDelegate, CreatePlayerEvent::eventType);
    emgr->addListener(_clientDcDelegate, ClientDisconnectedEvent::eventType);
    emgr->addListener(_objInitDelegate, ObjectiveInitiatedEvent::eventType);
    emgr->addListener(_gunFireDelegate, GunFiredEvent::eventType);
    emgr->addListener(_objStateDelegate, ObjectiveStateChangedEvent::eventType);
    emgr->addListener(_explodeDelegate, GrenadeExplodeEvent::eventType);

    _objectives.push_back(Objective(ObjectiveId::A));
    _objectives.push_back(Objective(ObjectiveId::B));
    _objectives.push_back(Objective(ObjectiveId::C));
}

ServerGameScene::~ServerGameScene() {
    EventManager *emgr = ServerServiceLocator::singleton()->getEventManager();
    emgr->removeListener(_createPlayerDelegate);
    emgr->removeListener(_clientDcDelegate);
    emgr->removeListener(_objInitDelegate);
    emgr->removeListener(_gunFireDelegate);
    emgr->removeListener(_objStateDelegate);
    emgr->removeListener(_explodeDelegate);
}

bool ServerGameScene::loadScene() {
    return !GamePlayScene::loadScene() ? false : true;

}

void ServerGameScene::logicUpdate(DeltaTime dt) {
    updatePhysics(dt);
    sendPhysicsUpdatedEvent();

    for (Player *p : _players) {
        if (!p->isActive()) {
            spawnPlayer(p);
        }
    }

    EventManager *emgr = ServerServiceLocator::singleton()->getEventManager();

    // Check if the mercenaries have won the game yet
    if (_gameTimer.getElapsedSeconds() > ROUND_TIME_LIMIT) {
        emgr->queueEvent(new TimeExhaustedEvent());   
        return;
    }


    // Update objectives being hacked, check for win-condition
    int hacked = 0;
    for (Objective &o : _objectives) {
        if (o.getTriggeredTime() >= TERMINAL_HACK_TIME) {
            PlayerId pid = o.getHackingPlayerId();
            o.setState(ObjectiveState::HACKED);
            
            emgr->queueEvent(new ObjectiveStateChangedEvent(o.getObjectiveId(), 
                                                            o.getState(), 
                                                            pid,
                                                            NetworkDirection::NET_OUT));
        }

        if (o.getState() == ObjectiveState::HACKED) {
            hacked++;
        }
    }

    if (hacked && hacked == _objectives.size()) {
        // Disable all the objectives to prevent this event from firing again
        for (Objective &o : _objectives) {
            o.setState(ObjectiveState::DISABLED);
        }

        emgr->queueEvent(new AllObjectivesCompletedEvent());
    }
}


Player* ServerGameScene::createRemotePlayer(PlayerId id, Team team, std::string name) {
    Player *player = createCommonPlayer(id, team, PlayerType::REMOTE, name);

    addComponent<PlayerStateUploadComponent>(player);
    addComponent<PlayerStateDownloadComponent>(player);

    if (team == Team::SPY) {
        addComponent<GrenadeThrowComponent>(player, getWorld(), GrenadeType::FLASHBANG);
    }

    return player;
}


void ServerGameScene::spawnPlayer(Player *player) {
    Vec2 pos = findSpawnPosition(player);
    player->spawn(pos);

    EventManager *emgr = ServerServiceLocator::singleton()->getEventManager();
    emgr->queueEvent(new SpawnPlayerEvent(player->getPlayerId(), pos));
}

Vec2 ServerGameScene::findSpawnPosition(Player *player) {
    std::vector<Vec2> spawns = getMap()->getSpawnPositions(player->getTeam());
    
    if (!spawns.size()) {
        Log::error("Unable to find spawn position for player '%u'",
                   player->getPlayerId());
        return Vec2(0.f, 0.f);
    }
    std::uniform_int_distribution<int> range(0, spawns.size() - 1);

    return spawns.at(range(utility::defaultRandom));
}

Player* ServerGameScene::getPlayer(PlayerId playerId) {
    for (Player *p : _players) {
        if (p->getPlayerId() == playerId) {
            return p;
        }
    }

    return nullptr;
}

Objective* ServerGameScene::getObjective(ObjectiveId id) {
    for (Objective &o : _objectives) {
        if (o.getObjectiveId() == id) {
            return &o;
        }
    }

    return nullptr;
}

void ServerGameScene::onCreatePlayer(const EventData *e) {
    CreatePlayerEvent *evt = (CreatePlayerEvent*)e;

    Client *client = evt->getClient();
    Team team = client->getTeam();
    std::string name = client->getUserName();

    Log::debug("ServerGameScene: Creating player %u (%p)", 
               client->getPlayerId(), client);
    Player *player = createRemotePlayer(client->getPlayerId(), team, name);

    _players.push_back(player);
}

void ServerGameScene::onClientDisconnect(const EventData *e) {
    ClientDisconnectedEvent *evt = (ClientDisconnectedEvent*)e;

    auto it = _players.begin();
    for (; it != _players.end(); it++) {
        if ((*it)->getPlayerId() == evt->getPlayerId()) {
            removeGameObject(*it);
            _players.erase(it);
            break;
        }
    }
}

void ServerGameScene::onObjectiveInitiated(const EventData *e) {
    ObjectiveInitiatedEvent *evt = (ObjectiveInitiatedEvent*)e;

    PlayerId playerId = evt->getPlayerId();
    Team team = evt->getTeam();
    ObjectiveId objId = evt->getObjectiveId();

    Objective *objective = getObjective(objId);

    // The player must obviously be in the game to be able to do anything. It's
    // also preferrable if the objective actually exists.
    if (!getPlayer(playerId) || !objective) {
        return;
    }

    if (team == Team::SPY) {
        // If a spy initiated the objective, no other objectives can be active, 
        // and the objective must be available for hacking.
        if (objective->getState() != ObjectiveState::AVAILABLE) {
            return;
        }

        for (Objective &o : _objectives) {
            if (o.getState() == ObjectiveState::TRIGGERED) {
                return;
            }
        }
        
        // Trigger the objective
        objective->hackInitiated(playerId);
    } else {
        return;
    }

    ObjectiveState newState = objective->getState();

    EventManager *mgr = ServerServiceLocator::singleton()->getEventManager();

    ObjectiveStateChangedEvent *oscEvt = new ObjectiveStateChangedEvent(
                                                objId, 
                                                newState, 
                                                playerId, 
                                                NetworkDirection::NET_OUT);
    mgr->queueEvent(oscEvt);
}

void ServerGameScene::onObjectiveStateChanged(const EventData *e) {
    ObjectiveStateChangedEvent *evt = (ObjectiveStateChangedEvent*)e;

    if (evt->getNetworkDirection() != NetworkDirection::NET_IN) {
        return;
    }

    Objective *obj = getObjective(evt->getObjectiveId());
    if (!obj || obj->getState() != ObjectiveState::TRIGGERED)
        return;

    PlayerId pid = evt->getPlayerId();
    if (pid != obj->getHackingPlayerId()) {
        return;
    }

    obj->setState(evt->getObjectiveState());
    
    // Coolio, notify everyone
    EventManager *mgr = ServerServiceLocator::singleton()->getEventManager();
    mgr->queueEvent(new ObjectiveStateChangedEvent(evt->getObjectiveId(),
                                                   evt->getObjectiveState(),
                                                   evt->getPlayerId(),
                                                   NetworkDirection::NET_OUT));
}

void ServerGameScene::onGunFired(const EventData *e) {
    GunFiredEvent *evt = (GunFiredEvent*)e;

    Player *firePlayer = getPlayer(evt->getPlayerId());
    if (!firePlayer) {
        Log::warning("GunFiredEvent not handled - invalid Player ID");
        return;
    }

    b2Vec2 p1 = evt->getGunPoint();      
    b2Vec2 p2 = evt->getEndPoint();
    
    RayCastCallback cb(RayCastCallback::gunFireCategoryFilter);
    getWorld()->RayCast(&cb, p1, p2);

    if (cb.didHitAnything()) {
        // Check if we hit a player.
#ifdef ASM_DEBUG
        Player *hitPlayer = dynamic_cast<Player*>(cb.getHitGameObject());
#else
        Player *hitPlayer = (Player*)cb.getHitGameObject();
#endif
        if (hitPlayer) {
            EventManager *emgr = ServerServiceLocator::singleton()->getEventManager();

            // Deduct 20 hit points from the player
            hitPlayer->setHealth(hitPlayer->getHealth() - 20);
            int health = hitPlayer->getHealth();
            
            // Fire an event notifying of the damage done
            BulletHitEvent *evt = new BulletHitEvent(
                                        firePlayer->getPlayerId(),
                                        hitPlayer->getPlayerId(),
                                        health,
                                        health == 0);
            emgr->queueEvent(evt);

            // Respawn the player if he died
            if (health == 0) {
                spawnPlayer(hitPlayer);
            }
        }
    }
}

void ServerGameScene::onGrenadeExplode(const EventData *e) {
    GrenadeExplodeEvent *evt = (GrenadeExplodeEvent*)e;
    Vec2 ePos = evt->getPosition();

    auto emgr = ServerServiceLocator::singleton()->getEventManager();
    
    for (Player *p : _players) {
        Vec2 pPos = p->getWorldPosition();

        float dist = (ePos - pPos).Length();

        if (dist <= GRENADE_RADIUS) {
            float factor = dist / GRENADE_RADIUS;
            auto hitEvt = new GrenadeHitEvent(p->getPlayerId(), 
                                              evt->getGrenadeType(), 
                                              factor);
            emgr->queueEvent(hitEvt);
        }
    }
}


}
