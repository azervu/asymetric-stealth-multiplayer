#include "Client.h"
#include "Socket.h"
#include "ServerServiceLocator.h"
#include "EventManager.h"
#include "ChatMessageEvent.h"
#include "ServerEvents.h"
#include "SharedEvents.h"

namespace server {

Client::Client(std::string userName, PlayerId id, SocketTCP *tcp, SocketUDP *udp) 
    :   _userName(userName),
        _playerId(id),
        _tcp(tcp),
        _udp(udp),
        _team(Team::NONE),
        _ready(false),
        _loaded(false) {
    if (!_tcp || !_tcp->isInitialized()) {
        Log::error("Client constructed with bad TCP socket");
        return;
    }

    if (!_udp || !_udp->isInitialized()) {
        Log::error("Client constructed with bad UDP socket");
        return;
    }

    setSocketTCP(_tcp);
    setSocketUDP(_udp);

    Log::debug("Client '%s' connected from IP %s", 
               userName.c_str(), tcp->getOctalIP().c_str());


    // Map Event delegate callbacks
    EventManager *emgr = ServerServiceLocator::singleton()->getEventManager();

    // Map packet callbacks
    mapCallback("ClientReadyToPlay", 
                BIND_PACKET_CALLBACK(Client::onClientReadyToPlay));
    mapCallback("LoadComplete", 
                BIND_PACKET_CALLBACK(Client::onLoadComplete));
    mapCallback("ObjectiveInitiated",
                BIND_PACKET_CALLBACK(Client::onObjectiveInitiated));
    mapCallback("GunFired",
                BIND_PACKET_CALLBACK(Client::onGunFired));
    mapCallback("ObjectiveStateChanged",
                BIND_PACKET_CALLBACK(Client::onObjectiveStateChanged));
}

Client::~Client() {
    if (_tcp) {
        delete _tcp;
    }

    if (_udp) {
        delete _udp;
    }
}

bool Client::update() {
    PacketFactory *pf = ServerServiceLocator::singleton()->getPacketFactory();
    
    if (_tcp->hasActivity()) {
        if (!_tcp->parseTrafficData(pf)) {
            return false;
        } else {
            Packet * tcp = _tcp->getPacket("chatMessage");

            if (tcp != nullptr) {
                EventManager * eventM = ServerServiceLocator::singleton()->getEventManager();

                MessageReceivedEvent *evt = new MessageReceivedEvent(
                    tcp->getString("userName"),
                    tcp->getString("message")
                );

                eventM->queueEvent(evt);
                Log::debug("Queued MessageReceivedEvent");
            }
        }
    }


    if (_udp->hasActivity()) {
        if (!_udp->parseTrafficData(pf)) {
            Log::warning("Client '%s' UDP error", _userName.c_str());
            return false;
        }
    }

    // @TODO
    // Update active conversations

    dispatchPacketQueue();
    return true;
}

bool Client::isConnectionGood() const {
    if (!_tcp) {
        Log::error("Client '%s': TCP socket is NULL", _userName.c_str());
        return false;
    }

    if (!_udp) {
        Log::error("Client '%s': UDP socket is NULL", _userName.c_str());
        return false;
    }

    if (!_tcp->isGood()) {
        Log::error("Client '%s': TCP socket is bad", _userName.c_str());
        return false;
    }

    if (!_udp->isGood()) {
        Log::error("Client '%s': UDP socket is bad", _userName.c_str());
        return false;
    }

    return true;
}

bool Client::isReady() const {
    return _ready;
}

bool Client::isLoaded() const {
    return _loaded;
}

void Client::queuePacket(TLProtocol protocol, Packet *packet) {
    SocketBase *socket = nullptr;

    if (protocol == TLProtocol::TCP) {
        socket = _tcp;
    } else if (protocol == TLProtocol::UDP) {
        socket = _udp;
    } else {
        Log::error("Client: Unable to queue packet on undefiend protocol");
        return;
    }

    socket->queuePacket(packet);
}

bool Client::flushPacketQueue() {
    return _udp->flushOutgoingPacketQueue() && _tcp->flushOutgoingPacketQueue();
}

void Client::setTeam(Team team) {
    _team = team;
}

std::string Client::getUserName() const {
    return _userName;
}

unsigned Client::getPlayerId() const {
    return _playerId;
}

Team Client::getTeam() const {
    return _team;
}


void Client::onClientReadyToPlay(const Packet *pkt) {
    _ready = true;

    EventManager *emgr = ServerServiceLocator::singleton()->getEventManager();
    emgr->queueEvent(new ClientReadyToPlayEvent(this));
}

void Client::onLoadComplete(const Packet *pkt) {
    _loaded = true;

    EventManager *emgr = ServerServiceLocator::singleton()->getEventManager();
    emgr->queueEvent(new ClientLoadedLevelEvent(this));
}

void Client::onObjectiveInitiated(const Packet *pkt) {
    PlayerId playerId = pkt->getUnsigned("playerId");
    Team team = static_cast<Team>(pkt->getByte("team"));
    ObjectiveId objId = static_cast<ObjectiveId>(pkt->getByte("objectiveId"));

    EventManager *emgr = ServerServiceLocator::singleton()->getEventManager();
    emgr->queueEvent(new ObjectiveInitiatedEvent(playerId, team, objId));
};

void Client::onGunFired(const Packet *pkt) {
    EventManager *emgr = ServerServiceLocator::singleton()->getEventManager();   

    PlayerId playerId = pkt->getUnsigned("playerId");
    b2Vec2 gunPoint(pkt->getFloat("gunX"), pkt->getFloat("gunY"));
    b2Vec2 endPoint(pkt->getFloat("endX"), pkt->getFloat("endY"));

    emgr->queueEvent(new GunFiredEvent(playerId, gunPoint, endPoint));
}

void Client::onObjectiveStateChanged(const Packet *packet) {
    ObjectiveId objId = static_cast<ObjectiveId>(packet->getByte("objectiveId"));
    ObjectiveState state = static_cast<ObjectiveState>(packet->getByte("state"));
    PlayerId playerId = packet->getUnsigned("playerId");

    ObjectiveStateChangedEvent *evt;
    evt = new ObjectiveStateChangedEvent(objId, state, playerId, 
                                         NetworkDirection::NET_IN);
    ServerServiceLocator::singleton()->getEventManager()->queueEvent(evt);
}

}
