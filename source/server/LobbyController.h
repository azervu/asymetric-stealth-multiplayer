#pragma once

#include <vector>
#include <ctime>
#include "ConnectionListener.h"
#include "Engine.h"
#include "EventManager.h"


namespace server {

class Client;
class ClientJoinConversation;

/**
 * The number of seconds before a client which has not sent a ClientJoinRequest
 * packet is dropped.
 */
#define JOIN_REQUEST_TIMEOUT 15

#define LOBBY_MAX_PLAYERS 4


/**
 * @brief   Class that manages the lobby. Accepts new clients, kicks 
 *          unresponsive ones and dispatches important events to the remaining
 *          clients. Basically, it is the boss of everything that relates to
 *          TCP in the protocol and is most active while the game session is in
 *          the lobby-phase. Consider this class the HR representative of the
 *          program.
 */
class LobbyController {
public:
    LobbyController();
    ~LobbyController();

    /**
     * Proxy-method for ConnectionListener::isGood().
     */
    bool isConnectionListenerGood();

    /**
     * This method should be called relatively often so that the controller 
     * can perform routine checks and tasks.
     */
    void update();

    /**
     * Dispatch ServerShutdown to all connected clients. The packet queues of 
     * all connected clients are also flushed.
     * @param message       The message to embed in the ServerShutdown packet
     */
    void onServerShutdown(std::string message);

    void setLobbySize(int size);
    int getLobbySize() const;

    /**
     * Notify remaining clients and the event queue that the game has finished.
     * @param winner    The winning team. Can be NONE.
     * @param reason    The reason the game ended. Can be empty.
     */
    void endGame(Team winner, std::string reason);

private:
    /**
     * Store new connections from the ConnectionListener in _newClients. 
     */
    void handleNewConnections();

    void updateClientJoinConversations();

    void updateClients();

    /**
     * Get the team the next player joining should be placed in.
     */
    Team getTeamForNextClient() const;

    void notifyArrival(Client *client);
    void notifyDeparture(Client *client);

    /**
     * Determine whether or not new clients can join. Potential clients stored
     * in _newClients are treated as fully worthy clients for this purpose.
     */
    bool isLobbyJoinable() const;

    /**
     * The lobby is regarded as 'full' when '_lobbySize' clients have been 
     * accepted into the lobby and their connections are fully established.
     */
    bool isLobbyFull() const;

    bool allClientsReady() const;
    bool allClientsLoaded() const;

    ConnectionListener _connectionListener;
    ProtocolState _protoState;

    /* New sockets which may not yet have received ClientJoinRequest are stored
     * here, along with the time of arrival. Clients taking too long to send
     * the join request are dropped.
     */
    std::vector< std::pair<SocketTCP*,ClientJoinConversation*> > _newClients;

    /**
     * Connected clients are stored here.
     */
    std::vector<Client*> _clients;

    /**
     * The number of players allowed in the lobby. Defaults to LOBBY_MAX_PLAYERS
     * but can be overriden with the cmd arg "--lobby_size=N"
     */
    unsigned int _lobbySize;

    /**
     * Delegate method for server::ClientReadyToPlayEvent
     */
    EventListenerDelegate _clientReadyDelegate;
    void onClientReadyToPlay(const EventData *e);

    /**
     * Delegate method for server::ClientLoadedLevelEvent
     */
    EventListenerDelegate _clientLoadedDelegate;
    void onClientLoaded(const EventData *e);

    /**
     * Delegate method for server::GameStartingEvent
     */
    EventListenerDelegate _gameStartingDelegate;
    void onGameStarting(const EventData *e);

    /**
     * Delegate method for SpawnPlayerEvent
     */
    EventListenerDelegate _spawnPlayerDelegate;
    void onPlayerSpawned(const EventData *e);

    /* Delegate method for PlayerTransformUpdateEvent 
     */
    EventListenerDelegate _playerTransDelegate;
    void onPlayerTransformOut(const EventData *e);

    /* Delegate method for ObjectiveStateChangedEvent
     */
    EventListenerDelegate _objStateDelegate;
    void onObjectiveStateChanged(const EventData *e);

    /* Delegate method for GunFiredEvent
     */
    EventListenerDelegate _gunFireDelegate;
    void onGunFired(const EventData *e);

    /* Delegate method for BulletHitEvent
     */
    EventListenerDelegate _bulletHitDelegate;
    void onBulletHit(const EventData *e);

    /* Delegate method for GrenadeThrowEvent
     */
    EventListenerDelegate _throwDelegate;
    void onGrenadeThrow(const EventData *e);

    /* Delegate method for GrenadeExplodeEvent
     */
    EventListenerDelegate _explodeDelegate;
    void onGrenadeExplode(const EventData *e);

    /* Delegate method for GrenadeHitEvent
     */
    EventListenerDelegate _grenadeHitDelegate;
    void onGrenadeHit(const EventData *e);
};



/**
 * The server-ClientJoinConversation is initiated by a new TCP-connection. The
 * client then sends a ClientJoinRequest, and the server responds with a 
 * ClientJoinResponse. 
 *
 * The ClientJoinConversation creates an UDP socket with the connection info
 * given by the client in ClientJoinRequest.
 */
class ClientJoinConversation {
public:
    ClientJoinConversation();
    ~ClientJoinConversation();

    /**
     * Converse with the remote client. The client is accepted if 'admission'
     * is true and the client sends sane data.
     * @param socket        The TCP socket the client connected on
     * @param admission     Whether or not to accept the client if his data 
     *                      is sane.
     * @param team          The team to place this client on if the admission
     *                      succeeds.
     */
    bool update(SocketBase *socket, bool admission, Team team);
    bool isDone();

    bool clientGotAccepted();
    PlayerId getPlayerId();
    std::string getClientUserName();

    /**
     * Retrieve the created UDP socket. The ownership of the Socket is given to
     * the caller. THIS METHOD CAN ONLY BE CALLED ONCE.
     */
    SocketUDP* getUdpSocket();

private:
    bool _done;
    bool _accepted;
    SocketUDP *_socketUdp;
    Packet *_clientJoinRequest;
    PlayerId _playerId;
    std::string _clientName;
    unsigned _clientUdpPort;
    std::time_t _startTime;

    /** 
     * Create UDP client, socket and send ClientJoinResponse to the client.
     * If anything goes wrong, the method calls "rejectClient()".
     * @return          True if the client got accepted successfully, false if
     *                  anything fails. 
     */
    bool acceptClient(SocketBase *socket, Team team);

    /**
     * Reject the client with a negative ClientJoinResponse.
     * @return          True if the packet could be sent, false otherwise.
     */
    bool rejectClient(SocketBase *socket);

    PlayerId getNextPlayerId();
};


}
