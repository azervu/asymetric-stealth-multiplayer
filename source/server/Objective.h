#pragma once

#include "Engine.h"
#include "Timer.h"


namespace server {


/**
 * Objective represents the state of an objective on the map.
 */
class Objective {
public:
    Objective(ObjectiveId objId);

    ObjectiveId getObjectiveId() const;

    void hackInitiated(PlayerId playerId);
    PlayerId getHackingPlayerId() const;

    /**
     * Set the state of this Objective. If state is TRIGGERED, this method
     * must NOT be used. Use Objective::hackInitiated instead.
     */
    void setState(ObjectiveState state);
    ObjectiveState getState() const;

    /**
     * If this objective is being hacked, the time since the hack begun is
     * returned. If this objective is in another state, 0.0f is returned.
     */
    float getTriggeredTime() const;

private:
    ObjectiveId _objId;
    ObjectiveState _state;
    PlayerId _hackingPlayerId;
    Timer _timer;
};

}
