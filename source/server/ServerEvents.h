/**
 * @brief   Declaring file of all events on the server. Until the number of 
 *          events on the server exceeds a too large number, declare everything
 *          in this file.
 */

#pragma once

#include "EventManager.h"
#include "Engine.h"

namespace server {

class Client;


/**
 * Fired when a client has flagged himself as ready to play.
 */
class ClientReadyToPlayEvent : public EventData {
public:
    static const EventType eventType;

    ClientReadyToPlayEvent(const Client *client);

    EventType getEventType() const;
    const Client* getClient() const;

private:
    const Client *_client;
};


/**
 * Fired when a client has flagged himself as done loading and is ready to
 * start the game.
 */
class ClientLoadedLevelEvent : public EventData {
public:
    static const EventType eventType;

    ClientLoadedLevelEvent(const Client *client);

    EventType getEventType() const;
    const Client* getClient() const;

private:
    const Client *_client;
};


/**
 * Fired when all clients are ready and are told to start loading the level.
 * The server itself must also load the level to properly simulate the world.
 */
class StartLoadingLevelEvent : public EventData {
public:
    static const EventType eventType;

    StartLoadingLevelEvent(std::string levelName);

    EventType getEventType() const;
    std::string getLevelName() const;

private:
    std::string _level;
};


/**
 * Event dispatched after the level-loading system has heeded StartLoadingEvent
 * and has completed the loading.
 */
class ServerLoadedLevelEvent : public EventData {
public:
    static const EventType eventType;

    EventType getEventType() const;
};


/**
 * Fired when all clients (and the server) have loaded successfully into the 
 * game.
 */
class GameStartingEvent : public EventData {
public:
    static const EventType eventType;
    EventType getEventType() const;
};


/**
 * Fired when a client disconnects.
 */
class ClientDisconnectedEvent : public EventData {
public:
    static const EventType eventType;

    ClientDisconnectedEvent(unsigned playerId);
    EventType getEventType() const;
    PlayerId getPlayerId() const;

private:
    PlayerId _playerId;
};


/**
 * Fired when the LobbyController has been notified that the game is starting.
 */
class CreatePlayerEvent : public EventData {
public:
    static const EventType eventType;

    CreatePlayerEvent(Client *client);

    EventType getEventType() const;
    Client* getClient() const;   

private:
    Client *_client;
};


/**
 * Fired after the GamePlayScene has spawned a player.
 */
class PlayerDidSpawnEvent : public EventData {
public:
    static const EventType eventType;

    PlayerDidSpawnEvent(Vec2 position, float rotation);

    EventType getEventType() const;
    Vec2 getPosition() const;
    float getRotation() const;

private:
    Vec2 _position;
    float _rotation;
};


/* Fired by ServerGameScene when all terminals has been hacked.
 */
class AllObjectivesCompletedEvent : public EventData {
public:
    static const EventType eventType;

    EventType getEventType() const;
};

/* Fired by ServerGameScene when the time has run out and the mercs won.
 */
class TimeExhaustedEvent : public EventData {
public:
    static const EventType eventType;

    EventType getEventType() const;
};

}
