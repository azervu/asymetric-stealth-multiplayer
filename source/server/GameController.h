#pragma once 

#include "Engine.h"
#include "EventManager.h"

class Settings;

namespace server {

class LobbyController;
class ServerGameScene;

/**
 * @brief   The GameController is responsible for managing high-level game logic
 *          and game-flow - starting the game when clients and server are ready,
 *          updating the lobby controller and other middle-management tasks.
 */
class GameController {
public:
    GameController();
    ~GameController();

    bool initialize(Settings *settings);
    void onServerShutdown();

    void update(); 
    
private:
    LobbyController *_lobbyController;
    ServerGameScene *_gameScene;

    /* Event delegate for StartLoadingLevelEvent
     */
    EventListenerDelegate _startLoadDelegate;
    void onStartLoadingLevel(const EventData *e);

    /* Event delegate for GameOverEvent
     */
    EventListenerDelegate _gameOverDelegate;
    void onGameOver(const EventData *e);

    /* Event delegate for AllObjectivesCompletedEvent
     */
    EventListenerDelegate _objCompDelegate;
    void onAllObjectivesCompleted(const EventData *e);

    /* Event delegate for TimeExhaustedEvent
     */
    EventListenerDelegate _timeExhaustDelegate;
    void onTimeExhausted(const EventData *e);
};


}
