#pragma once

#include "ServiceLocator.h"

class PathTranslator;

namespace server {

/**
 * Service locator used in the game client. Provices named methods for 
 * commonly used game client services, hides the ServiceId interface and 
 * implements the Singleton pattern.
 */
class ServerServiceLocator : public ServiceLocator {
public:
    using ServiceLocator::provide;

    /**
     * As defined in ServiceLocator.h, target specific services are required to
     * start IDs at TARGET_SPECIFIC_SERVICE_FIRST_ID.
     */
    enum ServerServiceId {

    };

    static ServerServiceLocator* singleton();

private:
    static ServerServiceLocator* _serverSingleton;
};


}
