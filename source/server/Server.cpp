#include "Server.h"
#include "ServerServiceLocator.h"
#include "PathTranslator.h"
#include "LobbyController.h"
#include "EventManager.h"
#include "ChatMessageService.h"
#include "SpriteManager.h"
#include "ServerGameScene.h"
#include "ServerEvents.h"
#include "GameController.h"
#include "Preloader.h"

namespace server {


Server::Server() 
    :   _isInitialized(false),
        _quit(false),
        _gameController(nullptr) {
    ServerServiceLocator::singleton();

    _gameController = new GameController();
}

Server::~Server() {
    delete _gameController;
    ServerServiceLocator::singleton()->destroyServices();
}

int Server::execute(Settings* settings) {
    if (!_isInitialized) {
        _isInitialized = initialize(settings);
    }

    if (!_isInitialized) {
        Log::error("Failed to initialize server");
        return -1;
    }
    
    mainLoop();
    return 0;
}

bool Server::initialize(Settings* settings) {
    PathTranslator *pt = new PathTranslator(settings);
    pt->init(settings);
    ServerServiceLocator::singleton()->provide(pt);

    if (!pt->addAssetDirectory("assets")) {
        delete pt;
        Log::fatal("PathTranslator failed to find directory 'assets'");
        return false;
    }

    EventManager *eventManager = new EventManager();
    eventManager->init(settings);
    ServerServiceLocator::singleton()->provide(eventManager);

    PacketFactory *packetFactory = new PacketFactory(pt);
    packetFactory->init(settings);
    ServerServiceLocator::singleton()->provide(packetFactory);

    ChatMessageService *chatMessageService = new ChatMessageService();
    chatMessageService->init(settings);
    ServerServiceLocator::singleton()->provide(chatMessageService);

    SpriteManager::Manager *spriteManager = new SpriteManager::Manager();
    spriteManager->init(settings);
    ServerServiceLocator::singleton()->provide(spriteManager);

    Preloader preloader;
    std::string preloadingFile = settings->getString(PRELOADING_FILE_NAME_KEY, PRELOADING_DEFAULT_FILE_NAME);
    auto preloadingFuture = preloader.startLoading(preloadingFile);


    if (!_gameController->initialize(settings)) {
        Log::fatal("GameController initialization failed");
        return false;
    }

    // Check for "--notif_init". If this flag was passed, send a 
    // ServerInitalized packet to localhost UDP-port 
    // SERVER_INITIALIZED_DEST_UDP_PORT (defined in Protocol.h).
    std::string notif = settings->getString("notif_init");
    if (notif.size() > 0) {
        sendServerInitializedPacket();
    }

    return preloadingFuture.get();
}

void Server::sendServerInitializedPacket() {
    SocketUDP udp("localhost", SERVER_INITIALIZED_DEST_UDP_PORT, 0);
    
    if (!udp.initSocket() || !udp.isGood()) {
        Log::error("Failed to create '--notify_init' socket");
        return;
    }

    PacketFactory *pf = ServerServiceLocator::singleton()->getPacketFactory();
    Packet *pkt = pf->createPacketFromTemplate("ServerInitialized");

    if (!udp.sendPacket(pkt)) {
        Log::error("Failed to send packet 'ServerInitialized'");
    } else {
        Log::info("Sent ServerInitialized packet");
    }

    delete pkt;
}

void Server::mainLoop() {
    while (!_quit) {
        ServerServiceLocator::singleton()->getEventManager()->update();
        _gameController->update();
        ServerServiceLocator::singleton()->getEventManager()->update();

        std::this_thread::sleep_for(std::chrono::milliseconds(16));
        handleSDLEvents();
    }

    _gameController->onServerShutdown();
}

void Server::handleSDLEvents() {
    SDL_Event evt;

    while (SDL_PollEvent(&evt)) {
        if (evt.type == SDL_QUIT) {
            _quit = true;
        }
    }
}

}
