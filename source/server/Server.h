#pragma once

#include "ConnectionListener.h"
#include "EventManager.h"

namespace server {

class LobbyController;
class ServerGameScene;
class GameController;


/**
 * @brief   The Server-class is responsible for main program flow and execution.
 */
class Server {
public:
    Server();
    ~Server();

    /**
     * Initializes the Server and starts the main server loop. The method does
     * not return until the program should quit. 
     * @return      0 on success, !0 on error.
     */
    int execute(Settings* settings);

private:
    /**
     * Initialize the server. The required services are provided to the 
     * ServerServiceLocator in this method.
     * @return      True on success, false on failure. Execution of the server
     *              should terminate immediately if initialization fails.
     */
    bool initialize(Settings* settings);

    /**
     * Triggered by the "--notif_init" command line argument. Sends a packet
     * 'ServerInitialized' over UDP on port 'SERVER_INITIALIZED_DEST_UDP_PORT'.
     */
    void sendServerInitializedPacket();

    void mainLoop();

    /**
     * Handle SDL_QUIT. All other events are ignored. On SDL_QUIT, _quit is 
     * set to true and the main server loop will eventually shut down.
     */
    void handleSDLEvents();

    GameController *_gameController;
    bool _isInitialized;
    bool _quit;
};


}
