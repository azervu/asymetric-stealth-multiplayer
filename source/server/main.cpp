/**
 * @file    source/server/main.cpp
 *
 * @brief   Implements the main class for the server.
 */

#include <iostream>
#include "ServerServiceLocator.h"
#include "PathTranslator.h"
#include "Server.h"

#ifndef ASM_UNITTEST 

using namespace server;

#ifdef main
    #undef main
#endif //main

int main(int argc, char **argv) {
    Settings* settings = new Settings(argc, (const char**) argv);
    Log::setSettings(settings);

    if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
        Log::error("SDL_Init() failed: %s", SDL_GetError());
        return -1;
    }

    if (SDLNet_Init() < 0) {
        Log::error("SDLNet_Init() failed: %s", SDLNet_GetError());
        return -1;
    }

    Server *server = new Server;
    int ret = server->execute(settings);
    delete server;

    SDL_Quit();
    SDLNet_Quit();

    return ret;
}

#endif //ASM_UNITTEST
