#pragma once

#include "GamePlayScene.h"
#include "Objective.h"
#include "Timer.h"


namespace server {


/**
 * ServerGameScene handles game events and world simulation on the server side.
 */
class ServerGameScene : public GamePlayScene {
public:
    ServerGameScene(std::string mapName);
    ~ServerGameScene();

    virtual bool loadScene();
    void logicUpdate(DeltaTime dt);

protected:
    virtual Player* createRemotePlayer(PlayerId playerId, Team team, std::string name);

private:
    void spawnPlayer(Player *pc);
    Vec2 findSpawnPosition(Player *pc);

    std::vector<Player*> _players;
    std::vector<Objective> _objectives;

    Timer _gameTimer;


    Player* getPlayer(PlayerId playerId);
    Objective* getObjective(ObjectiveId id);


    /* CreatePlayerEvent delegate method
     */
    EventListenerDelegate _createPlayerDelegate;
    void onCreatePlayer(const EventData *e);

    /* ClientDisconnectedEvent delegate method
     */
    EventListenerDelegate _clientDcDelegate;
    void onClientDisconnect(const EventData *e);

    /* ObjectiveInitiatedEvent delegate method
     */
    EventListenerDelegate _objInitDelegate;
    void onObjectiveInitiated(const EventData *e);

    /* ObjectiveStateChanged delegate method.
     */
    EventListenerDelegate _objStateDelegate;
    void onObjectiveStateChanged(const EventData *e);

    /* GunFiredEvent delegate method
     */
    EventListenerDelegate _gunFireDelegate;
    void onGunFired(const EventData *e);

    /* GrenadeExplodeEvent delegate method
     */
    EventListenerDelegate _explodeDelegate;
    void onGrenadeExplode(const EventData *e);
};


}
