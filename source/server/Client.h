#pragma once

#include <string>
#include "Protocol.h"
#include "Engine.h"
#include "NetworkInterface.h"
#include "EventManager.h"

class SocketTCP;
class SocketUDP;


namespace server {


/**
 * Abstraction of the TCP aspect of the game protocol. Nearly all communication
 * between the server and the client goes through this interface (the setup and
 * admission is done elsewhere).
 */
class Client : public NetworkInterface {
public:
    /**
     * @param userName  The name of the client
     * @param id        The player ID of the client
     * @param tcp       Connected and good TCP connection to client
     * @param udp       Good UDP connection to client
     */
    Client(std::string userName, PlayerId id, SocketTCP *tcp, SocketUDP *udp);
    ~Client();

    /**
     * Update sockets and active conversations. 
     * @return          False if the client has disconnected, true otherwise
     */
    bool update();

    /**
     * Check if the connection is good. 
     * @return       True if the TCP connection is good, false otherwise.
     */
    bool isConnectionGood() const;
    bool isReady() const;
    bool isLoaded() const;

    /**
     * Add a packet to the queue of packets to be dispatched. The ownership of
     * the packet is transferred to the Client when it is added to the queue.
     */
    void queuePacket(TLProtocol protocol, Packet *packet);

    /**
     * Send all packets on the queue.
     * @return      True if all the packets were properly sent, false if any 
     *              errors occurred. Errors in this method means that either of
     *              the sockets have gone sour, and the client should be 
     *              discarded.
     */
    bool flushPacketQueue();

    void setTeam(Team team);

    std::string getUserName() const;
    PlayerId getPlayerId() const;
    Team getTeam() const;

private:
    std::string _userName;
    PlayerId _playerId;
    Team _team;
    SocketTCP *_tcp;
    SocketUDP *_udp;
    bool _ready;
    bool _loaded;

    /* Packet Callback Methods */
    void onClientReadyToPlay(const Packet *pkt);
    void onLoadComplete(const Packet *pkt);
    void onObjectiveInitiated(const Packet *pkt);
    void onGunFired(const Packet *pkt);
    void onObjectiveStateChanged(const Packet *pkt);
};


}
