#include "LobbyController.h"
#include "Socket.h"
#include "Protocol.h"
#include "ServerServiceLocator.h"
#include "Client.h"
#include "ServerEvents.h"
#include "Player.h"
#include "SharedEvents.h"


namespace server {


LobbyController::LobbyController() 
    :   _lobbySize(LOBBY_MAX_PLAYERS),
        _protoState(ProtocolState::IN_LOBBY) {

    _clientReadyDelegate.bind(this, &LobbyController::onClientReadyToPlay);
    _clientLoadedDelegate.bind(this, &LobbyController::onClientLoaded);
    _gameStartingDelegate.bind(this, &LobbyController::onGameStarting);
    _spawnPlayerDelegate.bind(this, &LobbyController::onPlayerSpawned);
    _playerTransDelegate.bind(this, &LobbyController::onPlayerTransformOut);
    _objStateDelegate.bind(this, &LobbyController::onObjectiveStateChanged);
    _gunFireDelegate.bind(this, &LobbyController::onGunFired);
    _bulletHitDelegate.bind(this, &LobbyController::onBulletHit);
    _throwDelegate.bind(this, &LobbyController::onGrenadeThrow);
    _explodeDelegate.bind(this, &LobbyController::onGrenadeExplode);
    _grenadeHitDelegate.bind(this, &LobbyController::onGrenadeHit);

    EventManager *emgr = ServerServiceLocator::singleton()->getEventManager();
    emgr->addListener(_clientReadyDelegate, ClientReadyToPlayEvent::eventType);
    emgr->addListener(_clientLoadedDelegate, ClientLoadedLevelEvent::eventType);
    emgr->addListener(_gameStartingDelegate, GameStartingEvent::eventType);
    emgr->addListener(_spawnPlayerDelegate, SpawnPlayerEvent::eventType);
    emgr->addListener(_playerTransDelegate, PlayerTransformUpdateEvent::eventType);
    emgr->addListener(_objStateDelegate, ObjectiveStateChangedEvent::eventType);
    emgr->addListener(_gunFireDelegate, GunFiredEvent::eventType);
    emgr->addListener(_bulletHitDelegate, BulletHitEvent::eventType);
    emgr->addListener(_throwDelegate, GrenadeThrowEvent::eventType);
    emgr->addListener(_explodeDelegate, GrenadeExplodeEvent::eventType);
    emgr->addListener(_grenadeHitDelegate, GrenadeHitEvent::eventType);
}

LobbyController::~LobbyController() {
    EventManager *emgr = ServerServiceLocator::singleton()->getEventManager();
    emgr->removeListener(_clientReadyDelegate);
    emgr->removeListener(_clientLoadedDelegate);
    emgr->removeListener(_gameStartingDelegate);
    emgr->removeListener(_spawnPlayerDelegate);
    emgr->removeListener(_playerTransDelegate);
    emgr->removeListener(_objStateDelegate);
    emgr->removeListener(_gunFireDelegate);
    emgr->removeListener(_bulletHitDelegate);
    emgr->removeListener(_throwDelegate);
    emgr->removeListener(_explodeDelegate);
    emgr->removeListener(_grenadeHitDelegate);

    for (Client *c : _clients) {
        delete c;
    }

    for (auto pair : _newClients) {
        delete pair.first;
    }
}

bool LobbyController::isConnectionListenerGood() {
    return _connectionListener.isGood();
}

void LobbyController::update() {
    handleNewConnections();
    updateClientJoinConversations();
    updateClients();
}

void LobbyController::onServerShutdown(std::string message) {
    PacketFactory *pf = ServerServiceLocator::singleton()->getPacketFactory();

    for (Client *c : _clients) {
        Packet *pkt = pf->createPacketFromTemplate("ServerShutdown");
        pkt->setString("message", message);
        c->queuePacket(TLProtocol::TCP, pkt);
        c->flushPacketQueue();
        
        Log::debug("LobbyController: ServerShutdown sent to %s (%i)",
                    c->getUserName().c_str(), c->getPlayerId());
    }

    // Allow the sockets to flush properly
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
}

void LobbyController::setLobbySize(int size) {
    _lobbySize = size;
}

int LobbyController::getLobbySize() const {
    return _lobbySize;
}

void LobbyController::endGame(Team winner, std::string reason) {
    if (_protoState != ProtocolState::IN_GAME) {
        Log::error("LobbyController::endGame: Unable to end the game. "
                   "The game is not active.");
        return;
    }

    // Notify via the EventQueue
    GameOverEvent *goe = new GameOverEvent(winner, reason);
    ServerServiceLocator::singleton()->getEventManager()->queueEvent(goe);

    // Notify the connected clients
    PacketFactory *pf = ServerServiceLocator::singleton()->getPacketFactory();

    for (Client *c : _clients) {
        Packet *pkt = pf->createPacketFromTemplate("GameOver");
        pkt->setByte("winningTeam", (byte)winner);
        pkt->setString("reason", reason);

        c->queuePacket(TLProtocol::TCP, pkt);
    }

    // Change the state out of IN_GAME. 
    Log::debug("LobbyController going back to lobby");
    _protoState = ProtocolState::IN_LOBBY;
}


void LobbyController::handleNewConnections() {
    while (_connectionListener.hasNewConnection()) {
        SocketTCP *socket = _connectionListener.popNewConnection();
        if (socket == nullptr || !socket->isInitialized ()) {
            Log::warning("LobbyController: New socket was bad");
            delete socket;
            continue;
        }
        
        std::pair<SocketTCP*,ClientJoinConversation*> client;
        client.first = socket;
        client.second = new ClientJoinConversation;
        _newClients.push_back(client);
    }
}

void LobbyController::updateClientJoinConversations() {
    PacketFactory *pf = ServerServiceLocator::singleton()->getPacketFactory();

    int clientCount = _clients.size();

    auto iter = _newClients.begin();
    while (iter != _newClients.end()) {
        clientCount++;

        // Admit players if we have room and are in proto state IN_LOBBY.
        bool admission = (clientCount <= _lobbySize) && 
                         (_protoState == ProtocolState::IN_LOBBY);

        SocketTCP *socket = iter->first;
        ClientJoinConversation *convo = iter->second;
        Team team = getTeamForNextClient();

        if (    !socket->parseTrafficData(pf) || 
                !socket->isGood() ||
                !convo->update(socket, admission, team)) {
            Log::error("Dropping bad client");
            delete socket;
            delete convo;
            iter = _newClients.erase(iter);
            clientCount--;
        } else if (convo->isDone()) {
            if (convo->clientGotAccepted()) {
                Client *client = new Client(convo->getClientUserName(),
                                            convo->getPlayerId(),
                                            socket,
                                            convo->getUdpSocket());
                client->setTeam(team);

                if (client->isConnectionGood()) {
                    // Officially accept the client as one of the hurd
                    _clients.push_back(client);
                    notifyArrival(client);
                } else {
                    delete client;
                }
            } else {
                delete socket;
            }

            delete convo;
            iter = _newClients.erase(iter);
        } else {
            iter++;
        }
    }
}

void LobbyController::updateClients() {
    auto iter = _clients.begin();
    while (iter != _clients.end()) {
        Client *client = *iter;

        if (!client->update() || !client->isConnectionGood()) {
            notifyDeparture(client);
            Log::debug("Client '%s' disconnected", 
                       client->getUserName().c_str());
            delete client;
            iter = _clients.erase(iter);

            if (_clients.size() == 0) {
                endGame(Team::NONE, "All players disconnected");
            }
        } else {
            client->flushPacketQueue();
            iter++;
        }
    }
}

Team LobbyController::getTeamForNextClient() const {
    int spy = 0;
    int merc = 0;

    for (Client *client : _clients) {
        if (client->getTeam() == Team::SPY) {
            spy++;
        } else if (client->getTeam() == Team::MERC) {
            merc++;
        }
    }

    return (spy > merc) ? (Team::MERC) : (Team::SPY);
}

void LobbyController::notifyDeparture(Client *client) {
    PacketFactory *pf = ServerServiceLocator::singleton()->getPacketFactory();

    for (Client *c : _clients) {
        if (c != client) {
            Packet *pkt = pf->createPacketFromTemplate("ClientLeft");
            pkt->setUnsigned("playerId", client->getPlayerId());
            pkt->setString("userName", client->getUserName());

            c->queuePacket(TLProtocol::TCP, pkt);
        }
    }

    EventManager *emgr = ServerServiceLocator::singleton()->getEventManager();
    emgr->queueEvent(new ClientDisconnectedEvent(client->getPlayerId()));
}

void LobbyController::notifyArrival(Client *newClient) {
    PacketFactory *pf = ServerServiceLocator::singleton()->getPacketFactory();

    // Notify the other clients about the new client
    for (Client *c : _clients) {
        if (c != newClient) {
            Packet *pkt = pf->createPacketFromTemplate("ClientJoined");
            pkt->setUnsigned("playerId", newClient->getPlayerId());
            pkt->setString("userName", newClient->getUserName());
            pkt->setByte("team", static_cast<byte>(newClient->getTeam()));

            c->queuePacket(TLProtocol::TCP, pkt);
        }     
    }

    // Notify the new client about the existing clients
    for (Client *c : _clients) {
        if (c != newClient) {
            Packet *pkt = pf->createPacketFromTemplate("ClientJoined");
            pkt->setUnsigned("playerId", c->getPlayerId());
            pkt->setString("userName", c->getUserName());
            pkt->setByte("team", static_cast<byte>(c->getTeam()));

            newClient->queuePacket(TLProtocol::TCP, pkt);
        }
    }
}

bool LobbyController::isLobbyJoinable() const {
    if (_protoState != ProtocolState::IN_LOBBY) {
        return false;
    }

    // Joining clients 'take up space'.
    if (_clients.size() + _newClients.size() >= getLobbySize()) {
        return false;
    }

    return true;
}

bool LobbyController::isLobbyFull() const {
    // Only count _connected_ clients.
    return (_clients.size() == getLobbySize());
}

bool LobbyController::allClientsReady() const {
    for (Client *c : _clients)
        if (!c->isReady())
            return false;
    return true;
}

bool LobbyController::allClientsLoaded() const {
    for (Client *c : _clients)
        if (!c->isLoaded())
            return false;
    return true;
}

void LobbyController::onClientReadyToPlay(const EventData *e) {
    ClientReadyToPlayEvent *evt = (ClientReadyToPlayEvent*)e;
    const Client *client = evt->getClient();
    
    Log::info("Client %u (%s) is ready", 
              client->getPlayerId(), client->getUserName().c_str());

    if (allClientsReady() && isLobbyFull()) {
        Log::info("All clients ready - prompting level load");
        EventManager *emgr = ServerServiceLocator::singleton()->getEventManager();

        // Discard any queued ClientReadyToPlayEvents - if the last N clients
        // sent the packets at the same time, multiple instances are queued.
        emgr->abortEvent(ClientReadyToPlayEvent::eventType, true);

        PacketFactory *pf = ServerServiceLocator::singleton()->getPacketFactory();
        const std::string level = "test.tmx";

        // Tell all clients to start loading the game
        for (Client *c : _clients) {
            Packet *pkt = pf->createPacketFromTemplate("StartLoading");   
            pkt->setString("level", level);
            c->queuePacket(TLProtocol::TCP, pkt);
        }
    }
}

void LobbyController::onClientLoaded(const EventData *e) {
    ClientLoadedLevelEvent *evt = (ClientLoadedLevelEvent*)e;
    const Client *client = evt->getClient();

    Log::info("Client %u (%s) is loaded and ready",
              client->getPlayerId(), client->getUserName().c_str());

    if (allClientsLoaded() && isLobbyFull()) {
        Log::debug("All clients ready - starting level load");
        EventManager *emgr = ServerServiceLocator::singleton()->getEventManager();

        // Abort all other CLLEs, we have been thoroughly notified already.
        emgr->abortEvent(ClientLoadedLevelEvent::eventType, true);

        // Fire game-starting event. GameController will load the level and
        // fire a GameStartingEvent when done. We want this process to be FAST, 
        // so don't bother queueing the event.
        emgr->triggerEvent(new StartLoadingLevelEvent("test.tmx"));
    }
}

void LobbyController::onGameStarting(const EventData *e) {
    Log::debug("LobbyController: Notifying clients that the game is starting");
    _protoState = ProtocolState::IN_GAME;

    // Tell all the clients the game is starting
    PacketFactory *pf = ServerServiceLocator::singleton()->getPacketFactory();
    for (Client *c : _clients) {
        Packet *pkt = pf->createPacketFromTemplate("GameStarting");
        c->queuePacket(TLProtocol::TCP, pkt);
    }

    // Tell all the clients to create themselves and their peer
    for (Client *c : _clients) {
        for (Client *co : _clients) {
            PlayerType ptype;
            if (c == co) {
                ptype = PlayerType::LOCAL;
            } else {
                ptype = PlayerType::REMOTE;
            }

            Packet *pkt = pf->createPacketFromTemplate("CreatePlayer");
            pkt->setString("userName", co->getUserName());
            pkt->setUnsigned("playerId", co->getPlayerId());
            pkt->setByte("team", static_cast<byte>(co->getTeam()));
            pkt->setByte("type", static_cast<byte>(ptype));
            c->queuePacket(TLProtocol::TCP, pkt);
        }
    }

    // Tell the server to instantiate the player objects
    EventManager *emgr = ServerServiceLocator::singleton()->getEventManager();
    for (Client *c : _clients) {
        Log::debug("Queueing CreatePlayerEvent (%p)", c);
        emgr->queueEvent(new CreatePlayerEvent(c));
    }
}

void LobbyController::onPlayerSpawned(const EventData *e) {
    SpawnPlayerEvent *evt = (SpawnPlayerEvent*)e;
    PacketFactory *pf = ServerServiceLocator::singleton()->getPacketFactory();

    // Notify all the clients
    for (Client *c : _clients) {
        Packet *pkt = pf->createPacketFromTemplate("SpawnPlayer");
        pkt->setUnsigned("playerId", evt->getPlayerId());
        pkt->setFloat("x", evt->getPosition().x);
        pkt->setFloat("y", evt->getPosition().y);

        c->queuePacket(TLProtocol::TCP, pkt);
    }
}

void LobbyController::onPlayerTransformOut(const EventData *e) {
    PlayerTransformUpdateEvent *evt = (PlayerTransformUpdateEvent*)e;

    if (evt->getDirection() == NetworkDirection::NET_OUT) {
        PacketFactory *pf = ServerServiceLocator::singleton()->getPacketFactory();

        for (Client *c : _clients) {
            if (c->getPlayerId() == evt->getPlayerId()) {
                // Don't distort the self-perception of the clients.
                continue;
            }

            Packet *pkt = pf->createPacketFromTemplate("PlayerTransformUpdate");
            pkt->setUnsigned("playerId", evt->getPlayerId());
            pkt->setFloat("x", evt->getPosition().x);
            pkt->setFloat("y", evt->getPosition().y);
            pkt->setFloat("rotation", evt->getRotation());
            pkt->setFloat("velX", evt->getVelocity().x);
            pkt->setFloat("velY", evt->getVelocity().y);
            pkt->setFloat("angVel", evt->getAngularVelocity());
            pkt->setUnsigned("step", evt->getStep());

            c->queuePacket(TLProtocol::UDP, pkt);
        }
    }
}

void LobbyController::onObjectiveStateChanged(const EventData *e) {
    ObjectiveStateChangedEvent *evt = (ObjectiveStateChangedEvent*)e;

    if (evt->getNetworkDirection() != NetworkDirection::NET_OUT) {
        return;
    }

    PacketFactory *pf = ServerServiceLocator::singleton()->getPacketFactory();

    for (Client *c : _clients) {
        Packet *pkt = pf->createPacketFromTemplate("ObjectiveStateChanged");
        
        pkt->setByte("objectiveId", static_cast<byte>(evt->getObjectiveId()));
        pkt->setByte("state", static_cast<byte>(evt->getObjectiveState()));
        pkt->setUnsigned("playerId", evt->getPlayerId());

        c->queuePacket(TLProtocol::UDP, pkt);
    }
}

void LobbyController::onGunFired(const EventData *e) {
    GunFiredEvent *evt = (GunFiredEvent*)e;

    PacketFactory *pf = ServerServiceLocator::singleton()->getPacketFactory();

    for (Client *c : _clients) {
        if (c->getPlayerId() == evt->getPlayerId()) {
            continue;
        }

        Packet *pkt = pf->createPacketFromTemplate("GunFired");       
        pkt->setUnsigned("playerId", evt->getPlayerId());
        pkt->setFloat("gunX", evt->getGunPoint().x);
        pkt->setFloat("gunY", evt->getGunPoint().y);
        pkt->setFloat("endX", evt->getEndPoint().x);
        pkt->setFloat("endY", evt->getEndPoint().y);

        c->queuePacket(TLProtocol::UDP, pkt);
    }
}

void LobbyController::onBulletHit(const EventData *e) {
    BulletHitEvent *evt = (BulletHitEvent*)e;

    PacketFactory *pf = ServerServiceLocator::singleton()->getPacketFactory();

    for (Client *c : _clients) {
        Packet *pkt = pf->createPacketFromTemplate("BulletHit");
        
        pkt->setUnsigned("firePlayerId", evt->getFiringPlayerId());
        pkt->setUnsigned("hitPlayerId", evt->getHitPlayerId());
        pkt->setInt("health", evt->getPlayerHealth());
        pkt->setByte("kill", evt->didPlayerDie());

        c->queuePacket(TLProtocol::UDP, pkt);
    }
}

void LobbyController::onGrenadeThrow(const EventData *e) {
    GrenadeThrowEvent *evt = (GrenadeThrowEvent*)e;

    if (evt->getNetworkDirection() != NetworkDirection::NET_IN) {
        return;
    }

    PacketFactory *pf = ServerServiceLocator::singleton()->getPacketFactory();

    // Send a packet to all but the throwing player
    for (Client *c : _clients) {
        if (c->getPlayerId() == evt->getPlayerId())
            continue;

        Packet *pkt = pf->createPacketFromTemplate("GrenadeThrow");
        pkt->setUnsigned("playerId", evt->getPlayerId());
        pkt->setFloat("posX", evt->getPosition().x);
        pkt->setFloat("posY", evt->getPosition().y);
        pkt->setFloat("rot", evt->getRotation());

        c->queuePacket(TLProtocol::UDP, pkt);
    }
}

void LobbyController::onGrenadeExplode(const EventData *e) {
    GrenadeExplodeEvent *evt = (GrenadeExplodeEvent*)e;

    PacketFactory *pf = ServerServiceLocator::singleton()->getPacketFactory();

    // Send a packet to all but the throwing player
    for (Client *c : _clients) {
        Packet *pkt = pf->createPacketFromTemplate("GrenadeExplode");
        pkt->setFloat("posX", evt->getPosition().x);
        pkt->setFloat("posY", evt->getPosition().y);
        pkt->setUnsigned("type", static_cast<unsigned>(evt->getGrenadeType()));

        c->queuePacket(TLProtocol::UDP, pkt);
    }
}

void LobbyController::onGrenadeHit(const EventData *e) {
    GrenadeHitEvent *evt = (GrenadeHitEvent*)e;

    PacketFactory *pf = ServerServiceLocator::singleton()->getPacketFactory();

    for (Client *c : _clients) {
        Packet *pkt = pf->createPacketFromTemplate("GrenadeHit");
        pkt->setUnsigned("playerId", evt->getPlayerId());
        pkt->setFloat("factor", evt->getHitFactor());
        pkt->setUnsigned("type", static_cast<unsigned>(evt->getGrenadeType()));

        c->queuePacket(TLProtocol::UDP, pkt);
    }
}



/*
==================
server::ClientJoinConversation
==================
*/
ClientJoinConversation::ClientJoinConversation() 
    :   _done(false),
        _accepted(false),
        _socketUdp(nullptr),
        _clientJoinRequest(nullptr),
        _playerId(0),
        _startTime(std::time(0)) {
}

ClientJoinConversation::~ClientJoinConversation() {
    if (_clientJoinRequest) {
        delete _clientJoinRequest;
    }

    if (_socketUdp) {
        // Only performed if "getUdpSocket()" never got called - should rarely
        // or never happen.
        delete _socketUdp;
    }
}

bool ClientJoinConversation::update(SocketBase *socket, bool admission, Team team) {
    if (isDone()) {
        // Prompt caller for destruction if client got rejected
        return clientGotAccepted();
    }

    if (std::time(0) - _startTime > JOIN_REQUEST_TIMEOUT) {
        Log::debug("Client timed out (JOIN_REQUEST_TIMEOUT)");
        rejectClient(socket);
        _done = true;
        return false;
    }

    _clientJoinRequest = socket->getPacket("ClientJoinRequest");
    if (_clientJoinRequest) {
        _clientName = _clientJoinRequest->getString("userName");
        _clientUdpPort = _clientJoinRequest->getUnsigned("udpPort");

        if (admission) {
            _accepted = true;
            if (!acceptClient(socket, team)) {
                Log::debug("Faied to accept client");
                _accepted = false;
                return false;
            }
        } else {
            Log::debug("Rejected client (admission denied)");
            _accepted = false;
            if (!rejectClient(socket)) {
                Log::debug("Failed to reject client");
                return false;
            }
        }

        _done = true;
    }

    return true;
}

bool ClientJoinConversation::isDone() {
    return _done;
}

bool ClientJoinConversation::clientGotAccepted() {
    return _accepted;
}

PlayerId ClientJoinConversation::getPlayerId() {
    return _playerId;
}

std::string ClientJoinConversation::getClientUserName() {
    return _clientName;
}

SocketUDP* ClientJoinConversation::getUdpSocket() {
    // Transfer ownership to caller
    SocketUDP *udp = _socketUdp;
    _socketUdp = nullptr;
    return udp;
}

bool ClientJoinConversation::acceptClient(SocketBase *socket, Team team) {
    IPaddress ipaddr = socket->getIPAddress();       
    Uint16 hoport = (Uint16)_clientUdpPort;
    SDLNet_Write16(hoport, &ipaddr.port);

    _socketUdp = new SocketUDP(ipaddr, 0);
    if (!_socketUdp->initSocket() || !_socketUdp->isGood()) {
        Log::error("Unable to initiate UDP socket to client at '%s:%u'",
                   socket->getOctalIP().c_str(), _clientUdpPort);
        delete _socketUdp;
        _socketUdp = nullptr;
        rejectClient(socket);
        return false;
    }

    _playerId = getNextPlayerId();

    PacketFactory *pf = ServerServiceLocator::singleton()->getPacketFactory();
    Packet *pkt = pf->createPacketFromTemplate("ClientJoinResponse");
    pkt->setByte("response", 1);
    pkt->setUnsigned("playerId", _playerId);
    pkt->setUnsigned("udpPort", _socketUdp->getListenPort());
    pkt->setByte("team", static_cast<byte>(team));

    bool status = socket->sendPacket(pkt);
    delete pkt;
    return status;
}

bool ClientJoinConversation::rejectClient(SocketBase *socket) {
    PacketFactory *pf = ServerServiceLocator::singleton()->getPacketFactory();
    Packet *pkt = pf->createPacketFromTemplate("ClientJoinResponse");
    pkt->setByte("response", 0);
    pkt->setUnsigned("playerId", 0);
    pkt->setUnsigned("udpPort", 0);

    bool status = socket->sendPacket(pkt);
    delete pkt;
    return status;
}

PlayerId ClientJoinConversation::getNextPlayerId() {
    static unsigned playerId = 1;
    return playerId++;
}

}
