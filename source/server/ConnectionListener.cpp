#include "ConnectionListener.h"
#include <assert.h>
#include <chrono>



namespace server {


/*
================
Thread Functions
================
*/
static void tcpLoop(bool *abort, ConnectionListener *listener,
					ConnectionListener::NewConnectionCallback connCb,
                    ConnectionListener::ErrorCallback errCb) {
    // Set up a listening socket on port TCP_SERVER_PORT
	IPaddress ip;
	TCPsocket server_socket;

	if (SDLNet_ResolveHost(&ip, NULL, TCP_SERVER_PORT) == -1) {
        Log::error("SDLNet_ResolveHost() failed: %s", SDLNet_GetError());
        (*listener.*errCb)(-1);
        return;
	}

	server_socket = SDLNet_TCP_Open(&ip);
	if (!server_socket) {
        Log::error("SDLNet_TCP_Open() failed: %s", SDLNet_GetError());
        (*listener.*errCb)(-2);
        return;
	}

	while (!(*abort)) {
		TCPsocket new_socket = SDLNet_TCP_Accept(server_socket);

		if (new_socket) {
			Log::debug("New TCP connection");
			(*listener.*connCb)(new_socket);
		} else {
            SDLNet_TCP_Close(new_socket);
        }
		
        // Sleep, no need to fry the CPU for this 
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}

    SDLNet_TCP_Close(server_socket);
}


/*
================
ConnectionListener
================
*/
ConnectionListener::ConnectionListener()
	:   _abortThreads(false),
        _threadError(0) {

	_thread = new std::thread(tcpLoop, &_abortThreads, this, 
                              &ConnectionListener::addNewSocket,
                              &ConnectionListener::setError);
}

ConnectionListener::~ConnectionListener() {
	_abortThreads = true;
	_thread->join();
	delete _thread;

    for (TCPsocket sock : _newConnections) {
        SDLNet_TCP_Close(sock);
    }
}

bool ConnectionListener::isGood() {
    bool good;

    _mutex.lock();
    good = (_threadError == 0);
    _mutex.unlock();

    return good;
}

bool ConnectionListener::hasNewConnection() {
	_mutex.lock();
	int count = _newConnections.size();
	_mutex.unlock();

	return (count > 0);
}

SocketTCP* ConnectionListener::popNewConnection() {
	TCPsocket socket = 0;

	_mutex.lock();

	if (_newConnections.size() > 0) {
		socket = _newConnections[0];
		_newConnections.erase(_newConnections.begin());
	}

	_mutex.unlock();

	if (socket == 0) {
		return nullptr;
	}

    SocketTCP *tcp = new SocketTCP(socket);
    if (!tcp->initSocket()) {
        Log::error("ConnectionListener: Unable to initialize new connection");
        delete tcp;
        return nullptr;
    }

    return tcp;
}


void ConnectionListener::addNewSocket(TCPsocket socket) {
	if (socket > 0) {
		_mutex.lock();
		_newConnections.push_back(socket);
		_mutex.unlock();
	}
}

void ConnectionListener::setError(int e) {
    _mutex.lock();
    _threadError = e;
    _mutex.unlock();

    Log::error("ConnectionListener received error from worker thread: %i", e);
}


}
