#pragma once

#include <thread>
#include <mutex>

#include "Protocol.h"
#include "Socket.h"


namespace server {


/* @brief   Runs a background thread listening for new connections over TCP. The 
 *          TCP socket is bound on port TCP_SERVER_PORT (defined in Protocol.h). 
 */
class ConnectionListener {
public:
    typedef void (ConnectionListener::*NewConnectionCallback)(TCPsocket);
    typedef void (ConnectionListener::*ErrorCallback)(int);

	ConnectionListener();
	~ConnectionListener();

    /**
     * Check if the ConnectionListener is actively listening for connections
     * without error.
     * @return          True if everything is OK, false otherwise.
     */
    bool isGood();

    /**
     * Check if the ConnectionListener has received a new connection.
     * @return          True if there are waiting connections, and a call to
     *                  popNewConnection() 
     */
	bool hasNewConnection();

    /**
     * Retrieve a new TCP socket, initialized with the oldest connection.
     * @return          A valid, connected SocetTCP if there are waiting 
     *                  sockets, NULL otherwise.
     */
	SocketTCP* popNewConnection();

private:
    std::thread *_thread;
	std::mutex _mutex;

    /**
     * Set by the ConnectionListener, read by the background worker thread. 
     * If it is ever true, the background thread will exit.
     */
	bool _abortThreads;

    /**
     * Set by the background worker thread by using a pointer to setError(int).
     */
    int _threadError;

    std::vector<TCPsocket> _newConnections;
    

    /**
     * Called by hte background worker thread when an error has occurred.
     */
    void setError(int error);

    /**
     * Called by the background worker thread when a new connection is 
     * available. 
     */
	void addNewSocket(TCPsocket);
};


}
