#include "Objective.h"

namespace server {

Objective::Objective(ObjectiveId id) 
    :   _objId(id),
        _hackingPlayerId(0) {
    setState(ObjectiveState::AVAILABLE);
}

ObjectiveId Objective::getObjectiveId() const {
    return _objId;
}

void Objective::hackInitiated(PlayerId playerId) {
    assert(_state == ObjectiveState::AVAILABLE);

    _hackingPlayerId = playerId;
    _state = ObjectiveState::TRIGGERED;
    _timer.start();
}

PlayerId Objective::getHackingPlayerId() const {
    if (_state != ObjectiveState::TRIGGERED) {
        return 0;
    }

    return _hackingPlayerId;
}

void Objective::setState(ObjectiveState state) {
    assert(state != ObjectiveState::TRIGGERED);
    _state = state;
}

ObjectiveState Objective::getState() const {
    return _state;
}

float Objective::getTriggeredTime() const {
    if (_state != ObjectiveState::TRIGGERED) {
        return 0.f;
    }

    return _timer.getElapsedSeconds();
}

}
