#include "ServerServiceLocator.h"
#include "PathTranslator.h"
#include "ServiceLocator.h"

namespace server {


ServerServiceLocator* ServerServiceLocator::_serverSingleton = nullptr;


ServerServiceLocator* ServerServiceLocator::singleton() {
    if (!_serverSingleton) {
        _serverSingleton = new ServerServiceLocator();
        _singleton = _serverSingleton;
    }

    return _serverSingleton;
}


}
