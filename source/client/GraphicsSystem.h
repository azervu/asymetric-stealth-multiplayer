/**
 * @file    source/client/GraphicsSystem.h
 *
 * @brief   Declares the graphics system class.
 */

#pragma once

#include "SDLHeaders.h"
#include "Engine.h"
#include "GameObject.h"
#include "Map.h"
#include "ClientEvents.h"
#include "Renderable.h"
#include "ResourceManager.h"
#include "ShaderHandler.h"

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

#include <string>
#include <vector>
#include <atomic>
#include <mutex>
#include <thread>
#include <future>

namespace Renderer {

    /**
    * @struct  MapData
    *
    * @brief   Represent 1 square on the map.
    */

    struct MapData {
        glm::vec2 pos;
        glm::vec2 texPos;
    };

    struct Gfx2DData {
        glm::vec2 vertex;
        glm::vec2 texCoord;
    };

    /**
     * @class   Layer
     *
     * @brief   An Abstract class representing a layer.
     */
    class Layer {
    public:

        Layer();
        virtual ~Layer();

        virtual void draw() = 0;
        virtual void updateUniform(const glm::mat4 &projectionMatrix, const glm::mat4 &viewMatrix);

    protected:

        ShaderProgram* _shader = nullptr;
        GLuint _vao = 0;
        GLuint _vbo = 0;

        GLint _projectionUniform = -1;
        GLint _viewUniform = -1;
    };

    /**
     * @class   MapLayer
     *
     * @brief   A map layer.
     */
    class MapLayer : public Layer {
    public:

        MapLayer(const std::string& tileset);
        virtual ~MapLayer();

        virtual void draw();

        void updateData(const std::vector<MapData> &buffer);

    protected:

        size_t _size = 0;

        Texture* _testTexture = nullptr;

        GLint _textureSheetUniform = -1;
        GLint _texSizeUniform = -1;
        GLint _texTileSizeUniform = -1;

        GLint _tileSizeUniform = -1;
        GLint _tintUniform = -1;
        GLint _zDepthUniform = -1;
    };

    /**
     * @class   DrawableLayer
     *
     * @brief   A drawable layer.
     */
    class DrawableLayer : public Layer {
    public:

        DrawableLayer();
        virtual ~DrawableLayer();

        virtual void draw();

        void updateData(
            const std::vector<Drawable::DrawableData>::iterator& from,
            const std::vector<Drawable::DrawableData>::iterator& to
        );

    protected:

        struct TextureSortData {
            size_t start;
            size_t size;
            ResourceId id;
        };

        GLint _texUniform = -1;
        GLint _texSizeUniform = -1;

        std::vector<TextureSortData> _renderSegments;

        size_t _size = 0;
        GLint _tintUniform = -1;
    };

    class DebugLayer : public Layer {
    public:

        DebugLayer(int vertexPerObject);
        virtual ~DebugLayer();

        virtual void draw();

        void updateData(
            const std::vector<Drawable::DrawableData>::iterator& from,
            const std::vector<Drawable::DrawableData>::iterator& to
        );

    protected:

        size_t _size = 0;

    private:

        int _vertexPerObject;
    };

    class LosLayer : public DebugLayer {
    public:

        LosLayer(double viewRadius);
        virtual ~LosLayer();

        virtual void draw();
        void updateLos(Vec2 cameraPos);

    private:

        double _viewRadius;
        size_t _viewVertexAmount = 0;
        GLuint _vaoCircle = 0;
        GLuint _vboCircle = 0;
    };
}

/**
 * The graphics system *acts* like a Subsystem, but isn't one at all. This is
 * because it requires so much special treatment compared to other Subsystems,
 * that the default Subsystem interface didn't make sense to use. However, the
 * GraphicsSystem runs it's own completely independent thread which handles
 * all things regarding graphics.
 */
class GraphicsSystem {
public:

    GraphicsSystem();
    ~GraphicsSystem();

    /**
     * @fn  bool GraphicsSystem::init(Settings* settings);
     *
     * @brief   Initialises this object.
     *
     * @param   settings    Options for controlling the operation.
     *
     * @return  true if it succeeds, false if it fails.
     */

    bool init(Settings* settings);

    /**
     * @fn  void GraphicsSystem::entityUpdate (DeltaTime delta);
     *
     * @brief   Updates the system in relation to delta.
     *
     * @param   delta   The time delta.
     */

    void entityUpdate(DeltaTime delta, const std::vector<Drawable::BaseDrawable*>& buffer);
    void mapUpdate(const Map& map, const std::vector<int>& deltaBackground, const std::vector<int>& deltaForeground);
    void mapSet(const Map* map);
    
    void renderThread();

    /**
     * @fn  bool GraphicsSystem::initOpenGL();
     *
     * @brief   Initialises OpenGL and GLEW. Executed on the background thread.
     *
     * @return  true if it succeeds, false if it fails.
     */

    bool initOpenGL();

    /**
     * @fn  bool GraphicsSystem::initShaders(Settings* settings);
     *
     * @brief   Initialises the shaders.
     *
     * @return  true if it succeeds, false if it fails.
     */

    bool initShaders();

private:
    /**
     * SDL 2 has some funky requirements to properly function on Linux.
     *  1. The window must be created on the main application thread.
     *  2. SDL_INIT_VIDEO must be called from the thread using the GL context.
     *      (The video subsystem is implicitly initialized when a window is 
     *       created)
     *  3. The OpenGL context must be created on the thread that will use it.
     *  4. The window must be created before the context.
     *
     * In addition, the worker thread must NOT perform any rendering until the
     * entire application is initialized fully.
     *
     * The GrahpicsSystem must manage the initialization choreography flawlessly
     * for the program to even run under Linux.
     */
    enum class InitState : int {
        // Default state - nothing has been initiated.
        INIT_NOT_STARTED,

        // The worker thread should run SDL_INIT_VIDEO.
        WORK_THR_INIT_VIDEO,

        // The main thread should create the window.
        MAIN_THR_INIT_WINDOW,

        // The worker thread should create the OpenGL context
        WORK_THR_INIT_CONTEXT,

        // The worker thread sigaling the main thread that init completes
        GRAPHICS_INIT_COMPLETE,

        // The main thread signaling the worker thread to wait for system init.
        WAIT_FOR_SYSTEM_INIT,

        // The main thread signaling the worke thread that the entire 
        // application has initialized successully.
        SYSTEM_INIT_COMPLETE,

        // Flagged by either main- or worker-thread when an error has occurred.
        // Both parties should terminate immediately following this.
        INIT_ERROR,
    };

    /**
     * Wait for a specific InitState to be flagged. The method blocks the
     * calling thread until another thread sets _initState to the desired state.
     * @param state The desired state that must be flagged in order to continue
     * @return      True if the desired state was flagged, false if 
     *              InitState::INIT_ERROR was flagged.
     */
    bool waitForInitState(InitState state);
    std::atomic<InitState> _initState;

    /**
     * RENDER-THREAD METHOD
     * Updated the viewport to match the Window-service's dimensions. Called by
     * the rendering thread shortly after _dirtyViewport is set to true.
     */
    void updateViewport();

    /** 
     * RENDER-THREAD METHOD
     * Translate the view-matrix according to the camera's position.
     */
    void copyCameraTransform();
    void applyCameraTransform();
    Vec2 _cameraTopLeft;
    Vec2 _cameraCenter;

    void onClientInitComplete(const EventData *e);
    EventListenerDelegate _clientInitDelegate;
    
    void onLevelLoadingComplete(const EventData *e);
    EventListenerDelegate _levelLoadedDelegate;

    void onWindowResized(const EventData *e);
    EventListenerDelegate _winResizeDelegate;

    void onViewFrameChanged(const EventData *e);
    EventListenerDelegate _viewFrameChangeDelegate;

    ResourceManager* _resourceManager;
    ShaderHandler* _shaderHandler;

    int _refreshRate;

    SDL_GLContext _context;

    std::mutex _backBufferMutex;
    std::mutex _eventMutex;
    std::atomic_bool _continueThread;
    std::atomic_bool _dirtyViewport;
    std::thread _graphicsThread;

    glm::mat4 _projection;
    glm::mat4 _view;
    glm::quat _rotation;

    std::vector<Renderer::Layer*> _layers;
    std::vector<Drawable::DrawableData> _backBufferEntity;
    std::array<int, 8> _backBufferSegmentStart;
    std::array<int, 8> _backBufferSegmentEnd;

    std::vector<MapLightProperties> _backBufferLights;

    bool _mapChange = false;
    std::vector<Renderer::MapData> _backBufferBackground;
    std::vector<Renderer::MapData> _backBufferForeground;

    // Used for delta update
    std::map<int, int> _backgroundMapping;
    std::map<int, int> _foregroundMapping;
};

