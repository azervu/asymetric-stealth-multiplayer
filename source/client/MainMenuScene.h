#pragma once

#include "Engine.h"
#include "Scene.h"
#include "EventManager.h"
#include "SceneLoader.h"
#include <atomic>


class ClientGameScene;


/**
 * MainMenuScene is the initial client side Scene. It displays nothing, but 
 * pushes a ClientGameScene when the network negotiation has ended and the
 * server has told us to start loading.
 */
class MainMenuScene : public Scene {
public:
    MainMenuScene();
    ~MainMenuScene();

    virtual bool loadScene();
    virtual void logicUpdate(DeltaTime);
    virtual void onResume();
	virtual Scene::State getState();
	virtual Scene* getNextScene();

private:
    void checkSceneLoader();

    /* StartLoadingLevelEvent callback method. 
     */
    void onStartLoading(const EventData *evt);
    EventListenerDelegate _startLoadingDelegate;   

    SceneLoader<ClientGameScene> *_sceneLoader;
    ClientGameScene *_gameScene;
    Scene::State _state;
};
