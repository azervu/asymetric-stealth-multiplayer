/**
 * @file    source/client/OpenGLUtility.h
 *
 * @brief   Declares the open gl utility class.
 */

#pragma once

#include <GL/glew.h>

#include <string>

// Macro unwrapper
#define S1(x) #x
#define S2(x) S1(x)

#define OPENGL_ERROR_CHECK(t) utility::GL_errorCheck(               \
        (std::string)S2(__LINE__)+": "+__FUNCTION__+": "+t);


namespace utility {

    /**
     * @fn  void loadShaderFromFile(GLuint shader, const std::string &path);
     *
     * @brief   Loads shader from file.
     *
     * @param   shader  The shader.
     * @param   path    Filename of the file.
     */

    void loadShaderFromFile(GLuint shader, const std::string &path);

    /**
     * @fn  GLuint loadCompileShader(const std::string &path, GLenum type);
     *
     * @brief   Load and compile shader.
     *
     * @param   path    path to shader file.
     * @param   type    The type.
     *
     * @return  The compile shader.
     */

    GLuint loadCompileShader(const std::string &path, GLenum type);

    /**
     * @fn  bool GL_errorCheck(const std::string &place);
     *
     * @brief   OpenGL error check.
     *
     * @param   place   The place.
     *
     * @return  true if there is an GL_ERROR, false otherwise.
     */

    bool GL_errorCheck(const std::string &place);

    /**
     * @fn  bool checkProgramLog(GLuint programID, int flag);
     *
     * @brief   Check program log.
     *
     * @param   programID   Identifier for the program.
     * @param   flag        The flag.
     *
     * @return  true if there was errors, false if not.
     */

    bool checkProgramLog(GLuint programID, int flag);

    /**
    * @fn  void checkShaderLog(GLuint shaderID, int flag);
    *
    * @brief   Check shader log.
    *
    * @param   shaderID    Identifier for the shader.
    * @param   flag        The flag.
    */

    void checkShaderLog(GLuint shaderID, int flag);
}
