/**
 * @file    source/client/ShaderHandler.h
 *
 * @brief   Declares the shader handler class.
 */

#pragma once

#include <GL/glew.h>

#include <map>
#include <vector>
#include <string>
#include <mutex>

// @TODO support for tessellation evaluation and control shader?
enum class SHADERTYPE : GLenum {
    VERTEX      = GL_VERTEX_SHADER,
    GEOMETRY    = GL_GEOMETRY_SHADER,
    FRAGMENT    = GL_FRAGMENT_SHADER
};

// contains information needed for a complete shader program for OpenGL
class ShaderProgram {
public:
	ShaderProgram (GLuint programID);
	~ShaderProgram ();

	// get data
	GLuint getVertexShader () const;
    GLuint getGeometryShader() const;
	GLuint getFragmentShader () const;
	GLuint getProgramID () const;
	GLint getRefrences (const std::string &name) const;

	// set some values
	void setShader (GLint shaderID, SHADERTYPE type);
	void createAttributeRefrence(const std::string &name);
	void createUniformRefrence(const std::string &name);

private:

    std::mutex _threadLock;

    GLuint _vertexShader;
    GLuint _geometryShader;
    GLuint _fragmentShader;
    GLuint _programID;

	std::map<std::string, GLint> _refrences;
};

// contains information for a single shader for OpenGL
class Shader {
public:
	Shader (GLint shaderID, SHADERTYPE type);
	~Shader ();

	GLuint getShaderID () const;
	SHADERTYPE getType () const;

private:

	GLuint _shaderID;
	SHADERTYPE _type;
};

// 
class ShaderHandler {
public:

	ShaderHandler ();
	~ShaderHandler ();

	ShaderProgram * getShaderProgram (GLuint ID);
	ShaderProgram * getShaderProgram (const std::string &path);

private:

    /**
     * @fn  ShaderProgram * ShaderHandler::loadShaderProgram (const std::string &path);
     *
     * @brief   Loads shader program.
     *
     * @param   path    Full pathname of the shader program file.
     *
     * @return  null if it fails, else the shader program.
     */

	ShaderProgram * loadShaderProgram (const std::string &path);

    /**
     * @fn  Shader * ShaderHandler::loadShader (const std::string &path, SHADERTYPE type);
     *
     * @brief   Loads a shader.
     *
     * @param   path    Full pathname of the file.
     * @param   type    The type.
     *
     * @return  null if it fails, else the shader.
     */

	Shader * loadShader (const std::string &path, SHADERTYPE type);

    std::mutex _threadLock;

	std::map<std::string, GLuint>  _fileToID;
	std::map<GLuint, ShaderProgram*> _IDtoShaderProgram;
	std::map<GLuint, Shader*> _IDtoShader;

	std::vector<ShaderProgram*> _shaderPrograms;
	std::vector<Shader*> _shaders;
};