/**
 * @file    source/client/ClientEvents.h
 *          
 * @brief   Declares all event that are strictly located on the client side of
 *          the application.
 */

#pragma once

#include "Engine.h"
#include "EventManager.h"
#include "InputState.h"
#include "Map.h"
#include "SharedEvents.h"
#include <string>

/**
 * Abstract class used as super for all input events
 */
class InputEvent : public EventData {
public:
    InputEvent(const InputState *inputState);
    const InputState* getInputState() const;

private:
    const InputState *_inputState;
};

/**
 * Sent by the Input Subsystem when a key is pressed or released.
 */
class KeyEvent : public InputEvent {
public:
    static const EventType eventType;

    KeyEvent(const InputState *is, SDL_Keycode key, bool pressed);

    EventType getEventType() const;
    bool isKeyDown() const;
    SDL_Keycode getKeyCode() const;

private:
    SDL_Keycode _keyCode;
    bool _keyPressed;
};

/**
 * Sent by the Input Subsystem when the mouse is moved.
 */
class MouseMotionEvent : public InputEvent {
public:
    static const EventType eventType;

    MouseMotionEvent(const InputState *is);

    EventType getEventType() const;
    Vec2 getMousePosition() const;
    Vec2 getMouseRelative() const;
};

/**
 * Sent by the Input Subsystem when a button is pressed or released.
 */
class ButtonEvent : public InputEvent {
public:
    static const EventType eventType;

    ButtonEvent(const InputState *is, Uint8 key, bool presed, Vec2 pos);

    EventType getEventType() const;
    bool isKeyDown() const;
    Uint8 getButton() const;
    Vec2 getPosition() const;

private:
    Vec2 _position;
    Uint8 _button;
    bool _pressed;
};



/**
 * Proxy-event fired by Client upon SDL_WINDOWEVENT_RESIZED.
 */
class WindowResizedEvent : public EventData {
public:
    static const EventType eventType;

    WindowResizedEvent(std::pair<int, int> size);

    EventType getEventType() const;
    std::pair<int, int> getSize() const;

private:
    std::pair<int, int> _size;
};

/**
 * Fired when the camera's viewport has changed. The passed viewport is in
 * metric coordinates.
 */
class CameraViewFrameChangedEvent : public EventData {
public:
    static const EventType eventType;

    CameraViewFrameChangedEvent(Vec2 vp);

    EventType getEventType() const;
    Vec2 getViewport() const;

private:
    Vec2 _viewport;
};

/**
 * Fired when the Client is done initializing all subsystems and all services.
 */
class ClientInitiationCompleteEvent : public EventData {
public:
    static const EventType eventType;

    EventType getEventType() const;
};



/**
 * Event dispatched when the user has flagged himself as ready to start the
 * game.
 */
class ClientReadyToPlayEvent : public EventData {
public:
    static const EventType eventType;

    EventType getEventType() const;
};

/**
 * Event dispatched from the Network System when the server has told the
 * clients to start loading the level.
 */
class StartLoadingLevelEvent : public EventData {
public:
    static const EventType eventType;

    StartLoadingLevelEvent(std::string level);

    EventType getEventType() const;
    std::string getLevelName() const;

private:
    std::string _level;
};

/**
 * Event dispatched after the level-loading system has heeded StartLoadingEvent
 * and has completed the loading.
 */
class LevelLoadingCompleteEvent : public EventData {
public:
    static const EventType eventType;

    LevelLoadingCompleteEvent(const Map* map);

    EventType getEventType() const;
    const Map* getMap() const;

private:

    const Map* _map;
};

/**
 * Event dispatched from the Network System after the server has received
 * 'load-complete' packet from every client.
 */
class GameStartingEvent : public EventData {
public:
    static const EventType eventType;

    EventType getEventType() const;
};



/**
 * Event that signalizes that the client should start connecting to the server.
 */
class InitiateConnectionEvent : public EventData {
public:
    static const EventType eventType;

    InitiateConnectionEvent(std::string userName, std::string hostName);

    EventType getEventType() const;
    std::string getUserName() const;
    std::string getHostName() const;

private:
    std::string _userName;
    std::string _hostName;
};

/**
 * Event sent from the Network Subsystem when the connection failed
 */
class ConnectionFailedEvent : public EventData {
public:
    static const EventType eventType;

    EventType getEventType() const;
};

/**
 * Event sent from the Network Subsystem when the client disconnects 
 */
class DisconnectEvent : public EventData {
public:
    static const EventType eventType;

    EventType getEventType() const;
};

/**
 * Event sent from the Network Subsystem when the connection has been properly
 * established and both the client and the server has prepared the UDP and the
 * TCP sockets.
 */
class ConnectionEstablishedEvent : public EventData {
public:
    static const EventType eventType;

    ConnectionEstablishedEvent(PlayerId id, Team team);

    EventType getEventType() const;
    PlayerId getPlayerId() const;
    Team getTeam() const;

private:
    PlayerId _playerId;
    Team _team;
};

/**
 * Event sent from the Network Subsystem when a new client joins.
 */
class ClientJoinedSessionEvent : public EventData {
public:
    static const EventType eventType;

    ClientJoinedSessionEvent(std::string clientName, PlayerId playerId, Team team);
    EventType getEventType() const;
    std::string getClientName() const;
    PlayerId getPlayerId() const;
    Team getTeam() const;

private:
    PlayerId _playerId;
    Team _team;
    std::string _clientName;
};

/**
 * Event sent from the Network Subsystem when a client leaves.
 */
class ClientLeftSessionEvent : public EventData {
public:
    static const EventType eventType;

    ClientLeftSessionEvent(std::string clientName, unsigned playerId);
    EventType getEventType() const;
    std::string getClientName() const;
    unsigned getPlayerId() const;

private:
    unsigned _playerId;
    std::string _clientName;
};

/**
 * Event sent from the Network Subsystem when the server shuts down
 */
class ServerShutdownEvent : public EventData {
public:
    static const EventType eventType;

    ServerShutdownEvent(std::string message);

    EventType getEventType() const;
    std::string getMessage() const;

private:
    std::string _message;
};

/**
* Event sent to the Sound subsystem to load a sound.
*/
class LoadSoundEvent : public EventData {
public:
    static const EventType eventType;
    LoadSoundEvent(std::string path);
    const std::string getSoundFile();
    EventType getEventType() const;
private:
    std::string  _soundFile;
};

/**
* Event sent to the Sound subsystem to affect a sound.
*/
class AffectSoundEvent : public EventData {
public:

    static const EventType eventType;
    AffectSoundEvent(int soundInstanceId, Vec2 position, bool loop = true);
    AffectSoundEvent(int soundInstanceId,  bool loop = true);
    EventType getEventType() const;
    bool getLoop();
    
    int getSoundInstanceId();
    Vec2 getPosition();

private:
    int _soundInstanceId;
    bool _loop;

    Vec2 _position;
};

/**
* Event sent to the Sound subsystem to create and play a sound that needs
to be updated at some point.
* The sound will be automatically deleted when it has finished playing.
*/
class CreateSoundEvent : public AffectSoundEvent {
public:
    static const EventType eventType;
    EventType getEventType() const;
    CreateSoundEvent(ResourceId soundId, int soundInstanceId, Vec2 position, bool loop = true);
    CreateSoundEvent(ResourceId soundId, int soundInstanceId, bool loop = true);
    CreateSoundEvent(ResourceId soundId, Vec2 position);
    CreateSoundEvent(ResourceId soundId);
    const ResourceId getSoundId();
    bool getTdSound();
    bool getAffectable();
private:
    ResourceId _soundId;
    bool _tdSound;
    bool _affectable;
};

/**
* Event sent to the Sound subsystem to destroy an affectable sound.
*/
class DestroyAffectableSoundEvent : public EventData {
public:
    static const EventType eventType;
    EventType getEventType() const;
    DestroyAffectableSoundEvent(int soundInstanceId);
    const ResourceId getSoundId();
    int getSoundInstanceId();
private:
    int _soundInstanceId;
};

/**
 * ClientGunFiredEvent is a subclassof GunFiredEvent. This class exists to
 * differentiate between incoming and outgoing GunFiredEvents.
 */
class ClientGunFiredEvent : public GunFiredEvent {
public:
    ClientGunFiredEvent(NetworkDirection dir, PlayerId playerId,
                        b2Vec2 gunPoint, b2Vec2 endPoint);

    NetworkDirection getDirection() const;

private:
    NetworkDirection _dir;
};


/**
* Event sent to the GUI objets to update them
*/
class UpdateGuiEvent : public EventData {
public:
    UpdateGuiEvent(int value, int guiId);
    static const EventType eventType;
    EventType getEventType() const;
    
    int getValue();
    int getGuiId();
private:
    int  _value;
    int  _guiId;
};

class InitiateGuiEvent : public EventData {
public:
    static const EventType eventType;
    EventType getEventType() const;
};

/* Fired by PlayerAreaComponent when the player enters or exits an area. The
 * ObjectiveId refers to the area of the objective.
 */
class PlayerAreaTransitionEvent : public EventData {
public:
    static const EventType eventType;

    PlayerAreaTransitionEvent(PlayerId playerId, ObjectiveId area);

    EventType getEventType() const;
    PlayerId getPlayerId() const;
    ObjectiveId getObjectiveId() const;

private:
    PlayerId _playerId;
    ObjectiveId _areaId;
};

