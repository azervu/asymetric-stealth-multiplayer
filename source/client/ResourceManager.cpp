/**
 * @file    source/client/ResourceManager.cpp
 *
 * @brief   Implements the resource manager and other related classes.
 */

#include "ResourceManager.h"

#include "PathTranslator.h"
#include "OpenGLUtility.h"

ResourceId hashResource(std::string resourceName) {
    std::hash<std::string> strHash;
    return strHash(resourceName);
}

const unsigned FONT_IMAGE_WIDTH = 1024;
const unsigned FONT_IAMGE_HEIGHT = 1024;
std::hash<std::string> STRING_HASH;

/*
==================
ResourceManager
==================
*/
ResourceManager::ResourceManager() {
    FreeImage_Initialise();
    if (TTF_Init() != 0) {
        Log::error("TTF_Init: %s", TTF_GetError());
    }
}

ResourceManager::~ResourceManager() {
    for (auto &iter : _resources) {
        delete iter.second;
    }

    TTF_Quit();
    FreeImage_DeInitialise();
}

Resource* ResourceManager::getLoadResource(ResourceType type, const std::string &assetPath) {
    if (type == ResourceType::UNDEFINED) {
        return nullptr;
    }

    ResourceContainer *container = nullptr;
    Resource *resource = nullptr;
    ResourceId id = hashResource(assetPath);

    std::unique_lock<std::recursive_mutex> lock(this->_dataGuard);

    try {
        container = this->_resources.at(id);
    } catch (std::out_of_range) {

        container = this->loadResource(type, id, assetPath);

        if (container == nullptr) {
            return nullptr;
        }

        _resources[id] = container;
    }

    switch (type) {
        case ResourceType::TEXTURE:
            resource = new Texture((TextureContainer*) container);
        break;

        case ResourceType::AUDIO:
            resource = new Audio((AudioContainer*) container);
        break;

        case ResourceType::FONT:
            resource = new Font((FontContainer*) container);
        break;
    }

    return resource;
}

Resource* ResourceManager::getResource(ResourceType type, ResourceId id) {
    ResourceContainer *container = nullptr;
    Resource *resource = nullptr;

    std::unique_lock<std::recursive_mutex> lock(this->_dataGuard);

    try {
        container = this->_resources.at(id);
    } catch (std::out_of_range) {
        return nullptr;
    }

    switch (type) {
        case ResourceType::TEXTURE:
            resource = new Texture((TextureContainer*) container);
        break;

        case ResourceType::FONT:
            resource = new Font((FontContainer*) container);
        break;
    }

    return resource;
}

std::future<Resource*> ResourceManager::queueResource(ResourceType type, const std::string &assetPath) {
    return std::async(&ResourceManager::getLoadResource, this, type, assetPath);
}

ResourceManager::ResourceContainer* ResourceManager::loadResource(ResourceType type, ResourceId id, const std::string &assetPath) {
    ResourceContainer *container = nullptr;

    switch (type) {
        case ResourceType::TEXTURE:
            container = new TextureContainer();
        break;

        case ResourceType::FONT:
            container = new FontContainer();
        break;

        default:
            Log::fatal("ResourceManager::loadResource: Type loading unimplemented for file %s", assetPath.c_str());
            return nullptr;
        break;
    }

    // Attempt to load the resource
    if (!container->load(assetPath)) {
        delete container;
        Log::fatal("ResourceManager::loadResource: failed to load %s", assetPath.c_str());
        return nullptr;
    }

    return container;
}

bool ResourceManager::isResourceLoaded(std::string assetPath) {
    ResourceId id = hashResource(assetPath);
    return (_resources.count(id) != 0);
}

void ResourceManager::destroyReleasedResources() {
    auto iter = _resources.begin();
    while (iter != _resources.end()) {
        ResourceContainer *container = iter->second;   

        if (container->getRetainCount() <= 0) {
            Log::info("Deleted orphaned resource '%s'", container->getAssetPath().c_str());
            delete container;
            iter = _resources.erase(iter);
        } else {
            iter++;
        }
    }
}

void ResourceManager::insertResource(ResourceContainer* data) {
    std::unique_lock<std::recursive_mutex> lock(this->_dataGuard);

    this->_resources[data->getResourceId()] = data;
}

/*
==================
ResourceManager::ResourceContainer
==================
*/
ResourceManager::ResourceContainer::ResourceContainer(ResourceType type)
    :   _retainCount(0),
        _loaded(false),
        _type(type) {
    static ResourceId nextResourceId = 1;

    _id = (nextResourceId++);
}

ResourceManager::ResourceContainer::~ResourceContainer() {
}

bool ResourceManager::ResourceContainer::load(const std::string &asset) {
    // This method can only be called once per instance, regardless of success
    if (this->_loaded) {
        return false;
    } else {
        this->_loaded = true;
    }

    this->_assetPath = asset;

    return this->_loaded;
}

void ResourceManager::ResourceContainer::retain() {
    this->_retainCount++;
}

void ResourceManager::ResourceContainer::release() {
    this->_retainCount--;

    if (_retainCount < 0) {
        Log::error("Resource was over-released");
    }
}

int ResourceManager::ResourceContainer::getRetainCount() const {
    return this->_retainCount;
}

ResourceId ResourceManager::ResourceContainer::getResourceId() const {
    return this->_id;
}

ResourceType ResourceManager::ResourceContainer::getResourceType() const {
    return this->_type;
}

std::string ResourceManager::ResourceContainer::getAssetPath() const {
    return this->_assetPath;
}


/*
==================
ResourceManager::TextureContainer
==================
*/
ResourceManager::TextureContainer::TextureContainer()
    :   ResourceContainer(ResourceType::TEXTURE) {
}

ResourceManager::TextureContainer::~TextureContainer() {
    if (this->_image) {
        delete this->_image;
    }
}

bool ResourceManager::TextureContainer::load(const std::string &asset) {
    if (!ResourceContainer::load(asset)) {
        return false;
    }

    // Attempt to load the resource
    bool result = loadTexture(asset);

    if (!result) {
        Log::error("Failed to load asset '%s'", asset.c_str());
    }

    return result;
}

bool ResourceManager::TextureContainer::loadTexture(const std::string &asset) {
    bool error = false;

    PathTranslator *pt = ClientServiceLocator::singleton()->getPathTranslator();
    std::string path = pt->translatePath(asset);

    fipImage* image = new fipImage();

    image->load(path.c_str());

    this->_texFormat = 0;
    this->_internalFormat = GL_RGBA8;
    unsigned int colCount = image->getColorType();
    unsigned int redMask = FreeImage_GetRedMask(*image);
    unsigned int bitsPerChannel = image->getBitsPerPixel();

    if (colCount == FIC_RGBALPHA) {
        if (redMask == 0x000000ff) {
            this->_texFormat = GL_RGBA8;
        } else {
            this->_texFormat = GL_BGRA;
        }
    } else if (colCount == FIC_RGB) {
        if (redMask == 0x000000ff) {
            this->_texFormat = GL_RGB8;
        } else {
            this->_texFormat = GL_BGR;
        }
    } else {
        Log::error("Unsupported color format %d", colCount);
        return false;
    }

    this->_image = image;

    return !error;
}

bool ResourceManager::TextureContainer::generateTexture(unsigned int width, unsigned int height) {
    fipImage* image = new fipImage(FIT_BITMAP, width, height, 4U);

    this->_image = image;
    this->_texFormat = GL_RGBA8;
    this->_internalFormat = GL_RGBA8;

    return true;
}

bool ResourceManager::TextureContainer::uploadTexture() {
    if (this->_uploaded) {
        return true;
    }
    bool error = false;

    static int unit = 0;
    if (this->_texUnit == -1) {
        this->_texUnit = unit++;
    }

    glActiveTexture(GL_TEXTURE0 + this->_texUnit);

    if (!this->_hasID) {
        glGenTextures(1, &this->_texRef);
        error |= OPENGL_ERROR_CHECK("glGenTextures");
        this->_hasID = true;
    }

    //Bind to the new texture ID
    glBindTexture(GL_TEXTURE_2D, this->_texRef);
    error |= OPENGL_ERROR_CHECK("glBindTexture");

    //Set texture parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    error |= OPENGL_ERROR_CHECK("glTexParameteri");
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    error |= OPENGL_ERROR_CHECK("glTexParameteri");
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
    error |= OPENGL_ERROR_CHECK("glTexParameteri");
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
    error |= OPENGL_ERROR_CHECK("glTexParameteri");

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    error |= OPENGL_ERROR_CHECK("glTexParameteri");
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    error |= OPENGL_ERROR_CHECK("glTexParameteri");

    //Store the texture data for OpenGL use
    glTexImage2D(
        GL_TEXTURE_2D,
        0,
        this->_internalFormat,
        this->_image->getWidth(),
        this->_image->getHeight(),
        0,
        this->_texFormat,
        GL_UNSIGNED_BYTE,
        this->_image->accessPixels()
    );
    error |= OPENGL_ERROR_CHECK("glTexImage2D");

    this->_uploaded = !error;
    return !error;
}

GLuint ResourceManager::TextureContainer::getTextureRefrence() const {
    return this->_texRef;
}

int ResourceManager::TextureContainer::getTextureUnit() {
    // @TODO refactor, it is a quick hack
    if (!this->uploadTexture()) {
        Log::error("Failed to upload texture to gfx card");
        return -1;
    }

    return this->_texUnit;
}

std::pair<int, int> ResourceManager::TextureContainer::getSize() const {
    return std::pair<int, int>(this->_image->getWidth(), this->_image->getHeight());
}

fipImage* ResourceManager::TextureContainer::getImageRef() {
    return this->_image;
}

/*
==================
ResourceManager::AudioContainer
==================
*/

ResourceManager::AudioContainer::AudioContainer()
    :   ResourceContainer(ResourceType::AUDIO) {
}

ResourceManager::AudioContainer::~AudioContainer() {
}

bool ResourceManager::AudioContainer::load(const std::string &asset) {
    if (!ResourceContainer::load(asset)) {
        return false;
    }

    bool result = this->loadAudio(asset);

    if (!result) {
        Log::error("Failed to load the sound '%s'", asset.c_str());
    }

    return result;
}

bool ResourceManager::AudioContainer::loadAudio(const std::string &asset) {
    //ISoundSource* sound = _soundEngine->addSoundSourceFromFile("../../assets/audio/getout.ogg");
    return false;
}

/*
==================
ResourceManager::FontContainer
==================
*/

ResourceManager::FontContainer::FontContainer()
    :   ResourceContainer(ResourceType::FONT) {

    TextureContainer* container = new TextureContainer();
    container->generateTexture(FONT_IMAGE_WIDTH, FONT_IAMGE_HEIGHT);
    this->_textImageList.push_back(container);

    ResourceManager* manager = ClientServiceLocator::singleton()->getResourceManager();
    manager->insertResource(container);
}

ResourceManager::FontContainer::~FontContainer() {
    // @TODO cleanup
    
    for (auto &font : this->_fontSizeMap) {
        TTF_CloseFont(font.second);
    }

#ifdef WIN32
    for (TextureContainer* t : this->_textImageList) {
        /* This crashes when compiled under g++ for whatever reason. The current
         * workaround is to ignore it. #yoloLongAndProsper
         */
        delete t;
    }
#endif
}

bool ResourceManager::FontContainer::load(const std::string &asset) {
    if (!ResourceContainer::load(asset)) {
        return false;
    }

    bool result = this->loadFont(asset);

    if (!result) {
        Log::error("Failed to load the sound '%s'", asset.c_str());
    }

    return result;
}

bool ResourceManager::FontContainer::loadFont(const std::string &asset) {

    bool error = false;

    PathTranslator *pt = ClientServiceLocator::singleton()->getPathTranslator();
    std::string path = pt->translatePath(asset);

    this->_fontName = asset;

    for (int i = 1; i <= 100; i++) {
        TTF_Font * load = TTF_OpenFont(path.c_str(), i);

        if (load == NULL) {
            Log::error("loadFont: %i: %s: %s", i, path.c_str(), TTF_GetError());
            TTF_CloseFont(load);
            continue;
        }

        this->_fontSizeMap[i] = load;
    }

    return !error;
}

Drawable::DrawableData ResourceManager::FontContainer::textToImage(const std::string& text, unsigned size) {

    std::pair<size_t, unsigned int> ref {STRING_HASH(text), size};

    Drawable::DrawableData imageRef;

    if (this->_textImageMap.count(ref)) {
        imageRef = this->_textImageMap[ref];
    } else {

        SDL_Surface* surface = TTF_RenderText_Blended(_fontSizeMap.at(size), text.c_str(), {255,255,255,255});
        SDL_PixelFormat* format = surface->format;

        int width = surface->w;
        int height = surface->h;
        long amount = width * height;

        fipImage* convert = new fipImage(FIT_BITMAP, width, height, 32U);

        Uint32* index = (Uint32*) surface->pixels;

        for (size_t y = 0; y < height; y++) {
            for (size_t x = 0; x < width; x++) {
                Uint32 pixel = *(index + (y * width + x));
                RGBQUAD data;
                data.rgbRed = pixel & format->Rmask;
                data.rgbGreen = pixel & format->Gmask;
                data.rgbBlue = pixel & format->Bmask;
                data.rgbReserved = pixel & format->Amask;
                convert->setPixelColor(x, y, &data);
            }
        }

        fipMemoryIO surfaceIo = {(BYTE*) surface->pixels, DWORD(width * height)};

        convert->loadFromMemory(surfaceIo);

        imageRef = this->storeTextRender(convert);

        this->_textImageMap[ref] = imageRef;
    }

    return imageRef;
}

Drawable::DrawableData ResourceManager::FontContainer::storeTextRender(fipImage* textImage) {

    if (_currentImageX + textImage->getWidth() > FONT_IMAGE_WIDTH) {
        _currentImageX = 0;
        _currentImageY += _previousTextImageH;

        if (_currentImageY > FONT_IAMGE_HEIGHT) {
            this->generateNextStorageImage();
        }
    }
    
    _previousTextImageW = textImage->getWidth();
    _previousTextImageH = textImage->getHeight();

    TextureContainer* container = this->_textImageList[this->_currentImage];
    container->getImageRef()->pasteSubImage(*textImage, _currentImageX, _currentImageY);

    Drawable::DrawableData data;
    data._dataType = Drawable::DrawableData::DataType::IMAGE;
    data._data.image.textureId = container->getResourceId();
    data._data.image.texX = _currentImageX;
    data._data.image.texY = _currentImageY;
    data._data.image.texW = _previousTextImageW;
    data._data.image.texH = _previousTextImageH;
    
    container->_uploaded = false;
    _currentImageX += _previousTextImageW;

    return data;
}

void ResourceManager::FontContainer::generateNextStorageImage() {
    _currentImageX = 0;
    _currentImageY = 0;

    TextureContainer* container = new TextureContainer();
    container->generateTexture(FONT_IMAGE_WIDTH, FONT_IAMGE_HEIGHT);
    this->_textImageList.push_back(container);

    ResourceManager* manager = ClientServiceLocator::singleton()->getResourceManager();
    manager->insertResource(container);

    ++_currentImage;
}

/*
==================
Resource
==================
*/
Resource::Resource(ResourceManager::ResourceContainer* container) 
    :   _container(container) {
    _container->retain();
}

Resource::~Resource() {
    _container->release();
}

int Resource::getRetainCount() const {
    return _container->getRetainCount();
}


/*
==================
Texture
==================
*/
Texture::Texture(ResourceManager::TextureContainer* container)
    : Resource(container) {
}

GLuint Texture::getTextureId() const {
    return ((ResourceManager::TextureContainer* )_container)->getTextureRefrence();
}

int Texture::getTextureUnit() {
    return ((ResourceManager::TextureContainer*)_container)->getTextureUnit();
}

std::pair<int, int> Texture::getSize() const {
    return ((ResourceManager::TextureContainer*)_container)->getSize();
}

/*
==================
Audio
==================
*/
Audio::Audio(ResourceManager::AudioContainer* container)
    :   Resource(container) {
}

/*
==================
Font
==================
*/
Font::Font(ResourceManager::FontContainer* container)
    :   Resource(container) {
}

Drawable::DrawableData Font::textToImage(const std::string& text, unsigned size) {
    return ((ResourceManager::FontContainer*) this->_container)->textToImage(text, size);
}
