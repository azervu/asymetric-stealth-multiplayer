#include "InputState.h"


const unsigned kBUTTON_FLAG = (1<<29);

InputState::InputState() {

}

InputState::~InputState() {

}

void InputState::clearFreshFlags() {
    _freshSet.clear();
    _mouseRel = Vec2(0.f, 0.f);
}

bool InputState::keyPressed(SDL_Keycode keyCode) {
    if (keyCode & kBUTTON_FLAG) {
        Log::error("The bit-flag used to distinguish between keys and buttons "
                   "is colliding with flagged bits in keycodes. "
                   "(kBUTTON_FLAG=%x, keyCode=%x)", kBUTTON_FLAG, keyCode);
    }

    if (!isKeyDown(keyCode)) {
        _freshSet.insert(keyCode);
        _keySet.insert(keyCode);
        return true;
    }

    return false;
}

bool InputState::keyReleased(SDL_Keycode keyCode) {
    if (isKeyDown(keyCode)) {
        _freshSet.erase(keyCode);
        _keySet.erase(keyCode);
        return true;
    }

    return false;
}

void InputState::mouseMoved(int x, int y, int xrel, int yrel) {
    _mousePos = Vec2(x, y);
    _mouseRel += Vec2(xrel, yrel);
}

bool InputState::buttonPressed(Uint8 button) {
    unsigned ubtn = button | kBUTTON_FLAG;

    if (!isButtonDown(button)) {
        _freshSet.insert(ubtn);
        _keySet.insert(ubtn);
        return true;
    }

    return false;
}

bool InputState::buttonReleased(Uint8 button) {
    unsigned ubtn = button | kBUTTON_FLAG;

    if (isButtonDown(button)) {
        _keySet.erase(ubtn);
        _freshSet.erase(ubtn);
        return true;
    }

    return false;
}

bool InputState::isKeyDown(SDL_Keycode keyCode) const {
    return (_keySet.count(keyCode) != 0);
}

bool InputState::isKeyFresh(SDL_Keycode keyCode) const {
    return (_freshSet.count(keyCode) != 0);
}

bool InputState::isButtonDown(Uint8 button) const {
    unsigned ubtn = button | kBUTTON_FLAG;
    return (_keySet.count(ubtn) != 0);
}

bool InputState::isButtonFresh(Uint8 button) const {
    unsigned ubtn = button | kBUTTON_FLAG;
    return (_freshSet.count(ubtn) != 0);
}

Vec2 InputState::getMousePosition() const {
    return _mousePos;
}

Vec2 InputState::getMouseRelative() const {
    return _mouseRel;
}
