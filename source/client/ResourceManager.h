/**
 * @file    source/client/ResourceManager.h
 *
 * @brief   Declares the resource manager class.
 */

#pragma once 

#include "Engine.h"
#include "ClientServiceLocator.h"
#include "SDLHeaders.h"
#include "Renderable.h"

#include <FreeImagePlus.h>

#include <map>
#include <vector>
#include <future>
#include <mutex>

class GraphicsSystem;
class ResourceManager;
class Resource;
class Texture;
class Audio;
class Font;

enum class ResourceType {
    UNDEFINED,
    TEXTURE,
    AUDIO,
    FONT,
};

ResourceId hashResource(std::string resourceName);

/**
 * @class   ResourceManager
 *
 * @brief   The resource manager is responsible for loading resources when they 
 *          are needed and destroying them when they are not. 
 *
 * The actual resources are stored internally in the ResourceManager, in a set
 * of ResourceManager::ResourceContainer objects. The 'Resource' class in turn 
 * holds a reference to this container.
 *
 * Resources are identified by their asset-file path. The paths are NOT safe
 * against path trickery. In the following scenario, the same texture would
 * be loaded twice:
 *      loadTexture("images/tex.png");
 *      loadTexture("images/../images/tex.png");
 *
 * Therefore, be cautious to address the resources in the SHORTEST POSSIBLE
 * MANNER. Capitalization is taken into account under most Unix systems, so be
 * careful to address the file correctly.
 */
class ResourceManager : public Service {
public:
    class ResourceContainer;
    class TextureContainer;
    class AudioContainer;
    class FontContainer;

    class ResourceContainer {
    public:
        explicit ResourceContainer(ResourceType type);
        virtual ~ResourceContainer();
        
        /**
         * Attempt to load a resource. This method can only be called once per
         * instance. If the method fails once, it will fail every subsequent 
         * call. 
         */ 
        virtual bool load(const std::string &asset);

        void retain();
        void release();
        int getRetainCount() const;
        ResourceId getResourceId() const;
        ResourceType getResourceType() const;
        std::string getAssetPath() const;

    private:

        int _retainCount;
        ResourceId _id;
        ResourceType _type;
        bool _loaded;
        std::string _assetPath;
    };

    class TextureContainer : public ResourceContainer {
        friend class GraphicsSystem;
        friend class ResourceManager::FontContainer;
    public:
        TextureContainer();
        ~TextureContainer();

        virtual bool load(const std::string &asset);

        GLuint getTextureRefrence() const;

        /**
         * @fn  int TextureContainer::getTextureUnit();
         *
         * @brief   Gets texture unit. if this texture is not active it will be set active in the first
         *          open or the oldest slot, deactivation any previous textures in that slot.
         *
         * @return  The texture index.
         */

        int getTextureUnit();
        std::pair<int, int> getSize() const;

    private:

        /**
         * Attempt to load a texture. 
         * @return          True on success, false on error.
         */
        bool loadTexture(const std::string& asset);

        /**
         * @fn  bool TextureContainer::generateTexture();
         *
         * @brief   Generates a blank texture.
         *
         * @return  true if it succeeds, false if it fails.
         */
        bool generateTexture(unsigned int width, unsigned int height);

        /**
         * @fn  bool TextureContainer::uploadTexture();
         *
         * @brief   Uploads the texture to the gfx card. If the upload succeeds, _data.tex is set to a
         *          valid OpenGL texture.
         *
         * @return  true if it succeeds, false if it fails.
         */

        bool uploadTexture();

        fipImage* getImageRef();

        fipImage* _image = nullptr;
        GLenum _texFormat;
        GLenum _internalFormat;
        GLuint _texRef;
    
        bool _uploaded = false;
        bool _hasID = false;
        int _texUnit = -1;
    };

    class AudioContainer : public ResourceContainer {
    public:
        AudioContainer();
        ~AudioContainer();

        virtual bool load(const std::string& asset);

    private:
 

        /**
        * Attempt to load a sound file.
        * @return          True on success, false on error.
        */
        bool loadAudio(const std::string& asset);
    };

    class FontContainer : public ResourceContainer {
        friend class ResourceManager::TextureContainer;
    public:
        FontContainer();
        ~FontContainer();

        virtual bool load(const std::string& asset);

        Drawable::DrawableData textToImage(const std::string& text, unsigned size);

    private:

        typedef size_t StringHash;

        bool loadFont(const std::string& asset);
        Drawable::DrawableData storeTextRender(fipImage* textImage);
        void generateNextStorageImage();

        std::string _fontName;
        size_t _currentImage = 0;
        unsigned _currentImageX = 0;
        unsigned _currentImageY = 0;

        unsigned _previousTextImageW = 0;
        unsigned _previousTextImageH = 0;

        std::vector<TextureContainer*> _textImageList;
        std::map<unsigned, TTF_Font*> _fontSizeMap;
        std::map<std::pair<size_t, unsigned int>, Drawable::DrawableData> _textImageMap;
    };

    ResourceManager();
    ~ResourceManager();

    /**
     * @fn  Resource* ResourceManager::getLoadResource(ResourceType type, const std::string &assetPath);
     *
     * @brief   Load a resource of the specified type.
     *
     * @param   type        The type of the resource to be loaded.
     * @param   assetPath   The asset path to the resource.
     *
     * @return  An initialized Resource on success, nullptr if the file does not exist or cannot be
     *          treated as the specified type. The returned object must be deleted by the caller.
     */

    Resource* getLoadResource(ResourceType type, const std::string &assetPath);

    /**
     * @fn  Resource* ResourceManager::getResource(ResourceType type, ResourceId id);
     *
     * @brief   Gets a resource of the specified type.
     *
     * @param   type    The type of the resource to get.
     * @param   id      The identifier to the resource.
     *
     * @return  An initialized Resource on success, nullptr if the id is nor loaded or cannot be
     *          treated as the specified type. The returned object must be deleted by the caller.
     */

    Resource* getResource(ResourceType type, ResourceId id);

    std::future<Resource*> queueResource (ResourceType type, const std::string &assetPath);

    /**
     * Check if a resource is loaded.
     * @return          True if the resource exists, false otherwise.
     */
    bool isResourceLoaded(std::string assetPath);

    /**
     * Delete resources which have a reference count <= 0.
     */
    void destroyReleasedResources();

    /**
     * @fn  void ResourceManager::insertResource(ResourceContainer* data);
     *
     * @brief   Inserts a resource described by data. Dangerous method, dirty hack.
     *
     * @param [in,out]  data    If non-null, the data.
     */

    void insertResource(ResourceContainer* data);

private:

    ResourceContainer* loadResource(ResourceType type, ResourceId id, const std::string &assetPath);

    

    std::recursive_mutex _dataGuard;
    std::map<ResourceId, ResourceContainer*> _resources;
};


/**
 * @class   Resource
 *
 * @brief   Resources are heavy media objects such as textures, fonts and
 *          sounds. The actual data is stored in the ResourceManager, but the
 *          Resource class holds a reference to it. All Resource-instances must
 *          be created by the ResourceManager.
 */
class Resource {
    friend class ResourceManager;

public:
    virtual ~Resource();

    int getRetainCount() const;

protected:
    explicit Resource(ResourceManager::ResourceContainer* container);

    ResourceManager::ResourceContainer* _container;
};

class Texture : public Resource {
    friend class ResourceManager;
    
public:
    GLuint getTextureId() const;
    std::pair<int, int> getSize() const;

    int getTextureUnit();

private:
    explicit Texture(ResourceManager::TextureContainer* container);
};

class Audio : public Resource {
    friend class ResourceManager;

public:

private:
    explicit Audio(ResourceManager::AudioContainer* container);
};

class Font : public Resource {
    friend class ResourceManager;

public:

    Drawable::DrawableData textToImage(const std::string& text, unsigned size);

private:
    explicit Font(ResourceManager::FontContainer* container);
};
