#include "TriggerComponent.h"
#include "CollisionFlags.h"
#include "SharedEvents.h"
#include "BodyCreator.h"
#include "ClientEvents.h"
#include "Map.h"
#include "ResourceManager.h"
#include "DrawableComponent.h"


/*
==================
TriggerComponent
==================
*/
TriggerComponent::TriggerComponent(b2World *world, MapTriggerProperties &props) 
    :   _world(world),
        _trigProps(props),
        _type(props.triggerType),
        _action(nullptr),
        _isPrompting(false) {
    _beginDelegate.bind(this, &TriggerComponent::onContactBegun);   
    _endDelegate.bind(this, &TriggerComponent::onContactEnd);   
    _keyPressDelegate.bind(this, &TriggerComponent::onKeyPress);

    EventManager *mgr = ServiceLocator::singleton()->getEventManager();
    mgr->addListener(_beginDelegate, ContactBegunEvent::eventType);
    mgr->addListener(_endDelegate, ContactEndedEvent::eventType);
    mgr->addListener(_keyPressDelegate, KeyEvent::eventType);

    _action = createTriggerAction();

    _spriteProvider = new SpriteProvider("UsePrompt");
    _spriteProvider->setSize({0.4f, 0.4f});
    _spriteProvider->setDepth(1.f);
    _spriteProvider->setPosition(props.position);
}

TriggerComponent::~TriggerComponent() {
    EventManager *mgr = ServiceLocator::singleton()->getEventManager();
    mgr->removeListener(_beginDelegate);
    mgr->removeListener(_endDelegate);
    mgr->removeListener(_keyPressDelegate);
    delete _spriteProvider;
}


void TriggerComponent::createTrigger() {
    if (getBody() != nullptr) {
        Log::info("TriggerComponent::createTrigger(): Body already exists.");
        return;
    }

    getGameObject()->setPosition(_trigProps.position);

    const unsigned flgMan = CollisionFlag::TRIG_MANUAL;
    const unsigned flgAuto = CollisionFlag::TRIG_AUTO;
    const bool automatic = _action->isAutomatic();

    BodyCreator bc(_world);
    bc.setPolygonalShape(_trigProps.vertices);
    bc.setCategoryBits(_action->isAutomatic() ? flgAuto : flgMan);
    bc.setMaskBits(_action->isAutomatic() ? flgAuto : flgMan);
    bc.setSensor(true);
    bc.setPosition(_trigProps.position);
    createBody(bc);

    if (getBody()) {
        getBody()->SetTransform(_trigProps.position, 0.f);
    }
}


void TriggerComponent::onContactBegun(const EventData *e) {
    ContactEndedEvent *evt = (ContactEndedEvent*)e;
    if (!evt->isRelevant(getBody()))
        return;

    GameObject *gob;
    Player *player;

    gob = getGameObjectNotMe(evt->getFixtureA(), evt->getFixtureB());
    player = dynamic_cast<Player*>(gob);

    if (player) {
        if (!_contacts.count(player))
            _contacts[player] = 0;

        _contacts[player]++;

        if (_contacts[player] == 1) {
            if (_action->willActOnPlayer(player)) {
                if (!_action->isAutomatic()) {
                    showPrompt();
                }

                _action->onPlayerEnter(player);
            }
        }
    }
}

void TriggerComponent::onContactEnd(const EventData *e) {
    ContactEndedEvent *evt = (ContactEndedEvent*)e;
    if (!evt->isRelevant(getBody()))
        return;

    GameObject *gob;
    Player *player;

    gob = getGameObjectNotMe(evt->getFixtureA(), evt->getFixtureB());
    player = dynamic_cast<Player*>(gob);

    if (player) {
        if (!_contacts.count(player)) {
            return;
        }

        _contacts[player]--;

        if (_contacts[player] <= 0) {
            hidePrompt();

            if (_action->willActOnPlayer(player)) {
                _action->onPlayerExit(player);
            }
        }
    }
}

void TriggerComponent::onKeyPress(const EventData *e) {
    KeyEvent *evt = (KeyEvent*)e;
    if (_action->isAutomatic())
        return;

    if (evt->getKeyCode() != SDLK_e || !evt->isKeyDown())
        return;

    if (!_isPrompting)
        return;

    for (auto pair : _contacts) {
        // Ignore non-touching players
        if (pair.second <= 0) {
            continue;
        }

        if (_action->willActOnPlayer(pair.first)) {
            _action->onManualActivation(pair.first);
            hidePrompt();
            break;
        }
    }
}


void TriggerComponent::showPrompt() {
    addComponent<DrawableComponent>(getGameObject(), _spriteProvider, false);
    _isPrompting = true;
}

void TriggerComponent::hidePrompt() {
    removeComponent<DrawableComponent>(getGameObject());
    _isPrompting = false;
}


b2Fixture* TriggerComponent::getFixtureNotMe(b2Fixture *fixA, b2Fixture *fixB) const {
    b2Body *body = getBody();
    if (!body)
        return nullptr;

    b2Fixture *me = body->GetFixtureList();

    while (me) {
        if (fixA == me)
            return fixB;
        if (fixB == me)
            return fixA;
        me = me->GetNext();
    }

    return nullptr;
}

GameObject* TriggerComponent::getGameObjectNotMe(b2Fixture *fixA, b2Fixture *fixB) const {
    b2Fixture *fix = getFixtureNotMe(fixA, fixB);
    if (!fix)
        return nullptr;

    void *udata = fix->GetUserData();
    if (!udata)
        return nullptr;

    return (GameObject*)udata;
}

TriggerAction* TriggerComponent::createTriggerAction() {
    switch (_type) {
        case TriggerType::PRINT:
            return new TriggerActionPrint(_trigProps);

        case TriggerType::PLAY_AMBIENT:
            return new TriggerActionPlayAmbient(_trigProps);

        case TriggerType::OBJECTIVE:
            return new TriggerActionObjective(_trigProps);

        default:
            break;
    }

    std::string action = _trigProps.attributes["action"];
    Log::fatal("Trigger type '%s' has no defined TriggerAction");
    exit(-1); 
}




/*
==================
TriggerAction
==================
*/
TriggerAction::TriggerAction(MapTriggerProperties &props)
    :   _attributes(props.attributes) { }

std::string TriggerAction::getAttribute(std::string key) const {
    if (_attributes.count(key)) {
        return _attributes.at(key);
    }

    return "";
}



/*
==================
TriggerActionPrint
==================
*/
TriggerActionPrint::TriggerActionPrint(MapTriggerProperties props)
    :   TriggerAction(props) { }

bool TriggerActionPrint::willActOnPlayer(Player *player) {
    return (player->getPlayerType() == PlayerType::LOCAL);
}

bool TriggerActionPrint::isAutomatic() {
    return true;
}

void TriggerActionPrint::onPlayerEnter(Player *player) {
    Log::debug("TriggerActionPrint: %s", getAttribute("message").c_str());
}



/*
==================
TriggerActionPlayAmbient
==================
*/
TriggerActionPlayAmbient::TriggerActionPlayAmbient(MapTriggerProperties props)
    :   TriggerAction(props) { 
    static int soundId = 0x1e9535de;
    _soundId = (soundId++);
}

bool TriggerActionPlayAmbient::willActOnPlayer(Player *player) {
    return (player->getPlayerType() == PlayerType::LOCAL);
}

bool TriggerActionPlayAmbient::isAutomatic() {
    return true;
}

void TriggerActionPlayAmbient::onPlayerEnter(Player *player) {
    std::string track = getAttribute("track").c_str();
    ResourceId rid = hashResource(track);

    Log::debug("Playing ambient track %s", track.c_str());

    EventManager *emgr = ServiceLocator::singleton()->getEventManager();

    EventData *e = new CreateSoundEvent(rid, _soundId, true);

    emgr->queueEvent(e);
}

void TriggerActionPlayAmbient::onPlayerExit(Player *player) {
    EventData *e = new DestroyAffectableSoundEvent(_soundId);
    EventManager *emgr = ServiceLocator::singleton()->getEventManager();
    emgr->queueEvent(e);
}



/*
==================
TriggerActionObjective
==================
*/
TriggerActionObjective::TriggerActionObjective(MapTriggerProperties props)
    :   TriggerAction(props),
        _state(ObjectiveState::AVAILABLE),
        _hackingPlayerId(0) { 
    static int alertId = 0xa63e166b;

    _alertSoundId = (alertId++);
    
    std::string id = getAttribute("id");

    if (id == "a") {
        _objectiveId = ObjectiveId::A;
    } else if (id == "b") {
        _objectiveId = ObjectiveId::B;
    } else if (id == "c") {
        _objectiveId = ObjectiveId::C;
    } else {
        Log::fatal("Invalid ID assigned to TriggerActionObjective: %s. "
                   "Must be 'a', 'b', or 'c'. RTFM.", id.c_str());
        exit(-1);
    }

    _objStateDelegate.bind(this, &TriggerActionObjective::onObjectiveStateChanged);
    _playerAreaDelegate.bind(this, &TriggerActionObjective::onPlayerAreaTransition);

    EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();
    emgr->addListener(_objStateDelegate, ObjectiveStateChangedEvent::eventType);
    emgr->addListener(_playerAreaDelegate, PlayerAreaTransitionEvent::eventType);
}

TriggerActionObjective::~TriggerActionObjective() {
    EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();
    emgr->removeListener(_objStateDelegate);
    emgr->removeListener(_playerAreaDelegate);
}

bool TriggerActionObjective::willActOnPlayer(Player *player) {
    if (player->getPlayerType() != PlayerType::LOCAL) {
        return false;
    }

    if (_state == ObjectiveState::AVAILABLE) {
        return (player->getTeam() == Team::SPY);
    }

    return false;
}

bool TriggerActionObjective::isAutomatic() {
    return false;
}

void TriggerActionObjective::onManualActivation(Player *player) {
    EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();

    ObjectiveInitiatedEvent *evt = new ObjectiveInitiatedEvent(player->getPlayerId(), 
                                                               player->getTeam(), 
                                                               _objectiveId);
    emgr->queueEvent(evt);
}

void TriggerActionObjective::onObjectiveStateChanged(const EventData *e) {
    ObjectiveStateChangedEvent *evt = (ObjectiveStateChangedEvent*)e;

    ObjectiveState state = evt->getObjectiveState();

    if (evt->getObjectiveId() == _objectiveId) {
        if (state == ObjectiveState::TRIGGERED) {
            _hackingPlayerId = evt->getPlayerId();

            playAlert();
        } else {
            _hackingPlayerId = 0;
            stopAlert();
        }

        _state = state;

        char obj = (static_cast<char>(_objectiveId) - 1) + 'A';
        switch (_state) {
            case ObjectiveState::AVAILABLE:
                Log::debug("Objective %c hack cancelled", obj);
                break;
            case ObjectiveState::TRIGGERED:
                Log::debug("Objective %c hack started", obj);
                break;
            case ObjectiveState::HACKED:
                Log::debug("Objective %c hack finished", obj);
                break;
        }

    } else if (_state != ObjectiveState::HACKED) {
        if (state == ObjectiveState::TRIGGERED) {
            _state = ObjectiveState::DISABLED;
        } else {
            _state = ObjectiveState::AVAILABLE;
        }
    }
}

void TriggerActionObjective::onPlayerAreaTransition(const EventData *e) {
    PlayerAreaTransitionEvent *evt = (PlayerAreaTransitionEvent*)e;

    if (_state != ObjectiveState::TRIGGERED) {
        return;
    }

    if (evt->getPlayerId() == _hackingPlayerId) {
        EventManager *mgr = ClientServiceLocator::singleton()->getEventManager();
        mgr->queueEvent(new ObjectiveStateChangedEvent(_objectiveId,
                                                       ObjectiveState::AVAILABLE,
                                                       _hackingPlayerId,
                                                       NetworkDirection::NET_OUT));
    }
}

void TriggerActionObjective::playAlert() {
    ResourceId rid = hashResource("audio/alert.ogg");
    EventManager *emgr = ServiceLocator::singleton()->getEventManager();
    EventData *e = new CreateSoundEvent(rid, _alertSoundId, true);
    emgr->queueEvent(e);
}

void TriggerActionObjective::stopAlert() {
    EventData *e = new DestroyAffectableSoundEvent(_alertSoundId);
    EventManager *emgr = ServiceLocator::singleton()->getEventManager();
    emgr->queueEvent(e);
}
