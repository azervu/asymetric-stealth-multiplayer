/**
 * @file    source/client/Client.cpp
 *
 * @brief   Implements the game class.
 */
#include "Client.h"

#include "ClientServiceLocator.h"
#include "PathTranslator.h"
#include "Camera.h"
#include "ResourceManager.h"
#include "Protocol.h"
#include "EventManager.h"
#include "ChatMessageService.h"
#include "GraphicsSystem.h"
#include "SoundSystem.h"
#include "InputSystem.h"
#include "NetworkSystem.h"
#include "ClientGameScene.h"
#include "MainMenuScene.h"
#include "MainLoopTimer.h"
#include "ClientEvents.h"
#include "Window.h"
#include "SpriteManager.h"
#include "Timer.h"

Client::Client() 
    :   _inputSystem(nullptr),
        _continueRunning(true) {
}

Client::~Client() {
    delete _graphicsSystem;

    while (_sceneStack.size()) {
        delete _sceneStack.top();
        _sceneStack.pop();
    }

    for (Subsystem *sub : _subsystems) {
        delete sub;
    }

    ClientServiceLocator::singleton()->destroyServices();
}

int Client::run(Settings* settings) {
    if (!init(settings)) {
        Log::fatal("Client initialization failed.");
        return -1;
    }

    std::string server = settings->getString("server", "localhost");

    // Instantly attempt to connect to the server
    ClientServiceLocator::singleton()->getEventManager()->queueEvent(
        new InitiateConnectionEvent("I_R_USER;WHO_R_U;", server)
    );
    
    mainLoop();

    return 0;
}

void Client::mainLoop() {
    MainLoopTimer timer;
    Scene::State runningScene = _sceneStack.top()->getState();

    Timer fpsTimer;
    int frames = 0;
    DeltaTime timeStep = MIN_TIME_STEP;

    while ( _continueRunning ) {
        handleSDLEvents();

        for (Subsystem *sub : _subsystems) {
            sub->update(timeStep);
        }

        ClientServiceLocator::singleton()->getEventManager()->update();
        _sceneStack.top()->updateGameObjects(timeStep);

        ClientServiceLocator::singleton()->getEventManager()->update();
        _sceneStack.top()->logicUpdate(timeStep);

        _sceneStack.top()->fillGraphicsBuffer();

        _graphicsSystem->entityUpdate(timeStep, _sceneStack.top()->getRenderVector());

        timeStep = timer.getTimeStep();

        runningScene = _sceneStack.top()->getState();

		switch (runningScene) {
		    case Scene::State::SET_CHILD_ACTIVE:
                _sceneStack.push(_sceneStack.top()->getNextScene());
            break;

		    case Scene::State::RETURN_TO_PARENT:
                _sceneStack.pop();

                if (_sceneStack.empty()) {
                    _continueRunning = false;
                } else {
                    _sceneStack.top()->onResume();
                }
            break;

		    case Scene::State::EXIT_APPLICATION:
                _continueRunning = false;
            break;

            default:
                // continue as normal
            break;
        }

        frames++;
        if (fpsTimer.getElapsedSeconds() >= 1.f) {
            Log::info("%i logic ticks/s", frames);
            fpsTimer.start();
            frames = 0;
        }
    }
}

void Client::handleSDLEvents() {
    if (_inputSystem != nullptr) {
        _inputSystem->invalidateOldState();
    }

    SDL_Event evt;
    while (SDL_PollEvent(&evt)) {
        switch (evt.type) {
        case SDL_QUIT:
            _continueRunning = false;
            Log::info("SDL_QUIT: Exiting");
        break;

        case SDL_WINDOWEVENT:
            if (evt.window.event == SDL_WINDOWEVENT_RESIZED) {
                int w = evt.window.data1;
                int h = evt.window.data2;
                Log::debug("Window resized to %i %i", w, h);

                EventManager *emgr = ServiceLocator::singleton()->getEventManager();
                emgr->queueEvent(new WindowResizedEvent(std::pair<int, int>(w, h)));
            }
        break;

        default:
            if (_inputSystem != nullptr) {
                _inputSystem->handleSDLEvent(evt);
            }
        break;
        }
    }
}

bool Client::init(Settings* settings) {
    Log::info("Initializing services...");
    if (!initServices(settings)) {
        Log::fatal("Service initialization failed");
        return false;
    }
    Log::info("Initialized services successfully");

    Log::info("Initializing subsystems..");
    if (!initSubsystems(settings)) {
        Log::fatal("Subsystem initialization failed");
        return false;
    }
    Log::info("Initialized subsystems successfully");

    Log::info("Initializing preloading");

    _preloader = new Preloader();
    std::string preloadingFile = settings->getString(PRELOADING_FILE_NAME_KEY, PRELOADING_DEFAULT_FILE_NAME);
    auto preloadingFuture = _preloader->startLoading(preloadingFile);

    Log::info("Initializing gamestate...");

    Scene *scene = new MainMenuScene();

    if (!scene->loadScene()) {
        Log::fatal("Initial scene initialization failed");
        return false;
    }
    _sceneStack.push(scene);
    Log::info("Initialized gamestate successfully");

    Log::info("Mapping event callbacks...");
    Log::info("Mapped event callbacks successfully");

    Log::info("Initialization successful");

    // Fire an event to notify everyone we are done with the init sequence.
    EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();
    emgr->queueEvent(new ClientInitiationCompleteEvent());

    Log::info("Waiting for preloader to finish");
    bool preloadingResult = preloadingFuture.get();
    if (preloadingResult) {
        Log::info("Preloading finished");
    } else {
        Log::info("Preloading failed");
    }

    return preloadingResult;
}

bool Client::initServices(Settings* settings) {
    ClientServiceLocator* serviceLocator = ClientServiceLocator::singleton();

    PathTranslator* pathTranslator = new PathTranslator(settings);
    if (!pathTranslator->addAssetDirectory("assets")) {
        return false;
    }
    serviceLocator->provide(pathTranslator);

    ResourceManager* resourceManager = new ResourceManager();
    resourceManager->init(settings);
    serviceLocator->provide(resourceManager);

    PacketFactory* pktFactory = new PacketFactory(pathTranslator);
    pktFactory->init(settings);
    serviceLocator->provide(pktFactory);

    EventManager* evtMgr = new EventManager();
    evtMgr->init(settings);
    serviceLocator->provide(evtMgr);

    ChatMessageService* chatService = new ChatMessageService();
    chatService->init(settings);
    serviceLocator->provide(chatService);

    Window* window = new Window();
    window->setTitle("Sneaky Sneaky Bang Bang");
    window->init(settings);
    serviceLocator->provide(window);

    // Camera depends on Window and EventManager.
    Camera* camera = new Camera();
    camera->init(settings);
    serviceLocator->provide(camera);

    SpriteManager::Manager* spriteManager = new SpriteManager::Manager();
    spriteManager->init(settings);
    serviceLocator->provide(spriteManager);

    return true;
}

bool Client::initSubsystems(Settings* settings) {
    _graphicsSystem = new GraphicsSystem();

    NetworkSystem *net = new NetworkSystem();
    _subsystems.push_back(net);

    SoundSystem *sound = new SoundSystem();
    _subsystems.push_back(sound);

    _inputSystem = new InputSystem();
    _subsystems.push_back(_inputSystem);

    for (Subsystem *sub : _subsystems) {
        if (!sub->init(settings)) {
            return false;
        }
    }
    
    return _graphicsSystem->init(settings);
}
