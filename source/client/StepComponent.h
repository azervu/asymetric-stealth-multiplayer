#pragma once

#include "Engine.h"
#include "GameObject.h"

class Map;

/**
 * StepComponent keeps track of the position of the GameObject, and plays
 * sound effects as the position changes. Speed is also taken into account
 * in some cases.
 */
class StepComponent : public Component {
public:
    StepComponent(const Map *map, Team playerTeam);

    void update(DeltaTime dt);

private:
    const Map *_map;
    Team _team;

    Vec2 _lastPos;
    float _length;

    ResourceId _defaultSound;
};
