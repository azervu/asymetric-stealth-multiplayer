/**
 * @file    source/client/main.cpp
 *
 * @brief   Implements the main class for the client.
 */

#include "Client.h"
#include "Log.h"
#include "Settings.h"
#include "Protocol.h"
#include "SDLHeaders.h"
#include "PathTranslator.h"

#ifndef ASM_UNITTEST

int main(int argc, char **argv)  {
    Settings* settings = new Settings(argc, (const char**) argv);
    Log::setSettings(settings);

    /* Initialize everything but VIDEO - video must be initialized by
     * the graphics thread.
     */
    if (SDL_Init(SDL_INIT_EVERYTHING & (~SDL_INIT_VIDEO)) < 0) {
        Log::error("SDL_Init() failed: %s", SDL_GetError());
        return -1;
    }

    if (SDLNet_Init() < 0) {
        Log::error("SDLNet_Init() failed: %s", SDLNet_GetError());
        return -1;
    }

    // The Client must be explicitly deallocated before SDL_Quit.
    Client *client = new Client();
    int ret = client->run(settings);
    delete client;

    SDL_Quit();
    SDLNet_Quit();
    return ret;
}

#endif //ASM_UNITTEST
