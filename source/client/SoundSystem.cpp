/**
* @file    source\client\SoundSystem.cpp
*
* @brief   Implements the sound system class.
*/

#include "SoundSystem.h"
#include "PathTranslator.h"
#include "ResourceManager.h"
#include "ClientServiceLocator.h"
#include "Camera.h"
#include <irrKlang.h>
using namespace irrklang;

SoundData::SoundData(irrklang::ISound* sound, Vec2 position, bool tdSound, bool loop)
    :   _sound(sound),
        _position(position.x, position.y, 0.0f),
        _tdSound(tdSound),
        _looping(loop){}

SoundData::SoundData() {}

SoundSystem::SoundSystem() {
    _loadSoundEventDelegate.bind(this, &SoundSystem::onLoadSoundEvent);

    _affectSoundEventDelegate.bind(this, &SoundSystem::onAffectSoundEvent);
    _createSoundEventDelegate.bind(this, &SoundSystem::onCreateSoundEvent);
    _destroyAffectableSoundEventDelegate.bind(this, &SoundSystem::onDestroyAffectableSoundEvent);

    _soundEngine = createIrrKlangDevice();
    if (!_soundEngine) {
        Log::error("Sound engine cration failed.");
    }
}

SoundSystem::~SoundSystem() {
    EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();
    emgr->removeListener(_loadSoundEventDelegate);
    emgr->removeListener(_affectSoundEventDelegate);
    emgr->removeListener(_createSoundEventDelegate);
    emgr->removeListener(_destroyAffectableSoundEventDelegate);
    _soundEngine->drop();
    _soundEngine = NULL;
}

bool SoundSystem::init(Settings *) {
    EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();
    emgr->addListener(_loadSoundEventDelegate, LoadSoundEvent::eventType);
    emgr->addListener(_affectSoundEventDelegate, AffectSoundEvent::eventType);
    emgr->addListener(_createSoundEventDelegate, CreateSoundEvent::eventType);
    emgr->addListener(_destroyAffectableSoundEventDelegate, DestroyAffectableSoundEvent::eventType);
    return true;
}


void SoundSystem::update(DeltaTime) {
    Camera * cam = ClientServiceLocator::singleton()->getCamera();
    Vec2 cam2 = cam->getCenterPosition();
    _cameraPosition = vec3df(cam2.x, cam2.y, 2.0f);

    auto itA = _unaffectable3dSounds.begin();
    while (itA != _unaffectable3dSounds.end()) {
        itA->_sound->setPosition(itA->_position - _cameraPosition);
        if (itA->_sound->isFinished()) {
            irrklang::ISound* snd = itA->_sound;
            itA = _unaffectable3dSounds.erase(itA);
            snd->stop();
            snd->drop();
        } else {
            ++itA;
        }
    }
    
    auto itB = _affectableSoundInstances.begin();
    while (itB != _affectableSoundInstances.end()) {
        if (itB->second._tdSound) {
            itB->second._sound->setPosition(itB->second._position - _cameraPosition);
        }
        if (itB->second._looping && itB->second._sound->isFinished()) {
            irrklang::ISound* snd = itB->second._sound;
            itB = _affectableSoundInstances.erase(itB);
            snd->stop();
            snd->drop();

        } else {
            itB++;
        }
    }
}

void SoundSystem::onAffectSoundEvent(const EventData *e) {
    AffectSoundEvent* soundEvent = (AffectSoundEvent*) e;
    auto it = _affectableSoundInstances.find(soundEvent->getSoundInstanceId());
    if (it == _affectableSoundInstances.end()) {
        Log::warning("SoundInstanceId does not exist, cant affect failed.");
        return;
    }
    it->second._position.X = soundEvent->getPosition().x;
    it->second._position.Y = soundEvent->getPosition().y;
}

void SoundSystem::onDestroyAffectableSoundEvent(const EventData *e) {
    DestroyAffectableSoundEvent* soundEvent = (DestroyAffectableSoundEvent*) e;

    auto it = _affectableSoundInstances.find(soundEvent->getSoundInstanceId());
    if (it == _affectableSoundInstances.end()) {
        Log::warning("SoundInstanceId does not exist, destruction failed.");
        return;
    }
    irrklang::ISound* snd = it->second._sound;
    _affectableSoundInstances.erase(it);
    snd->stop();
    snd->drop();
    return;
}

void SoundSystem::onCreateSoundEvent(const EventData *e) {
    CreateSoundEvent* soundEvent = (CreateSoundEvent*) e;
    ResourceId soundId = soundEvent->getSoundId();
    ISoundSource* sound = _sounds[soundId];
    if (sound == nullptr) {
        Log::error("Sound with resource ID %i not found. "
            "Did you remember to load it?", soundId);
        return;
    }

    if (!soundEvent->getAffectable() && !soundEvent->getTdSound()) {
        _soundEngine->play2D(sound);
        return;
    }

    if (soundEvent->getAffectable() && _affectableSoundInstances.find(soundEvent->getSoundInstanceId()) != _affectableSoundInstances.end()) {
        Log::error("SoundInstanceId already in use, creation failed.");
        return;
    }

    ISound* iSnd;
    if (soundEvent->getTdSound()) {
        iSnd = _soundEngine->play3D(sound, vec3df(soundEvent->getPosition().x, soundEvent->getPosition().y, 0.0f) - _cameraPosition, soundEvent->getLoop(), false, true);
    } else {
        iSnd = _soundEngine->play2D(sound, soundEvent->getLoop(), false, true);
    }

    if (soundEvent->getAffectable()) {
        _affectableSoundInstances[soundEvent->getSoundInstanceId()] = SoundData(iSnd, soundEvent->getPosition(), true, soundEvent->getLoop());
    } else {
        _unaffectable3dSounds.push_back(SoundData(iSnd, soundEvent->getPosition(), true, false));
    }
    

}

void SoundSystem::onLoadSoundEvent(const EventData *e) {

    PathTranslator *pt = ClientServiceLocator::singleton()->getPathTranslator();
    LoadSoundEvent *evt = (LoadSoundEvent*) e;

    std::string file = evt->getSoundFile();
    ResourceId resId = hashResource(file);

    // Retrieve the absolute path of the sound file and add it to the engine
    std::string path = pt->translatePath(file.c_str());
    ISoundSource* sound = _soundEngine->addSoundSourceFromFile(path.c_str());

    if (sound == nullptr) {
        Log::error("Unable to load sound '%s'", file.c_str());
    } else {
        // Refer to the resource by the hash of the file
        _sounds.insert(std::pair<ResourceId, ISoundSource*>(resId, sound));
    }
}