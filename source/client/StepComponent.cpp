#include "StepComponent.h"
#include "ClientEvents.h"
#include "ResourceManager.h"


static const float STEP_LENGTH = 1.f;


StepComponent::StepComponent(const Map *map, Team team) 
    :   _map(map),
        _team(team),
        _length(0.f) {
    if (team == Team::SPY) {
        _defaultSound = hashResource("audio/spy_slow.ogg");
    } else {
        _defaultSound = hashResource("audio/merc.ogg");
    }
}

void StepComponent::update(DeltaTime dt) {
    Vec2 pos = getGameObject()->getPosition();
    _length += (pos - _lastPos).Length();
    _lastPos = pos;

    if (_length >= STEP_LENGTH) {
        _length = 0.f;

        Vec2 pos = getGameObject()->getPosition();

        EventManager *emgr = ServiceLocator::singleton()->getEventManager();
        emgr->queueEvent(new CreateSoundEvent(_defaultSound, pos));
    }
}
