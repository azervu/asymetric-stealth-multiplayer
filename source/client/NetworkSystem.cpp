#include "NetworkSystem.h"
#include "ClientServiceLocator.h"
#include "EventManager.h"
#include "NetworkClient.h"
#include "ClientEvents.h"


NetworkSystem::NetworkSystem() {
    _initDelegate.bind(this, &NetworkSystem::onInitiateConnection);
    _shutdownDelegate.bind(this, &NetworkSystem::onServerShutdown);
    _clientReadyDelegate.bind(this, &NetworkSystem::onClientReadyToPlay);
    _levelLoadedDelegate.bind(this, &NetworkSystem::onLevelLoadingComplete);

    _networkClient = NULL;
}

NetworkSystem::~NetworkSystem() {
    EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();
    emgr->removeListener(_initDelegate);
    emgr->removeListener(_shutdownDelegate);
    emgr->removeListener(_clientReadyDelegate);
    emgr->removeListener(_levelLoadedDelegate);

    if (_networkClient) {
        delete _networkClient;
        _networkClient = nullptr;
    }
}

bool NetworkSystem::init(Settings* settings) {
    EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();
    emgr->addListener(_initDelegate, InitiateConnectionEvent::eventType);
    emgr->addListener(_shutdownDelegate, ServerShutdownEvent::eventType);
    emgr->addListener(_clientReadyDelegate, ClientReadyToPlayEvent::eventType);
    emgr->addListener(_levelLoadedDelegate, LevelLoadingCompleteEvent::eventType);

    return true;
}

void NetworkSystem::update(DeltaTime dt) {
    if (_networkClient != nullptr) {
        _networkClient->update();
    }
}


void NetworkSystem::disconnect() {
    Log::debug("Disconnecting...");
    if (_networkClient != nullptr) {
        delete _networkClient;
        _networkClient = nullptr;
    }

    EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();
    emgr->queueEvent(new DisconnectEvent());
}

void NetworkSystem::onInitiateConnection(const EventData *e) {
    Log::info("NetworkSystem::onInitiateConnection()");

    InitiateConnectionEvent *evt = (InitiateConnectionEvent*)e;

    if (_networkClient == nullptr) {
        _networkClient = new NetworkClient(evt->getUserName());
        if (!_networkClient->initiateConnection(evt->getHostName())) {
            Log::warning("Failed to connect");
            EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();
            emgr->queueEvent(new ConnectionFailedEvent);
            disconnect();
        }
    }
}

void NetworkSystem::onServerShutdown(const EventData *e) {
    Log::info("NetworkSystem::onServerShutdown()");
    disconnect();
}

void NetworkSystem::onClientReadyToPlay(const EventData *e) {
    Log::info("NetworkSystem::onReadyToPlay()");
    _networkClient->sendClientReadyPacket();
}

void NetworkSystem::onLevelLoadingComplete(const EventData *e) {
    Log::info("NetworkSystem::onLevelLoadingComplete()");
    _networkClient->sendLoadCompletePacket();
}

