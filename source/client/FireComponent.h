#pragma once

#include "GameObject.h"
#include "Engine.h"
#include "EventManager.h"
#include "Timer.h"


/**
 * FireComponent gives a GameObject the functionality of gunfire. The component
 * listens for a "left mouse button" input event, and reacts to it by firing
 * a GunFiredEvent.
 *
 * This component heavily relies on the GameObjects ability to rotate towards
 * the current mouse pointer position. 
 */
class FireComponent : public Component {
public:
    FireComponent(b2World *world);
    ~FireComponent();

    void update(DeltaTime dt);

private:
    b2World *_world;
    Timer _timer;
    bool _doFire;

    void fire();

    /* Event delegate for ButtonEvent
     */
    EventListenerDelegate _buttonDelegate;
    void onButtonClicked(const EventData *e);
};

