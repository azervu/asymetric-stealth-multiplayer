#pragma once

#include "Engine.h"
#include "GameObject.h"
#include "PhysicsComponent.h"
#include "EventManager.h"
#include "Map.h"
#include "ClientEvents.h"


class Player;
class DrawableComponent;


/**
 * Triggers work by utilizing the Command Pattern. Upon creating a new 
 * TriggerComponent, the correct TriggerAction is instantiated. The 
 * TriggerAction instance is further queried for more details on the
 * initialization of the TriggerComponent.
 *
 * Subclasses are only required to implement the initialization-defining
 * methods. 
 */
class TriggerAction {
public:
    TriggerAction(MapTriggerProperties &props);
    ~TriggerAction() { }

    virtual bool willActOnPlayer(Player *player) = 0;
    virtual bool isAutomatic() = 0;

    virtual void onPlayerEnter(Player *player) { };
    virtual void onManualActivation(Player *player) { };
    virtual void onPlayerExit(Player *player) { };

protected:
    std::string getAttribute(std::string key) const;

private:
    std::map<std::string,std::string> _attributes;
};

class TriggerActionPrint : public TriggerAction {
public:
    TriggerActionPrint(MapTriggerProperties p);

    virtual bool willActOnPlayer(Player *player) final;
    virtual bool isAutomatic() final;

    virtual void onPlayerEnter(Player *player) final;
};

class TriggerActionPlayAmbient : public TriggerAction {
public:
    TriggerActionPlayAmbient(MapTriggerProperties p);

    virtual bool willActOnPlayer(Player *player) final;
    virtual bool isAutomatic() final;

    virtual void onPlayerEnter(Player *player) final;
    virtual void onPlayerExit(Player *player) final;

private:
    int _soundId;
};

class TriggerActionObjective : public TriggerAction {
public:
    TriggerActionObjective(MapTriggerProperties p);
    ~TriggerActionObjective();

    virtual bool willActOnPlayer(Player *player) final;
    virtual bool isAutomatic() final;

    virtual void onManualActivation(Player *player) final;

private:
    ObjectiveState _state;
    ObjectiveId _objectiveId;
    PlayerId _hackingPlayerId;
    int _alertSoundId;

    EventListenerDelegate _objStateDelegate;
    void onObjectiveStateChanged(const EventData *e);

    EventListenerDelegate _playerAreaDelegate;
    void onPlayerAreaTransition(const EventData *e);

    void playAlert();
    void stopAlert();
};


/* TriggerComponent defines (a) an arbitrary polygonal area in the world, and
 * (b) a function to be executed when certain conditions are met. The 
 * TriggerAction class is responsible for deciding *WHEN* to react, and reacting
 * to one of the three events:
 *      1. A player entered the area
 *      2. A player left the area
 *      3. A player manually interacted with the area whilst inside it.
 *  
 * Number 3 is only available if the trigger action is MANUAL (i.e., not 
 * automatic), and is unavailable of the trigger action is AUTOMATIC.
 *
 * Manual triggers only react to the local player. Manual triggers display a 
 * prompt when the local player enters the area, and is "activated" when the
 * player clicks the "E" button. Do note that ALL triggers the player reside
 * in are activated upon "E" being clicked - be therefore extremely cautious to
 * never overlap two manual trigger areas.
 *
 * Triggers are defined in the map-file as according to the map format 
 * defition document.
 */
class TriggerComponent : public PhysicsComponent {
public:
    TriggerComponent(b2World *world, MapTriggerProperties &props);
    ~TriggerComponent();

    const std::vector<GameObject*>& getTouchingObjects() const;

    // The b2Body is not created until this method is called. If the body
    // already exists, nothing is done.
    void createTrigger();

private:
    b2World *_world;
    std::map<Player*,int> _contacts;
    MapTriggerProperties _trigProps;

    TriggerType _type;
    TriggerAction *_action;

    EventListenerDelegate _endDelegate;
    EventListenerDelegate _beginDelegate;
    EventListenerDelegate _keyPressDelegate;

    bool _isPrompting;
    SpriteProvider* _spriteProvider;

    void onContactBegun(const EventData *e);
    void onContactEnd(const EventData *e);
    void onKeyPress(const EventData *e);

    void showPrompt();
    void hidePrompt();

    b2Fixture* getFixtureNotMe(b2Fixture *fixA, b2Fixture *fixB) const;
    GameObject* getGameObjectNotMe(b2Fixture *fixA, b2Fixture *fixB) const;

    TriggerAction* createTriggerAction();
};
