#include "ClientServiceLocator.h"
#include "ResourceManager.h"
#include "PathTranslator.h"
#include "ServiceLocator.h"
#include "Window.h"
#include "Camera.h"

ClientServiceLocator* ClientServiceLocator::_clientSingleton = nullptr;


ClientServiceLocator* ClientServiceLocator::singleton() {
    if (!_clientSingleton) {
        _clientSingleton = new ClientServiceLocator();
        _singleton = _clientSingleton;
    }

    return _clientSingleton;
}

void ClientServiceLocator::provide(ResourceManager *manager) {
    provide(manager, (ServiceId) ClientServiceId::RESOURCEMANAGER);
}

ResourceManager* ClientServiceLocator::getResourceManager() {
    ClientServiceId id = ClientServiceId::RESOURCEMANAGER;
    Service *service = ServiceLocator::getService((ServiceId) id);
    return (ResourceManager*) service;
}

void ClientServiceLocator::provide(Camera *camera) {
    provide(camera, (ServiceId)ClientServiceId::CAMERA);
}

Camera* ClientServiceLocator::getCamera() {
    ClientServiceId id = ClientServiceId::CAMERA;
    Service *service = ServiceLocator::getService((ServiceId) id);
    return (Camera*) service;
}

void ClientServiceLocator::provide(Window *window) {
    provide(window, (ServiceId)ClientServiceId::WINDOW);
}

Window* ClientServiceLocator::getWindow() {
    ClientServiceId id = ClientServiceId::WINDOW;
    Service *service = ServiceLocator::getService((ServiceId)id);
    return (Window*)service;
}
