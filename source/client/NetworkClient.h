/**
 * @file    source/client/NetworkClient.h
 *
 * @brief   Declares the network client class.
 */

#pragma once

#include "Protocol.h"
#include "Conversation.h"
#include "Engine.h"
#include "NetworkInterface.h"
#include "EventManager.h"


class EventData;
class SocketTCP;
class SocketUDP;
class ClientJoinConversation;


/**
 * The connection to the server. All network communication currently goes
 * through this interface. The NetworkClient is told what to do via events.
 *
 * The NetworkClient should NOT need to listen to any events dispatched via
 * the EventManager. Instead, handle events in NetworkSystem and use the 
 * NetworkClients existing API (or create a new one). This will server NO other
 * purpose other than keeping (a) the NetworkSystem in control of major flow 
 * decisions and (b) clearly distinguishing responsibilities. 
 */
class NetworkClient : public NetworkInterface {
public:
    NetworkClient(std::string userName);
    ~NetworkClient();

    ProtocolState getProtocolState() const;
    PlayerId getPlayerId() const;
    Team getTeam() const;

    /**
     * Initiate a connection with a remote host running the game server. Must
     * be called on an instance in State::DISCONNECTED.
     * @return      True if the connection was successfully initiated and the
     *              joining process begun. False is returned if the instance
     *              already was in a connected/initiated state or an error 
     *              occurred.
     */
    bool initiateConnection(std::string hostname);

    /**
     * Update the NetworkClient based on the current state it is in. 
     * @return      True if the connection to the server is good, false 
     *              if the NetworkClient is disconnected.
     */
    bool update();
    bool updateJoining();
    bool updateInLobby();
    bool updateInGame();

    /**
     * Send a ClientReadyToPlay packet to the server. Only valid if the client 
     * is in state IN_LOBBY.
     * @return      True if the client is 'IN_LOBBY' and the packet was sent
     *              successfully, false otherwise.
     */
    bool sendClientReadyPacket();

    /**
     * Send a LoadComplete packet to the server. Only valid if the client is 
     * in state IN_LOBBY.
     * @return      True if the client is 'IN_LOBBY' and the packet was sent
     *              successfully, false otherwise.
     */ 
    bool sendLoadCompletePacket();

protected:
    /**
     * This method should be used by the class for firing events, so that the
     * class can be overriden in the test suite for analysis of fired events.
     * The method is only a wrapper around EventManager::queueEvent(...).
     */
    virtual void fireEvent(EventData *event);

private:
    /**
     * Reset the state of the NetworkClient to State::DISCONNECTED and revert
     * this object to a blank, neutral slate.
     */
    void reset();
    
    void fireConnectionFailedEvent();   

    /* PlayerTransformUpdateEvent callback method
     */
    EventListenerDelegate _playerTransDelegate;
    void onPlayerTransformOut(const EventData *e);

    /* ObjectiveInitiatedEvent callback method
     */
    EventListenerDelegate _objInitDelegate;
    void onObjectiveInitiated(const EventData *e);

    /* ObjectiveStateChangedEvent callback method
     */
    EventListenerDelegate _objStateDelegate;
    void onObjectiveStateChangedEvt(const EventData *e);

    /* GunFiredEvent callback method
     */
    EventListenerDelegate _gunFireDelegate;
    void onGunFiredEvent(const EventData *e);

    /* GrenadeThrowEvent callback method
     */
    EventListenerDelegate _gThrowDelegate;
    void onGrenadeThrow(const EventData *e);


    /*
     * Packet Callback Methods
     */
    void onClientJoined(const Packet *packet);
    void onClientLeft(const Packet *packet);
    void onServerShutdown(const Packet *packet);
    void onReceivedChatMessage(const Packet *packet);
    void onStartLoading(const Packet *packet);
    void onGameStarting(const Packet *packet);
    void onCreatePlayer(const Packet *packet);
    void onSpawnPlayer(const Packet *packet);
    void onGameOver(const Packet *packet);
    void onObjectiveStateChangedPkt(const Packet *packet);
    void onGunFiredPacket(const Packet *packet);
    void onBulletHit(const Packet *packet);
    void onGrenadeExplode(const Packet *packet);
    void onGrenadeHit(const Packet *packet);

    std::string _userName;
    std::string _hostName;
    SocketTCP *_tcp;
    SocketUDP *_udp;
    ProtocolState _protoState;
    PlayerId _playerId;
    Team _team;

    ClientJoinConversation *_joinConvo;
};


/**
 * The ClientJoinConversation initiates the entire network protocol. The client
 * begins by sending it's UDP listening port and it's username, and the server
 * responds by:
 *      a)  Accepting the client, creating a new UDP socket, assigned the new
 *          client a player ID and a team and sends this data to it
 *      b)  Declines the client
 */
class ClientJoinConversation : public Conversation {
public:
    ClientJoinConversation(std::string userName, unsigned clientPort);
    ~ClientJoinConversation();
    
    bool start(SocketBase *socket);
    bool update(SocketBase *socket);

    bool isDone();

    bool didSuccessfullyJoin() const;
    unsigned getServerUdpPort() const;
    PlayerId getPlayerId() const;
    Team getTeam() const;

private:
    std::string _userName;
    unsigned _clientUdpPort;

    Packet *_serverResponse;
    bool _didJoin;
    unsigned _serverUdpPort;
    PlayerId _playerId;
    Team _team;
};
