/**
 * @file    source/client/ClientServiceLocator.h
 *
 * @brief   Declares the client service locator class.
 */

#pragma once

#include "ServiceLocator.h"

class PathTranslator;
class ChatMessageService;
class ResourceManager;
class Window;
class Camera;

/**
 * Service locator used in the game client. Provices named methods for
 * commonly used game client services, hides the ServiceId interface and
 * implements the Singleton pattern.
 */
class ClientServiceLocator : public ServiceLocator {
public:
    using ServiceLocator::provide;

    /**
     * As defined in ServiceLocator.h, target specific services are required to
     * start IDs at TARGET_SPECIFIC_SERVICE_FIRST_ID.
     */
    enum class ClientServiceId : ServiceId {
        RESOURCEMANAGER = TARGET_SPECIFIC_SERVICE_FIRST_ID,
        WINDOW,
        CAMERA,
    };

    static ClientServiceLocator* singleton();

    void provide(ResourceManager* manager);
    ResourceManager* getResourceManager();

    void provide(Camera* camera);
    Camera* getCamera();

    void provide(Window* window);
    Window* getWindow();

private:
    static ClientServiceLocator *_clientSingleton;
};

