/**
 * @file    source/client/NetworkClient.cpp
 *
 * @brief   Implements the network client class.
 */

#include "NetworkClient.h"
#include "Socket.h"
#include "ClientServiceLocator.h"
#include "Engine.h"
#include "SharedEvents.h"
#include "ClientEvents.h"
#include "ChatMessageEvent.h"
#include "EventManager.h"
#include "ClientEvents.h"


NetworkClient::NetworkClient(std::string userName) 
    :   _tcp(nullptr),
        _udp(nullptr),
        _protoState(ProtocolState::DISCONNECTED),
        _userName(userName),
        _joinConvo(nullptr) {

    mapCallback("ClientJoined", 
                BIND_PACKET_CALLBACK(NetworkClient::onClientJoined));
    mapCallback("ClientLeft", 
                BIND_PACKET_CALLBACK(NetworkClient::onClientLeft));
    mapCallback("ServerShutdown", 
                BIND_PACKET_CALLBACK(NetworkClient::onServerShutdown));
    mapCallback("ChatMessage", 
                BIND_PACKET_CALLBACK(NetworkClient::onReceivedChatMessage));
    mapCallback("StartLoading", 
                BIND_PACKET_CALLBACK(NetworkClient::onStartLoading));
    mapCallback("GameStarting", 
                BIND_PACKET_CALLBACK(NetworkClient::onGameStarting));
    mapCallback("CreatePlayer", 
                BIND_PACKET_CALLBACK(NetworkClient::onCreatePlayer));
    mapCallback("SpawnPlayer",
                BIND_PACKET_CALLBACK(NetworkClient::onSpawnPlayer));
    mapCallback("GameOver",
                BIND_PACKET_CALLBACK(NetworkClient::onGameOver));
    mapCallback("ObjectiveStateChanged",
                BIND_PACKET_CALLBACK(NetworkClient::onObjectiveStateChangedPkt));
    mapCallback("GunFired",
                BIND_PACKET_CALLBACK(NetworkClient::onGunFiredPacket));
    mapCallback("BulletHit",
                BIND_PACKET_CALLBACK(NetworkClient::onBulletHit));
    mapCallback("GrenadeExplode",
                BIND_PACKET_CALLBACK(NetworkClient::onGrenadeExplode));
    mapCallback("GrenadeHit",
                BIND_PACKET_CALLBACK(NetworkClient::onGrenadeHit));


    EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();
    
    _playerTransDelegate.bind(this, &NetworkClient::onPlayerTransformOut);
    emgr->addListener(_playerTransDelegate, PlayerTransformUpdateEvent::eventType);

    _objInitDelegate.bind(this, &NetworkClient::onObjectiveInitiated);
    emgr->addListener(_objInitDelegate, ObjectiveInitiatedEvent::eventType);

    _gunFireDelegate.bind(this, &NetworkClient::onGunFiredEvent);
    emgr->addListener(_gunFireDelegate, ClientGunFiredEvent::eventType);

    _objStateDelegate.bind(this, &NetworkClient::onObjectiveStateChangedEvt);
    emgr->addListener(_objStateDelegate, ObjectiveStateChangedEvent::eventType);

    _gThrowDelegate.bind(this, &NetworkClient::onGrenadeThrow);
    emgr->addListener(_gThrowDelegate, GrenadeThrowEvent::eventType);
}

NetworkClient::~NetworkClient() {
    EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();
    emgr->removeListener(_playerTransDelegate);
    emgr->removeListener(_objInitDelegate);
    emgr->removeListener(_gunFireDelegate);
    emgr->removeListener(_objStateDelegate);
    emgr->removeListener(_gThrowDelegate);

    reset();
}

ProtocolState NetworkClient::getProtocolState() const {
    return _protoState;
}

PlayerId NetworkClient::getPlayerId() const {
    return _playerId;
}

Team NetworkClient::getTeam() const {
    return _team;
}

bool NetworkClient::initiateConnection(std::string hostname) {
    if (_protoState != ProtocolState::DISCONNECTED) {
        Log::error("initiateConnection called on previously initiated instance");
        return false;
    }

    _hostName = hostname;

    if (_tcp != nullptr) {
        Log::warning("Attempted to initialize already existing connection");
        fireConnectionFailedEvent();
        
        reset();
        return false;
    }

    _tcp = new SocketTCP(_hostName, TCP_SERVER_PORT);
    if (!_tcp->initSocket() || !_tcp->isGood()) {
        Log::error("NetworkClient: Bad TCP socket");
        fireConnectionFailedEvent();
        reset();
        return false;
    }

    setSocketTCP(_tcp);

    // This UDP socket temporary. It is used to bind a local listening port
    // and notify the server about it. The server then binds it's UDP socket
    // to a port and notifies the client. At that point, the _udp socket is 
    // recreated with a new and known destination port.
    _udp = new SocketUDP(_hostName, (Uint16) ~0, 0);
    if (!_udp->initSocket() || !_udp->isGood()) {
        Log::error("NetworkClient: Bad UDP socket");
        fireConnectionFailedEvent();
        reset();
        return false;
    }

    _joinConvo = new ClientJoinConversation(_userName, _udp->getListenPort());
    if (!_joinConvo->start(_tcp)) {
        Log::error("Failed to start ClientJoinConversation");
        fireConnectionFailedEvent();
        reset();
        return false;
    }

    _protoState = ProtocolState::JOINING;

    return true;
}

bool NetworkClient::update() {
    PacketFactory *pf = ClientServiceLocator::singleton()->getPacketFactory();

    if (_tcp && _tcp->hasActivity()) {
        if (!_tcp->parseTrafficData(pf) || !_tcp->isGood()) {
            Log::error("NetworkClient: Bad TCP socket");
            reset();
            return false;
        }
    }

    if (_udp && _udp->hasActivity()) {
        if (!_udp->parseTrafficData(pf) || !_udp->isGood()) {
            Log::error("NetworkClient: Bad UDP socket");
            reset();
            return false;
        }
    }

    if (!_udp->flushOutgoingPacketQueue())
        return false;
    if (!_tcp->flushOutgoingPacketQueue())
        return false;

    switch (_protoState) {
        case ProtocolState::JOINING:
            return updateJoining();

        case ProtocolState::IN_LOBBY:
            return updateInLobby();

        case ProtocolState::IN_GAME:
            return updateInGame();

        case ProtocolState::DISCONNECTED:
            return false;

        default:
            Log::error("Default case for NetworkClient reached");
            return false;
    }
}

bool NetworkClient::updateJoining() {
    _joinConvo->update(_tcp);

    if (_joinConvo->isDone()) {
        if (_joinConvo->didSuccessfullyJoin()) {
            _playerId = _joinConvo->getPlayerId();
            unsigned serverUdp = _joinConvo->getServerUdpPort();
            unsigned clientUdp = _udp->getListenPort();

            delete _udp;

            _udp = new SocketUDP(_hostName, serverUdp, clientUdp);
            if (!_udp->initSocket() || !_udp->isGood()) {
                Log::error("Unable to recreate UDP socket");
                reset();
                return false;
            } else {
                setSocketUDP(_udp);

                Log::debug("Server accepted the join request");
                Log::debug("New state: ProtocolState::IN_LOBBY");
                _protoState = ProtocolState::IN_LOBBY;

                _playerId = _joinConvo->getPlayerId();
                _team = _joinConvo->getTeam();

                delete _joinConvo;
                _joinConvo = nullptr;

                // Fire 'ConnectionEstablishedEvent'
                EventData *evt = new ConnectionEstablishedEvent(_playerId, _team);
                fireEvent(evt);

                // TODO
                // In the future (when we've got a running GUI), the user must
                // himself flag himself as 'ready' by clicking a button.
                // Until then, simulate this action by firing the event directly
                // after the connection is established.
                ClientReadyToPlayEvent *r2p = new ClientReadyToPlayEvent();       
                fireEvent(r2p);
            }
        } else {
            Log::error("Server reclined the join request");
            reset();
            return false;
        }
    }

    return true;
}

bool NetworkClient::updateInLobby() {
    PacketFactory *pf = ClientServiceLocator::singleton()->getPacketFactory();

    if (!_udp->parseTrafficData(pf))
        return false;
    if (!_tcp->parseTrafficData(pf)) 
        return false;

    if (!_udp->flushOutgoingPacketQueue())
        return false;
    if (!_tcp->flushOutgoingPacketQueue())
        return false;

    dispatchPacketQueue();
    return true;
}

bool NetworkClient::updateInGame() {
    PacketFactory *pf = ClientServiceLocator::singleton()->getPacketFactory();

    if (!_udp->parseTrafficData(pf))
        return false;
    if (!_tcp->parseTrafficData(pf)) 
        return false;

    if (!_udp->flushOutgoingPacketQueue())
        return false;
    if (!_tcp->flushOutgoingPacketQueue())
        return false;

    dispatchPacketQueue();
    return true;
}

bool NetworkClient::sendClientReadyPacket() {
    if (_protoState != ProtocolState::IN_LOBBY) {
        return false;
    }

    PacketFactory *pf = ClientServiceLocator::singleton()->getPacketFactory();
    Packet *pkt = pf->createPacketFromTemplate("ClientReadyToPlay");

    pkt->setUnsigned("playerId", _playerId);
    bool sent = _tcp->sendPacket(pkt);
       
    delete pkt;
    return sent;
}

bool NetworkClient::sendLoadCompletePacket() {
    if (_protoState != ProtocolState::IN_LOBBY) {
        return false;
    }

    PacketFactory *pf = ClientServiceLocator::singleton()->getPacketFactory();
    Packet *pkt = pf->createPacketFromTemplate("LoadComplete");

    bool sent = _tcp->sendPacket(pkt);
    delete pkt;
    return sent;
}

void NetworkClient::fireEvent(EventData *event) {
    ClientServiceLocator::singleton()->getEventManager()->queueEvent(event);
}

void NetworkClient::reset() {
    if (_tcp) {
        delete _tcp;
        _tcp = nullptr;
    }

    if (_udp) {
        delete _udp;
        _udp = nullptr;
    }

    setSocketTCP(nullptr);
    setSocketUDP(nullptr);

    if (_joinConvo) {
        delete _joinConvo;
        _joinConvo = nullptr;
    }

    if (_protoState != ProtocolState::DISCONNECTED) {
        Log::debug("NetworkClient reset to ProtocolState::DISCONNECTED");
    }

    _protoState = ProtocolState::DISCONNECTED;
    _hostName = "";
}

void NetworkClient::fireConnectionFailedEvent() {
    EventData *evt = new ConnectionFailedEvent();
    fireEvent(evt);
}


void NetworkClient::onPlayerTransformOut(const EventData *e) {
    PlayerTransformUpdateEvent *evt = (PlayerTransformUpdateEvent*)e;

    if (evt->getDirection() == NetworkDirection::NET_OUT 
        && evt->getPlayerId() == _playerId) 
    {
        PacketFactory *pf = ClientServiceLocator::singleton()->getPacketFactory();

        Packet *pkt = pf->createPacketFromTemplate("PlayerTransformUpdate");
        pkt->setUnsigned("playerId", evt->getPlayerId());
        pkt->setFloat("x", evt->getPosition().x);
        pkt->setFloat("y", evt->getPosition().y);
        pkt->setFloat("angVel", evt->getAngularVelocity());
        pkt->setFloat("rotation", evt->getRotation());
        pkt->setFloat("velX", evt->getVelocity().x);
        pkt->setFloat("velY", evt->getVelocity().y);
        pkt->setUnsigned("step", evt->getStep());

        _udp->queuePacket(pkt);
    }
}

void NetworkClient::onObjectiveInitiated(const EventData *e) {
    ObjectiveInitiatedEvent *evt = (ObjectiveInitiatedEvent*)e;

    PacketFactory *pf = ClientServiceLocator::singleton()->getPacketFactory();

    Packet *pkt = pf->createPacketFromTemplate("ObjectiveInitiated");
    pkt->setUnsigned("playerId", evt->getPlayerId());
    pkt->setByte("team", static_cast<byte>(evt->getTeam()));
    pkt->setByte("objectiveId", static_cast<byte>(evt->getObjectiveId()));

    _udp->queuePacket(pkt);
}

void NetworkClient::onGunFiredEvent(const EventData *e) {
    ClientGunFiredEvent *evt = (ClientGunFiredEvent*)e;

    if (evt->getDirection() != NetworkDirection::NET_OUT) {
        return;
    }

    PacketFactory *pf = ClientServiceLocator::singleton()->getPacketFactory();
    Packet *pkt = pf->createPacketFromTemplate("GunFired");
    pkt->setUnsigned("playerId", evt->getPlayerId());
    pkt->setFloat("gunX", evt->getGunPoint().x);
    pkt->setFloat("gunY", evt->getGunPoint().y);
    pkt->setFloat("endX", evt->getEndPoint().x);
    pkt->setFloat("endY", evt->getEndPoint().y);

    _udp->queuePacket(pkt);
}

void NetworkClient::onObjectiveStateChangedEvt(const EventData *e) {
    ObjectiveStateChangedEvent *evt = (ObjectiveStateChangedEvent*)e;

    if (evt->getNetworkDirection() != NetworkDirection::NET_OUT) {
        return;
    }

    PacketFactory *pf = ClientServiceLocator::singleton()->getPacketFactory();
    Packet *pkt = pf->createPacketFromTemplate("ObjectiveStateChanged");
    
    pkt->setByte("objectiveId", static_cast<byte>(evt->getObjectiveId()));
    pkt->setByte("state", static_cast<byte>(evt->getObjectiveState()));
    pkt->setUnsigned("playerId", evt->getPlayerId());

    _udp->queuePacket(pkt);
}

void NetworkClient::onGrenadeThrow(const EventData *e) {
    GrenadeThrowEvent *evt = (GrenadeThrowEvent*)e;

    if (evt->getNetworkDirection() != NetworkDirection::NET_OUT) {
        return;
    }

    PacketFactory *pf = ClientServiceLocator::singleton()->getPacketFactory();
    Packet *pkt = pf->createPacketFromTemplate("GrenadeThrow");

    pkt->setUnsigned("playerId", evt->getPlayerId());
    pkt->setFloat("rot", evt->getRotation());
    pkt->setFloat("posX", evt->getPosition().x);
    pkt->setFloat("posY", evt->getPosition().y);

    _udp->queuePacket(pkt);
}


void NetworkClient::onClientJoined(const Packet *packet) {
    ClientJoinedSessionEvent *evt = nullptr;
    std::string userName = packet->getString("userName");
    PlayerId id = packet->getUnsigned("playerId");
    Team team = static_cast<Team>(packet->getByte("team"));

    Log::debug("Client '%s' (%u) joined", userName.c_str(), id);

    evt = new ClientJoinedSessionEvent(userName, id, team);
    fireEvent(evt);
}

void NetworkClient::onClientLeft(const Packet *packet) {
    ClientLeftSessionEvent *evt = nullptr;
    std::string userName = packet->getString("userName");
    PlayerId id = packet->getUnsigned("playerId");

    Log::debug("Client '%s' (%u) disconnected", userName.c_str(), id);

    evt = new ClientLeftSessionEvent(userName, id);
    fireEvent(evt);
}

void NetworkClient::onServerShutdown(const Packet *packet) {
    std::string msg = packet->getString("message");

    ServerShutdownEvent *evt = new ServerShutdownEvent(msg);
    fireEvent(evt);

    Log::debug("Server shutting down (%s)", msg.c_str());  
}

void NetworkClient::onReceivedChatMessage(const Packet *packet) {
    MessageReceivedEvent *evt = new MessageReceivedEvent(
        packet->getString("userName"),
        packet->getString("message")
    );

    fireEvent(evt);
}

void NetworkClient::onStartLoading(const Packet *packet) {
    Log::info("NetworkClient: 'StartLoading'");

    EventData *evt = new StartLoadingLevelEvent(packet->getString("level"));
    fireEvent(evt);
}

void NetworkClient::onGameStarting(const Packet *packet) {
    Log::info("NetworkClient: 'GameStarting'");
    Log::debug("New state: ProtocolState::IN_GAME");

    _protoState = ProtocolState::IN_GAME;

    EventData *evt = new GameStartingEvent();
    fireEvent(evt);
}

void NetworkClient::onCreatePlayer(const Packet *packet) {
    std::string userName = packet->getString("userName");
    PlayerId id = packet->getUnsigned("playerId");
    Team team = static_cast<Team>(packet->getByte("team"));
    PlayerType type = static_cast<PlayerType>(packet->getByte("type"));

    EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();
    emgr->queueEvent(new CreatePlayerEvent(id, userName, type, team));
}

void NetworkClient::onSpawnPlayer(const Packet *packet) {
    PlayerId id = packet->getUnsigned("playerId");
    Vec2 pos(packet->getFloat("x"), packet->getFloat("y"));

    SpawnPlayerEvent *evt;
    evt = new SpawnPlayerEvent(id, pos);
    fireEvent(evt);
}

void NetworkClient::onGameOver(const Packet *packet) {
    Team team = (Team)packet->getByte("winningTeam");
    std::string reason = packet->getString("reason");

    GameOverEvent *evt = new GameOverEvent(team, reason);
    fireEvent(evt);
}

void NetworkClient::onObjectiveStateChangedPkt(const Packet *packet) {
    ObjectiveId objId = static_cast<ObjectiveId>(packet->getByte("objectiveId"));
    ObjectiveState state = static_cast<ObjectiveState>(packet->getByte("state"));
    PlayerId playerId = packet->getUnsigned("playerId");

    ObjectiveStateChangedEvent *evt;
    evt = new ObjectiveStateChangedEvent(objId, state, playerId, NetworkDirection::NET_IN);
    ClientServiceLocator::singleton()->getEventManager()->queueEvent(evt);
}

void NetworkClient::onGunFiredPacket(const Packet *packet) {
    PlayerId playerId = packet->getUnsigned("playerId");
    b2Vec2 gunPt(packet->getFloat("gunX"), packet->getFloat("gunY"));
    b2Vec2 endPt(packet->getFloat("endX"), packet->getFloat("endY"));

    ClientGunFiredEvent *evt;
    evt = new ClientGunFiredEvent(NetworkDirection::NET_IN,
                                  playerId, gunPt, endPt);
    ClientServiceLocator::singleton()->getEventManager()->queueEvent(evt);
}

void NetworkClient::onBulletHit(const Packet *packet) {
    PlayerId fireId = packet->getUnsigned("firePlayerId");
    PlayerId hitId = packet->getUnsigned("hitPlayerId");
    int health = packet->getInt("health");
    bool kill = packet->getByte("kill");

    BulletHitEvent *evt = new BulletHitEvent(fireId, hitId, health, kill);
    ClientServiceLocator::singleton()->getEventManager()->queueEvent(evt);
}

void NetworkClient::onGrenadeExplode(const Packet *pkt) {
    Vec2 pos(pkt->getFloat("posX"), pkt->getFloat("posY"));
    
    unsigned utype = pkt->getUnsigned("type");
    GrenadeType type = static_cast<GrenadeType>(utype);

    auto evt = new GrenadeExplodeEvent(pos, type);
    ClientServiceLocator::singleton()->getEventManager()->queueEvent(evt);
}

void NetworkClient::onGrenadeHit(const Packet *pkt) {
    PlayerId pid = pkt->getUnsigned("playerId");
    float hitFactor = pkt->getFloat("factor");
    GrenadeType type = static_cast<GrenadeType>(pkt->getUnsigned("type"));

    auto emgr = ClientServiceLocator::singleton()->getEventManager();
    emgr->queueEvent(new GrenadeHitEvent(pid, type, hitFactor));
}


/*
==================
ClientJoinConversation
==================
*/
ClientJoinConversation::ClientJoinConversation(std::string name, unsigned port) 
    :   _userName(name),
        _clientUdpPort(port),
        _serverResponse(nullptr),
        _didJoin(false),
        _serverUdpPort(0),
        _playerId(0),
        _team(Team::NONE) {
    
}

ClientJoinConversation::~ClientJoinConversation() {
    if (_serverResponse) {
        delete _serverResponse;
    }
}

bool ClientJoinConversation::start(SocketBase *socket) {
    PacketFactory *pf = ClientServiceLocator::singleton()->getPacketFactory();
    Packet *joinRequest = pf->createPacketFromTemplate("ClientJoinRequest");

    joinRequest->setString("userName", _userName);
    joinRequest->setUnsigned("udpPort", _clientUdpPort);

    bool status = socket->sendPacket(joinRequest);
    delete joinRequest;

    if (status) {
        Log::debug("Sent ClientJoinRequest");
    } else {
        Log::debug("Failed to send ClientJoinRequest");
    }

    return status;
}

bool ClientJoinConversation::update(SocketBase *socket) {
    if (isDone())
        return true;

    _serverResponse = socket->getPacket("ClientJoinResponse");
    if (_serverResponse) {
        _didJoin = (_serverResponse->getByte("response") != 0);
        if (_didJoin) {
            _playerId = _serverResponse->getUnsigned("playerId");
            _serverUdpPort = _serverResponse->getUnsigned("udpPort");
            
            byte team = _serverResponse->getByte("team");
            _team = static_cast<Team>(team);

            Log::debug("Joined lobby with player ID %u", _playerId);
        } else {
            Log::debug("Server rejected");
        }
    }

    return true;
}

bool ClientJoinConversation::isDone() {
    return (_serverResponse != nullptr);      
}

bool ClientJoinConversation::didSuccessfullyJoin() const {
    return _didJoin;
}

unsigned ClientJoinConversation::getServerUdpPort() const {
    return _serverUdpPort;
}

PlayerId ClientJoinConversation::getPlayerId() const {
    return _playerId;
}

Team ClientJoinConversation::getTeam() const {
    return _team;
}

