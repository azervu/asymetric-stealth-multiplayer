#pragma once

#include "Subsystem.h"
#include "Engine.h"
#include "SDLHeaders.h"
#include "InputState.h"


/**
 * InputSystem is responsible for converting SDL input events into events which
 * are passable on the EventManager event bus. The inputs are _not_ translated
 * into actual game commands (move right, pause game, etc) - the contents of
 * the outgoing events are raw in nature and must be interpreted by the 
 * receiver.
 */
class InputSystem : public Subsystem {
public:
    InputSystem();
    ~InputSystem();

    virtual bool init(Settings* settings);
    virtual void update(DeltaTime dt);

    /**
     * Clear temporary flags in the input state. Should be called before 
     * processing input.
     */
    void invalidateOldState();

    /**
     * Handle a single SDL_Event.
     */
    void handleSDLEvent(const SDL_Event &event);

private:
    void handleKeyboardEvent(const SDL_KeyboardEvent *keyEvent);
    void handleMouseMotionEvent(const SDL_MouseMotionEvent *mouseEvent);
    void handleMouseButtonEvent(const SDL_MouseButtonEvent *mouseEvent);

    InputState _inputState;
};
