/**
 * @file    source\client\SoundSystem.h
 *
 * @brief   Declares the sound system class.
 */

#pragma once

#include "SDLHeaders.h"
#include "Subsystem.h"
#include "EventManager.h"
#include "ClientEvents.h"
#include <map>
#include <vector>
#include <string>
#include <irrKlang.h>

using namespace irrklang;

struct SoundData {
    SoundData();
    SoundData(irrklang::ISound* sound, Vec2 position, bool tdSound, bool loop);
    ISound* _sound;
    vec3df _position;
    bool _tdSound;
    bool _looping;
};

/**
 * @class   SoundSystem
 *
 * @brief   The sound system.
 */
class SoundSystem : public Subsystem {
public:
    SoundSystem();
    ~SoundSystem();
    bool init(Settings *);
    void update(DeltaTime);
private:
    std::map<ResourceId, ISoundSource*> _sounds;
    std::map<int, SoundData> _affectableSoundInstances;
    std::vector<SoundData> _unaffectable3dSounds;
    vec3df _cameraPosition;

    void onLoadSoundEvent(const EventData *e);
    EventListenerDelegate _loadSoundEventDelegate;

    void onAffectSoundEvent(const EventData *e);
    void onCreateSoundEvent(const EventData *e);
    void onDestroyAffectableSoundEvent(const EventData *e);
    EventListenerDelegate _affectSoundEventDelegate;
    EventListenerDelegate _createSoundEventDelegate;
    EventListenerDelegate _destroyAffectableSoundEventDelegate;
    ISoundEngine* _soundEngine;
};

