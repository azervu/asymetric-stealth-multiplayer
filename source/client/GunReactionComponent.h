#pragma once

#include "GameObject.h"
#include "EventManager.h"
#include "Engine.h"
#include "Timer.h"
#include "DrawableComponent.h"
#include "Renderable.h"

class Player;

/**
 * GunReactionComponent (GRC) is a component that reacts to gunfire... :-) Who'd 
 * guess??? 
 *
 * GRC is responsible for two primary things:
 *  1. Iniate a visual representation of bullet trajectories upon receiving a
 *     (Client)GunFiredEvent.
 *  2. Reduce the hitpoints of the owning Player (GameObject) when it gets
 *     hit by a bullet (BulletHitEvent).
 */
class GunReactionComponent : public Component {
public:
    GunReactionComponent();
    ~GunReactionComponent();

    void update(DeltaTime dt);

private:
    Player* getPlayer();

    /* Delegate method for ClientGunFiredEvent
     */
    EventListenerDelegate _gunFireDelegate;
    void onGunFired(const EventData *e);

    /* Delegate method for BulletHitEvent
     */
    EventListenerDelegate _bulletHitDelegate;
    void onBulletHit(const EventData *e);
};




/**
 * BulletComponent draws a line to display a bullets trajectory. The component
 * removes itself from the parent after a short duration.
 */
class BulletComponent : public Component, public DrawableProvider {
public:
    BulletComponent(Vec2 p1, Vec2 p2);

    void update(DeltaTime dt);

    virtual Drawable::BaseDrawable* getDrawable() final;

private:
    Vec2 _p1;
    Vec2 _p2;
    Timer _timer;
    bool _removed;
};

