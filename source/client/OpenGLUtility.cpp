/**
 * @file    source/client/OpenGLUtility.cpp
 *
 * @brief   Implements OpenGL utility functions.
 */

#include "OpenGLUtility.h"

#include "Log.h"
#include "Utility.h"

void utility::loadShaderFromFile(GLuint shader, const std::string &path) {
    std::string data = loadFileToString(path);

    GLchar const *shader_source = data.c_str();
    GLint const shader_length = data.size();

    glShaderSource(shader, 1, &shader_source, &shader_length);

    GL_errorCheck("loading shader from file " + path);
}

GLuint utility::loadCompileShader(const std::string &path, GLenum type) {
    GLuint ref = glCreateShader(type);

    loadShaderFromFile(ref, path);
    glCompileShader(ref);
    checkShaderLog(ref, GL_COMPILE_STATUS);

    return ref;
}

bool utility::GL_errorCheck(const std::string &place) {

    GLenum error = glGetError();
    bool isError = false;

    while (error != GL_NO_ERROR) {
        if ((error & GL_INVALID_ENUM) == 0) {
            isError = true;
        }

        Log::error("%s: %s", place.c_str(), gluErrorString(error));

        error = glGetError();
    };

    return isError;
}

bool utility::checkProgramLog(GLuint programID, int flag) {
    GLint valid = 0;
    glGetProgramiv(programID, flag, &valid);

    if (valid == GL_FALSE) {
        GLint len = 0;
        glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &len);

        std::vector<char> logArray;
        logArray.resize(len);

        glGetProgramInfoLog(programID, len, &len, &logArray.front());

        Log::error("Shader program error: %s", std::string(&logArray.front()).c_str());

        return true;
    }

    return false;
}

void utility::checkShaderLog(GLuint shaderID, int flag) {
    GLint valid = 0;
    glGetShaderiv(shaderID, flag, &valid);

    if (valid == GL_FALSE) {
        GLint len = 0;
        glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &len);

        std::vector<char> logArray;
        logArray.resize(len);

        glGetShaderInfoLog(shaderID, len, &len, &logArray.front());

        Log::error("Shader error: %s", std::string(&logArray.front()).c_str());
    }
}