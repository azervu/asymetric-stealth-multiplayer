/**
 * @file    source/client/NetworkSystem.h
 *
 * @brief   Declares the network system class.
 */

#pragma once

#include "SDLHeaders.h"
#include "Engine.h"
#include "Subsystem.h"
#include "ClientEvents.h"


class NetworkClient;


/**
 * The NetworkSystem is the primary connection layer between the game client and
 * the server.
 */
class NetworkSystem : public Subsystem {
public:
    NetworkSystem();
    ~NetworkSystem();

    virtual bool init(Settings* settings);
    virtual void update(DeltaTime dt);

private:
    /**
     * Disconnects the NetworkClient and fires a DisconnectEvent.
     */
    void disconnect();

    /**
     * Delegate method for event type InitiateConnectionEvent
     */
    void onInitiateConnection(const EventData *e);
    EventListenerDelegate _initDelegate;

    /**
     * Delegate method for event type ServerShutdownEvent
     */
    void onServerShutdown(const EventData *e);
    EventListenerDelegate _shutdownDelegate;
    
    /**
     * Delegate method for event type ClientReadyToPlayEvent
     */
    void onClientReadyToPlay(const EventData *e);
    EventListenerDelegate _clientReadyDelegate;

    /**
     * Delegate method for event type LevelLoadingCompleteEvent
     */
    void onLevelLoadingComplete(const EventData *e);
    EventListenerDelegate _levelLoadedDelegate;


    NetworkClient * _networkClient = nullptr;
};
