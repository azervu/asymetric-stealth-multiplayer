#pragma once

#include "Engine.h"

class Map;

/**
 * VisbilityMapper maps the visible area from a point by exclusion. Given a
 * point and a radius, a set of triangles is returned; this set of triangles
 * marks the regions invisible from the given point.
 */
class VisibilityMapper {
public:
    VisibilityMapper(const Map *map);

    /**
     * The returned vertices are a set of triangles which define the areas 
     * within the given radius that are invisible from the given point.
     */
    std::vector<Vec2> map(Vec2 point, float radius);

private:
    const Map *_map;

    bool circleOverlapsRect(Vec2 pt, float radius, const Rect &rect);
    bool lineIntersectsCircle(Vec2 pt, float radius, Vec2 l1, Vec2 l2);

    /**
     * Project a line "radius" units from "cpt". Two triangles are pushed onto
     * the vertex-vector.
     * @param verts     The vector to store the triangles in
     * @param cpt       The center of the circle
     * @param radius    The radius of the circle
     * @param pt1       The first point of the line
     * @param pt2       The second point of the line
     */
    void project(std::vector<Vec2>& verts, Vec2 cpt, float radius, Vec2 pt1, Vec2 pt2);
};
