#include "FireComponent.h"
#include "ClientServiceLocator.h"
#include "ClientEvents.h"
#include "RayCastCallback.h"
#include "CollisionFlags.h"
#include "Player.h"
#include "SharedEvents.h"
#include "ResourceManager.h"


static const float FIRE_COOLDOWN = 0.15f;


FireComponent::FireComponent(b2World *world) 
    :   _world(world),
        _doFire(false) {
    EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();

    _buttonDelegate.bind(this, &FireComponent::onButtonClicked);
    emgr->addListener(_buttonDelegate, ButtonEvent::eventType);
}

FireComponent::~FireComponent() {
    EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();
    emgr->removeListener(_buttonDelegate);
}

void FireComponent::update(DeltaTime dt) {
    if (_doFire && _timer.getElapsedSeconds() > FIRE_COOLDOWN) {
        _timer.start();

        fire();
    }
}


void FireComponent::fire() {
    float rot = getGameObject()->getRotation();
    rot += M_PI / 2.f;

    b2Vec2 unit(std::cos(rot), std::sin(rot));
    unit *= 100.f;

    b2Vec2 p1 = getGameObject()->getPosition();
    b2Vec2 p2 = p1 + unit;

    RayCastCallback cb(RayCastCallback::gunFireCategoryFilter);
    _world->RayCast(&cb, p1, p2);

    /* If we are attached to a Player, we are required to fire an event 
     * indicating that we fired a bullet.
     */
#ifdef ASM_DEBUG
    Player *player = dynamic_cast<Player*>(getGameObject());
#else
    Player *player = (Player*)getGameObject();
#endif
    if (player) {
        EventManager *emgr = ServiceLocator::singleton()->getEventManager();

        EventData *evt;
        evt = new ClientGunFiredEvent(NetworkDirection::NET_OUT,
                                      player->getPlayerId(), p1, cb.getHitPoint());
        emgr->queueEvent(evt);
    }
}

void FireComponent::onButtonClicked(const EventData *e) {
    ButtonEvent *evt = (ButtonEvent*)e;

    if (evt->getButton() == SDL_BUTTON_LEFT) {
        _doFire = evt->isKeyDown();
    }
}

