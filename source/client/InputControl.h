#pragma once
#include "ControlComponent.h"
#include <map>
#include <vector>

class InputControl: public Control {
public:
    enum KeyCommand {
        MOVE_LEFT,
        MOVE_RIGHT,
        MOVE_UP,
        MOVE_DOWN
    };

    InputControl();
    virtual ~InputControl();
    virtual std::vector<Command> getCommands();

    void mouseMovement(const EventData*);
    void keyPress(const EventData*);

private:
    void checkMovement(Command& command);

    std::vector<Command> _commands;
    Command _moveCommand;
    Command _turnCommand;
    
    std::map<SDL_Keycode, std::pair<KeyCommand, bool>> _keyMap;
    std::map<KeyCommand, SDL_Keycode> _commandMap;
    
};
