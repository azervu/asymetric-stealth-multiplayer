#pragma once

#include <set>
#include "Engine.h"
#include "SDLHeaders.h"


/**
 * InputState keeps the information about the current state of input; which 
 * keys and buttons (mouse & keyboard) are pressed, which keys and buttons are 
 * fresh, and where the mouse pointer is located.
 *
 * The distinction between a 'key' and a 'button' is clear - a key can be 
 * found on the keyboard, a button is something pressable on the mouse or on
 * a game controller.
 */
class InputState {
public:
    InputState();
    ~InputState();

    void clearFreshFlags();

    /**
     * @return      True if the key was not already pressed, false if the key
     *              was not already down.
     */
    bool keyPressed(SDL_Keycode keyCode);

    /**
     * @return      True if the key was previously pressed, false if it was
     *              undepressed.
     */
    bool keyReleased(SDL_Keycode keyCode);

    void mouseMoved(int x, int y, int xrel, int yrel);

    bool buttonPressed(Uint8 button);
    bool buttonReleased(Uint8 button);

    bool isKeyDown(SDL_Keycode keyCode) const;
    bool isKeyFresh(SDL_Keycode keyCode) const;
    bool isButtonDown(Uint8 button) const;
    bool isButtonFresh(Uint8 button) const;

    /**
     * Get the absolute position of the mouse in screen-space coordinates.
     */
    Vec2 getMousePosition() const;
    
    /**
     * Get the relative movement of the mouse pointer since the last frame in
     * screen-space coordinates.
     */
    Vec2 getMouseRelative() const;

private:
    /**
     * The keys which are down are stored in _keyMap. The keys which were 
     * pressed this game-tick are also stored in _freshMap. 
     */
    std::set<SDL_Keycode> _keySet;
    std::set<SDL_Keycode> _freshSet;

    Vec2 _mousePos;
    Vec2 _mouseRel;
};
