#include "LightComponent.h"


LightComponent::LightComponent(const MapLightProperties &props, const Map *map)
    :   _props(props) {
    // Map the visible area from this light at creation. This eliminate the
    // need to do the heavy mapping at runtime.
    VisibilityMapper vmapper(map);
    _vertCache = vmapper.map(_props.pos, _props.radius); 
}

void LightComponent::update(DeltaTime dt) {
    // hurr burr
}

void LightComponent::fillShadowTriangles(std::vector<Vec2> &verts) const {
    verts.insert(verts.end(), _vertCache.begin(), _vertCache.end());
}

Color LightComponent::getLightColor() const {
    return _props.color;
}

float LightComponent::getLightRadius() const {
    return _props.radius;
}

Vec2 LightComponent::getLightPosition() const {
    return _props.pos;
}
