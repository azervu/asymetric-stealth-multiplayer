/**
 * @file    source/client/Client.h
 *
 * @brief   Declares the resource Client class.
 */

#pragma once

#include <stack>
#include <vector>

#include "SDLHeaders.h"
#include "Scene.h"
#include "Subsystem.h"
#include "GraphicsSystem.h"
#include "EventManager.h"
#include "Settings.h"
#include "Preloader.h"

class InputSystem;


/**
 * This class is atop the client side hierarchy. Client handles high level
 * delegation and main loop control. 
 */
class Client {
public:
    Client();
    ~Client();
    int run(Settings* settings);

private:
    
    /**
     * Main Loop performs the following actions in the following order:
     *      1. Handle SDL events
     *      2. Update the current scene
     *      3. Update the subsystems
     *      4. Scene transition (if applicable)
     */
    void mainLoop();

    void handleSDLEvents();

    bool init(Settings* settings);
    bool initServices(Settings* settings);
    bool initSubsystems(Settings* settings);

	std::stack<Scene*> _sceneStack;

    std::vector<Subsystem*> _subsystems;

    GraphicsSystem* _graphicsSystem;
    Preloader* _preloader;
    
    // SDL_Events are dispatched to the InputSystem, so a direct reference 
    // must be kept.
    InputSystem *_inputSystem;
    bool _continueRunning;
};
