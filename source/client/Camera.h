/**
 * @file    source/client/Camera.h
 *
 * @brief   Declares the camera class.
 */

#pragma once

#include "Engine.h"
#include "ServiceLocator.h"
#include "EventManager.h"


/**
 * Camera defines the position and the viewport of what is being drawn.
 * The position and viewport can be altered by anyone at any point. Both the
 * position and the viewport are in metric coordinates; if position={1,1},
 * viewport={4,3} and one meter is 10 pixels, the screen will display 40x30
 * pixels, with the top left pixel having pixel-space coordinate {10,10}. 
 *
 * Note that all conversion from metric space coordinates to pixel coordinates 
 * must be done by another class than Camera - Camera is only aware of tile
 * metric coordinates.
 *
 * The Camera service is dependent on the EventManager and the Window services. 
 * Do not create the Camera before any of these.
 */
class Camera : public Service {
public:
    Camera();
    ~Camera();
    virtual void onExit() final;

    /**
     * Center the camera on the specified position.
     */
    void setPosition(Vec2 position);

    /**
     * Set the number of meters to display vertically. The final camera frame is 
     * calculated based on the aspect ratio of the window and the number of
     * meters to display vertically.
     */
    void setVerticallyVisibleMeters(float meters);

    /**
     * Set the rotation of the camera in radians.
     */
    void setRotation(float radians);

    Vec2 getTopLeftPosition() const;
    Vec2 getCenterPosition() const;
    Vec2 getBottomRightPosition() const;

    /**
     * Retrieve the dimensions of the visible camera frame. The returned size
     * is guaranteed to have the same aspect ratio as the window.
     * @return  The visible frame in metric dimensions.
     */
    Vec2 getViewFrame() const;

    /**
     * Get the rotation of the camera in radians.
     */
    float getRotation() const;

private:
    Vec2 _position;
    Vec2 _viewFrame;
    float _rotation;

    // When the window changes, the viewport must be updated as well. The
    // aspect ratio of the window is very likely to have changed.
    void onWindowResized(const EventData *e);
    EventListenerDelegate _winResizeDelegate;
};
