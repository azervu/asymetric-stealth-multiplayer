#include "MainMenuScene.h"
#include "ClientGameScene.h"
#include "ClientServiceLocator.h"
#include "ClientEvents.h"

#include <thread>


MainMenuScene::MainMenuScene() 
    :   _gameScene(nullptr),
        _sceneLoader(nullptr),
        _state(Scene::State::RUNNING) {
    _startLoadingDelegate.bind(this, &MainMenuScene::onStartLoading);

    EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();
    emgr->addListener(_startLoadingDelegate, StartLoadingLevelEvent::eventType);
}

MainMenuScene::~MainMenuScene() {
    EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();
    emgr->removeListener(_startLoadingDelegate);

    if (_sceneLoader) {
        Log::warning("Client closing before SceneLoader finished loading. "
                     "Waiting for it to finish..");
        while (_sceneLoader) {
            checkSceneLoader();
            std::this_thread::sleep_for(std::chrono::milliseconds(3));
        }

        delete _gameScene;
        Log::warning("SceneLoader finished successfully");
    }
}

bool MainMenuScene::loadScene() {
    // yes
    return true;
}

void MainMenuScene::logicUpdate(DeltaTime dt) {
    checkSceneLoader();
}

void MainMenuScene::onResume() {
    Log::debug("MainMenuScene resuming");
    _state = Scene::State::RUNNING;
}

Scene::State MainMenuScene::getState() {
    return _state;
}

Scene* MainMenuScene::getNextScene() {
    // Transfer ownership to the caller
    return _gameScene;
}

void MainMenuScene::checkSceneLoader() {
    if (_sceneLoader && _sceneLoader->doneLoading()) {
        if (!_sceneLoader->success()) {
            Log::error("MainMenuScene: Failed to load ClientGameScene");
            Log::error("(Stop slacking and notify the server!)");
            delete _gameScene;

            // TODO
            // Notify the server about the fail
        } else {
            _state = Scene::State::SET_CHILD_ACTIVE;

            EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();
            emgr->queueEvent(new LevelLoadingCompleteEvent(_gameScene->getMap()));
        }

        delete _sceneLoader;
        _sceneLoader = nullptr;
    }
}


void MainMenuScene::onStartLoading(const EventData *e) {
    StartLoadingLevelEvent *evt = (StartLoadingLevelEvent*)e;
    
    std::string map = evt->getLevelName();

    _gameScene = new ClientGameScene(map);
    _sceneLoader = new SceneLoader<ClientGameScene>(_gameScene);
    _sceneLoader->loadAsynchronously();
}

