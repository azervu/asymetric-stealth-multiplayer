#include "GunReactionComponent.h"
#include "ClientServiceLocator.h"
#include "SharedEvents.h"
#include "ClientEvents.h"
#include "Player.h"
#include "ResourceManager.h"
#include "ClientEvents.h"
#include "Scene.h"


static const float BULLET_VISIBILITY = 0.05f;


GunReactionComponent::GunReactionComponent() {
    EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();

    _gunFireDelegate.bind(this, &GunReactionComponent::onGunFired);
    _bulletHitDelegate.bind(this, &GunReactionComponent::onBulletHit);

    emgr->addListener(_gunFireDelegate, ClientGunFiredEvent::eventType);
    emgr->addListener(_bulletHitDelegate, BulletHitEvent::eventType);
}

GunReactionComponent::~GunReactionComponent() {
    EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();

    emgr->removeListener(_gunFireDelegate);
    emgr->removeListener(_bulletHitDelegate);
}

void GunReactionComponent::update(DeltaTime dt) {

}


Player* GunReactionComponent::getPlayer() {
    GameObject *gob = getGameObject();
    Player *player;

#ifdef ASM_DEBUG
    player = dynamic_cast<Player*>(gob);
    if (!player) {
        Log::fatal("GunReactionComponent attached to non-Player GameObject");
        exit(-1);
    }
#else
    player = (Player*)gob;
#endif

    return player;
}

void GunReactionComponent::onGunFired(const EventData *e) {
    ClientGunFiredEvent *evt = (ClientGunFiredEvent*)e;

    if (evt->getPlayerId() != getPlayer()->getPlayerId()) {
        return;
    }

    Vec2 p1 = evt->getGunPoint();
    Vec2 p2 = evt->getEndPoint();

    GameObject *gob = new GameObject();
    getGameObject()->getScene()->addGameObject(gob);
    addComponent<BulletComponent>(gob, p1, p2);
    addComponent<DrawableComponent>(gob, getComponent<BulletComponent>(gob), false);

    ResourceId gunfire = hashResource("audio/gunshot.ogg");
    CreateSoundEvent* pse = new CreateSoundEvent(gunfire,p1);
    ClientServiceLocator::singleton()->getEventManager()->queueEvent(pse);
}

void GunReactionComponent::onBulletHit(const EventData *e) {
    BulletHitEvent *evt = (BulletHitEvent*)e;

    if (evt->getHitPlayerId() != getPlayer()->getPlayerId()) {
        return;
    }

    getPlayer()->setHealth(evt->getPlayerHealth());
}



BulletComponent::BulletComponent(Vec2 p1, Vec2 p2) 
    :   _p1(p1),
        _p2(p2),
        _removed(false) {

}

void BulletComponent::update(DeltaTime dt) {
    if (_timer.getElapsedSeconds() >= BULLET_VISIBILITY && !_removed) {
        GameObject* gob = getGameObject();

        Scene *scene = gob->getScene();
        scene->removeGameObject(gob);
        _removed = true;
    }
}

Drawable::BaseDrawable* BulletComponent::getDrawable() {
    Drawable::GeometryCloud::TYPE t = Drawable::GeometryCloud::TYPE::LINE;
    Drawable::GeometryCloud *cloud = new Drawable::GeometryCloud(t);       

    Drawable::Vertex vert;
    vert.r = 1.f; 
    vert.g = 0.5f; 
    vert.b = 0.f;
    vert.a = 1.f;
    vert.z = 1.f;

    vert.x = _p1.x;
    vert.y = _p1.y;
    cloud->addVertex(vert);

    vert.x = _p2.x;
    vert.y = _p2.y;
    cloud->addVertex(vert);

    return cloud;
}
