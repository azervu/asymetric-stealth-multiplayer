/**
 * @file    source/client/Window.cpp
 *
 * @brief   Implements the window class.
 */

#include "Window.h"

#include "ClientEvents.h"


static const int DEFAULT_WIN_W = 800;
static const int DEFAULT_WIN_H = 600;


Window::Window() 
    :   _window(nullptr),
        _title("Sneaky Sneaky Bang Bang"),
        _winSize(DEFAULT_WIN_W, DEFAULT_WIN_H) {
    
    _winResizeDelegate.bind(this, &Window::onWindowResized);

    EventManager *emgr = ServiceLocator::singleton()->getEventManager();
    emgr->addListener(_winResizeDelegate, WindowResizedEvent::eventType);
}

Window::~Window() {
    std::lock_guard<std::mutex> lock(_mutex);

    destroyWindow();
}

void Window::onExit() {
    EventManager *emgr = ServiceLocator::singleton()->getEventManager();
    emgr->removeListener(_winResizeDelegate);
}


void Window::init(Settings *settings) {
    std::lock_guard<std::mutex> lock(_mutex);

    _winSize.first = settings->getInt("window-width", DEFAULT_WIN_W);
    _winSize.second = settings->getInt("window-height", DEFAULT_WIN_H);
}

void Window::createWindow() {
    std::lock_guard<std::mutex> lock(_mutex);

    Log::info("Creating window");

    _window = SDL_CreateWindow(
        _title.c_str(),
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        _winSize.first,
        _winSize.second,
        SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL
    );

    if (_window == nullptr) {
        Log::fatal("Failed to create window. Taking the easy way out.");
        exit(1);
    }

    // Creating a window does not cause SDL to fire a "window resized" event.
    // Manually fire a WindowResizedEvent to get everyone up to speed.
    EventManager *emgr = ServiceLocator::singleton()->getEventManager();
    emgr->queueEvent(new WindowResizedEvent(_winSize));

    Log::info("Window creation successful");
}

void Window::setTitle(std::string title) {
    std::lock_guard<std::mutex> lock(_mutex);

    _title = title;

    if (_window) {
        // Set the fkn title
    }
}

std::pair<int, int> Window::getSize() {
    std::lock_guard<std::mutex> lock(_mutex);

    return _winSize;
}

SDL_Window* Window::getSDLWindow() {
    return _window;
}

void Window::swapBuffers() {
    std::lock_guard<std::mutex> lock(_mutex);

    if (_window) {
        SDL_GL_SwapWindow(_window);
    }
}


void Window::destroyWindow() {
    if (_window) {
        Log::info("Destroying window...");

        SDL_DestroyWindow(_window);
        _window = nullptr;

        Log::info("Window destroyed");
    }
}

void Window::onWindowResized(const EventData *e) {
    std::lock_guard<std::mutex> lock(_mutex);

    WindowResizedEvent *evt = (WindowResizedEvent*)e;
    _winSize = evt->getSize();
}
