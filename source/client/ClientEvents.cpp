#include "ClientEvents.h"

/*
==================
InputEvent
==================
*/
InputEvent::InputEvent(const InputState *is) 
    :   _inputState(is) {
}

const InputState* InputEvent::getInputState() const {
    return _inputState;
}


/*
==================
KeyEvent
==================
*/
const EventType KeyEvent::eventType = 0xf311a19bda534b18;

KeyEvent::KeyEvent(const InputState *is, SDL_Keycode kc, bool pressed) 
    :   InputEvent(is), 
        _keyCode(kc),
        _keyPressed(pressed) {
}

EventType KeyEvent::getEventType() const {
    return eventType;
}

bool KeyEvent::isKeyDown() const {
    return _keyPressed;
}

SDL_Keycode KeyEvent::getKeyCode() const {
    return _keyCode;
}


/*
==================
MouseMotionEvent
==================
*/
const EventType MouseMotionEvent::eventType = 0x37a4453ba363489b;

MouseMotionEvent::MouseMotionEvent(const InputState *is) 
    :   InputEvent(is) {
}

EventType MouseMotionEvent::getEventType() const {
    return eventType;
}

Vec2 MouseMotionEvent::getMousePosition() const {
    return getInputState()->getMousePosition();
}

Vec2 MouseMotionEvent::getMouseRelative() const {
    return getInputState()->getMouseRelative();
}


/*
==================
ButtonEvent
==================
*/
const EventType ButtonEvent::eventType = 0xdb542c24cfd64cf8;

ButtonEvent::ButtonEvent(const InputState *is, Uint8 btn, bool pressed, Vec2 p) 
    :   InputEvent(is), 
        _button(btn),
        _pressed(pressed),
        _position(p) {
}

EventType ButtonEvent::getEventType() const {
    return eventType;
}

bool ButtonEvent::isKeyDown() const {
    return _pressed;
}

Uint8 ButtonEvent::getButton() const {
    return _button;
}

Vec2 ButtonEvent::getPosition() const {
    return _position;
}




/*
==================
WindowResizedEvent
==================
*/
const EventType WindowResizedEvent::eventType = 0x5440470d982e46a1;

WindowResizedEvent::WindowResizedEvent(std::pair<int, int> size)
    :   _size(size) { }

EventType WindowResizedEvent::getEventType() const {
    return eventType;
}

std::pair<int, int> WindowResizedEvent::getSize() const {
    return _size;
}


/*
==================
CameraViewFrameChangedEvent
==================
*/
const EventType CameraViewFrameChangedEvent::eventType = 0xb1cb248cba134856;

CameraViewFrameChangedEvent::CameraViewFrameChangedEvent(Vec2 vp) 
    :   _viewport(vp) {}

EventType CameraViewFrameChangedEvent::getEventType() const {
    return eventType;
}

Vec2 CameraViewFrameChangedEvent::getViewport() const {
    return _viewport;
}


/*
==================
ClientInitiationCompleteEvent
==================
*/
const EventType ClientInitiationCompleteEvent::eventType = 0x49dbc3a5454343ed;

EventType ClientInitiationCompleteEvent::getEventType() const {
    return eventType;
}



/*
==================
ClientReadyToPlayerEvent
==================
*/
const EventType ClientReadyToPlayEvent::eventType = 0xad5af752fbb54606;

EventType ClientReadyToPlayEvent::getEventType() const {
    return eventType;
}



/*
==================
StartLoadingLevelEvent
==================
*/
const EventType StartLoadingLevelEvent::eventType = 0x008d3bab4b4c4a58;

StartLoadingLevelEvent::StartLoadingLevelEvent(std::string level) 
    :   _level(level) {
}

EventType StartLoadingLevelEvent::getEventType() const {
    return eventType;
}

std::string StartLoadingLevelEvent::getLevelName() const {
    return _level;
}



/*
==================
LevelLoadingCompleteEvent
==================
*/
const EventType LevelLoadingCompleteEvent::eventType = 0x54c450bce6a84dfc;

LevelLoadingCompleteEvent::LevelLoadingCompleteEvent(const Map* map) : EventData(), _map(map) {
}

EventType LevelLoadingCompleteEvent::getEventType() const {
    return eventType;
}

const Map* LevelLoadingCompleteEvent::getMap() const {
    return this->_map;
}

/*
==================
GameStartingEvent
==================
*/
const EventType GameStartingEvent::eventType = 0x491f860e52ef4763;

EventType GameStartingEvent::getEventType() const {
    return eventType;
}




/*
==================
InitiateConnectionEvent
==================
*/
const EventType InitiateConnectionEvent::eventType = 0x6EC4CE2F61C64415;

InitiateConnectionEvent::InitiateConnectionEvent(std::string userName, 
                                                 std::string hostName) {
    _userName = userName;
    _hostName = hostName;
}

EventType InitiateConnectionEvent::getEventType() const {
    return eventType;   
}

std::string InitiateConnectionEvent::getUserName() const {
    return _userName;
}

std::string InitiateConnectionEvent::getHostName() const {
    return _hostName;
}


/*
================== 
ConnectionFailedEvent
==================
*/
const EventType ConnectionFailedEvent::eventType = 0x396CCDC3D04A473C;

EventType ConnectionFailedEvent::getEventType() const {
    return eventType;
}


/*
================== 
DisconnectEvent
==================
*/
const EventType DisconnectEvent::eventType = 0x5CCC93F3B9AA470A;

EventType DisconnectEvent::getEventType() const {
    return eventType;
}


/*
================== 
ConnectionEstablishedEvent
==================
*/
const EventType ConnectionEstablishedEvent::eventType = 0x180CF2058D714987;

ConnectionEstablishedEvent::ConnectionEstablishedEvent(PlayerId id, Team t) 
    :   _playerId(id),
        _team(t) {
}

EventType ConnectionEstablishedEvent::getEventType() const {
    return eventType;
}

PlayerId ConnectionEstablishedEvent::getPlayerId() const {
    return _playerId;
}

Team ConnectionEstablishedEvent::getTeam() const {
    return _team;
}


/*
================== 
ClientJoinedSessionEvent
==================
*/
const EventType ClientJoinedSessionEvent::eventType = 0xE141EE951C914ABA;

ClientJoinedSessionEvent::ClientJoinedSessionEvent(std::string clientName,
                                                   PlayerId playerId, Team team) 
    :   _clientName(clientName),
        _playerId(playerId),
        _team(team) {
    
}

EventType ClientJoinedSessionEvent::getEventType() const {
    return eventType;
}

std::string ClientJoinedSessionEvent::getClientName() const {
    return _clientName;
}

PlayerId ClientJoinedSessionEvent::getPlayerId() const {
    return _playerId;
}

Team ClientJoinedSessionEvent::getTeam() const {
    return _team;
}


/*
================== 
ClientLeftSessionEvent
==================
*/
const EventType ClientLeftSessionEvent::eventType = 0xEAE32F83DACB4318;

ClientLeftSessionEvent::ClientLeftSessionEvent(std::string clientName,
                                               PlayerId playerId) 
    :   _clientName(clientName),
        _playerId(playerId) {
    
}

EventType ClientLeftSessionEvent::getEventType() const {
    return eventType;
}

std::string ClientLeftSessionEvent::getClientName() const {
    return _clientName;
}

unsigned ClientLeftSessionEvent::getPlayerId() const {
    return _playerId;
}


/*
================== 
ServerShutdownEvent
==================
*/
const EventType ServerShutdownEvent::eventType = 0x347C344880844CCC;

ServerShutdownEvent::ServerShutdownEvent(std::string msg)
    :   _message(msg) {

}

EventType ServerShutdownEvent::getEventType() const {
    return eventType;
}

std::string ServerShutdownEvent::getMessage() const {
    return _message;
}

/*
==================
AffecteSoundEvent
==================
*/

const EventType AffectSoundEvent::eventType = 0x4F931159F5BE;
EventType AffectSoundEvent::getEventType() const {
    return eventType;
}

AffectSoundEvent::AffectSoundEvent(int soundInstanceId, Vec2 position, bool loop)
    :   _soundInstanceId(soundInstanceId),
        _loop(loop),
        _position(position) {
}

AffectSoundEvent::AffectSoundEvent(int soundInstanceId, bool loop)
: _soundInstanceId(soundInstanceId),
_loop(loop) {}

int AffectSoundEvent::getSoundInstanceId() {
    return _soundInstanceId;
}

bool AffectSoundEvent::getLoop() {
    return _loop;
}

Vec2 AffectSoundEvent::getPosition() {
    return _position;
}


/*
==================
CreateSoundEvent
==================
*/
const EventType CreateSoundEvent::eventType = 0x08CCD6527C8A;
EventType CreateSoundEvent::getEventType() const {
    return eventType;
}

CreateSoundEvent::CreateSoundEvent(ResourceId soundId, int soundInstanceId, Vec2 position, bool loop)
    :   AffectSoundEvent(soundInstanceId, position, loop),
        _soundId(soundId),
        _tdSound(true),
        _affectable(true) {
    
}

CreateSoundEvent::CreateSoundEvent(ResourceId soundId, int soundInstanceId, bool loop)
    :   AffectSoundEvent(soundInstanceId, loop),
        _soundId(soundId),
        _tdSound(false),
        _affectable(true) {

}

CreateSoundEvent::CreateSoundEvent(ResourceId soundId, Vec2 position)
    :   AffectSoundEvent(0, position, false),
        _soundId(soundId),
        _tdSound(true),
        _affectable(false) {

}

CreateSoundEvent::CreateSoundEvent(ResourceId soundId)
    :   AffectSoundEvent(0, false),
    _soundId(soundId),
    _tdSound(false),
    _affectable(false) {

}

bool CreateSoundEvent::getTdSound() {
    return _tdSound;
}

bool CreateSoundEvent::getAffectable() {
    return _affectable;
}

const ResourceId CreateSoundEvent::getSoundId() {
    return _soundId;
}

/*
==================
DestroyAffectableSoundEvent
==================
*/
const EventType DestroyAffectableSoundEvent::eventType = 0x6849440CD82C;

DestroyAffectableSoundEvent::DestroyAffectableSoundEvent(int soundInstanceId) {
    _soundInstanceId = soundInstanceId;
}

int DestroyAffectableSoundEvent::getSoundInstanceId() {
    return _soundInstanceId;
}

EventType DestroyAffectableSoundEvent::getEventType() const {
    return eventType;
}


/*
==================
LoadSoundEvent
==================
*/
const EventType LoadSoundEvent::eventType = 0x761CADA66D9C4AC1;

LoadSoundEvent::LoadSoundEvent(std::string path) {
    _soundFile = path;
}

const std::string LoadSoundEvent::getSoundFile() {
    return _soundFile;
}

EventType LoadSoundEvent::getEventType() const {
    return eventType;
}


/*
==================
ClientGunFiredEvent
==================
*/
ClientGunFiredEvent::ClientGunFiredEvent(NetworkDirection dir, PlayerId playerId,
                                         b2Vec2 gunPoint, b2Vec2 endPoint) 
    :   _dir(dir), GunFiredEvent(playerId, gunPoint, endPoint) {

}

NetworkDirection ClientGunFiredEvent::getDirection() const {
    return _dir;
}


/*
==================
UpdateGuiEvent
==================
*/
UpdateGuiEvent::UpdateGuiEvent(int value, int guiId) {
    _value = value;
    _guiId = guiId;
}

int UpdateGuiEvent::getGuiId() {
    return _guiId;
}

int UpdateGuiEvent::getValue() {
    return _value;
}

const EventType UpdateGuiEvent::eventType = 0xBFD23F803CA3;
EventType UpdateGuiEvent::getEventType() const {
    return eventType;
}

/*
==================
InitiateGuiEvent
==================
*/
const EventType InitiateGuiEvent::eventType = 0x0D1C559D28A8;
EventType InitiateGuiEvent::getEventType() const {
    return eventType;
}

/*
==================
PlayerAreaTransitionEvent
==================
*/
const EventType PlayerAreaTransitionEvent::eventType = 0x11d1d11e0adf4958;

PlayerAreaTransitionEvent::PlayerAreaTransitionEvent(PlayerId p, ObjectiveId o) 
    :   _playerId(p),
        _areaId(o) {

}

EventType PlayerAreaTransitionEvent::getEventType() const {
    return eventType;
}

PlayerId PlayerAreaTransitionEvent::getPlayerId() const {
    return _playerId;
}

ObjectiveId PlayerAreaTransitionEvent::getObjectiveId() const {
    return _areaId;
}

