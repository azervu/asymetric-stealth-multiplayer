#include "InputSystem.h"
#include "ClientEvents.h"
#include "ClientServiceLocator.h"


InputSystem::InputSystem() {

}

InputSystem::~InputSystem() {

}

bool InputSystem::init(Settings* settings) {
    return true;
}

void InputSystem::update(DeltaTime dt) {

}

void InputSystem::invalidateOldState() {
    _inputState.clearFreshFlags();
}

void InputSystem::handleSDLEvent(const SDL_Event &event) {
    switch (event.type) {
        case SDL_KEYDOWN:   
        case SDL_KEYUP:
            handleKeyboardEvent(&event.key);
            break;

        case SDL_MOUSEMOTION:
            handleMouseMotionEvent(&event.motion);
            break;

        case SDL_MOUSEBUTTONDOWN:
        case SDL_MOUSEBUTTONUP:
            handleMouseButtonEvent(&event.button);
            break;
    }
}

void InputSystem::handleKeyboardEvent(const SDL_KeyboardEvent *evt) {
    SDL_Keycode kc = evt->keysym.sym;
    bool pressed = false;

    if (evt->type == SDL_KEYDOWN) {
        pressed = true;
        if (!_inputState.keyPressed(kc)) {
            // The key was already pressed
            return;
        }
    } else if (evt->type == SDL_KEYUP) {
        pressed = false;
        if (!_inputState.keyReleased(kc)) {
            // The key was not pressed
            return;
        }
    } else {
        Log::warning("SDL_KeyboardEvent::type has unexpected value: %u", 
                     evt->type);
        return;
    }

    KeyEvent *ke = new KeyEvent(&_inputState, evt->keysym.sym, pressed);   
    ClientServiceLocator::singleton()->getEventManager()->queueEvent(ke);
}

void InputSystem::handleMouseMotionEvent(const SDL_MouseMotionEvent *evt) {
    _inputState.mouseMoved(evt->x, evt->y, evt->xrel, evt->yrel);

    MouseMotionEvent *me = new MouseMotionEvent(&_inputState);
    ClientServiceLocator::singleton()->getEventManager()->queueEvent(me);
}

void InputSystem::handleMouseButtonEvent(const SDL_MouseButtonEvent *evt) {
    bool pressed = false;

    if (evt->type == SDL_MOUSEBUTTONDOWN) {
        pressed = true;
        if (!_inputState.buttonPressed(evt->button)) {
            return;
        }
    } else if (evt->type == SDL_MOUSEBUTTONUP) {
        pressed = false;
        if (!_inputState.buttonReleased(evt->button)) {
            return;
        }
    } else {
        Log::warning("SDL_MouseButtonEvent::type has unexpected value: %u",
                     evt->type);
        return;
    }

    Vec2 pos(evt->x, evt->y);
    ButtonEvent *be = new ButtonEvent(&_inputState, evt->button, pressed, pos);
    ClientServiceLocator::singleton()->getEventManager()->queueEvent(be);
}

