#include "VisibilityMapper.h"
#include "Map.h"


VisibilityMapper::VisibilityMapper(const Map *map) 
    :   _map(map) {

}

std::vector<Vec2> VisibilityMapper::map(Vec2 point, float radius) {
    auto &casters = _map->getVisionBlockers();           
    std::vector<Vec2> verts;

    for (const Rect &rect : casters) {
        if (!circleOverlapsRect(point, radius, rect)) {
            continue;
        }

        // Project the vertices of the rectangle 
        project(verts, point, radius, rect.topLeft(), rect.topRight());       
        project(verts, point, radius, rect.topRight(), rect.botRight());
        project(verts, point, radius, rect.botRight(), rect.botLeft());
        project(verts, point, radius, rect.botLeft(), rect.topLeft());
    }

    return verts;
}


inline bool VisibilityMapper::circleOverlapsRect(Vec2 pt, float radius, 
                                                 const Rect &rect) {
    return rect.contains(pt) ||
           lineIntersectsCircle(pt, radius, rect.topLeft(), rect.topRight()) ||
           lineIntersectsCircle(pt, radius, rect.topRight(), rect.botRight())||
           lineIntersectsCircle(pt, radius, rect.botRight(), rect.botLeft()) ||
           lineIntersectsCircle(pt, radius, rect.botLeft(), rect.topLeft());
}

inline bool VisibilityMapper::lineIntersectsCircle(Vec2 pt, float radius, 
                                                   Vec2 l1, Vec2 l2) {
    l1 -= pt;
    l2 -= pt;

    /* Implementation from:
     * http://mathworld.wolfram.com/Circle-LineIntersection.html
     */
    float dx = l2.x - l1.x;
    float dy = l2.y - l1.y;
    float dr = sqrt(dx*dx + dy*dy);
    float D = (l1.x * l2.y) - (l2.x * l1.y);

    float incidence = (radius * radius * dr * dr) - (D * D);
    return incidence >= 0.f;
}

inline void VisibilityMapper::project(std::vector<Vec2> &verts, Vec2 cpt, 
                                      float radius, Vec2 pt1, Vec2 pt2) {
    Vec2 diffPt1 = pt1 - cpt;
    Vec2 diffPt2 = pt2 - cpt;

    diffPt1.Normalize();
    diffPt2.Normalize();
    
    diffPt1 *= 100.f;
    diffPt2 *= 100.f;

    Vec2 proj1 = pt1 + diffPt1;
    Vec2 proj2 = pt2 + diffPt2;

    verts.push_back(pt1);
    verts.push_back(pt2);
    verts.push_back(proj1);

    verts.push_back(proj1);
    verts.push_back(proj2);
    verts.push_back(pt2);
}
