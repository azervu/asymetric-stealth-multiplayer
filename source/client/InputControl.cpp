#include "InputControl.h"
#include "ClientEvents.h"
#include "InputState.h"
#include "ClientServiceLocator.h"
#include "Window.h"
#include "ResourceManager.h"//@TODO debug
InputControl::InputControl() {

    EventListenerDelegate keyEventListenerDelegate;
    keyEventListenerDelegate.bind(this, &InputControl::keyPress);
    ServiceLocator::singleton()->getEventManager()->addListener(keyEventListenerDelegate, KeyEvent::eventType);
    EventListenerDelegate mouseEventListenerDelegate;
    mouseEventListenerDelegate.bind(this, &InputControl::mouseMovement);
    ServiceLocator::singleton()->getEventManager()->addListener(mouseEventListenerDelegate, MouseMotionEvent::eventType);

    _keyMap[SDLK_w] = std::make_pair(MOVE_UP, false);
    _commandMap[MOVE_UP] = SDLK_w;

    _keyMap[SDLK_a] = std::make_pair(MOVE_LEFT, false);
    _commandMap[MOVE_LEFT] = SDLK_a;

    _keyMap[SDLK_s] = std::make_pair(MOVE_DOWN, false);
    _commandMap[MOVE_DOWN] = SDLK_s;

    _keyMap[SDLK_d] = std::make_pair(MOVE_RIGHT, false);
    _commandMap[MOVE_RIGHT] = SDLK_d;
}

InputControl::~InputControl() {}

void InputControl::mouseMovement(const EventData* eventData) {
    MouseMotionEvent* mouseData = (MouseMotionEvent*) eventData;
    Vec2 pos = mouseData->getMousePosition();

    Window *window = ClientServiceLocator::singleton()->getWindow();
    auto winSize = window->getSize();

    _turnCommand._type = Command::Type::AIM;
    _turnCommand._vector = pos;
    _turnCommand._vector.x -= winSize.first / 2.f;
    _turnCommand._vector.y -= winSize.second / 2.f;

}

void InputControl::checkMovement(Command& command) {
    if(_keyMap[_commandMap[MOVE_UP]].second||_keyMap[_commandMap[MOVE_DOWN]].second||_keyMap[_commandMap[MOVE_LEFT]].second||_keyMap[_commandMap[MOVE_RIGHT]].second) {
        command._type = Command::Type::MOVE;
    } else {
        command._type = Command::Type::STOP;
    }
    command._vector.Normalize();
}

void InputControl::keyPress(const EventData* eventData) {

    




    KeyEvent* keyData = (KeyEvent*)eventData;
    _keyMap[keyData->getKeyCode()].second = keyData->isKeyDown();

    switch(_keyMap[keyData->getKeyCode()].first) {
    case MOVE_UP:
        if(keyData->isKeyDown()) {
            _moveCommand._vector.y = -1.0f;
        } else {
            _moveCommand._vector.y = 0.0f;
        }
        checkMovement(_moveCommand);
        break;
    case MOVE_DOWN:
        if(keyData->isKeyDown()) {
            _moveCommand._vector.y = 1.0f;
        } else {
            _moveCommand._vector.y = 0.0f;
        }
        checkMovement(_moveCommand);
        break;
    case MOVE_LEFT:
        if(keyData->isKeyDown()) {
            _moveCommand._vector.x = -1.0f;
        } else {
            _moveCommand._vector.x = 0.0f;
        }
        checkMovement(_moveCommand);
        break;
    case MOVE_RIGHT:
        if(keyData->isKeyDown()) {
            _moveCommand._vector.x = 1.0f;
        } else {
            _moveCommand._vector.x = 0.0f;
        }
        checkMovement(_moveCommand);
        break;
    }
}

std::vector<Command> InputControl::getCommands() {
    _commands.push_back(_moveCommand);
    _commands.push_back(_turnCommand);
    std::vector<Command> tmp = _commands;
    _commands.clear();
    return tmp;
}
