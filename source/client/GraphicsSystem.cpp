/**
 * @file    source/client/GraphicsSystem.cpp
 *
 * @brief   Implements the graphics system class.
 */

#include "GraphicsSystem.h"

#include "OpenGLUtility.h"
#include "PathTranslator.h"
#include "Window.h"
#include "Camera.h"
#include "Timer.h"

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace Renderer {
    ShaderHandler* shaderHandler;
    ResourceManager* resourceManager;
}

/*
 * Layer
 */

Renderer::Layer::Layer() {
    glGenVertexArrays(1, &this->_vao);
    glGenBuffers(1, &this->_vbo);
}

Renderer::Layer::~Layer() {
    glDeleteVertexArrays(1, &this->_vao);
    glDeleteBuffers(1, &this->_vbo);
}

void Renderer::Layer::updateUniform(const glm::mat4 &projectionMatrix, const glm::mat4 &viewMatrix) {
    glUseProgram(this->_shader->getProgramID());

    glUniformMatrix4fv(
        this->_projectionUniform,
        1,
        GL_FALSE,
        glm::value_ptr(projectionMatrix)
    );

    glUniformMatrix4fv(
        this->_viewUniform,
        1,
        GL_FALSE,
        glm::value_ptr(viewMatrix)
    );

    glUseProgram(0);
}

/*
 * MapLayer
 */

Renderer::MapLayer::MapLayer(const std::string& tileset)
    :   Layer() {
    
    this->_shader = shaderHandler->getShaderProgram("shader/map.json");
    this->_testTexture = (Texture*) resourceManager->getLoadResource(ResourceType::TEXTURE, tileset);

    assert(this->_shader != nullptr);

    this->_projectionUniform = this->_shader->getRefrences("projection");
    this->_viewUniform = this->_shader->getRefrences("view");

    this->_textureSheetUniform = this->_shader->getRefrences("textureSheet");
    this->_texSizeUniform = this->_shader->getRefrences("texSize");
    this->_texTileSizeUniform = this->_shader->getRefrences("texTileSize");

    this->_tileSizeUniform = this->_shader->getRefrences("tileSize");

    this->_tintUniform = this->_shader->getRefrences("tint");
    this->_zDepthUniform = this->_shader->getRefrences("zDepth");

    glUseProgram(this->_shader->getProgramID());

    auto texSize = this->_testTexture->getSize();

    // The texture contains 16 by 16 tiles.
    glUniform2f(
        this->_texTileSizeUniform,
        16.f, 
        16.f
    );

    // The texture terrain.png is 256x256 pixels.
    glUniform2f(
        this->_texSizeUniform,
        (float) texSize.first,
        (float) texSize.second
    );

    // Tiles are TILE_SIZE x TILE_SIZE pixels.
    glUniform1f(
        this->_tileSizeUniform,
        ASM_TILE_SIZE
    );

    glUniform3f(
        this->_tintUniform,
        1.f, 1.f, 1.f
    );

    // @TODO some proper values or remove?
    glUniform1f(
        this->_zDepthUniform,
        0.2f
    );

    GLint positionAttribute = this->_shader->getRefrences("position");
    GLint texPosAttribute = this->_shader->getRefrences("texPos");

    glBindVertexArray(this->_vao);
    glBindBuffer(GL_ARRAY_BUFFER, this->_vbo);

    glEnableVertexAttribArray(positionAttribute);
    glEnableVertexAttribArray(texPosAttribute);

    glVertexAttribPointer(positionAttribute, 2, GL_FLOAT, GL_FALSE, sizeof(MapData), 0);
    glVertexAttribPointer(texPosAttribute, 2, GL_FLOAT, GL_FALSE, sizeof(MapData), (const GLvoid*) (sizeof(glm::vec2)));

    glUseProgram(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

Renderer::MapLayer::~MapLayer() {
    delete this->_testTexture;
}

void Renderer::MapLayer::draw() {
    if (this->_size > 0) {
        glUseProgram(this->_shader->getProgramID());
        glBindVertexArray(this->_vao);
        glBindBuffer(GL_ARRAY_BUFFER, this->_vbo);

        glUniform1i(
            this->_textureSheetUniform,
            this->_testTexture->getTextureUnit()
        );

        glDrawArrays(GL_POINTS, 0, this->_size);
    }
}

void Renderer::MapLayer::updateData(const std::vector<MapData> &buffer) {
    this->_size = buffer.size();

    if (this->_size == 0) {
        return;
    }

    glBindBuffer(GL_ARRAY_BUFFER, this->_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(MapData) * buffer.size(), &buffer.front(), GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

/*
 * DrawableLayer
 */

Renderer::DrawableLayer::DrawableLayer()
    :   Layer() {

    this->_shader = shaderHandler->getShaderProgram("shader/entity.json");

    assert(this->_shader != nullptr);

    this->_projectionUniform = this->_shader->getRefrences("projection");
    this->_viewUniform = this->_shader->getRefrences("view");

    this->_tintUniform = this->_shader->getRefrences("tint");
    this->_texUniform = this->_shader->getRefrences("tex");
    this->_texSizeUniform = this->_shader->getRefrences("texSize");

    glUseProgram(this->_shader->getProgramID());

    glUniform3f(
        this->_tintUniform,
        1.0f, 1.0f, 1.0f
    );

    GLint positionAttribute = this->_shader->getRefrences("position");
    GLint anchorAttribute = this->_shader->getRefrences("anchor");
    GLint sizeAttribute = this->_shader->getRefrences("size");
    GLint angleAttribute = this->_shader->getRefrences("angle");
    GLint zDepthAttribute = this->_shader->getRefrences("zDepth");
    GLint texPosAttribute = this->_shader->getRefrences("texPos");
    GLint texTileAttribute = this->_shader->getRefrences("texTile");

    glBindVertexArray(this->_vao);
    glBindBuffer(GL_ARRAY_BUFFER, this->_vbo);

    glEnableVertexAttribArray(zDepthAttribute);
    glEnableVertexAttribArray(positionAttribute);
    glEnableVertexAttribArray(sizeAttribute);
    glEnableVertexAttribArray(anchorAttribute);
    glEnableVertexAttribArray(angleAttribute);
    glEnableVertexAttribArray(texPosAttribute);
    glEnableVertexAttribArray(texTileAttribute);

    size_t dataSize = sizeof(Drawable::DrawableData);

    glVertexAttribPointer(zDepthAttribute,      1, GL_FLOAT, GL_FALSE, dataSize, (const GLvoid*) 0);
    glVertexAttribPointer(positionAttribute,    2, GL_FLOAT, GL_FALSE, dataSize, (const GLvoid*) (1 * sizeof(float)));
    glVertexAttribPointer(sizeAttribute,        2, GL_FLOAT, GL_FALSE, dataSize, (const GLvoid*) (3 * sizeof(float)));
    glVertexAttribPointer(anchorAttribute,      2, GL_FLOAT, GL_FALSE, dataSize, (const GLvoid*) (5 * sizeof(float)));
    glVertexAttribPointer(angleAttribute,       1, GL_FLOAT, GL_FALSE, dataSize, (const GLvoid*) (7 * sizeof(float)));
    glVertexAttribPointer(texPosAttribute,      2, GL_FLOAT, GL_FALSE, dataSize, (const GLvoid*) (8 * sizeof(float)));
    glVertexAttribPointer(texTileAttribute,     2, GL_FLOAT, GL_FALSE, dataSize, (const GLvoid*) (10 * sizeof(float)));

    glUseProgram(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

Renderer::DrawableLayer::~DrawableLayer() {
}

void Renderer::DrawableLayer::draw() {
    glBindVertexArray(this->_vao);
    glUseProgram(this->_shader->getProgramID());
    glBindBuffer(GL_ARRAY_BUFFER, this->_vbo);

    for (const auto& segment : _renderSegments) {

        Texture* texture = (Texture*) Renderer::resourceManager->getResource(ResourceType::TEXTURE, segment.id);

        glUniform1i(
            this->_texUniform,
            texture->getTextureUnit()
        );

        auto size = texture->getSize();

        glUniform2f(
            this->_texSizeUniform,
            (float) size.first,
            (float) size.second
        );

        glDrawArrays(GL_POINTS, segment.start, segment.size);

        delete texture;
    }

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glUseProgram(0);
    glBindVertexArray(0);
}

void Renderer::DrawableLayer::updateData(
        const std::vector<Drawable::DrawableData>::iterator& from,
        const std::vector<Drawable::DrawableData>::iterator& to) {
    if (from == to) {
        this->_size = 0;
        return;
    }

    std::vector<Drawable::DrawableData> temp {from, to};

    std::sort(temp.begin(), temp.end(), [] (const Drawable::DrawableData& a, const Drawable::DrawableData& b) {
        return a._data.image.textureId < b._data.image.textureId;
    });

    _renderSegments.clear();
    size_t lastId = 0;
    for (size_t i = 0; i < temp.size(); i++) {
        if (temp[i]._data.image.textureId != lastId) {
            lastId = temp[i]._data.image.textureId;

            if (_renderSegments.size() > 0) {
                auto& prev = _renderSegments[_renderSegments.size() - 1];
                prev.size = i - prev.start;
            }

            TextureSortData data;
            data.id = lastId;
            data.start = i;

            _renderSegments.push_back(data);
        }
    }

    auto& last = _renderSegments[_renderSegments.size() - 1];
    last.size = temp.size() - last.start;

    glBindBuffer(GL_ARRAY_BUFFER, this->_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Drawable::DrawableData) * temp.size(), &temp.front(), GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

/*
 * DebugLayer
 */

Renderer::DebugLayer::DebugLayer(int vertexPerObject)
    :   Layer(),
        _vertexPerObject(vertexPerObject) {

    this->_shader = shaderHandler->getShaderProgram("shader/debug.json");

    assert(this->_shader != nullptr);

    this->_projectionUniform = this->_shader->getRefrences("projection");
    this->_viewUniform = this->_shader->getRefrences("view");

    GLint vertexAttribute = this->_shader->getRefrences("vertex");
    GLint colorAttribute = this->_shader->getRefrences("color");

    glBindVertexArray(this->_vao);
    glBindBuffer(GL_ARRAY_BUFFER, this->_vbo);

    glEnableVertexAttribArray(vertexAttribute);
    glEnableVertexAttribArray(colorAttribute);
    
    glVertexAttribPointer(vertexAttribute, 3, GL_FLOAT, GL_FALSE, sizeof(Drawable::Vertex), (const GLvoid*) 0);
    glVertexAttribPointer(colorAttribute, 4, GL_FLOAT, GL_FALSE, sizeof(Drawable::Vertex), (const GLvoid*) (3 * sizeof(float)));

    glUseProgram(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

Renderer::DebugLayer::~DebugLayer() {
}

void Renderer::DebugLayer::draw() {
    glBindVertexArray(this->_vao);
    glUseProgram(this->_shader->getProgramID());
    glBindBuffer(GL_ARRAY_BUFFER, this->_vbo);

    switch (_vertexPerObject) {
        case 1:
            glDrawArrays(GL_POINTS, 0, this->_size);
        break;

        case 2:
            glDrawArrays(GL_LINES, 0, this->_size);
        break;

        case 3:
            glDrawArrays(GL_TRIANGLES, 0, this->_size);
        break;
    }

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glUseProgram(0);
    glBindVertexArray(0);
}

// @TODO optimize
void Renderer::DebugLayer::updateData(
        const std::vector<Drawable::DrawableData>::iterator& from,
        const std::vector<Drawable::DrawableData>::iterator& to) {
    if (from == to) {
        this->_size = 0;
        return;
    }

    std::vector<Drawable::DrawableData> temp {from, to};

    std::vector<Drawable::Vertex> data; 

    for (Drawable::DrawableData &drawable : temp) {
        switch (_vertexPerObject) {
            case 1:
                data.push_back(drawable._data.point.pos[0]);
            break;

            case 2:
                data.push_back(drawable._data.line.pos[0]);
                data.push_back(drawable._data.line.pos[1]);
            break;

            case 3:
                data.push_back(drawable._data.triangle.pos[0]);
                data.push_back(drawable._data.triangle.pos[1]);
                data.push_back(drawable._data.triangle.pos[2]);
            break;
        }
    }

    this->_size = data.size();

    glBindBuffer(GL_ARRAY_BUFFER, this->_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Drawable::Vertex) * data.size(), &data.front(), GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

/*
 * LosLayer
 */

Renderer::LosLayer::LosLayer(double viewRadius)
    :   DebugLayer(3),
        _viewRadius(viewRadius) {
    glGenVertexArrays(1, &this->_vaoCircle);
    glGenBuffers(1, &this->_vboCircle);

    GLint vertexAttribute = this->_shader->getRefrences("vertex");
    GLint colorAttribute = this->_shader->getRefrences("color");

    glBindVertexArray(this->_vaoCircle);
    glBindBuffer(GL_ARRAY_BUFFER, this->_vboCircle);

    glEnableVertexAttribArray(vertexAttribute);
    glEnableVertexAttribArray(colorAttribute);

    glVertexAttribPointer(vertexAttribute, 3, GL_FLOAT, GL_FALSE, sizeof(Drawable::Vertex), (const GLvoid*) 0);
    glVertexAttribPointer(colorAttribute, 4, GL_FLOAT, GL_FALSE, sizeof(Drawable::Vertex), (const GLvoid*) (3 * sizeof(float)));

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

Renderer::LosLayer::~LosLayer() {
    glDeleteVertexArrays(1, &this->_vaoCircle);
    glDeleteBuffers(1, &this->_vboCircle);
}

void Renderer::LosLayer::draw() {
    
    glUseProgram(this->_shader->getProgramID());
    
    glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

    glStencilMask(0x1);
    glDepthMask(GL_FALSE);
    glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);

    glClearStencil(0);
    glClear(GL_STENCIL_BUFFER_BIT);

    glStencilFunc(GL_ALWAYS, 0x1, 0x1);
    glBindVertexArray(this->_vaoCircle);
    glBindBuffer(GL_ARRAY_BUFFER, this->_vboCircle);
    glDrawArrays(GL_TRIANGLES, 0, this->_viewVertexAmount);

    glStencilFunc(GL_ALWAYS, 0x0, 0x1);
    glBindVertexArray(this->_vao);
    glBindBuffer(GL_ARRAY_BUFFER, this->_vbo);
    glDrawArrays(GL_TRIANGLES, 0, this->_size);

    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
    glDepthMask(GL_TRUE);
    glStencilMask(0x0);

    glStencilFunc(GL_EQUAL, 0x1, 0x1);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glUseProgram(0);
    glBindVertexArray(0);
}

void Renderer::LosLayer::updateLos(Vec2 cameraPos) {
    std::vector<Drawable::Vertex> circle;

    static const double spacing = 8.5;

    const int numSegments = ceil((this->_viewRadius * 2.0 * glm::pi<double>()) / spacing) + 0.1;
    const double angle = (2.0 * glm::pi<double>()) / (double) numSegments;

    for (int i = 0; i < numSegments; i++) {
        float x = glm::cos<double>(angle * i) * this->_viewRadius + cameraPos.x;
        float y = glm::sin<double>(angle * i) * this->_viewRadius + cameraPos.y;

        float xb = glm::cos<double>(angle * (i + 1)) * this->_viewRadius + cameraPos.x;
        float yb = glm::sin<double>(angle * (i + 1)) * this->_viewRadius + cameraPos.y;

        Drawable::Vertex vertexa = {
            x,
            y,
            1.0f,
            1.0f,
            1.0f,
            1.0f,
            1.0f
        };

        circle.push_back(vertexa);

        Drawable::Vertex vertexb = {
            xb,
            yb,
            1.0f,
            1.0f,
            1.0f,
            1.0f,
            1.0f
        };

        circle.push_back(vertexb);

        Drawable::Vertex vertexc = {
            cameraPos.x,
            cameraPos.y,
            1.0f,
            1.0f,
            1.0f,
            1.0f,
            1.0f
        };

        circle.push_back(vertexc);
    }

    this->_viewVertexAmount = circle.size();

    glBindBuffer(GL_ARRAY_BUFFER, this->_vboCircle);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Drawable::Vertex) * circle.size(), &circle.front(), GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

GraphicsSystem::GraphicsSystem() {
    _clientInitDelegate.bind(this, &GraphicsSystem::onClientInitComplete);
    _levelLoadedDelegate.bind(this, &GraphicsSystem::onLevelLoadingComplete);
    _winResizeDelegate.bind(this, &GraphicsSystem::onWindowResized);
    _viewFrameChangeDelegate.bind(this, &GraphicsSystem::onViewFrameChanged);

    EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();
    emgr->addListener(_clientInitDelegate, ClientInitiationCompleteEvent::eventType);
    emgr->addListener(_levelLoadedDelegate, LevelLoadingCompleteEvent::eventType);
    emgr->addListener(_winResizeDelegate, WindowResizedEvent::eventType);
    emgr->addListener(_viewFrameChangeDelegate, CameraViewFrameChangedEvent::eventType);

    this->_resourceManager = ClientServiceLocator::singleton()->getResourceManager();

    // Force the rendering thread to update the viewport in the first draw cycle
    _dirtyViewport = true;
}

GraphicsSystem::~GraphicsSystem() {
    EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();
    emgr->removeListener(_clientInitDelegate);
    emgr->removeListener(_levelLoadedDelegate);
    emgr->removeListener(_winResizeDelegate);
    emgr->removeListener(_viewFrameChangeDelegate);

    // @TODO wait for renderer to finish?
    this->_continueThread = false;
    this->_graphicsThread.join();

    for (auto layer : this->_layers) {
        delete layer;
    }

    delete this->_shaderHandler;
    
    SDL_GL_DeleteContext(this->_context);
}

bool GraphicsSystem::init(Settings* settings) {
    /* POINT A: Wait for the worker thread to initialize video */
    _initState = InitState::WORK_THR_INIT_VIDEO;
    Log::debug("Creating rendering thread");
    this->_continueThread.store(true);
    this->_graphicsThread = std::thread(&GraphicsSystem::renderThread, this);

    /* POINT B: Create the window */
    if (!waitForInitState(InitState::MAIN_THR_INIT_WINDOW)) {
        return false;
    }

    Log::info("POINT B: Creating window from main thread");

    // Retrieve the display mode
    SDL_DisplayMode displayMode;
    int errorCode = SDL_GetCurrentDisplayMode(0, &displayMode);
    if (errorCode != 0) {
        Log::error("Failed to get the first display mode: %s", SDL_GetError());
        _refreshRate = 60;
    } else {
        _refreshRate = displayMode.refresh_rate;
        if (_refreshRate == 0) {
            _refreshRate = 60;
            Log::info("SDL set refresh rate to 0, assuming 60.");
        }

        Log::info("Refresh rate set to: %d", _refreshRate);
    }

    ClientServiceLocator::singleton()->getWindow()->createWindow();

    /* POINT C: Tell the worker thread to continue, wait for it to finish */
    _initState = InitState::WORK_THR_INIT_CONTEXT;
    if (!waitForInitState(InitState::GRAPHICS_INIT_COMPLETE)) {
        return false;
    }

    // The graphics are now fully initialized, but it cannot start it's 
    // rendering loop until all subsystems and services are initialized.
    _initState = InitState::WAIT_FOR_SYSTEM_INIT;

    Log::debug("Graphics initialization completed");
    return true;
}

void GraphicsSystem::entityUpdate(DeltaTime delta, const std::vector<Drawable::BaseDrawable*>& buffer) {
    std::lock_guard<std::mutex> lock(this->_backBufferMutex);

    copyCameraTransform();

    _backBufferEntity.clear();
    
    for (auto drawable : buffer) {
        auto list = drawable->getRenderData();
        for (auto &d : list) {
            _backBufferEntity.push_back(d);
        }

        delete drawable;
    }

    std::sort(_backBufferEntity.begin(), _backBufferEntity.end(), [] (const Drawable::DrawableData& a, const Drawable::DrawableData& b) {
        return a._dataType < b._dataType;
    });

    for (int &i : _backBufferSegmentStart) {
        i = -1;
    }

    for (int &i : _backBufferSegmentEnd) {
        i = -1;
    }

    for (int i = 0; i < _backBufferEntity.size(); i++) {
        int type = (int) _backBufferEntity[i]._dataType;

        if (_backBufferSegmentStart[type] == -1) {
            _backBufferSegmentStart[type] = i;
        }

        if (_backBufferSegmentEnd[type] <= i) {
            _backBufferSegmentEnd[type] = i + 1;
        }
    }

    for (int &i : _backBufferSegmentStart) {
        if (i == -1) {
            i = 0;
        }
    }

    for (int &i : _backBufferSegmentEnd) {
        if (i == -1) {
            i = 0;
        }
    }
};

void GraphicsSystem::mapUpdate(const Map& map, const std::vector<int>& deltaBackground, const std::vector<int>& deltaForeground) {
    std::lock_guard<std::mutex> lock(this->_backBufferMutex);

    const MapLayer* bgLayer = map.getBackgroundLayer();
    for (const int index : deltaBackground) {
        auto pos = this->_backgroundMapping.find(index);

        Renderer::MapData data;

        data.pos.x = (float) (index % bgLayer->getWidth());
        data.pos.y = (float) (index / bgLayer->getWidth());

        int texIndex = bgLayer->getTileTexIndex((int) data.pos.x, (int) data.pos.y) - 1;
        data.texPos = glm::vec2((float) (texIndex % 16), (float) (texIndex / 16));

        if (pos == this->_backgroundMapping.end()) {
            this->_backgroundMapping[index] = this->_backBufferBackground.size();
            this->_backBufferBackground.push_back(data);
        } else {
            this->_backBufferBackground.at((*pos).second).texPos = data.texPos;
        }
    }

    const MapLayer* fgLayer = map.getForegroundLayer();
    for (const int index : deltaForeground) {
        auto pos = this->_foregroundMapping.find(index);

        Renderer::MapData data;

        data.pos.x = (float) (index % fgLayer->getWidth());
        data.pos.y = (float) (index / fgLayer->getWidth());

        int texIndex = fgLayer->getTileTexIndex((int) data.pos.x, (int) data.pos.y) - 1;
        data.texPos = glm::vec2((float) (texIndex % 16), (float) (texIndex / 16));

        if (pos == this->_foregroundMapping.end()) {
            this->_foregroundMapping[index] = this->_backBufferForeground.size();
            this->_backBufferForeground.push_back(data);
        } else {
            this->_backBufferForeground.at((*pos).second).texPos = data.texPos;
        }
    }

    this->_mapChange = true;
}

void GraphicsSystem::mapSet(const Map* map) {
    std::lock_guard<std::mutex> lock(this->_backBufferMutex);

    this->_foregroundMapping.clear();
    this->_backgroundMapping.clear();
    this->_backBufferBackground.clear();
    this->_backBufferForeground.clear();

    const MapLayer* bgLayer = map->getBackgroundLayer();

    int bgW = bgLayer->getWidth();
    int bgH = bgLayer->getHeight();

    for (int x = 0; x < bgW; x++) {
        for (int y = 0; y < bgH; y++) {
            Renderer::MapData data;

            int index = x + (y * bgW);

            data.pos.x = (float) x;
            data.pos.y = (float) y;
            int texIndex = bgLayer->getTileTexIndex((int) data.pos.x, (int) data.pos.y) - 1;
            data.texPos = glm::vec2((float) (texIndex % 16), (float) (texIndex / 16));

            this->_backgroundMapping[index] = this->_backBufferBackground.size();
            this->_backBufferBackground.push_back(data);
        }
    }

    const MapLayer* fgLayer = map->getForegroundLayer();

    int fgW = fgLayer->getWidth();
    int fgH = fgLayer->getHeight();

    for (int x = 0; x < fgW; x++) {
        for (int y = 0; y < fgH; y++) {
            Renderer::MapData data;

            int index = x + (y * fgW);

            data.pos.x = (float) x;
            data.pos.y = (float) y;
            int texIndex = fgLayer->getTileTexIndex((int) data.pos.x, (int) data.pos.y) - 1;
            data.texPos = glm::vec2((float) (texIndex % 16), (float) (texIndex / 16));

            this->_foregroundMapping[index] = this->_backBufferForeground.size();
            this->_backBufferForeground.push_back(data);
        }
    }

    this->_mapChange = true;
}

void GraphicsSystem::onClientInitComplete(const EventData *e) {
    // All subsystems and services has been initialized, the rendering thread
    // may continue at will.
    if (_initState == InitState::WAIT_FOR_SYSTEM_INIT) {
        _initState = InitState::SYSTEM_INIT_COMPLETE;
    } else {
        Log::fatal("GraphicsSystem::onClientInitComplete: Expected _initState "
                   "to be WAIT_FOR_SYSTEM_INIT, found something else. "
                   "Joakim has got some debugging to do!");
        _initState = InitState::INIT_ERROR;
    }
}

void GraphicsSystem::onLevelLoadingComplete(const EventData *e) {
    const LevelLoadingCompleteEvent* data = static_cast<const LevelLoadingCompleteEvent*>(e);

    this->mapSet(data->getMap());
}

void GraphicsSystem::onWindowResized(const EventData *e) {
    /* Note that while the viewport is extremely likely to require an update
     * after a window resize, the Camera issues a CameraViewportChangedEvent
     * when it does.
     */
    WindowResizedEvent *evt = (WindowResizedEvent*)e;
    std::pair<int, int> size = evt->getSize();

    glm::mat4 projTmp = glm::ortho(0.f, (float) size.first, (float) size.second, 0.f);
    // The rendering thread will update the viewport at it's earliest 
    // convenience.
    _dirtyViewport = true;
}

void GraphicsSystem::onViewFrameChanged(const EventData *e) {
    Camera *camera = ClientServiceLocator::singleton()->getCamera();
    Vec2 frame = camera->getViewFrame();
    frame *= (float)ASM_PIXELS_PER_METER;

    glm::mat4 projTmp = glm::ortho(0.f, frame.x, frame.y, 0.f);

    // Lock the rendering thread as briefly as possible
    _eventMutex.lock();
    this->_projection = projTmp;
    _eventMutex.unlock();
}

bool GraphicsSystem::waitForInitState(InitState state) {
    // Avoid atomic locking operations as much as possible. "Cache" the value.
    InitState currentState = _initState;

    while (currentState != state && currentState != InitState::INIT_ERROR) {
        SDL_Delay(10);
        currentState = _initState;
    }

    return currentState != InitState::INIT_ERROR;
}


void GraphicsSystem::renderThread() {
    Log::info("Graphics thread initialized");

    int lastDrawTime = SDL_GetTicks();
    int currentTime;

    bool openGLInited = this->initOpenGL();

    if (!openGLInited) {
        Log::fatal("GraphicsSystem::renderThread: Initialization of OpenGL failed");
        _continueThread = false;
        _initState = InitState::INIT_ERROR;
        return;
    }

    bool shadersInited = this->initShaders();

    if (!shadersInited) {
        Log::fatal("GraphicsSystem::renderThread: Initialization of shaders failed");
        _continueThread = false;
        _initState = InitState::INIT_ERROR;
        return;
    }

    Renderer::LosLayer* losLayer = new Renderer::LosLayer(256.);

    Renderer::MapLayer* backgroundLayer = new Renderer::MapLayer("terrain.png");
    Renderer::MapLayer* foregroundLayer = new Renderer::MapLayer("terrain.png");

    Renderer::MapLayer* backgroundBLayer = new Renderer::MapLayer("terrain_blueprint.png");
    Renderer::MapLayer* foregroundBLayer = new Renderer::MapLayer("terrain_blueprint.png");

    Renderer::DrawableLayer* drawableLayer = new Renderer::DrawableLayer();
    Renderer::DrawableLayer* guiLayer = new Renderer::DrawableLayer();

    Renderer::DebugLayer* triangleLayer = new Renderer::DebugLayer(3);
    Renderer::DebugLayer* lineLayer = new Renderer::DebugLayer(2);
    Renderer::DebugLayer* pointLayer = new Renderer::DebugLayer(1);

    this->_layers.push_back(losLayer);
    this->_layers.push_back(backgroundLayer);
    this->_layers.push_back(foregroundLayer);
    this->_layers.push_back(backgroundBLayer);
    this->_layers.push_back(foregroundBLayer);
    this->_layers.push_back(drawableLayer);
    this->_layers.push_back(guiLayer);
    this->_layers.push_back(triangleLayer);
    this->_layers.push_back(lineLayer);
    this->_layers.push_back(pointLayer);

    for (int& i : _backBufferSegmentStart) {
        i = 0;
    }

    for (int& i : _backBufferSegmentEnd) {
        i = 0;
    }

    /* POINT C: Tell the main thread initialization completed */
    _initState = InitState::GRAPHICS_INIT_COMPLETE;

    // Wait for ClientInitiationCompleteEvent to fire on the main thread before
    // we start the rendering loop.
    if (!waitForInitState(InitState::SYSTEM_INIT_COMPLETE)) {
        Log::error("Error occurred before SYSTEM_INIT_COMPLETE. "
                   "Render thread aborting.");
        return;
    }

    Log::debug("Render thread starting rendering-loop");

    Window *window = ClientServiceLocator::singleton()->getWindow();

    Timer fpsTimer;
    int frames = 0;

    while(_continueThread) {
        if (_dirtyViewport) {
            updateViewport();
        }

        {
            std::lock_guard<std::mutex> lock(this->_backBufferMutex);
            applyCameraTransform();

            // @TODO identical data guards for speedup?
            // @TODO proper sort of incoming Drawable for debug layer
            // 
            drawableLayer->updateData(
                this->_backBufferEntity.begin() + _backBufferSegmentStart[(int) Drawable::DrawableData::DataType::IMAGE],
                this->_backBufferEntity.begin() + _backBufferSegmentEnd[(int) Drawable::DrawableData::DataType::IMAGE]
            );

            triangleLayer->updateData(
                this->_backBufferEntity.begin() + _backBufferSegmentStart[(int) Drawable::DrawableData::DataType::TRIANGLE],
                this->_backBufferEntity.begin() + _backBufferSegmentEnd[(int) Drawable::DrawableData::DataType::TRIANGLE]
            );

            lineLayer->updateData(
                this->_backBufferEntity.begin() + _backBufferSegmentStart[(int) Drawable::DrawableData::DataType::LINE],
                this->_backBufferEntity.begin() + _backBufferSegmentEnd[(int) Drawable::DrawableData::DataType::LINE]
            );

            pointLayer->updateData(
                this->_backBufferEntity.begin() + _backBufferSegmentStart[(int) Drawable::DrawableData::DataType::POINT],
                this->_backBufferEntity.begin() + _backBufferSegmentEnd[(int) Drawable::DrawableData::DataType::POINT]
            );

            losLayer->updateData(
                this->_backBufferEntity.begin() + _backBufferSegmentStart[(int) Drawable::DrawableData::DataType::LOS],
                this->_backBufferEntity.begin() + _backBufferSegmentEnd[(int) Drawable::DrawableData::DataType::LOS]
            );

            guiLayer->updateData(
                this->_backBufferEntity.begin() + _backBufferSegmentStart[(int) Drawable::DrawableData::DataType::OVERLAY],
                this->_backBufferEntity.begin() + _backBufferSegmentEnd[(int) Drawable::DrawableData::DataType::OVERLAY]
            );

            if (this->_mapChange) {
                this->_mapChange = false;
                backgroundLayer->updateData(this->_backBufferBackground);
                foregroundLayer->updateData(this->_backBufferForeground);
                backgroundBLayer->updateData(this->_backBufferBackground);
                foregroundBLayer->updateData(this->_backBufferForeground);
            }
        }

        currentTime = SDL_GetTicks();
        if(currentTime < (lastDrawTime + (1000 / this->_refreshRate))) {
            SDL_Delay(lastDrawTime + (1000 / this->_refreshRate) - currentTime);
        }
        
        float deltaTime = (currentTime - lastDrawTime) * 0.5f;

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

        // The projection matrix may be updated by the window resizing in the
        // middle of a draw-call. Copy the matrix before drawing any layers.
        _eventMutex.lock();
        glm::mat4 projectionCopy = this->_projection;
        _eventMutex.unlock();

        for (Renderer::Layer* layer : this->_layers) {
            layer->updateUniform(projectionCopy, this->_view);
            //layer->draw();
        }

        losLayer->updateLos(_cameraCenter);

        glEnable(GL_STENCIL_TEST);

        losLayer->draw();

        glEnable(GL_DEPTH_TEST);

        backgroundLayer->draw();
        foregroundLayer->draw();
        drawableLayer->draw();

        triangleLayer->draw();
        lineLayer->draw();
        pointLayer->draw();

        glStencilFunc(GL_EQUAL, 0x0, 0x1);

        backgroundBLayer->draw();
        foregroundBLayer->draw();

        glStencilFunc(GL_EQUAL, 0x1, 0x1);

        glDisable(GL_DEPTH_TEST);
        glDisable(GL_STENCIL_TEST);

        guiLayer->draw();


        window->swapBuffers();

        frames++;
        if (fpsTimer.getElapsedSeconds() >= 1.f) {
            Log::info("%i gfx frames/s", frames);
            fpsTimer.start();
            frames = 0;
        }
    }
}

bool GraphicsSystem::initOpenGL() {
    /* POINT A: Block main thread until init_video is called */
    if (!waitForInitState(InitState::WORK_THR_INIT_VIDEO)) {
        return false;
    }

    Log::info("POINT A: Initializing SDL_Video from worker thread");
    Log::debug("Initializing SDL video system");
    SDL_Init(SDL_INIT_VIDEO);

    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
    SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

    _initState = InitState::MAIN_THR_INIT_WINDOW;

    /* POINT C: Create the context once the window has been created */
    if (!waitForInitState(InitState::WORK_THR_INIT_CONTEXT)) {
        return false;
    }

    Log::info("POINT C: Creating OpenGL Context from worker thread");
    Log::debug("Creating context");

    Window *window = ClientServiceLocator::singleton()->getWindow();
    SDL_Window *sdlw = window->getSDLWindow();
    this->_context = SDL_GL_CreateContext(sdlw);
    if (this->_context == nullptr) {
        Log::fatal("GL Context not created. Reason: %s", SDL_GetError());
        return false;
    }

    //Initialize glew
    glewExperimental = GL_TRUE;

    GLenum glewStatus = glewInit();
    bool glewOpenGLError = OPENGL_ERROR_CHECK("GLEW");
    if (glewStatus != GLEW_OK && glewOpenGLError) {
        Log::fatal("GLEW Error: %s", glewGetErrorString(glewStatus));
        return false;
    } else {
        Log::debug("GLEW status OK");
    }

    int countDown = 20;
    while (!::glGetString(GL_VERSION)) {
        if (countDown == 0) {
            Log::fatal("Coudnt retrive OpenGL version SOMETHING IS WRONG.");
            return false;
        }

        Log::warning("Waiting for context");
        --countDown;
        std::this_thread::sleep_for(std::chrono::milliseconds(5));
    };

    //Print out some OpenGL details
    Log::debug("Vendor: %s", ::glGetString(GL_VENDOR));
    Log::debug("Renderer: %s", ::glGetString(GL_RENDERER));
    Log::debug("OpenGL version: %s", ::glGetString(GL_VERSION));

    //Set global OpenGL parameters
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glEnable(GL_STENCIL_TEST);
    glStencilFunc(GL_EQUAL, 1, 0x1);
    glStencilMask(0x0);

    glDisable(GL_CULL_FACE);
    //glCullFace(GL_BACK);

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    std::pair<int, int> winSize = window->getSize();
    glViewport(0, 0, winSize.first, winSize.second);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);


    return true;
}

bool GraphicsSystem::initShaders() {
    bool worked = true;
    Log::info("Started loading of shader files");

    this->_shaderHandler = new ShaderHandler();

    Renderer::resourceManager = this->_resourceManager;
    Renderer::shaderHandler = this->_shaderHandler;

    Window *window = ClientServiceLocator::singleton()->getWindow();
    std::pair<int, int> winSize = window->getSize();

    return worked;
}

void GraphicsSystem::copyCameraTransform() {
    Camera *camera = ClientServiceLocator::singleton()->getCamera();

    _cameraCenter = camera->getCenterPosition();
    _cameraCenter *= (float) ASM_PIXELS_PER_METER;

    _cameraTopLeft = camera->getTopLeftPosition();
    _cameraTopLeft *= (float)ASM_PIXELS_PER_METER;
}

void GraphicsSystem::applyCameraTransform() {
    glm::vec3 pos(-_cameraTopLeft.x, -_cameraTopLeft.y, 0.f);
    this->_view = glm::translate(glm::mat4(), pos);
}

void GraphicsSystem::updateViewport() {
    _dirtyViewport = false;

    Window *window = ClientServiceLocator::singleton()->getWindow();
    std::pair<int, int> winSize = window->getSize();

    glViewport(0, 0, winSize.first, winSize.second);
}
