/**
 * @file    source/client/Renderable.h
 *
 * @brief   Declares the renderable class.
 */

#pragma once

#include "SDLHeaders.h"

#include "Engine.h"

#include <Box2D/Box2D.h>
#include <glm/glm.hpp>

#include <vector>
#include <array>

class ResourceManager;

namespace Drawable {

    /**
     * @struct  DrawableData
     *
     * @brief   A drawable data. Is the actual data sent to the graphics card. Should never be
     *          created directly unless you know exactly what you are doing.
     */
    struct Vertex {
        float x, y, z;
        float r, g, b, a;
    };

    struct DrawableData {
        union {
            struct {
                float zDepth;
                float x, y;
                float w, h;
                float xA, yA;
                float angle;
                float texX, texY;
                float texW, texH;
                ResourceId textureId;
            } image;

            struct {
                Vertex pos[3];
            } triangle;

            struct {
                Vertex pos[2];
            } line;

            struct {
                Vertex pos[1];
            } point;
        } _data;

        enum class DataType : int {
            UNDEFINED,
            IMAGE,
            TRIANGLE,
            LINE,
            POINT,
            LOS,
            LIGHT,
            OVERLAY
        } _dataType = DataType::UNDEFINED;
    };

    class BaseDrawable {
    public:

        BaseDrawable();
        virtual ~BaseDrawable();

        virtual std::vector<DrawableData> getRenderData() = 0;

        /**
         * @fn  void BaseDrawable::tagOverride(DrawableData::DataType tag);
         *
         * @brief   Tag override. dirty hack. its also up to the subclasses to use them;
         *
         * @param   tag The tag.
         */

        void tagOverride(DrawableData::DataType tag);

    protected:
        DrawableData::DataType _tag;
    };

    class Geometry : public BaseDrawable {
    public:

        Geometry();
        virtual ~Geometry();

        /**
         * Add a vertex to this Geometry object. The X and Y members of the
         * vertex *MUST* be in metric coordinates.
         */
        void addVertex(Vertex vertex);

    protected:

        std::vector<Vertex> _data;
    };

    class Polygon : public Geometry {
    public:

        Polygon();
        virtual ~Polygon();

        virtual std::vector<DrawableData> getRenderData();
    };

    class GeometryCloud : public Geometry {
    public:
        enum class TYPE {
            TRIANGLE,
            LINE,
            POINT
        };

        GeometryCloud(TYPE type);
        virtual ~GeometryCloud();

        virtual std::vector<DrawableData> getRenderData();

    protected:
        TYPE _type;
    };

    class Image : public BaseDrawable {
    public:

        Image(float zDepth);
        virtual ~Image();

        void setPosition(b2Vec2 pos);
        void setSize(b2Vec2 size);
        void setAnchor(b2Vec2 anchor);
        void setAngle(float angle);

        virtual std::vector<DrawableData> getRenderData() = 0;

        DrawableData _drawableData;
    };

    class Sprite : public Image {
    public:

        Sprite(ResourceId spriteId, size_t frame, float zDepth);
        virtual ~Sprite();

        virtual std::vector<DrawableData> getRenderData();

    protected:

        ResourceId _spriteId;
        size_t _frame;
    };

    class Text : public Image {
    public:

        // @TODO add text parameters for look
        Text(const std::string& text, unsigned size);
        virtual ~Text();

        virtual std::vector<DrawableData> getRenderData();

    protected:


    };
}
