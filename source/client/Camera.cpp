/**
 * @file    source/client/Camera.cpp
 *
 * @brief   Implements the camera class.
 */

#include "Camera.h"

#include "ClientServiceLocator.h"
#include "Window.h"
#include "ClientEvents.h"


Camera::Camera() 
    :   _position(b2Vec2_zero),
        _rotation(0.f) {
    setVerticallyVisibleMeters(10.f);

    _winResizeDelegate.bind(this, &Camera::onWindowResized);

    EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();
    emgr->addListener(_winResizeDelegate, WindowResizedEvent::eventType);
}

Camera::~Camera() {

}

void Camera::onExit() {
    EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();
    emgr->removeListener(_winResizeDelegate);
}

void Camera::setPosition(Vec2 position) {
    _position = position;
}

void Camera::setVerticallyVisibleMeters(float meters) {
    Window *window = ClientServiceLocator::singleton()->getWindow();

    std::pair<int, int> winSize = window->getSize();
    float ar = (float) winSize.first / (float) winSize.second;
    
    _viewFrame.x = meters * ar;
    _viewFrame.y = meters;

    // Notify of the viewport update
    EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();
    emgr->queueEvent(new CameraViewFrameChangedEvent(_viewFrame));
}

void Camera::setRotation(float radians) {
    _rotation = radians;
}

Vec2 Camera::getTopLeftPosition() const {
    Vec2 pt = _position;
    pt.x -= _viewFrame.x / 2.f;
    pt.y -= _viewFrame.y / 2.f;
    return pt;
}

Vec2 Camera::getCenterPosition() const {
    return _position;
}

Vec2 Camera::getBottomRightPosition() const {
    Vec2 pt = _position;
    pt.x += _viewFrame.x / 2.f;
    pt.y += _viewFrame.y / 2.f;
    return pt;
}

Vec2 Camera::getViewFrame() const {
    return _viewFrame;
}

float Camera::getRotation() const {
    return _rotation;
}


void Camera::onWindowResized(const EventData *e) {
    // Adjust the viewport
    setVerticallyVisibleMeters(_viewFrame.y);
}
