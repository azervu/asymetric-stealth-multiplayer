/**
 * @file    source/client/ShaderHandler.cpp
 *
 * @brief   Implements the shader handler class.
 */

#include "ShaderHandler.h"

#include "Utility.h"
#include "OpenGLUtility.h"
#include "Log.h"
#include "ServiceLocator.h"
#include "PathTranslator.h"

#include <rapidjson/document.h>
#include <rapidjson/filestream.h>

#include <fstream>
#include <string>
#include <map>

static const char* VERTEX_KEY = "Vertex";
static const char* GEOMETRY_KEY = "Geometry";
static const char* FRAGMENT_KEY = "Fragment";

static const char* FRAG_DATA_KEY = "FragDataLocation";
static const char* FRAG_DATA_INDEX_KEY = "Index";
static const char* FRAG_DATA_NAME_KEY = "Name";

static const char* ATTRIBUTE_KEY = "Attribute";
static const char* UNIFORM_KEY = "Uniform";

// ---- ShaderProgram ----

ShaderProgram::ShaderProgram(GLuint programID) :
	_programID(programID),
	_vertexShader(-1),
    _geometryShader(-1),
	_fragmentShader(-1) {
}

ShaderProgram::~ShaderProgram () {
	glDeleteProgram(_programID);
}


GLuint ShaderProgram::getVertexShader() const{
	return this->_vertexShader;
}

GLuint ShaderProgram::getGeometryShader() const {
    return this->_geometryShader;
}

GLuint ShaderProgram::getFragmentShader() const{
	return this->_fragmentShader;
}

GLuint ShaderProgram::getProgramID() const{
	return this->_programID;
}

GLint ShaderProgram::getRefrences(const std::string &name) const {
    try {
        return this->_refrences.at(name);
    } catch (std::out_of_range e) {
        Log::error("ShaderProgram::getRefrences: %s does not exist in this shader program", name.c_str());
        return -1;
    }
}


void ShaderProgram::setShader(GLint shaderID, SHADERTYPE type) {
    std::lock_guard<std::mutex> lock(this->_threadLock);

	glAttachShader(this->_programID, shaderID);

	if (type == SHADERTYPE::VERTEX && this->_vertexShader == -1) {
		this->_vertexShader = shaderID;
    } else if (type == SHADERTYPE::GEOMETRY && this->_geometryShader == -1) {
        this->_geometryShader = shaderID;
    } else if (type == SHADERTYPE::FRAGMENT && this->_fragmentShader == -1) {
        this->_fragmentShader = shaderID;
    }
}

void ShaderProgram::createAttributeRefrence(const std::string &name) {
    std::lock_guard<std::mutex> lock(this->_threadLock);

	this->_refrences[name] = glGetAttribLocation(this->getProgramID(), name.c_str());
	if (this->_refrences[name] == -1) {
        Log::error("ShaderHanler: Could not get reference to Attribute: %s", name.c_str());
	}
}

void ShaderProgram::createUniformRefrence(const std::string &name) {
    std::lock_guard<std::mutex> lock(this->_threadLock);

	this->_refrences[name] = glGetUniformLocation(this->getProgramID(), name.c_str());
	if (this->_refrences[name] == -1) {
        Log::error("ShaderHanler: Could not get reference to Uniform: %s", name.c_str());
	}
}

// ---- Shader ----

Shader::Shader(GLint shaderID, SHADERTYPE type) :
	_shaderID(shaderID),
	_type(type) {
}

Shader::~Shader() {
	glDeleteShader(_shaderID);
}

GLuint Shader::getShaderID() const {
	return this->_shaderID;
}

SHADERTYPE Shader::getType() const {
	return this->_type;
}

// ---- ShaderHandler ----

ShaderHandler::ShaderHandler() {
}

ShaderHandler::~ShaderHandler() {
	for (auto &s : this->_shaders) {
		delete s;
	}

	for (auto &s : this->_shaderPrograms) {
		delete s;
	}
}

ShaderProgram * ShaderHandler::getShaderProgram(GLuint ID) {
    std::lock_guard<std::mutex> lock(this->_threadLock);

	try {
		return this->_IDtoShaderProgram.at(ID);
	} catch (std::out_of_range ofr) {
        Log::error("ShaderHandler::getShaderProgram: nonexistent shader: %u", ID);
        return nullptr;
	}
}

ShaderProgram * ShaderHandler::getShaderProgram(const std::string &path) {
    std::lock_guard<std::mutex> lock(this->_threadLock);

	try {
		return this->_IDtoShaderProgram.at(this->_fileToID.at(path));
	} catch (std::out_of_range ofr) {
        Log::debug("ShaderHandler::getShaderProgram: Shader not loaded: %s", path.c_str());
		return this->loadShaderProgram (path);
	}
}

ShaderProgram * ShaderHandler::loadShaderProgram(const std::string &path) {
    Log::debug("ShaderHandler::loadShaderProgram: Started loading Shader: %s", path.c_str());
    
    FILE* file = ServiceLocator::singleton()->getPathTranslator()->openCHandle(path);
    bool errors = false;

    if (!file) {
        return nullptr;
	}

    rapidjson::FileStream fileStream(file);
    rapidjson::Document document;
    document.ParseStream<0>(fileStream);

    fclose(file);

    bool validVertex = (document.HasMember(VERTEX_KEY) && document[VERTEX_KEY].IsString());
    bool validGeometry = (document.HasMember(GEOMETRY_KEY) && document[GEOMETRY_KEY].IsString());
    bool validFragment = (document.HasMember(FRAGMENT_KEY) && document[FRAGMENT_KEY].IsString());

    // at least 1 of 3 IS REQUIERD
    if ((!validVertex && !validGeometry && !validFragment) || !validFragment) {
        Log::error("ShaderHandler::loadShaderProgram: Invalid Shader, at least the fragment shader is required: %s", path.c_str());
        return nullptr;
    }

    // START
    ShaderProgram * program = new ShaderProgram(glCreateProgram());

    if (validVertex) {
        program->setShader(loadShader(document[VERTEX_KEY].GetString(), SHADERTYPE::VERTEX)->getShaderID(), SHADERTYPE::VERTEX);
    }

    if (validGeometry) {
        program->setShader(loadShader(document[GEOMETRY_KEY].GetString(), SHADERTYPE::GEOMETRY)->getShaderID(), SHADERTYPE::GEOMETRY);
    }

    if (validFragment) {
        program->setShader(loadShader(document[FRAGMENT_KEY].GetString(), SHADERTYPE::FRAGMENT)->getShaderID(), SHADERTYPE::FRAGMENT);
    }
    

    // PRE
    // @TODO replace with schema validation?
    if (document.HasMember(FRAG_DATA_KEY)) {
        if (!document[FRAG_DATA_KEY].IsArray()) {
            Log::error("ShaderHandler::loadShaderProgram: Malformed JSON file, %s is not an array", FRAG_DATA_KEY);
            return nullptr;
        }

        const auto& frag = document[FRAG_DATA_KEY];

        for (size_t i = 0; i < frag.Size(); i++) {

            if (frag[i].IsObject()) {
                const auto& entry = frag[i];

                if (!entry.HasMember(FRAG_DATA_INDEX_KEY)) {
                    Log::error("ShaderHandler::loadShaderProgram: Malformed JSON file, %s is missing", FRAG_DATA_INDEX_KEY);
                    return nullptr;
                }
                if (!entry[FRAG_DATA_INDEX_KEY].IsInt()) {
                    Log::error("ShaderHandler::loadShaderProgram: Malformed JSON file, %s is not an int", FRAG_DATA_INDEX_KEY);
                    return nullptr;
                }
                if (!entry.HasMember(FRAG_DATA_NAME_KEY)) {
                    Log::error("ShaderHandler::loadShaderProgram: Malformed JSON file, %s is missing", FRAG_DATA_NAME_KEY);
                    return nullptr;
                }
                if (!entry[FRAG_DATA_NAME_KEY].IsString()) {
                    Log::error("ShaderHandler::loadShaderProgram: Malformed JSON file, %s is not an int", FRAG_DATA_NAME_KEY);
                    return nullptr;
                }

                glBindFragDataLocation(
                    program->getProgramID(),
                    entry[FRAG_DATA_INDEX_KEY].GetInt(),
                    entry[FRAG_DATA_NAME_KEY].GetString()
                );
                
                errors |= OPENGL_ERROR_CHECK("glBindFragDataLocation");

            } else {
                Log::error("ShaderHandler::loadShaderProgram: Malformed JSON file, %s[%i] is not an object", FRAG_DATA_KEY, i);
            }
        }
    } else {
        Log::info("ShaderHandler::loadShaderProgram: no %s, skipping glBindFragDataLocation step", FRAG_DATA_KEY);
    }

    // POST
    glLinkProgram(program->getProgramID());
    errors |= utility::checkProgramLog(program->getProgramID(), GL_LINK_STATUS);
    errors |= OPENGL_ERROR_CHECK("glLinkProgram");

    if (errors) {
        return nullptr;
    }

    if (document.HasMember(ATTRIBUTE_KEY)) {
        if (!document[ATTRIBUTE_KEY].IsArray()) {
            Log::error("ShaderHandler::loadShaderProgram: Malformed JSON file, %s is not an array", ATTRIBUTE_KEY);
            return nullptr;
        }

        const auto& attribute = document[ATTRIBUTE_KEY];

        for (size_t i = 0; i < attribute.Size(); i++) {
           
            if (attribute[i].IsString()) {
                program->createAttributeRefrence(attribute[i].GetString());
                errors |= OPENGL_ERROR_CHECK("createAttributeRefrence");
            } else {
                Log::error("ShaderHandler::loadShaderProgram: Malformed JSON file, %s[%i] is not a string", ATTRIBUTE_KEY, i);
            }
        }
    } else {
        Log::info("ShaderHandler::loadShaderProgram: no %s, skipping createAttributeRefrence step", ATTRIBUTE_KEY);
    }

    if (document.HasMember(UNIFORM_KEY)) {
        if (!document[UNIFORM_KEY].IsArray()) {
            Log::error("ShaderHandler::loadShaderProgram: Malformed JSON file, %s is not an array", UNIFORM_KEY);
            return nullptr;
        }

        const auto& uniform = document[UNIFORM_KEY];

        for (size_t i = 0; i < uniform.Size(); i++) {
            if (uniform[i].IsString()) {
                program->createUniformRefrence(uniform[i].GetString());
                errors |= OPENGL_ERROR_CHECK("createUniformRefrence");
            } else {
                Log::error("ShaderHandler::loadShaderProgram: Malformed JSON file, %s[%i] is not a string", UNIFORM_KEY, i);
            }
        }
    } else {
        Log::info("ShaderHandler::loadShaderProgram: no %s, skipping createUniformRefrence step", UNIFORM_KEY);
    }

    // END
    this->_fileToID[path] = program->getProgramID();
    this->_IDtoShaderProgram[program->getProgramID()] = program;
    this->_shaderPrograms.push_back(program);

    if (errors) {
        return nullptr;
    }

    return program;
}

Shader * ShaderHandler::loadShader (const std::string &path, SHADERTYPE type) {
	Shader * shade = new Shader(utility::loadCompileShader(path, (GLenum) type), type);
    this->_IDtoShader[shade->getShaderID()] = shade;
    this->_shaders.push_back(shade);

	return shade;
}
