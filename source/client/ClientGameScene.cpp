/**
 * @file    source/client/ClientGameScene.cpp
 *
 * @brief   Implements the client game scene class.
 */

#include "ClientGameScene.h"

#include "ClientServiceLocator.h"
#include "ClientEvents.h"
#include "SharedEvents.h"
#include "InputControl.h"
#include "Camera.h"
#include "ResourceManager.h"

#include "PlayerStateDownloadComponent.h"
#include "PlayerStateUploadComponent.h"
#include "TriggerComponent.h"
#include "FireComponent.h"
#include "GunReactionComponent.h"
#include "PlayerAreaComponent.h"
#include "LightComponent.h"
#include "StepComponent.h"
#include "GrenadeThrowComponent.h"


ClientGameScene::ClientGameScene(std::string mapFile) 
    :   GamePlayScene(mapFile),
        _localPlayer(nullptr) {
    _gameStartingDelegate.bind(this, &ClientGameScene::onGameStarting);
    _dcDelegate.bind(this, &ClientGameScene::onDisconnect);
    _createPlayerDelegate.bind(this, &ClientGameScene::onCreatePlayer);
    _spawnPlayerDelegate.bind(this, &ClientGameScene::onSpawnPlayer);
    _clientLeftDelegate.bind(this, &ClientGameScene::onClientLeftSession);
    _gameOverDelegate.bind(this, &ClientGameScene::onGameOver);
    _grenadeHitDelegate.bind(this, &ClientGameScene::onGrenadeHit);

    addGameObject(new GuiBar(1, Vec2(-2.0, 4.7f), 4.0f, 0.25f, "Bar"));
    addGameObject(new ObjectiveGuiObject(2, Vec2(-3.0f, -4.5f), ObjectiveId::A, "ObjectiveA"));
    addGameObject(new ObjectiveGuiObject(3, Vec2(0.0f, -4.5f), ObjectiveId::B, "ObjectiveB"));
    addGameObject(new ObjectiveGuiObject(4, Vec2(3.0f, -4.5f), ObjectiveId::C, "ObjectiveC"));

    GameObject *gob = new GameObject();
    addComponent<FlashOverlayComponent>(gob);
    _flashOverlay = getComponent<FlashOverlayComponent>(gob);
    addComponent<DrawableComponent>(gob, _flashOverlay, false);
    addGameObject(gob);
}

ClientGameScene::~ClientGameScene() {
    EventManager *emgr = ServiceLocator::singleton()->getEventManager();
    emgr->removeListener(_gameStartingDelegate);
    emgr->removeListener(_dcDelegate);
    emgr->removeListener(_createPlayerDelegate);
    emgr->removeListener(_spawnPlayerDelegate);
    emgr->removeListener(_clientLeftDelegate);
    emgr->removeListener(_gameOverDelegate);
    emgr->removeListener(_grenadeHitDelegate);
}

bool ClientGameScene::loadScene() {
    if (!GamePlayScene::loadScene()) {
        return false;
    }

    EventManager *emgr = ServiceLocator::singleton()->getEventManager();
    emgr->addListener(_gameStartingDelegate, GameStartingEvent::eventType);
    emgr->addListener(_dcDelegate, DisconnectEvent::eventType);
    emgr->addListener(_createPlayerDelegate, CreatePlayerEvent::eventType);
    emgr->addListener(_spawnPlayerDelegate, SpawnPlayerEvent::eventType);
    emgr->addListener(_clientLeftDelegate, ClientLeftSessionEvent::eventType);
    emgr->addListener(_gameOverDelegate, GameOverEvent::eventType);
    emgr->addListener(_grenadeHitDelegate, GrenadeHitEvent::eventType);

    return true;
}

void ClientGameScene::logicUpdate(DeltaTime dt) {
    updatePhysics(dt);
    updateCamera(dt);
    sendPhysicsUpdatedEvent();
}

std::vector<Drawable::BaseDrawable*>& ClientGameScene::getRenderVector() {
    auto &vec = GamePlayScene::getRenderVector();

    //auto lightVerts = getLightShadowVertices();
    auto losVerts = getVisibilityVertices();

    Drawable::GeometryCloud* losCloud = new Drawable::GeometryCloud(Drawable::GeometryCloud::TYPE::TRIANGLE);
    for (Vec2 &v : losVerts) {
        losCloud->addVertex({
            v.x,
            v.y,
            1.0f,
            1.0f,
            1.0f,
            1.0f,
            1.0f
        });
    }

    losCloud->tagOverride(Drawable::DrawableData::DataType::LOS);

    vec.push_back(losCloud);

    return vec;
}


Player* ClientGameScene::createLocalPlayer(PlayerId playerId, Team team, std::string name) {
    Player *player = createCommonPlayer(playerId, team, PlayerType::LOCAL, name);

    addComponent<ControlComponent>(player, new InputControl());
    addComponent<PlayerStateUploadComponent>(player);
    addComponent<PlayerStateDownloadComponent>(player);
    addComponent<GunReactionComponent>(player);
    addComponent<StepComponent>(player, getMap(), team);

    if (team == Team::MERC) {
        addComponent<FireComponent>(player, getWorld());
    } else if (team == Team::SPY) {
        addComponent<PlayerAreaComponent>(player, player, getMap());
        addComponent<GrenadeThrowComponent>(player, getWorld(), GrenadeType::FLASHBANG);
    }

    return player;
}

Player* ClientGameScene::createRemotePlayer(PlayerId id, Team team, std::string name) {
    Player *player = createCommonPlayer(id, team, PlayerType::REMOTE, name);

    addComponent<PlayerStateDownloadComponent>(player);
    addComponent<GunReactionComponent>(player);
    addComponent<StepComponent>(player, getMap(), team);

    if (team == Team::SPY) {
        addComponent<GrenadeThrowComponent>(player, getWorld(), GrenadeType::FLASHBANG);
    }

    return player;
}

void ClientGameScene::updateCamera(DeltaTime dt) {
    const float maxSpeed = 100.f;

    if (_localPlayer != nullptr) {
        Vec2 pos = _localPlayer->getPosition();

        Camera *camera = ClientServiceLocator::singleton()->getCamera();
        camera->setPosition(pos);
    }
}

std::vector<Vec2> ClientGameScene::getLightShadowVertices() {
    std::vector<Vec2> verts;   

    for (LightComponent *light : _lightComps) {
        light->fillShadowTriangles(verts);
    }

    return verts;
}

std::vector<Vec2> ClientGameScene::getVisibilityVertices() {
    VisibilityMapper mapper(getMap());
    
    if (_localPlayer) {
        return mapper.map(_localPlayer->getPosition(), 8.f);
    }

    std::vector<Vec2> bogus;
    return bogus;
}


void ClientGameScene::createMapObjects() {
    GamePlayScene::createMapObjects();
    
    // Add TriggerComponents
    GameObject *trigRoot = new GameObject();
    trigRoot->setRotation(0.f);
    addGameObject(trigRoot);

    for (MapTriggerProperties props : getMap()->getTriggers()) {
        GameObject *trigger = new GameObject();

        addComponent<TriggerComponent>(trigger, getWorld(), props);
        getComponent<TriggerComponent>(trigger)->createTrigger();

        trigRoot->addChild(trigger);
        Log::debug("Added trigger '%s'", props.name.c_str());
    }

    // Add LightComponents
    GameObject *lightRoot = new GameObject();
    addGameObject(lightRoot);

    for (const MapLightProperties &props : getMap()->getLights()) {
        GameObject *light = new GameObject();
        addComponent<LightComponent>(light, props, getMap());
        lightRoot->addChild(light);

        _lightComps.push_back(getComponent<LightComponent>(light));
    }
}

void ClientGameScene::onGameStarting(const EventData *e) {
    Log::debug("ClientGameScene: Game Starting!!");
    EventManager *emgr = ServiceLocator::singleton()->getEventManager();
    emgr->queueEvent(new InitiateGuiEvent());
}

void ClientGameScene::onDisconnect(const EventData *e) {
    Log::debug("ClientGameScene: Returning to parent scene");
    setSceneState(Scene::State::RETURN_TO_PARENT);
}

void ClientGameScene::onCreatePlayer(const EventData *e) {
    CreatePlayerEvent *evt = (CreatePlayerEvent*)e;

    std::string userName = evt->getPlayerName();
    Team team = evt->getTeam();
    PlayerId id = evt->getPlayerId();
    PlayerType type = evt->getPlayerType();

    Player *player = nullptr;

    if (type == PlayerType::LOCAL) {
        assert(_localPlayer == nullptr);

        player = createLocalPlayer(id, team, userName);
        _localPlayer = player;
    } else if (type == PlayerType::REMOTE) {
        player = createRemotePlayer(id, team, userName);
    } else {
        Log::error("ClientGameScene: Undefined value for PlayerType: %i",
                   static_cast<int>(type));
        return;
    }

    if (!player) {
        Log::error("ClientGameScene: Unable to create %s player (%s, %u)",
                   (type==PlayerType::LOCAL ? "local" : "remote"),
                   userName.c_str(), id);
        return;
    }

    _players.push_back(player);

    Log::debug("ClientGameScene: created %s player (%s, %u)",
               (type==PlayerType::LOCAL ? "local" : "remote"),
               userName.c_str(), id);
}

void ClientGameScene::onSpawnPlayer(const EventData *e) {
    SpawnPlayerEvent *evt = (SpawnPlayerEvent*)e;

    for (Player *p : _players) {
        if (p->getPlayerId() == evt->getPlayerId()) {
            if (p->isActive()) {
                Log::warning("Server commanded a SpawnPlayer on already active "
                             "player instance (%s, %u)", 
                             p->getPlayerName().c_str(), p->getPlayerId());
            }

            Log::info("Spawning player '%s'", p->getPlayerName().c_str());
            p->spawn(evt->getPosition());
        }
    }
}

void ClientGameScene::onClientLeftSession(const EventData *e) {
    ClientLeftSessionEvent *evt = (ClientLeftSessionEvent*)e;

    bool removed = false;

    // Remove the player
    auto it = _players.begin();
    while (it != _players.end()) {
        if ((*it)->getPlayerId() == evt->getPlayerId()) {
            if ((*it)->getPlayerType() == PlayerType::LOCAL) {
                Log::error("ClientGameScene::onClientLeftSession: Attempted "
                           "to remove the local player (%s, %u)",
                           evt->getClientName().c_str(), evt->getPlayerId());
            } else {
                removeGameObject(*it);
                it = _players.erase(it);
                removed = true;
            }
        } else {
            it++;
        }
    }

    if (!removed) {
        Log::warning("ClientGameScene::onClientLeftSession: Unable to find "
                     "and remove player %s (%u)", 
                     evt->getClientName().c_str(), evt->getPlayerId());
    }
}

void ClientGameScene::onGameOver(const EventData *e) {
    GameOverEvent *evt = (GameOverEvent*)e;
        
    Log::debug("GAME OVER!");
    Log::debug("Team %s won!", (evt->getWinningTeam() == Team::SPY ? "Spy" : "Merc"));
    Log::debug("Winning reason: %s", evt->getReason().c_str());
    Log::debug("This is where we would display some text or something, dunno.");
    Log::debug("Time to revert to some kind of score-screen scene or something, "
               "all other players will now appear stationary.");
}

void ClientGameScene::onGrenadeHit(const EventData *e) {
    GrenadeHitEvent *evt = (GrenadeHitEvent*)e;

    // Ignore grenade hits on other players
    if (evt->getPlayerId() != _localPlayer->getPlayerId())
        return;

    // Spies are unaffected by flashbangs
    if (evt->getGrenadeType() == GrenadeType::FLASHBANG &&
        _localPlayer->getTeam() == Team::SPY) {
        return;
    }

    _flashOverlay->onFlash(evt->getHitFactor());

    ResourceId rid = hashResource("audio/flashring.ogg");

    auto emgr = ClientServiceLocator::singleton()->getEventManager();
    emgr->queueEvent(new CreateSoundEvent(rid));
}
