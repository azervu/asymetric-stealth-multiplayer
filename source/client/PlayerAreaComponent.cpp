#include "PlayerAreaComponent.h"
#include "Map.h"
#include "Player.h"
#include "ClientEvents.h"


PlayerAreaComponent::PlayerAreaComponent(const Player *player, const Map *map)
    :   _player(player),
        _map(map),
        _area(ObjectiveId::NONE) {
    assert(_map != nullptr);
}

void PlayerAreaComponent::update(DeltaTime dt) {
    ObjectiveId current = getCurrentArea();

    if (current != _area) {
        Log::debug("Player changed area");
        _area = current;
        PlayerId id = _player->getPlayerId();

        EventManager *emgr = ServiceLocator::singleton()->getEventManager();
        emgr->queueEvent(new PlayerAreaTransitionEvent(id, _area));
    }
}


ObjectiveId PlayerAreaComponent::getCurrentArea() {
    Vec2 pos = _player->getPosition();
    pos.x *= ASM_TILES_PER_METER;
    pos.y *= ASM_TILES_PER_METER;

    int tileX = (int)pos.x;
    int tileY = (int)pos.y;

    if (tileX < 0 || tileY < 0 || 
            tileX >= _map->getWidth() || 
            tileY >= _map->getHeight()) {
        return ObjectiveId::NONE;
    }

    unsigned flags = _map->getTileFlags((int)pos.x, (int)pos.y);

    if (flags & TileFlag::TILE_AREA_A) {
        return ObjectiveId::A;
    }

    if (flags & TileFlag::TILE_AREA_B) {
        return ObjectiveId::B;
    }

    if (flags & TileFlag::TILE_AREA_C) {
        return ObjectiveId::C;
    }

    return ObjectiveId::NONE;
}

