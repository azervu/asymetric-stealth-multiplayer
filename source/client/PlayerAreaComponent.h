#pragma once

#include "GameObject.h"
#include "Engine.h"

class Player;
class Map;

/**
 * PlayerAreaComponent notifies via the EventManager when the given Player
 * object enters or leaves an objective area. Objective areas are defined in
 * the maps "areas" layer.
 */
class PlayerAreaComponent : public Component {
public:
    PlayerAreaComponent(const Player *player, const Map *map);

    void update(DeltaTime dt);

private:
    const Player *_player;
    const Map *_map;
    ObjectiveId _area;


    ObjectiveId getCurrentArea();
};
