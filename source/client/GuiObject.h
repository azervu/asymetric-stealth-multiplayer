#pragma once

#include <DrawableComponent.h>
#include "GameObject.h"
#include "EventManager.h"
#include "Timer.h"
#include "DrawableComponent.h"
#include "Renderable.h"

enum class Alignment {
    LEFT,
    RIGHT,
    CENTER,
};

class GuiObject : public GameObject {
public:
    GuiObject(int guiId, Vec2 placement, float width, float height, std::string graphichName);
    virtual ~GuiObject();
    void setValue(int value);

    virtual float getWidth();
    virtual float getHeight();

protected:
    SpriteProvider* _spriteProvider;
    float _width;
    float _height;
    int _guiId;

private:
    void onInitiateGuiEvent(const EventData *e);
    EventListenerDelegate _guiInitiateEventDelegate;


};

class GuiBar : public GuiObject {
public:
    GuiBar(int guiId, Vec2 placement, float width, float height, std::string graphichName);
    virtual ~GuiBar();
    virtual float getWidth();
private:
    virtual void onUpdateGuiEvent(const EventData *e);
    EventListenerDelegate _guiUpdateEventDelegate;
    int _value;
    int _maxValue;
};

class ObjectiveGuiObject : public GuiObject {
public:
    ObjectiveGuiObject(int guiId, Vec2 placement, ObjectiveId objectiveId, std::string graphichName);
    void setValue(int value);
private:
    void onObjectiveStateChanged(const EventData *e);
    EventListenerDelegate _objectiveStateChangedEventDelegate;
    ObjectiveId _objectiveId;
    ObjectiveState _state;
};

class GuiComponent : public Component {
public:
    GuiComponent(Vec2 placement, Alignment alignment, SpriteProvider* spriteProvider);
    ~GuiComponent();

    virtual void update(DeltaTime dt);

private:
    Alignment _alignment;
    Vec2 _placement;
    Vec2 _position;
    SpriteProvider* _spriteProvider;

    EventListenerDelegate _physicsDelegate;
    void onPhysicsUpdated(const EventData *e);
};


class FlashOverlayComponent : public Component, public DrawableProvider {
public:
    FlashOverlayComponent();

    void update(DeltaTime dt);
    void onFlash(float hitFactor);

    Drawable::BaseDrawable* getDrawable();

private:
    Timer _timer;
    float _alpha;
    float _remaining;
};
