/**
 * @file    source/client/Renderable.cpp
 *
 * @brief   Implements the renderable class.
 */

#include "Renderable.h"
#include "SpriteManager.h"
#include "ResourceManager.h"
#include "ClientServiceLocator.h"

using namespace Drawable;

const std::string DEFAULT_FONT = "font/perfect_dark/pdark.ttf";

/*
========
BaseDrawable
========
*/
Drawable::BaseDrawable::BaseDrawable() {
}

Drawable::BaseDrawable::~BaseDrawable() {
}

void Drawable::BaseDrawable::tagOverride(DrawableData::DataType tag) {
    this->_tag = tag;
}

/*
========
Geometry
========
*/
Drawable::Geometry::Geometry()
    :   BaseDrawable() {
}

Drawable::Geometry::~Geometry() {
}

void Drawable::Geometry::addVertex(Vertex vertex) {
    vertex.x *= ASM_PIXELS_PER_METER;
    vertex.y *= ASM_PIXELS_PER_METER;
    this->_data.push_back(vertex);
}

/*
========
Polygon
========
*/
Drawable::Polygon::Polygon()
    :   Geometry() {

}

Drawable::Polygon::~Polygon() {
}

std::vector<DrawableData> Drawable::Polygon::getRenderData() {
    std::vector<DrawableData> returnData;

    assert(this->_data.size() >= 3);

    for (size_t i = 0; i < _data.size() - 2; i++) {
        DrawableData dd;
    
        dd._dataType = DrawableData::DataType::TRIANGLE;
        dd._data.triangle.pos[0] = _data[i + 0];
        dd._data.triangle.pos[1] = _data[i + 1];
        dd._data.triangle.pos[2] = _data[i + 2];

        returnData.push_back(dd);
    }

    return returnData;
}

/*
========
GeometryCloud
========
*/
Drawable::GeometryCloud::GeometryCloud(GeometryCloud::TYPE type)
    :   Geometry(),
        _type(type) {

    switch (type) {
        case TYPE::TRIANGLE:
            this->_tag = DrawableData::DataType::TRIANGLE;
        break;

        case TYPE::LINE:
            this->_tag = DrawableData::DataType::LINE;
        break;

        case TYPE::POINT:
            this->_tag = DrawableData::DataType::POINT;
        break;
    }
}

Drawable::GeometryCloud::~GeometryCloud() {
}

std::vector<DrawableData> Drawable::GeometryCloud::getRenderData() {
    std::vector<DrawableData> dataList;

    switch (_type ) {
        case Drawable::GeometryCloud::TYPE::TRIANGLE:
            for (size_t i = 0; i < _data.size(); i += 3) {
                DrawableData data;
                data._dataType = this->_tag;

                for (size_t j = 0; j < 3; j++) {
                    data._data.triangle.pos[j] = _data[i + j];
                }

                dataList.push_back(data);
            }
        break;

        case Drawable::GeometryCloud::TYPE::LINE:
            for (size_t i = 0; i < _data.size(); i += 2) {
                DrawableData data;
                data._dataType = this->_tag;

                for (size_t j = 0; j < 2; j++) {
                    data._data.line.pos[j] = _data[i + j];
                }

                dataList.push_back(data);
            }
        break;

        case Drawable::GeometryCloud::TYPE::POINT:
            for (size_t i = 0; i < _data.size(); i += 1) {
                DrawableData data;
                data._dataType = this->_tag;

                data._data.point.pos[0] = _data[i];
                dataList.push_back(data);
            }
        break;
    }

    return dataList;
}

/*
========
Image
========
*/
Drawable::Image::Image(float zDepth)
    :   BaseDrawable() {

    this->_tag = Drawable::DrawableData::DataType::IMAGE;
    this->_drawableData._dataType = this->_tag;
    this->_drawableData._data.image.zDepth = zDepth;
}

Drawable::Image::~Image() {
}

void Drawable::Image::setPosition(b2Vec2 pos) {
    this->_drawableData._data.image.x = pos.x * ASM_PIXELS_PER_METER;
    this->_drawableData._data.image.y = pos.y * ASM_PIXELS_PER_METER;
}

void Drawable::Image::setSize(b2Vec2 size) {
    this->_drawableData._data.image.w = size.x * ASM_PIXELS_PER_METER;
    this->_drawableData._data.image.h = size.y * ASM_PIXELS_PER_METER;
}

void Drawable::Image::setAnchor(b2Vec2 anchor) {
    this->_drawableData._data.image.xA = anchor.x * ASM_PIXELS_PER_METER;
    this->_drawableData._data.image.yA = anchor.y * ASM_PIXELS_PER_METER;
}

void Drawable::Image::setAngle(float angle) {
    this->_drawableData._data.image.angle = angle;
}

/*
========
Sprite
========
*/
Drawable::Sprite::Sprite(ResourceId spriteId, size_t frame, float zDepth)
    :   Image(zDepth),
        _spriteId(spriteId),
        _frame(frame) {

    SpriteManager::Manager* sprMng = ServiceLocator::singleton()->getSpriteManager();
    auto sprite = sprMng->getSprite(_spriteId);
    auto sheet = sprite->_sheet;
    this->_drawableData._data.image.textureId = sheet->_textureId;

    auto imagePos = sprite->_images.at(_frame);
    _drawableData._data.image.texX = imagePos.first;
    _drawableData._data.image.texY = imagePos.second;
    _drawableData._data.image.texW = sprite->_width;
    _drawableData._data.image.texH = sprite->_height;
}

Drawable::Sprite::~Sprite() {
}

std::vector<DrawableData> Drawable::Sprite::getRenderData() {
    _drawableData._dataType = this->_tag;

    return {_drawableData};
}

/*
========
Text
========
*/
Drawable::Text::Text(const std::string& text, unsigned size)
    :   Image(2.0f) {

    // dirty hackery
    ResourceManager* manager = ClientServiceLocator::singleton()->getResourceManager();
    std::hash<std::string> hasher;
    Font* font = (Font*) manager->getResource(ResourceType::FONT, hasher(DEFAULT_FONT));

    Drawable::DrawableData data = font->textToImage(text, size);

    this->_drawableData._data.image.texW = data._data.image.texW;
    this->_drawableData._data.image.texH = data._data.image.texH;
    this->_drawableData._data.image.texX = data._data.image.texX;
    this->_drawableData._data.image.texY = data._data.image.texY;
    this->_drawableData._data.image.textureId = data._data.image.textureId;
}

Drawable::Text::~Text() {
}

std::vector<DrawableData> Drawable::Text::getRenderData() {
    return {_drawableData};
}
