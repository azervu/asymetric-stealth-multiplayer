#include "GuiObject.h"
#include "ClientServiceLocator.h"
#include "ClientEvents.h"
#include "Camera.h"

/*
    GuiObject
 */
GuiObject::GuiObject(int guiId, Vec2 placement, float width, float height, std::string graphichName)
    :   GameObject(),
        _guiId(guiId)    {
    _width = width;
    _height = height;
    _spriteProvider = new SpriteProvider(graphichName);
    _spriteProvider->setSize({width, height});
    _spriteProvider->setAnchor({0.0,0.0});
    _spriteProvider->setDepth(3.0f);
    _spriteProvider->setAlwaysVisible(true);

    addComponent<DrawableComponent>(this, _spriteProvider, true);

    _guiInitiateEventDelegate.bind(this, &GuiObject::onInitiateGuiEvent);
    EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();
    emgr->addListener(_guiInitiateEventDelegate, InitiateGuiEvent::eventType);

    addComponent<GuiComponent>(this, placement, Alignment::LEFT, _spriteProvider);
}

GuiObject::~GuiObject() {
    EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();
    emgr->removeListener(_guiInitiateEventDelegate);
}

float GuiObject::getWidth() {
    return _width;
}

float GuiObject::getHeight() {
    return _height;
}

void GuiObject::onInitiateGuiEvent(const EventData *e) {
    _spriteProvider->setDepth(0.01f);
}


/*
    GuiBar
 */
GuiBar::GuiBar(int guiId, Vec2 placement, float width, float height, std::string graphichName) : GuiObject(guiId, placement, width, height, graphichName) {
    _value = 100;
    _maxValue = 100;
    _guiUpdateEventDelegate.bind(this, &GuiBar::onUpdateGuiEvent);
    EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();
    emgr->addListener(_guiUpdateEventDelegate, UpdateGuiEvent::eventType);
}

GuiBar::~GuiBar() {
    EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();
    emgr->removeListener(_guiUpdateEventDelegate);
}


float GuiBar::getWidth() {
    return _width * _value / _maxValue;
}


void GuiBar::onUpdateGuiEvent(const EventData *e) {
    if (_guiId == ((UpdateGuiEvent*)e)->getGuiId()) {
        _value = ((UpdateGuiEvent*) e)->getValue();
    }
}


/*
    ObjectiveGuiObject
 */
ObjectiveGuiObject::ObjectiveGuiObject(int guiId, Vec2 placement, ObjectiveId objectiveId, std::string graphichName) : GuiObject(guiId, placement, 0.25f, 0.25f, graphichName) {
    _objectiveId = objectiveId;
    _objectiveStateChangedEventDelegate.bind(this, &ObjectiveGuiObject::onObjectiveStateChanged);

    _spriteProvider->setFrame(1);

    EventManager *emgr = ClientServiceLocator::singleton()->getEventManager();
    emgr->addListener(_objectiveStateChangedEventDelegate, ObjectiveStateChangedEvent::eventType);
}

void ObjectiveGuiObject::onObjectiveStateChanged(const EventData *e) {
    ObjectiveStateChangedEvent* objectiveEvent = (ObjectiveStateChangedEvent*)e;
    if (objectiveEvent->getNetworkDirection() != NetworkDirection::NET_IN) {
        return;
    }

    GuiComponent* guiComp = getComponent<GuiComponent>(this);
    if (guiComp == NULL) {
        return;
    }

    if (objectiveEvent->getObjectiveId() == _objectiveId) {
        _state = objectiveEvent->getObjectiveState();
    } else {
        if (_state != ObjectiveState::HACKED && (objectiveEvent->getObjectiveState() == ObjectiveState::AVAILABLE || objectiveEvent->getObjectiveState() == ObjectiveState::HACKED)) {
            _state = ObjectiveState::AVAILABLE;
        }

        if (objectiveEvent->getObjectiveState() == ObjectiveState::TRIGGERED && _state != ObjectiveState::HACKED) {
            _state = ObjectiveState::DISABLED;
        }
    }

    switch (_state) {

        case ObjectiveState::AVAILABLE:
            _spriteProvider->setFrame(1);
        break;

        case ObjectiveState::TRIGGERED:
            _spriteProvider->setFrame(2);
        break;

        case ObjectiveState::DISABLED:
            _spriteProvider->setFrame(0);
        break;

        case ObjectiveState::HACKED:
            _spriteProvider->setFrame(3);
        break;
    }
}

/*
    GuiComponent
 */
GuiComponent::GuiComponent(Vec2 placement, Alignment alignment, SpriteProvider* spriteProvider)
    :   _placement(placement),
        _alignment(alignment),
        _spriteProvider(spriteProvider) {
    _physicsDelegate.bind(this, &GuiComponent::onPhysicsUpdated);

    EventManager *mgr = ServiceLocator::singleton()->getEventManager();
    mgr->addListener(_physicsDelegate, PhysicsUpdatedEvent::eventType);
}

GuiComponent::~GuiComponent() {
    EventManager *mgr = ServiceLocator::singleton()->getEventManager();
    mgr->removeListener(_physicsDelegate);
}

void GuiComponent::update(DeltaTime dt) {
}

void GuiComponent::onPhysicsUpdated(const EventData *e) {
    Camera * cam = ClientServiceLocator::singleton()->getCamera();
    Vec2 pos = cam->getCenterPosition();

    GuiObject* root = (GuiObject*) this->getGameObject();

    root->setPosition(pos + _placement);
    root->setRotation(0.0f);
    _spriteProvider->setPosition(root->getPosition());
    _spriteProvider->setWidth(root->getWidth());
    _spriteProvider->setHeight(root->getHeight());
}

/*
    FlashOverlayComponent
 */
FlashOverlayComponent::FlashOverlayComponent()
    :   _alpha(0.f),
        _remaining(0.f) {

}

void FlashOverlayComponent::update(DeltaTime dt) {
    if (_remaining > 0.f) {
        _alpha = 0.7f * atan(_remaining * 5.f);
        _remaining -= _timer.getElapsedSeconds();
    } else {
        _remaining = 0.f;
        _alpha = 0.f;
    }
    
    _timer.start();
}

void FlashOverlayComponent::onFlash(float hitFactor) {
    _remaining += 1.f + cos(hitFactor * 1.4f);
}

Drawable::BaseDrawable* FlashOverlayComponent::getDrawable() {
    Camera *cam = ClientServiceLocator::singleton()->getCamera();
    Vec2 cpos = cam->getCenterPosition();

    auto t = Drawable::GeometryCloud::TYPE::TRIANGLE;

    Drawable::GeometryCloud *cloud = new Drawable::GeometryCloud(t);

    Drawable::Vertex vert;
    vert.r = 1.f;
    vert.g = 1.f;
    vert.b = 1.f;
    vert.a = _alpha;
    vert.z = 0.35f;

    Drawable::Vertex vertices[4];
    int i = 0;

    for (int x = -1; x <= 1; x += 2) {
        for (int y = -1; y <= 1; y += 2) {
            vert.x = 20.f * x + cpos.x;
            vert.y = 10.f * y + cpos.y;

            vertices[i++] = vert;
        }
    }

    cloud->addVertex(vertices[0]);
    cloud->addVertex(vertices[1]);
    cloud->addVertex(vertices[2]);

    cloud->addVertex(vertices[1]);
    cloud->addVertex(vertices[2]);
    cloud->addVertex(vertices[3]);

    return cloud;
}
