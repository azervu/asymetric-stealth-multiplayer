#pragma once

#include "GamePlayScene.h"
#include "GuiObject.h"
#include "Renderable.h"

class Player;
class LightComponent;


/**
 * ClientGameScene manages the game objects when the game is running.
 */
class ClientGameScene : public GamePlayScene {
public:
    ClientGameScene(std::string mapFile);
    ~ClientGameScene();

    virtual bool loadScene();
    virtual void logicUpdate(DeltaTime dt);

    virtual std::vector<Drawable::BaseDrawable*>& getRenderVector();

protected:
    Player* createLocalPlayer(PlayerId playerId, Team team, std::string name);
    virtual Player* createRemotePlayer(PlayerId playerId, Team team, std::string name);

    /**
     * Center the camera on the local player.
     */
    void updateCamera(DeltaTime dt);

    std::vector<Vec2> getLightShadowVertices();
    std::vector<Vec2> getVisibilityVertices();

private:
    Player *_localPlayer;
    std::vector<Player*> _players;
    std::vector<LightComponent*> _lightComps;
    FlashOverlayComponent *_flashOverlay;

    virtual void createMapObjects();

    void onGameStarting(const EventData *evt);
    EventListenerDelegate _gameStartingDelegate;

    void onDisconnect(const EventData *evt);
    EventListenerDelegate _dcDelegate;

    void onCreatePlayer(const EventData *e);
    EventListenerDelegate _createPlayerDelegate;

    void onSpawnPlayer(const EventData *e);
    EventListenerDelegate _spawnPlayerDelegate;

    void onClientLeftSession(const EventData *e);
    EventListenerDelegate _clientLeftDelegate;

    void onGameOver(const EventData *e);
    EventListenerDelegate _gameOverDelegate;

    void onGrenadeHit(const EventData *e);
    EventListenerDelegate _grenadeHitDelegate;
};
