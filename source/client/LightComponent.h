#pragma once

#include "Engine.h"
#include "GameObject.h"
#include "Map.h"
#include "VisibilityMapper.h"


/**
 * LightComponent provides simple data about a light that should be displayed
 * on the map. The LightComponent also provides the vertices of the shadow
 * cast by this light.
 */
class LightComponent : public Component {
public:
    LightComponent(const MapLightProperties &props, const Map *_map);
    
    void update(DeltaTime dt);

    /**
     * Add this LightComponent's shadow vertices to the vector
     */
    void fillShadowTriangles(std::vector<Vec2> &verts) const;

    Color getLightColor() const;
    float getLightRadius() const;
    Vec2 getLightPosition() const;

private:
    MapLightProperties _props;
    std::vector<Vec2> _vertCache;
};
