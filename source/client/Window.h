/**
 * @file    source/client/Window.h
 *
 * @brief   Declares the window class.
 */

#pragma once

#include <mutex>

#include "Engine.h"
#include "ClientServiceLocator.h"
#include "EventManager.h"

/**
 * The Window class abstracts away the window creation, and keeps track of
 * relevant window data. The GraphicsSystem will swap the window's buffers
 * very often, so all public methods are protected by a mutex - the Window will
 * only ever perform one task at a time.
 */
class Window : public Service {
public:
    Window();
    ~Window();
    virtual void onExit() final;

    virtual void init(Settings *settings);

    /**
     * Actually create the window. The timing of the window creation is
     * critical on UNIX systems due to the funky SDL implementation. The
     * window must be created AFTER the video system of SDL is initialized,
     * but BEFORE an OpenGL context is created.
     */
    void createWindow();

    /**
     * Destroy the window. The OpenGL context should be destroyed before this
     * method is called to avoid undefined behaviour. I don't need to tell you
     * to be careful when calling this method.
     */
    void destroyWindow();

    /**
     * Sets the window title. The title can be assigned before and after the
     * actual window is created.
     */
    void setTitle(std::string title);

    /**
     * Retrieve the window pixel dimensions. Returns the default size if
     * the window has not yet been created.
     */
    std::pair<int, int> getSize();

    SDL_Window* getSDLWindow();

    /**
     * Swap the buffers of the window. The window must have been created.
     */
    void swapBuffers();


private:
    SDL_Window *_window;
    std::string _title;
    std::pair<int, int> _winSize;
    std::mutex _mutex;

    /* WindowResizedEvent delegate method. Instead of calling SDL_GetWindowSize
     * every time the window size is requested, Window caches the size.
     */
    void onWindowResized(const EventData *e);
    EventListenerDelegate _winResizeDelegate;
};
