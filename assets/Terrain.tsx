<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Terrain" tilewidth="16" tileheight="16">
 <image source="terrain.png" width="256" height="256"/>
 <terraintypes>
  <terrain name="Wall" tile="-1"/>
 </terraintypes>
 <tile id="0" terrain="0,,0,"/>
 <tile id="1" terrain=",,0,0"/>
 <tile id="2" terrain=",0,,0"/>
 <tile id="3" terrain="0,0,,"/>
 <tile id="4" terrain=",,0,"/>
 <tile id="5" terrain=",,,0"/>
 <tile id="6" terrain=",0,,"/>
 <tile id="7" terrain="0,,,"/>
 <tile id="8" terrain=",0,0,"/>
 <tile id="9" terrain="0,,,0"/>
 <tile id="10" terrain="0,,0,0"/>
 <tile id="11" terrain=",0,0,0"/>
 <tile id="12" terrain="0,0,,0"/>
 <tile id="13" terrain="0,0,0,"/>
 <tile id="112" terrain="0,0,0,0"/>
 <tile id="113" terrain="0,0,0,0"/>
 <tile id="114" terrain="0,0,0,0"/>
 <tile id="115" terrain="0,0,0,0"/>
 <tile id="116" terrain="0,0,0,0"/>
 <tile id="117" terrain="0,0,0,0"/>
 <tile id="118" terrain="0,0,0,0"/>
 <tile id="119" terrain="0,0,0,0"/>
</tileset>
