#version 330

// In
in Data {
    vec4 color;
} DataIn;

// Out
out vec4 outColor;

void main() {

    outColor = DataIn.color;
}
