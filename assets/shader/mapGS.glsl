#version 330

// Layout
layout (points) in;
layout (triangle_strip, max_vertices=4) out;

// Uniforms
uniform float tileSize;
uniform vec2 texSize;
uniform vec2 texTileSize;

uniform mat4 projection;
uniform mat4 view;

// In
in Data {
    vec2 texCoord;
} DataIn[];

// Out
out Data {
    vec4 pos;
    vec2 texCoord;
} DataOut;

void main() {
    vec4 pos;
    vec2 tex;

    pos = gl_in[0].gl_Position;
    pos = projection * view * pos;

    gl_Position = pos;
    DataOut.pos = pos;

    tex = DataIn[0].texCoord;
    tex.y = texSize.y - tex.y;

    tex.x /= texSize.x;
    tex.y /= texSize.y;

    DataOut.texCoord = tex;

    EmitVertex();


    pos = gl_in[0].gl_Position + vec4(0.0, tileSize, 0.0, 0.0);
    pos = projection * view * pos;

    gl_Position = pos;
    DataOut.pos = pos;

    tex = DataIn[0].texCoord + vec2(0.0, texTileSize.y);
    tex.y = texSize.y - tex.y;

    tex.x /= texSize.x;
    tex.y /= texSize.y;

    DataOut.texCoord = tex;

    EmitVertex();


    pos = gl_in[0].gl_Position + vec4(tileSize, 0.0, 0.0, 0.0);
    pos = projection * view * pos;

    gl_Position = pos;
    DataOut.pos = pos;

    tex = DataIn[0].texCoord + vec2(texTileSize.x, 0.0);
    tex.y = texSize.y - tex.y;

    tex.x /= texSize.x;
    tex.y /= texSize.y;

    DataOut.texCoord = tex;

    EmitVertex();


    pos = gl_in[0].gl_Position + vec4(tileSize, tileSize, 0.0, 0.0);
    pos = projection * view * pos;

    gl_Position = pos;
    DataOut.pos = pos;

    tex = DataIn[0].texCoord + vec2(texTileSize.x, texTileSize.y);
    tex.y = texSize.y - tex.y;

    tex.x /= texSize.x;
    tex.y /= texSize.y;

    DataOut.texCoord = tex;

    EmitVertex();

    EndPrimitive();
}
