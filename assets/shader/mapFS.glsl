#version 330

// Uniforms
uniform vec3 tint;
uniform sampler2D textureSheet;

// In
in Data {
    vec4 pos;
    vec2 texCoord;
} DataIn;

// Out
out vec4 outColor;

void main() {

    outColor = texture(textureSheet, DataIn.texCoord) * vec4(tint, 1.0);
    //outColor = vec4(DataIn.texCoord, 1.0, 1.0) * vec4(tint, 1.0);
}
