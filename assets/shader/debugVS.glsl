#version 330

// Uniform
uniform mat4 projection;
uniform mat4 view;

// In
in vec3 vertex;
in vec4 color;

// Out
out Data {
    vec4 color;
} DataOut;

void main() {

    gl_Position = projection * view * vec4(vertex, 1.0);
    DataOut.color = color;
}
