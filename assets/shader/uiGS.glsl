#version 330
 
layout(triangles) in;
layout(triangle_strip, max_vertices=6) out;

// Uniform
uniform mat4 projection;

// In
in Data {
    vec2 texCoord;
} DataIn[];

// Out
out Data {
    vec2 texCoord;
} DataOut;

void main() {
    for(int i = 0; i < gl_in.length(); i++) {
        gl_Position = projection * gl_in[i].gl_Position;
        DataOut.texCoord = DataIn[i].texCoord;

        EmitVertex();
    }

    EndPrimitive();

    for(int i = 0; i < gl_in.length(); i++) {
        gl_Position = projection * (gl_in[i].gl_Position + vec4(50.0, 0.0, 0.0, 0.0));
        DataOut.texCoord = DataIn[i].texCoord;

        EmitVertex();
    }

    EndPrimitive();
}
