#version 330

// In
in vec2 position;
in vec2 anchor;
in vec2 size;
in float angle;
in float zDepth;
in vec2 texPos;
in vec2 texTile;

// Out
out Data {
    vec2 anchor;
    vec2 size;
    float angle;
    vec2 texPos;
    vec2 texTile;
} DataOut;

void main() {

    DataOut.anchor = anchor;
    DataOut.size = size;
    DataOut.angle = angle;
    DataOut.texPos = texPos;
    DataOut.texTile = texTile;

    gl_Position = vec4(position, zDepth, 1.0);
}
