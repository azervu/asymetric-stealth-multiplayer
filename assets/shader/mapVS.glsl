#version 330

// Uniforms
uniform float zDepth = 0.1f;
uniform vec2 texTileSize;
uniform float tileSize;

// In
in vec2 position;
in vec2 texPos;

// out
out Data {
    vec2 texCoord;
} DataOut;

void main() {
    DataOut.texCoord = vec2(texPos.x * texTileSize.x, texPos.y * texTileSize.y);
    gl_Position = vec4(position * tileSize, zDepth, 1.0);
}
