#version 330

// Layout
layout (points) in;
layout (triangle_strip, max_vertices=4) out;

// Uniforms
uniform mat4 projection;
uniform mat4 view;
uniform vec2 texSize;

// In
in Data {
    vec2 anchor;
    vec2 size;
    float angle;
    vec2 texPos;
    vec2 texTile;
} DataIn[];

// Out
out Data {
    vec4 pos;
    vec2 texCoord;
} DataOut;

void main() {
    vec4 pos;
    vec2 texCoord;
    vec4 offset;
    mat4 rotation;

    rotation[0][0] = cos(DataIn[0].angle);
    rotation[1][0] = -sin(DataIn[0].angle);
    rotation[0][1] = sin(DataIn[0].angle);
    rotation[1][1] = cos(DataIn[0].angle);
    rotation[2][2] = 1.0;
    rotation[3][3] = 1.0;


    offset = -vec4(DataIn[0].anchor,0.0, 0.0);
    pos = (rotation * offset) + gl_in[0].gl_Position;

    texCoord = DataIn[0].texPos;

    texCoord.x /= texSize.x;
    texCoord.y /= texSize.y;

    texCoord.y = 1.0 - texCoord.y;
    pos = projection * view * pos;
    
    gl_Position = pos;
    DataOut.pos = pos;
    DataOut.texCoord = texCoord;

    EmitVertex();


    offset = -vec4(DataIn[0].anchor,0.0, 0.0) + vec4(0.0, DataIn[0].size.y, 0.0, 0.0);
    pos = (rotation * offset) + gl_in[0].gl_Position;

    texCoord = DataIn[0].texPos;
    texCoord.y += DataIn[0].texTile.y;

    texCoord.x = texCoord.x / texSize.x;
    texCoord.y = texCoord.y / texSize.y;

    texCoord.y = 1.0 - texCoord.y;
    pos = projection * view * pos;

    gl_Position = pos;
    DataOut.pos = pos;
    DataOut.texCoord = texCoord;

    EmitVertex();


    offset = -vec4(DataIn[0].anchor,0.0, 0.0) + vec4(DataIn[0].size.x, 0.0, 0.0, 0.0);
    pos = (rotation * offset) + gl_in[0].gl_Position;

    texCoord = DataIn[0].texPos;
    texCoord.x += DataIn[0].texTile.x;

    texCoord.x /= texSize.x;
    texCoord.y /= texSize.y;


    texCoord.y = 1.0 - texCoord.y;
    pos = projection * view * pos;

    gl_Position = pos;
    DataOut.pos = pos;
    DataOut.texCoord = texCoord;

    EmitVertex();


    offset = -vec4(DataIn[0].anchor,0.0, 0.0) + vec4(DataIn[0].size, 0.0, 0.0);
    pos = (rotation * offset) + gl_in[0].gl_Position;

    texCoord = DataIn[0].texPos;
    texCoord.y += DataIn[0].texTile.y;
    texCoord.x += DataIn[0].texTile.x;

    texCoord.x /= texSize.x;
    texCoord.y /= texSize.y;

    texCoord.y = 1.0 - texCoord.y;
    pos = projection * view * pos;

    gl_Position = pos;
    DataOut.pos = pos;
    DataOut.texCoord = texCoord;

    EmitVertex();

    EndPrimitive();
}
