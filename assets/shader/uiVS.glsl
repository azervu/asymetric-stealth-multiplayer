#version 330

// Uniforms
uniform float zDepth;

// In
in vec2 vertex;
in vec2 vertexTexCoord;

// Out
out Data {
    vec2 texCoord;
} DataOut;

void main() {
    DataOut.texCoord = vertexTexCoord;

    gl_Position = vec4(vertex, zDepth, 1.0);
}
