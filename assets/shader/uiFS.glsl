#version 330

// Uniforms
uniform sampler2D baseTex;
uniform vec3 tint;

// In
in Data {
    vec2 texCoord;
} DataIn;

// Out
out vec4 outColor;

void main() {

    //outColor = vec4(DataIn.texCoord, 1.0, 1.0) * vec4(tint, 1.0);
    outColor = texture(baseTex, DataIn.texCoord) * vec4(tint, 1.0);
}
