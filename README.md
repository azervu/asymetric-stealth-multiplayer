# README #

See the [wiki](https://bitbucket.org/azervu/asymetric-stealth-multiplayer/wiki) 
for detailed information about *all* the things.
To make this you will need CMake 2.8 or higher.

Also, these libraries are used, note that precompiled windows dll's are included.

 * SDL 2.0.3
 * SDL_net 2.0.0
 * SDL_ttf 2.0.12
 * FreeImage
 * FreeImagePlus
 * glm 0.9.5.4
 * GLEW 1.11.0
 * irrKlang
 * Box2D
 * github.com/pimms/tmx-parser


## Important Temporary Solutions ##

Below is a list of temporary solutions that are very likely to be fixed in the
relatively near future. However, "undoing" the temporar solutions may not be
as trivial as they were to implement, so they are listed here.

- The client side local player is currently not adjusting it's position based 
  on server estimations, nor does the server estimate anything. The server does
  not notify clients about the local state. The server can be altered to notify 
  the clients in server::LobbyController::onPlayerTransformUpdateOut(..).