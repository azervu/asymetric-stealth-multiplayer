Place Windows libraries in this directory. The libraries must conform to
the following folder structure:
--- lib (this directory)
     |
	 +--- your_lib
     |     |
     |     +--- lib
     |     |     |
     |     |     +--- binary.lib
     |     |     +--- binary.dll (optional)
     |     +--- include
     |           |
     |           +--- /*directories, files, whatever*/
     +--- config_specific_lib
           |
           +--- include
           |     |
           |     +--- /*directories, files, whatever*/
           +--- lib
                 +--- Debug
                 |     |
                 |     +--- binary.lib
                 +--- Release
                       |
                       +--- binary.lib

The libraries can then be assigned to a target by modifying the CMakeLists.txt
by adding the following in the "Link Management" section:
    AddLibrary(game your_lib binary false)

If the library is dependent on any DLLs, remember to also add a call to CopyDll:
    CopyDll(your_lib binary)

Do not include more than one configuration of a library (e.g, debug & release). 
Do not give a custom name to the binary, such as SDL2_x86.lib. If the binary already
has a non-standard name, change it to something sane. By doing this, the "AddLibrary()"
function in CMakeLists.txt will properly links the library for all platforms. 

If libraries require specifically compiled versions for Debug and Release builds
under Windows, use the "Debug/" and "Release/" directory structure as seen
above. You may need to add the lbraries with "git add -f ...", as "Debug" and 
"Release" are added to the .gitignore file. Config specific builds does not 
currently support DLLs. They are added with a call:
    AddLibrary(game config_specific_lib binary true)
